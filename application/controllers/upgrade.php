<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting(0);

class upgrade extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        if( !isset( $this->session->userdata['id'] ) )
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! You Must login First</div>');
            redirect(base_url().'login', 'refresh');
        }
        
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }


    
	public function upgradeUser()
	{
        if($_POST){
    		$userId 	= 	$this->session->userdata['id'];
    		$userName 	= 	$this->session->userdata['name'];
    		$userEmail 	= 	$this->session->userdata['email'];
    		
            $planName 	= 	$_POST['plan'];
    		$this->session->set_userdata('Plan', $planName);

            $DOR        =   date('Y-m-d');
            $this->session->set_userdata('DOR', $DOR);
    		
            $query      =   $this->db->query("SELECT end_of_registration FROM user_registration WHERE user_id = $userId") -> row_array();
            $OLD_EOR = $query['end_of_registration'];

            if(isset($_POST['numberOfmonths'])){
    			$duration = $_POST['numberOfmonths'];
                if(strtotime($OLD_EOR) > strtotime($DOR)){
                    $EOR    =   date('Y-m-d', strtotime("+".$duration." months", strtotime($OLD_EOR)));
                }
                else{
                    $EOR    =   date("Y-m-d", strtotime("+".$duration." months"));
                }
    		}
    		else{
    			$duration = $_POST['numberOfYears'];
                 if(strtotime($OLD_EOR) > strtotime($DOR)){
                    $EOR    =   date('Y-m-d', strtotime("+".$duration." years", strtotime($OLD_EOR)));
                }
                else{
                    $EOR    =   date("Y-m-d", strtotime("+".$duration." years"));
                }
    		}

    		$this->session->set_userdata('Duration', $duration);
    		$userAmount = 	$_POST['amount'] * $duration; 
    		
    		$this->session->set_userdata('EOR', $EOR);

    		require_once APPPATH."libraries/ccapi/cclib.php"; 
            //require_once APPPATH."libraries/ccapi/Aes.php"; 
            
            $Merchant_Id        =   "M_kaz18545_18545" ;//This id(also User Id) available at "Generate Working Key" of "Settings & Options" 
            $Order_Id           =   rand(5, 15);//your script should substitute the order description in the quotes provided here 
            $Amount             =   $userAmount;//your script should substitute the amount in the quotes provided here 
            $Redirect_Url       =   base_url() . 'index.php/upgrade/returnPayment';         //your redirect URL where your customer will be redirected after authorisation from CCAvenue
            $WorkingKey         =   "b9gfae1mobs9u1ols5" ;//put in the 32 bit alphanumeric key in the quotes provided here.Please note that get this key ,login to your CCAvenue merchant account and visit the "Generate Working Key" section at the "Settings & Options" page. 
            

            $Checksum               = getCheckSum($Merchant_Id, $Amount, $Order_Id ,$Redirect_Url, $WorkingKey);   

            $query  = $this->db->query("SELECT * FROM profile WHERE user_id = $userId") -> row_array();
            //print_r($query);
            $billing_cust_name      =   $userName;
            $billing_cust_email     =   $userEmail;
            $billing_cust_address   =   $query['street'];
            $billing_cust_country   =   $query['country'];
            $billing_cust_state     =   $query['state'];
            $billing_cust_city      =   $query['city'];
            $billing_cust_zip       =   $query['pin'];
            $billing_cust_tel       =   $query['phone'];
           
            //die;
        ?>                    
            <form method="post" name="redirect" action="http://www.ccavenue.com/shopzone/cc_details.jsp"> 
                    
                <input type="hidden"    name="Merchant_Id"              value="<?php echo $Merchant_Id; ?>"> 
                <input type="hidden"    name="Amount"                   value="<?php echo $Amount; ?>"> 
                <input type="hidden"    name="Order_Id"                 value="<?php echo $Order_Id; ?>"> 
                <input type="hidden"    name="Redirect_Url"             value="<?php echo $Redirect_Url; ?>"> 
                <input type="hidden"    name="Checksum"                 value="<?php echo $Checksum; ?>">

                <input type="hidden"    name="billing_cust_name"       value="<?php echo $billing_cust_name; ?>">
                <input type="hidden"    name="billing_cust_email"       value="<?php echo $billing_cust_email; ?>">
                <input type="hidden"    name="billing_cust_address"       value="<?php echo $billing_cust_address; ?>">
                <input type="hidden"    name="billing_cust_country"       value="<?php echo $billing_cust_country; ?>">
                <input type="hidden"    name="billing_cust_state"       value="<?php echo $billing_cust_state; ?>">
                <input type="hidden"    name="billing_cust_city"       value="<?php echo $billing_cust_city; ?>">
                <input type="hidden"    name="billing_cust_zip"       value="<?php echo $billing_cust_zip; ?>">
                <input type="hidden"    name="billing_cust_tel"       value="<?php echo $billing_cust_tel; ?>"> 

               
            </form>
                <script language='javascript'>
                    document.redirect.submit();
                </script>
            
            <?php
        }
        else{
            redirect(base_url().'index.php/login/four_zero_four', 'refresh');
        }
	}
	

	public function returnPayment(){
		if($_POST){
            require_once APPPATH."libraries/ccapi/cclib.php"; 
            $WorkingKey           =     "b9gfae1mobs9u1ols5";
            $Merchant_Id          =     $_REQUEST['Merchant_Id'];
            $Amount               =     $_REQUEST['Amount'];
            $Order_Id             =     $_REQUEST['Order_Id'];
            $Merchant_Param       =     $_REQUEST['Merchant_Param'];
            $Checksum             =     $_REQUEST['Checksum'];
            $AuthDesc             =     $_REQUEST['AuthDesc'];
            $veriChecksum         =     verifyChecksum($Merchant_Id,$Order_Id,$Amount,$AuthDesc,$Checksum,$WorkingKey);
            // $veriChecksum         =     verifyChecksum($Merchant_Id,$Order_Id,$Amount,$AuthDesc,$veriChecksum,$WorkingKey);


            if($veriChecksum == TRUE && $AuthDesc === "Y")
            {
                $userId 	= 	$this->session->userdata['id'];
    			$DOR 		= 	$this->session->userdata['DOR'];
    			$EOR 		= 	$this->session->userdata['EOR'];
    			$Plan 		= 	$this->session->userdata['Plan'];
    			$Duration 	= 	$this->session->userdata['Duration'];
                $query 		=   $this->db->query("UPDATE user_registration SET date_of_registration = '$DOR', end_of_registration = '$EOR', payment_status = 1 WHERE user_id = $userId");
                $this->session->unset_userdata('DOR');
            	$this->session->unset_userdata('EOR');
            	$this->session->unset_userdata('Plan');
            	$this->session->unset_userdata('Duration');
                $this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transaction Successfull. You have been Upgraded to '.$plan.' of '.$Duration.'.</div>');
                redirect(base_url().'index.php/user/plans', 'refresh');
                //echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
                //Here you need to put in the routines for a successful 
                //transaction such as sending an email to customer,
                //setting database status, informing logistics etc etc
            }
            else if($veriChecksum==TRUE && $AuthDesc==="B")
            {
            	$userId 	= 	$this->session->userdata['id'];
    			$DOR 		= 	$this->session->userdata['DOR'];
    			$EOR 		= 	$this->session->userdata['EOR'];
    			$Plan 		= 	$this->session->userdata['Plan'];
    			$Duration 	= 	$this->session->userdata['Duration'];
                $query 		= $this->db->query("UPDATE user_registration SET date_of_registration = '$DOR', end_of_registration = '$EOR', payment_status = 1 WHERE user_id = $userId");
                $this->session->unset_userdata('DOR');
            	$this->session->unset_userdata('EOR');
            	$this->session->unset_userdata('Plan');
            	$this->session->unset_userdata('Duration');
                $this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transaction Successfull. You have been Upgraded to '.$plan.' of '.$Duration.'.</div>');
                redirect(base_url().'index.php/user/plans', 'refresh');
                //echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
                //Here you need to put in the routines/e-mail for a  "Batch Processing" order
                //This is only if payment for this transaction has been made by an American Express Card
                //since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
            }
            else if($veriChecksum==TRUE && $AuthDesc==="N")
            {
            	$this->session->unset_userdata('DOR');
            	$this->session->unset_userdata('EOR');
            	$this->session->unset_userdata('Plan');
            	$this->session->unset_userdata('Duration');
                $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Security Error! Transaction Declined. Payment can not be completed. </div>');
                redirect(base_url().'index.php/user/plans', 'refresh');
                //echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
                //Here you need to put in the routines for a failed
                //transaction such as sending an email to customer
                //setting database status etc etc
            }
            else
            {
            	$this->session->unset_userdata('DOR');
            	$this->session->unset_userdata('EOR');
            	$this->session->unset_userdata('Plan');
            	$this->session->unset_userdata('Duration');
                $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Security Error! Illegal access detected. Payment can not be completed. </div>');
                redirect(base_url().'index.php/user/plans', 'refresh');
            }


            // echo "<br><br>";


            // //************************************  DISPLAYING DATA RCVD ******************************************//

            // echo "<table cellspacing=4 cellpadding=4>";
            // for($i = 0; $i < $dataSize; $i++) 
            // {
            //     $information=explode('=',$decryptValues[$i]);
            //         echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
            // }

            // echo "</table><br>";
            // echo "</center>";
            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die();
        }
        else{
            redirect(base_url().'index.php/login/four_zero_four', 'refresh');
        }
	}

}

