<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	private $registrationStatus;
	private $pathType = 'user';

	
	function __construct()
	{
		parent::__construct();

		if( !isset( $this->session->userdata['id'] ) )
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! You Must login First</div>');
            redirect(base_url().'login', 'refresh');
        }
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function dashboard()
	{
		$appcode			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Dashboard";
		$data['fileName'] 	= 	"dashboard.php";
		$data['chartDataDaily'] 	= 	$this->user_model->getChartDataDaily( $appcode );
		$data['chartDataMonthly'] 	= 	$this->user_model->getChartDataMonthly( $appcode );
		$this->load->view('index', $data);
	}


	
	public function changePassword()
	{
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Change Password";
		$data['fileName'] 	= 	"changePassword.php";
		$this->load->view('index', $data);
	}


	
	public function changePasswordResult()
	{
		if($_POST)
		{
			if( $_POST['password1'] !== $_POST['password2'] )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error">
						<button class="close" data-dismiss="alert">×</button>
						Passwords You Entered Don\'t Match.</div>');
				redirect(base_url().'user/changePassword', 'refresh');
			}
			$password 	= md5($this->input->post('password1'));
			$id 		= $this->session->userdata['id'];
			$query 		= $this->db->query("UPDATE users SET password = '$password' WHERE id = '$id' AND status = 1");
			if( $query )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success">
						<button class="close" data-dismiss="alert">×</button>
						Password Changed Successfully!</div>');
				redirect(base_url().'user/changePassword', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error">
						<button class="close" data-dismiss="alert">×</button>
						Something Went Wrong. Please! Try Again After Sometime.</div>');
				redirect(base_url().'user/changePassword', 'refresh');
			}
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
