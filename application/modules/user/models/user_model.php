<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}


    function checkUserCanAccess( $role )
    {
        if( isset( $this->session->userdata['id'] ) )
        {
            if( $this->session->userdata[$role] !== '1' )
            {
                redirect(base_url().'login/four_zero_four', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! You Must login First</div>');
            redirect(base_url().'login', 'refresh');
        }
    }


    function isProfileUpdated()
    {
        $appcode    =   $this->session->userdata['appcode'];
        $query      =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
        if( $query->num_rows() )
        {
            return true;
        }
        else
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button>Please update your profile first!</div>');
            return false;
        }
    }
	   
    
    function getUserLogo()
    {
        $logo       =   '';
        $appcode    =   $this->session->userdata("appcode");
        $query      =   $this->db->query("SELECT id FROM users WHERE appcode ='$appcode' AND level = 1 AND status = 1 LIMIT 0,1") -> row_array();
        if( $query == 1 )
        {
            $logo       =   "uploads/logo/".$query['id'].'.png';
        }

        if ( file_exists($logo) ) 
        {
            return '<img src="'.base_url().$logo.'" alt="Conquer" style="height:41px; width:200px"/>';
        }
        
        else
        {
            return '<img src="'.base_url().'assets/img/logo.png" style="height:41px; width:200px"/>';
        }

    }


    function getUserProfile( $userId )
    {
        $profile = $this->db->query("SELECT * FROM profile WHERE user_id = $userId") -> row_array();
        return $profile;
    }


    function getUserLevel( $userId )
    {
        $user = $this->db->query("SELECT email, level FROM users WHERE id = $userId") -> row_array();
        if( $user['level'] == 1 )
            return '<span class="label label-primary">Admin</span>'; 
        elseif( $user['level'] == 2 )
            return '<span class="label label-primary">Manager</span>'; 
        elseif( $user['level'] == 3 )
        {
            $email = $user['email'];
            $user = $this->db->query("SELECT user_name FROM factory_store_users WHERE user_email = '$email' AND user_type = 'pos'") -> row_array();
            return $user['user_name']; 
        }
        else
            return '<span class="label label-primary">Manager</span>'; 
    }


    function getUserLevelName( $userId )
    {
        $user = $this->db->query("SELECT email, level FROM users WHERE id = $userId") -> row_array();
        if( $user['level'] == 1 )
            return 'Admin'; 
        elseif( $user['level'] == 2 )
            return 'Manager'; 
        elseif( $user['level'] == 3 )
        {
            $email = $user['email'];
            $user = $this->db->query("SELECT user_name FROM factory_store_users WHERE user_email = '$email' AND user_type = 'pos'") -> row_array();
            return $user['user_name']; 
        }
        else
            return 'Manager'; 
    }


    function getInvoiceUserName( $userId )
    {
        $user = $this->db->query("SELECT email, level FROM users WHERE id = $userId") -> row_array();
        if( $user['level'] == 2 )
        {
            $email = $user['email'];
            $user = $this->db->query("SELECT user_name FROM factory_store_users WHERE user_email = '$email' AND user_type = 'store_manager'") -> row_array();
            return 'Mr '.$user['user_name']; 
        } 
        else
        {
            $email = $user['email'];
            $user = $this->db->query("SELECT user_name FROM factory_store_users WHERE user_email = '$email' AND user_type = 'pos'") -> row_array();
            return 'Mr '.$user['user_name']; 
        }
        
    }


    function getTOS( $appcode )
    {
        $tos = $this->db->query("SELECT * FROM tos WHERE appcode = '$appcode' AND status = 1 ORDER BY id ASC") -> result_array();
        return $tos;
    }


    function getChartDataDaily( $appcode )
    {
        $chartData  =   array();
        $date       =   date('Y-m-d');
        for ($i=0; $i < 12; $i++) 
        { 
            $n = strtotime($date."- $i days");
            $m = date("Y-m-d",$n);
            $advancePayment    =   $this->db->query("SELECT SUM(advance_payment) as advancePayment FROM `services_invoices` WHERE appcode = '$appcode' AND DATE_FORMAT(invoice_date, '%Y-%m-%d') = '$m' AND status = 'Active'")->row_array();
            
            if(is_numeric($advancePayment['advancePayment']))
                $chartData[$i]['advancePayment'] = $advancePayment['advancePayment'];
            else
                 $chartData[$i]['advancePayment'] = 0;
            $chartData[$i]['invoice_date'] = $m;
            $totalAmount    =   $this->db->query("SELECT SUM(total_amount) as totalAmount FROM `services_invoices` WHERE appcode = '$appcode' AND DATE_FORMAT(invoice_date, '%Y-%m-%d') = '$m' AND status = 'Active'")->row_array();
            
            if(is_numeric($totalAmount['totalAmount']))
                $chartData[$i]['totalAmount'] = $totalAmount['totalAmount'];
            else
                 $chartData[$i]['totalAmount'] = 0;
        }
        return $chartData;
    }


    function getChartDataMonthly( $appcode )
    {
        $chartData  =   array();
        $date       =   date('Y-m');
        for ( $i=0; $i < 12; $i++ ) 
        { 
            $n = strtotime($date."- $i months");
            $m = date("Y-m",$n);
            $advancePayment    =   $this->db->query("SELECT SUM(advance_payment) as advancePayment FROM `services_invoices` WHERE appcode = '$appcode' AND DATE_FORMAT(invoice_date, '%Y-%m') = '$m' AND status = 'Active'")->row_array();
            if(is_numeric($advancePayment['advancePayment']))
                $chartData[$i]['advancePayment'] = $advancePayment['advancePayment'];
            else
                 $chartData[$i]['advancePayment'] = 0;

            $chartData[$i]['invoice_date'] = $m;

            $totalAmount    =   $this->db->query("SELECT SUM(total_amount) as totalAmount FROM `services_invoices` WHERE appcode = '$appcode' AND DATE_FORMAT(invoice_date, '%Y-%m') = '$m' AND status = 'Active'")->row_array();
            if(is_numeric($totalAmount['totalAmount']))
                $chartData[$i]['totalAmount'] = $totalAmount['totalAmount'];
            else
                 $chartData[$i]['totalAmount'] = 0;
        }
        return $chartData;
    }

}