<?php echo $this->session->flashdata('notification');?>

<div class="row-fluid">
	<div class="span12">
		<table id="source" style="display:none; margin-top: -10%!important;">
		   	<caption>Daily Sales Statistics ( Last 12 Days )</caption>
		  	<thead>
			<?php 
			   	echo '<tr><th></th>';
			   	foreach( $chartDataDaily as $data)
			    {
			       	echo '<th>'.date("jS F, Y", strtotime($data['invoice_date'])).'</th>';

			    }
			    echo '</tr>';
			?>
		   	</thead>
  			
  			<tbody>
			<?php 
			   	echo '<tr><th>Amount</th>';
		      	foreach( $chartDataDaily as $data)
		      	{
		       		echo '<td>'.$data['totalAmount'].'</td>';
		      	}
		      	echo '</tr><tr><th>Paid</th>';
		      	foreach( $chartDataDaily as $data)
		      	{
		       		echo '<td>'.$data['advancePayment'].'</td>';
		      	}
		       	echo '</tr><tr><th>Balance</th>';

		      	foreach( $chartDataDaily as $data)
		      	{
		       		echo '<td>'.($data['totalAmount'] - $data['advancePayment']).'</td>';
		      	}
		      	echo '</tr>';
		    ?>
		   
		  	</tbody>
 		</table>


		<table id="source2" style="display:none;">
   			<caption> Monthly Sales Statistics ( Last 12 Months )</caption>
  				<thead>
				<?php 
				   	echo '<tr><th></th>';
				   	foreach( $chartDataMonthly as $data)
				    {
				       	echo '<th>'.date(" F, Y", strtotime($data['invoice_date'])).'</th>';

				    }
				    echo '</tr>';
				?>
  				</thead>
  			<tbody>
			<?php 
			   	echo '<tr><th>Amount</th>';
			    foreach( $chartDataMonthly as $data)
			    {
			       	echo '<td>'.$data['totalAmount'].'</td>';
			    }
			    echo '</tr><tr><th>Paid</th>';
			    foreach( $chartDataMonthly as $data)
			    {
			       	echo '<td>'.$data['advancePayment'].'</td>';
			    }
			    echo '</tr><tr><th>Balance</th>';
			    foreach( $chartDataMonthly as $data)
			    {
			       	echo '<td>'.($data['totalAmount'] - $data['advancePayment']).'</td>';
			    }
			    echo '</tr>';
		    ?>
   
  			</tbody>
 		</table>

		<div id="target" class="chart">
		</div>

		<div id="target2" class="chart" >
		</div>
	</div>	
</div>
