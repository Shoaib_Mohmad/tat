<script src="<?php echo base_url().'assets/js/countries.js';?>" type="text/javascript"></script>
<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<div class="row-fluid">
		<div class="span12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
			<div class="widget-title">
				<h4>
				<i class="icon-reorder"></i>Edit Customer
				</h4>
				<span class="tools"> <a href="javascript:;"
				class="icon-chevron-down"></a> <a href="#widget-config"
				data-toggle="modal" class="icon-wrench"></a> <a
				href="javascript:;" class="icon-refresh"></a> <a
				href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body form">
			<!-- BEGIN FORM-->
			<form action="<?php echo base_url().'customers/editCustomerResult';?>" method="POST" class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="input1">Name</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Customer Name" class="span6" id="input1" name="name" value = "<?php echo $customer['name'];?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Email Address</label>
					<div class="controls">
						<!-- Email validation : onblur= "return checkEmail();" -->
						<input type="text" required placeholder="Enter Email address" onblur="return checkEmail();" class="span6" id='txtEmail'	name="email" value = "<?php echo $customer['email'];?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Mobile No 
					</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Mobile Number" pattern="\d{10}" title="(Should be numeric 10 digits)" maxlength="10" class="span6" id="input1" name="phone" value = "<?php echo $customer['phone'];?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Landline No</label>
					<div class="controls">
						<input type="text" placeholder="Enter Landline Number" class="span6" id="input1" name="landline" value = "<?php echo $customer['landline'];?>"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="input1">Country</label>
					<div class="controls">
						<select required class="span6" id="country" name="country">
									
						</select>
					</div>
				</div>
               
                <div class="control-group">
					<label class="control-label" for="input1">State</label>
					<div class="controls">
						<select required class="span6" id="state" name="state">
									
						</select>
					</div>
               </div>
               
               <script src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>
               
               <script language="javascript">
					populateCountries("country", "state");
					populateCountries("country2");
				</script>
				
				<?php if(isset($customer['country']) && isset($customer['state'])):
				?>
					<script>
						$(document).ready(function() {
							//alert('Workinf')
							$("#country [value='<?php echo $customer['country'];?>']").attr("selected","selected");
							$("#country").change();
							$("#state [value='<?php echo $customer['state'];?>']").attr("selected","selected");
						});
						countrywe();
					</script>  
				<?php
					endif;
				?>
				
				<div class="control-group">
					<label class="control-label" for="input1">City</label>
					<div class="controls">
						<input type="text" required placeholder="Enter City"
						class="span6" id="input1" name="city" value = "<?php echo $customer['city'];?>"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Pin Code</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Pin Code"
						pattern="\d+(\.\d{2})?" class="span6" id="input1"
						name="pin" value = "<?php echo $customer['pin'];?>"/>
					</div>
				</div>

				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
				<input type="hidden" name="id" value="<?php echo $customer['id'];?>">
			</form>
			<!-- END FORM-->
			</div>
			</div>
		<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>


</div>
<script type="text/javascript">
	var state = '<?php echo $customer['state'];?>' ;
	document.getElementById('state').value = state ;
</script>
