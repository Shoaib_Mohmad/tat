<?php echo $this->session->flashdata('notification');?>
<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Customers</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'customers/addCustomer';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Customer</button></a>
						</div>
					</span>								
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>City</th>
								<th>State</th>
								<th>Country</th>
								<th>Pin</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
                  			$count = 1;
                  			foreach ($customers as $customer) { //invoicelist comes from controller,
                  		?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><?php echo $customer['name']; ?></td>
								<td><a href="mailto:<?php echo $customer['email']; ?>"><?php echo $customer['email']; ?></a></td>
								<td><?php echo $customer['phone']; ?></td>
								<td><?php echo $customer['city']; ?></td>
								<td><?php echo $customer['state']; ?></td>
								<td><?php echo $customer['country']; ?></td>
								<td><?php echo $customer['pin']; ?></td>
								<td>
									<a href="<?php echo base_url().'customers/editCustomer/'. $customer['id']?>" title="Edit Customer"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> | <a href="<?php echo base_url();?>customers/deleteCustomer/<?php echo $customer['id']; ?>" title="Delete Customer" onclick="return confirm('Are you sure you want to delete the Customer?')"><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a> 
								</td>
							</tr>
						<?php } ?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>