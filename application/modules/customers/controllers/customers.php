<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'customers';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('customers');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('customers/customers_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function index()
	{
		$appcode 			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Customers";
		$data['fileName'] 	= 	"customers.php";
		$data['customers'] 	= 	$this->customers_model->getCustomers( $appcode );
		$this->load->view('index', $data);
	}


	public function getCustomerDetails()
	{
		$id 		= 	$_POST['id'];
		$appcode	=	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM customers WHERE id = $id AND appcode = '$appcode' AND status = 1") ->row_array();
		echo json_encode($query);

	}

	
	public function addCustomer()
	{
		$id 				= 	$this->session->userdata['id'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Customer";
		$data['fileName'] 	= 	"addCustomer.php";
		$this->load->view('index', $data);	
	}


	public function addCustomerResult()
	{
		if($_POST)
		{
			$userId 	= 	$this->session->userdata['id'];
			$appcode 	= 	$this->session->userdata['appcode'];
			$query		= 	$this->db->query("INSERT INTO customers( appcode, name, email, phone, landline, city, state, country, pin ) VALUES( '$appcode', '".$_POST['name']."', '".$_POST['email']."', '".$_POST['phone']."', '".$_POST['landline']."', '".$_POST['city']."', '".$_POST['state']."', '".$_POST['country']."', '".$_POST['pin']."' )");
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Customer Added Successfully!</div>');	
			redirect(base_url().'customers', 'refresh');
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

	}

	
	public function editCustomer( $id )
	{
		if( !is_numeric($id) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Customer";
		$data['fileName'] 	= 	"editCustomer.php";
		$userId 			= 	$this->session->userdata['id'];
		$appcode 			= 	$this->session->userdata['appcode'];
		$query 				= 	$this->db->query("SELECT * FROM customers WHERE id = $id AND appcode = '$appcode' AND status = 1");
		if(!$query -> num_rows())
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$data['customer'] = $query -> row_array();
		$this->load->view('index', $data);
	}


	
	public function editCustomerResult()
	{
		if($_POST)
		{
			$id 		= 	$_POST['id'];
			$appcode	= 	$this->session->userdata['appcode'];
			$query		= 	$this->db->query("UPDATE customers SET name = '".$_POST['name']."', email = '".$_POST['email']."', phone = '".$_POST['phone']."', landline = '".$_POST['landline']."', city = '".$_POST['city']."', state = '".$_POST['state']."', country = '".$_POST['country']."', pin = '".$_POST['pin']."' WHERE id = $id AND appcode = '$appcode' AND status = 1" );
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Customer Updated Successfully!</div>');	
			redirect(base_url().'customers', 'refresh');
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

	}


	
	public function deleteCustomer( $id )
	{
		if( !is_numeric($id) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$userId 	= 	$this->session->userdata['id'];
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("UPDATE customers SET status = 0 WHERE id = $id AND appcode = '$appcode' AND status = 1");
		if(!$query)
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Customer Deleted Successfully!</div>');
		redirect(base_url().'customers', 'refresh');
	}
	
}

/* End of file customers.php */
/* Location: ./application/controllers/customers.php */
