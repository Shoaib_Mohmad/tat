<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function getCustomers( $appcode )
    {
        $customers = $this->db->query("SELECT * FROM customers WHERE appcode = '$appcode' AND status = 1 ORDER BY name ASC") -> result_array();
        return $customers;
    }


}