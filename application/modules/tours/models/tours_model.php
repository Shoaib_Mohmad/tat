<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tours_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   

    function getTours($appcode)
    {
        $tours = $this->db->query("SELECT * FROM tours WHERE appcode = '$appcode' AND status = 'Active' ORDER BY id DESC")-> result_array();
        return $tours;
    }

    
    function getTourDetails( $id, $appcode )
    {
        $tourdetails   =   $this->db->query("SELECT * FROM tours WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
        
        return $tourdetails->row_array();
    }


    function getTourTariffs( $tour_id, $appcode )
    {
        $tourTariffs   =   $this->db->query("SELECT * FROM tour_tariff WHERE tour_id = $tour_id AND appcode = '$appcode' AND status = 'Active'  ")->result_array();
        return $tourTariffs;
    }


    function getTourPlan($id, $appcode)
    {
        $plan   = $this->db->query("SELECT * FROM tour_tariff WHERE id = $id AND appcode = '$appcode' AND status = 'Active'  ")->row_array();
     
        return $plan;
    }

    

}