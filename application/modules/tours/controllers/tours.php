<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tours extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'tours';
	
	function __construct()
	{
		parent::__construct();
		
		$this->user_model->checkUserCanAccess('tours');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	
		
		$this->load->helper(array('form', 'url'));
  		$this->load->helper("file");
  		$this->load->model('tours/tours_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function index()
	{
		$appcode			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"View Tours";
		$data['fileName'] 	= 	"viewTours.php";
		$data['tours'] 		= 	$this->tours_model->getTours( $appcode );
		
		$this->load->view('index', $data);
	}

	
	public function addTour($param1 = '', $param2='')
	{
        
		$appcode 			=	$this->session->userdata['appcode'];
		if( $param1 == 'add' && $_POST )
		{
			$validity_date_received  	=   trim($_POST['validity_date']);
			$validity_date_object 		= 	new DateTime($validity_date_received);
			$validity_date				= 	$validity_date_object->format('Y-m-d');
					
			$data['appcode'] 		= 	$appcode;
			$data['heading']		=   trim($_POST['heading']);
			$data['details']		=   $_POST['details'];
			$data['inclusions'] 	=   $_POST['inclusions'];
			$data['validity']		=   $validity_date;

			$result 	= 	$this->db->insert('tours', $data); 
			$tourId 	= 	$this->db->insert_id();

			if($result)
			{
				if(!$_FILES['file_scan']['tmp_name']=="")
				{
				$UID = $tourId;
        		move_uploaded_file($_FILES['file_scan']['tmp_name'],'./uploads/tourimg/'.$UID.'.png');
        		}
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Details Saved Successfully!</div>');
			
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			
			}
			redirect(base_url().'tours/addTour/edit/'.$tourId, 'refresh');
		}
		if( $param1 == 'update' && $_POST )
		{

			if($_FILES['file_scan']['tmp_name'] !== "" )
			{
				$UID = $_POST['id'];
		    	move_uploaded_file($_FILES['file_scan']['tmp_name'],'./uploads/tourimg/'.$UID.'.png');
        	}
        	else
        	{
        		$UID 	= $_POST['id'];
        		$path 	= APPPATH.'../uploads/tourimg/'.$UID.'.png';
        		//die($path);
        		if (file_exists($path))
        		{
        			//die("Exists");
					unlink($path);
				} 
        	}
        	

			$validity_date_received =   trim($_POST['validity_date']);
		    $validity_date_object 	= 	new DateTime($validity_date_received);
			$validity_date			= 	$validity_date_object->format('Y-m-d');
			$id 					=   $_POST['id'];
			$data['heading']		=   trim($_POST['heading']);
			$data['details']		=   $_POST['details'];
			$data['inclusions'] 	=   $_POST['inclusions'];
			$data['validity']		=   $validity_date;

			$this->db->where( array('id' => $id, 'appcode' => $appcode ) );
			$result = $this->db->update('tours', $data); 

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'tours/addTour/edit/'.$id, 'refresh');
	    }
		
		if( $param1 == 'edit' )
		{
			$data['tour'] 			= 	$this->tours_model->getTourDetails( $param2, $appcode );
		    $data['tariffs'] 		= 	$this->tours_model->getTourTariffs( $param2, $appcode );
		}
		
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Tour";
		$data['fileName'] 	= 	"addTour.php";
		
		$this->load->view( 'index', $data );
	}



	public function addTourTariff( $param1 = '', $param2 = '' )
	{
		if( $_POST )
		{
			$appcode 	=	$this->session->userdata['appcode'];

			if( $param1 == 'update' )
			{
				$data['each_paying'] 	   		 = 	trim( $_POST['each_paying'] );
				$data['budget']  				 = 	trim( $_POST['budget'] );	
				$data['economic']  				 = 	trim( $_POST['economic'] );	
				$data['standard']  				 = 	trim( $_POST['standard'] );
				$data['super_deluxe']  			 = 	trim( $_POST['super_deluxe'] );
				

				$this->db->where( array('id' => $_POST['id'], 'appcode' => $appcode ) );
				$result 	= 	$this->db->update('tour_tariff', $data); 
			}
			else
			{
				$data['appcode'] 			= 	$appcode;
				$data['tour_id']  			= 	trim( $_POST['tour_id'] );
				$data['each_paying'] 	    = 	trim( $_POST['each_paying'] );
				$data['budget']  			= 	trim( $_POST['budget'] );	
				$data['economic']  			= 	trim( $_POST['economic'] );	
				$data['standard']  			= 	trim( $_POST['standard'] );
				$data['super_deluxe']  			= 	trim( $_POST['super_deluxe'] );

				$result 	= 	$this->db->insert('tour_tariff', $data); 
			}

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Tariff Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'tours/addTour/edit/'.$_POST['tour_id'], 'refresh');
		}
		else
		{
			redirect(base_url().'tours', 'refresh');
		}
	}

	

	public function deleteTourTariff( $param1 = '', $param2 = '' )
	{
		if( is_numeric( $param1 ) && is_numeric( $param2 ) )
		{
			$appcode 			=	$this->session->userdata['appcode'];
			$data['status']  	= 	'Deleted';
			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('tour_tariff', $data); 

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Tariff Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'tours/addTour/edit/'.$param1, 'refresh');
		}
		else
		{
			redirect(base_url().'tours', 'refresh');	
		}
	}


	public function deleteTour( $param1 = '', $param2 = '' )
	{
		$appcode 			=	$this->session->userdata['appcode'];

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$data['status']  	= 	'Deleted';

			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('tours', $data);

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'tours', 'refresh'); 
		}
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"View Tours";
		$data['fileName'] 	= 	"viewTours.php";
		$data['tours'] 		= 	$this->services_model->getTours( $appcode );
	
		$this->load->view('index', $data);
	}
	

	public function viewTour( $tourId )
    {

        if( !is_numeric( $tourId ) )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Tour Details');
        $pdf->SetSubject('OCTA Billing Tour Details');
        $pdf->SetKeywords('OCTA Billing, Tour Details');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Tour Details');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

        $tour     =   $this->tours_model->getTourDetails( $tourId, $appcode );

        $pdf->AddPage('P');

		$img = "uploads/tourimg/".$tourId.'.png';
		
		$pdf->Image(base_url().$img, 15, 20, 180, 70,'' ,'' ,'N' ,true ,100,'',false);

		$pdf->SetFont('helveticab', 'BU', 15);
		$pdf->SetTextColor(34,68,255);
		$pdf->ln(3);
		$pdf->Cell(180, 0, $tour['heading'], 0, 1,'C' );
		$pdf->ln(3);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('times', '', 12);
		$pdf->SetTextColor(255,0,0);
		$pdf->Cell(30, 6, 'Tour Validity', 0, 0 );
		$pdf->Cell(100, 6, date("jS F, Y", strtotime($tour['validity'])), 0, 1 );
		$pdf->SetTextColor(0,0,0);
		$pdf->Ln(2);
		$pdf->writeHTML($tour['details'], true, false, false, false, '');
		
		$pdf->AddPage('P');

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(10, 10, 'TOUR TARIFF', 0, 1 );
		
		$pdf->SetTextColor(0,0,0);
				//$pdf->Ln(3);
		$tbl_header = '<table cellpadding="10" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#333; font-size:13px; font-weight: bold">
						<th> Each Paying</th>
						<th style="text-align: center;"> Budget 	</th>
						<th style="text-align: center;"> Economic  </th>
						<th style="text-align: center;"> Standard  </th>
						<th style="text-align: center;"> Super Deluxe </th>
					
					</tr>';
		
		$tariffs 	= 	$this->tours_model->getTourTariffs( $tourId, $appcode );

		foreach ($tariffs as $tariff ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:13px; font-style:italic;">
							<td>'.$tariff['each_paying'].'</td>
							<td style="text-align: center;">'.$tariff['budget'].	'</td>
							<td style="text-align: center;">'.$tariff['economic'].'</td>
							<td style="text-align: center;">'.$tariff['standard'].'</td>
							<td style="text-align: center;">'.$tariff['super_deluxe'].'</td>
							
						</tr>
					';
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');



		$pdf->SetFont('times', '', 14);
		$pdf->Cell(100, 10, 'INCLUSIONS', 0, 1 );
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('times', 'i', 12);
		$inclusion_array = explode("\n", trim($tour['inclusions']));

        $inclusion_str='<ul>';
		foreach ($inclusion_array as $key ) {

			$inclusion_str= $inclusion_str.'<li>'.$key.'</li>';
		}
		$inclusion_str= $inclusion_str.'</ul>';

		$pdf->writeHTML($inclusion_str, true, false, false, false, '');
		
		$pdf->SetTextColor(0,0,0);
        ob_end_clean();
        $pdf->Output('Tour.pdf', 'I');

    }

    
	public function emailTour()
	{
		$appcode			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Email Tour Package";
		$data['fileName'] 	= 	"emailTour.php";
		$data['tours'] 		= 	$this->services_model->getTours( $appcode );
		$data['customers'] 	= 	$this->services_model->getCustomers( $appcode );
		
		$this->load->view('index', $data);
	}
    	
	
}

/* End of file tours.php */
/* Location: ./application/controllers/tours.php */