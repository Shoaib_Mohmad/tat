<!--<?php echo $this->session->flashdata('notification'); ?>-->

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Tours</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'tours/addTour' ?>" style="margin: -5px 6px 1px 0px; padding-right:30px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Tours </a>
						</div>
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Tour Name</th>
								<th>Created On</th>
								<th>Valid Till</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ( $tours as $tour ) 
							{ 
								
							?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $tour['heading']; ?></td>
									<td><?php print(date("jS F, Y", strtotime($tour['created_at'])));?></td>
									<td><?php print(date("jS F, Y", strtotime($tour['validity'])));?></td>
									
									
									<td> <a href="javascript:void(0);" title="View Tour" onClick = 'window.open("<?php echo base_url().'tours/viewTour/'.$tour['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> |
								<a href="<?php echo base_url().'tours/addTour/edit/'.$tour['id']; ?>" title="Edit Tour"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
								<a href="<?php echo base_url()?>tours/deleteTour/delete/<?php echo $tour['id']; ?>" title="Delete Tour" onclick="return confirm('Are you sure you want to delete the Tour?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
							</td>
								</tr>
							<?php  }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
