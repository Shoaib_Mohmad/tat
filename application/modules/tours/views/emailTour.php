<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Email Tour package</h4>
											
				</div>
					<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form onsubmit="return checkDate();" action="<?php echo base_url();?>email/emailTourPackage" method="POST" class="form-horizontal">
							
						<div class="control-group">
							<label class="control-label" for="input1">Tour Name</label>
							<div class="controls">
								<select required class="chosen span6" id="tour_id" name="tour_id" >
									<option value="na">Select Tour</option>
									<?php 
										foreach($tours as $tour)
										{
									?>
										<option value="<?php echo $tour['id'];?>"><?php echo $tour['heading'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Select Customers</label>
							<div class="controls">
								<select  class="chosen span6" multiple id="customer" name="customers[]" required>
									<option value="">Select Customers</option>
									<?php 
										foreach($customers as $customer)
										{
									?>
										<option value="<?php echo $customer['email'];?>" ><?php echo $customer['name'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						

						
					 
						
												
						
						
						

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Send</button>
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>

			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
