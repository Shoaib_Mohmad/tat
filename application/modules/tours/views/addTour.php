<?php echo $this->session->flashdata('notification');?>

<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i><?php if( isset( $tour['id'] ) ) echo 'Edit'; else echo 'Add'?>  Tour  					</h4>
									
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form  action="<?php if( isset( $tour['id'] ) ) echo base_url().'tours/addTour/update'; else echo base_url().'tours/addTour/add'?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
					 <?php if( isset( $tour['id'] ) ): ?>
                        <input type="hidden" name="id" value="<?php if( isset( $tour['id'] ) ) echo $tour['id']; ?>" />
                    <?php endif; ?>
						<div class="control-group">
							<label class="control-label" for="input1">Tour Name</label>
							<div class="controls">
								<input required class=" span6" required size="16"
									type="text" placeholder="Tour Name" name="heading" id="tour_name" value="<?php if( isset( $tour['heading'] ) ) echo $tour['heading']; ?>"  />
							</div>
						</div>
						<div class="control-group">

                         <label class="control-label" >Select Image</label>
                         <div class="controls">
                                
                            <?php 
                                if(isset($tour['id']))
                                {
                                 
                                  if(file_exists(APPPATH.'../uploads/tourimg/'.$tour['id'].'.png'))
                                     
                                  echo '<button type="button" class="btn btn-danger" id="remove" onclick="removeImage();">Remove Image</button>';
                                   else
                              
                                  echo '<input type="file" id="file" class="input-small" name="file_scan" onchange="upload();" />';
                               
                                }
                                else
                                {
                                    echo '<input type="file" id="file" class="input-small" name="file_scan"  onchange="upload();" /> ';
                                }
                            ?>

                            <input type="file" id="file" class="input-small" name="" style="visibility:hidden;" onchange="upload();" /> 

                         </div>
                      </div>
						
							
						<div class="control-group">
	                        <label class="control-label" for="input1">Detail</label>
	                        <div class="controls">

	                           <textarea required name="details" rows="10" placeholder="Enter  Tour Details" class="textarea span10"  > <?php if( isset( $tour['details'] ) ) echo $tour['details']; ?></textarea>
	                        </div>
	                     </div>

						<div class="control-group">
							<label class="control-label" for="input1">Valid Till</label>
							<div class="controls">
								<input required class="input-small date-picker span2" required size="16"
									type="text" placeholder="MM/DD/YYYY" name="validity" id="validity" value="<?php if( isset( $tour['validity'] ) ) echo $tour['validity']; ?>" />
							</div>
						</div>
					
						<div class="control-group">
	                        <label class="control-label" for="input1">Inclusions</label>
	                        <div class="controls">
	                            <textarea name="inclusions" rows="3" placeholder="Enter Inclusions Information (If Any)" class="span6" > <?php if( isset( $tour['inclusions'] ) ) echo $tour['inclusions']; ?> </textarea>
	                        </div>
	                     </div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>


 <!--- Tour Tariff Start Here -->

 <?php if( isset( $tour['id'] ) ): ?>

   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Tour Tariff</h4>
            </div>
            <div class="widget-body form">
                <!-- BEGIN TABLE-->
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th width="20%">Each Paying</th>
                        <th width="20%">Budget</th>
                        <th width="15%">Economics</th>
                        <th width="15%">Standard</th>
                        <th width="15%">Super Deluxe</th>
                        <th width="10%"><center>Actions</center></th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php foreach( $tariffs as $tariff ): ?>
                        <tr class="odd gradeX">
                           <form action="<?php echo base_url().'tours/addTourTariff/update'; ?>" method="POST">
                              <input type="hidden" name="id" value="<?php echo $tariff['id']; ?>">
                              <input type="hidden" name="tour_id" value="<?php echo $tour['id']; ?>">
                              <td><input type="Text" class="span10" placeholder="Each Paying" value="<?php echo $tariff['each_paying']; ?>" name="each_paying" required></td>
                              <td><input type="number" class="span10" placeholder="Budget" value="<?php echo $tariff['budget']; ?>" name="budget" required></td>
                              <td><input type="number" class="span10" placeholder="Economic" value="<?php echo $tariff['economic']; ?>" name="economic" required></td>
                              <td><input type="number" class="span10" placeholder="Standard" value="<?php echo $tariff['standard']; ?>" name="standard" required></td>
                              <td><input type="number" class="span10" placeholder="Super Deluxe" value="<?php echo $tariff['super_deluxe']; ?>" name="super_deluxe" required></td>
                             
                             <td>
                                 <center>
                                    <button class="btn btn-alert btn-sm" title="Update Tariff" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button>
                                    <a href="<?php echo base_url().'tours/deleteTourTariff/'.$tour['id'].'/'.$tariff['id']; ?>" title="Delete Tariff" class="btn btn-alert btn-sm"><i class="fa fa-trash-o" style="font-size: 18px; color: red;"></i></a>
                                 </center>
                              </td>
                           </form>
                        </tr>
                     <?php endforeach; ?>
                     <tr class="odd gradeX">
                        <form action="<?php echo base_url().'tours/addTourTariff'; ?>" method="POST">
                           
                              <input type="hidden" name="tour_id" value="<?php echo $tour['id']; ?>">
                             
                           
                              <td><input type="Text" class="span10" placeholder="Each Paying" value="" name="each_paying" required></td>
                              <td><input type="number" class="span10" placeholder="Budget" value="" name="budget" required></td>
                              <td><input type="number" class="span10" placeholder="Economic" value="" name="economic" required></td>
                              <td><input type="number" class="span10" placeholder="Standard" value="" name="standard" required></td>
                              <td><input type="number" class="span10" placeholder="Super Deluxe" value="" name="super_deluxe" required></td>
                             
                           <td><center><button class="btn btn-alert btn-sm" title="Add Tariff" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button></center></td>
                        </form>
                     </tr>
                  </tbody>
               </table>
                         <!-- END TABLE-->       
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>

   
      

 <?php endif; ?>

</div>
<script type="text/javascript">
   function upload()
   {
      var ext = $('#file').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid File Type! Upload Image only');
    $('#file').val('');
      }
   }
   function removeImage()
   {
      $('#file').val('');
      $("#file").css('visibility', 'visible');
      $('#file').attr('name', 'file_scan');

      $('#remove').hide();
   }
</script>

