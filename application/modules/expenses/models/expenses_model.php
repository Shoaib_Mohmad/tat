<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expenses_model extends CI_Model {
	
	
    function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}

	
    function BillNumberAvailable( $bill_number )
    { 
		$query = $this->db->get_where( 'bills', array( 'bill_number' => $bill_number ) );
        if ($query->num_rows() > 0 ) 
        {
            return false;        
        } 
        else 
        {
            return true;
        }
    }
    
   
    function getExpenseBills( $appcode )
    {
        $sql       =   $this->db->order_by('id', 'desc')->get_where( 'bills', array( 'appcode' => $appcode, 'status' => 'Active' ) );
        $result    =   $sql->result_array();
        return $result ;
    }


    function GetStoreManagerBillList( $appcode, $store_id )
    {
        $sql       =   $this->db->order_by('id', 'desc')->get_where( 'bills', array( 'appcode' => $appcode , 'store_id' => $store_id ) );
        $result    =   $sql->result_array();
        return $result ;
    }


    function GetFactoryManagerBillList( $appcode, $factory_id )
    {
        $sql       =   $this->db->order_by('id', 'desc')->get_where( 'bills', array( 'appcode' => $appcode , 'factory_id' => $factory_id ) );
        $result    =   $sql->result_array();
        return $result ;
    }


    function GetBillById( $id )
    {
    	$sql       =   $this->db->get_where( 'bills', array( 'id' => $id ) );
    	$result    =   $sql->row_array();
    	return $result ;
    }
    
    
    function getbillListByDate( $fromDate, $toDate, $user_id )
    {
    	$sql       =   "SELECT * FROM bills WHERE payment_date between '$fromDate' AND '$toDate' AND user_id='$user_id'";
    	$result    =   $this->db->query($sql)->result_array();
    	return $result ;
    }


    
}