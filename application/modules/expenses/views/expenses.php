<?php echo $this->session->flashdata('notification');?>

<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>Expense Bills</h4>
				<span class="tools">
					<div class="btn-group">
						<a href="<?php echo base_url().'expenses/addBill';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Expense</button></a>
					</div>
				</span>	
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th style="width:8px">#</th>
							<th>Bill No</th>
							<th>Bill Name</th>
							<th>Paid To</th>
							<th>Total Amount</th>
							<th>Amount Paid</th>
							<th>Balance</th>
							<th>Payment Date</th>
							<th style="width:8%">View</th>
							<th style="width:8%">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						$count = 1;//
						foreach ($billslist as $row) { //invoicelist comes from controller,
							$balance = $row['total_amount'] - $row['amount_paid'];
							if($balance == 0){
								$balance = '<strong>Nill</strong>';
							}
							else{
								$balance = '<i class="fa fa-fw fa-rupee"></i> '.$balance;
							}
					?>
						<tr class="odd gradeX">
							<td><?php echo $count++; ?></td>
							<td><center><span class="label label-primary"><?php echo $row['bill_number']; ?></span></center></td>
							<td><?php echo $row['bill_type']; ?></td>
							<td><?php echo $row['paid_to']; ?></td>
							<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['total_amount']; ?></td>
							<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['amount_paid']; ?></td>
							<td><?php echo $balance; ?></td>
							<td><?php print(date("jS F, Y", strtotime($row['payment_date'])));?></td>
							<td><center>
							<?php
								if( !empty($row['attachments'])):
							?>
							<a href="javascript:void(0);" onClick = 'window.open("<?php echo base_url().'uploads/bills/'.$row['attachments']; ?>", "myWindow", "width=800, height=750")'>
								<img src="<?php echo base_url().'assets/img/icons/view.png'; ?>" height="25px" width="25px" title="View Bill">
							</a> 
							<?php
								endif;
							?>
							<?php if( !empty($row['receipts'])): ?>|
							<a href="javascript:void(0);" onClick = 'window.open("<?php echo base_url().'uploads/receipts/'.$row['receipts']; ?>", "myWindow", "width=800, height=750")'>
								<img src="<?php echo base_url().'assets/img/icons/receipt.png'; ?>" height="25px" width="25px" title="View Receipts">
							</a>
							<?php endif; ?>
							</center></td>
							<td>
								<a href="<?php echo base_url().'expenses/editbill/'.$row['id'];?>" title="Edit Bill" ><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> | <a href="<?php echo base_url()?>expenses/bills/delete/<?php echo $row['id']; ?>" title="Delete Bill" onclick="return confirm('Are you sure you want to delete the Bill?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
							</td>
						</tr>
						 <?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
		<!-- END BORDERED TABLE PORTLET-->
