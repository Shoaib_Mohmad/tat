<?php echo $this->session->flashdata('notification');?>

<div id="page">
       <div class="row-fluid">
          <div class="span12">
             <!-- BEGIN SAMPLE FORM PORTLET-->	
             <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i>Add Expense Bill</h4>
                   <span class="tools">
                   <a href="javascript:;" class="icon-chevron-down"></a>
                   <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                   <a href="javascript:;" class="icon-refresh"></a>		
                   <a href="javascript:;" class="icon-remove"></a>
                   </span>							
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                   <form onsubmit="return checkExt();" action="<?php echo base_url();?>expenses/do_upload" method="POST" enctype="multipart/form-data" class="form-horizontal">

                      <div class="control-group">
                         <label class="control-label" for="input1">Bill Name</label>
                         <div class="controls">
                            <input type="text" required placeholder="Enter Bill Name" class="span6" id="input" name="bill_type" />
                         </div>
                      </div>
                      <div class="control-group">
                         <label class="control-label" for="input1">Bill Number</label>
                         <div class="controls">
                            <input type="text" required placeholder="Enter Bill Number" class="span6" id="input1" name="bill_number" />
                         </div>
                      </div>
						
					           <div class="control-group">
                         <label class="control-label" for="input1">Total Amount (<i class="fa fa-fw fa-rupee"></i>)</label>
                         <div class="controls">
                            <input type="text" pattern="\d+(\.\d{2})?" required placeholder="Enter Total Amount" class="span3" id="input1" name="total_amount"/>
                         </div>
                      </div>
                      
                      <div class="control-group">
                         <label class="control-label" for="input1">Amount Paid (<i class="fa fa-fw fa-rupee"></i>)</label>
                         <div class="controls">
                            <input type="text" pattern="\d+(\.\d{2})?" required placeholder="Enter Paid Amount" class="span3" id="input1" name="amount_paid"/>
                         </div>
                      </div>

                      <div class="control-group">
                         <label class="control-label" for="input1">Paid To</label>
                         <div class="controls">
                            <input type="text" required placeholder="Paid To" class="span6" id="input1" name="paid_to"/>
                         </div>
                      </div>
                      
                      <div class="control-group">
                                 <label class="control-label" >Payment Mode</label>
                                 <div class="controls">
                                    <select class="span3" data-placeholder="Choose a Category" tabindex="1" name="payment_mode">
                                       <option value="Cash">Cash</option>
                                       <option value="Check">Check</option>
                                       <option value="Draft">Draft</option>
                                       <option value="RTJS">RTJS</option>
                                    </select>
                                 </div>
                              </div>
					
					<div class="control-group">
                         <label class="control-label" for="input1">Reference No.</label>
                         <div class="controls">
                            <input type="text" required placeholder="Refrence Number" class="span6" id="input1" name="reference_no"/>
                         </div>
                     </div>
                      
                      <div class="control-group">
                         <label class="control-label" >Payment Date</label>
                         <div class="controls">
                            <input class="input-small date-picker" required size="16" type="text" placeholder="MM/DD/YY" name="payment_date"/>
                         </div>
                      </div>

                      <div class="control-group">
                         <label class="control-label" >Attach Bill</label>
                         <div class="controls">
                            <input type="file" id="file" class="input-small" name="file_scan"/>
                         </div>
                      </div>
                      
                       <div class="control-group">
                         <label class="control-label" >Attach Receipts</label>
                         <div class="controls">
                            <input type="file" id="file" class="input-small" name="file_scan_receipts"/>
                         </div>
                      </div>
                      
                     <div class="control-group">
                      <div class="form-actions">
                         <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                      </div>
                   </form>
                             <!-- END FORM-->			
                </div>
             </div>
             <!-- END SAMPLE FORM PORTLET-->
          </div>
       </div>
      
      
    </div>