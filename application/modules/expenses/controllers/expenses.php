<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting(0);

class Expenses extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'expenses';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('expenses');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	
		
		$this->load->model('expenses/expenses_model');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->helper("file");

		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function addBill()
	{ 									
		if( !$this->registrationStatus )
		{
			redirect(base_url().'expenses/expenses', 'refresh');
		}
		$data['pathType'] = $this->pathType;
		$data['pageName'] = "Add Expense Bill";
		$data['fileName'] = "addExpense.php";
		$this->load->view('index', $data);
	}


	public function bills( $param1 ='', $param2 = '' )
	{
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Expense Bills";
		$data['fileName'] 	= 	"expenses.php";
		$user_id		  	=	$this->session->userdata('id');
		$appcode		  	=	$this->session->userdata('appcode');
		$email			  	=	$this->session->userdata('email');
		
		$data['billslist']	=   $this->expenses_model->getExpenseBills( $appcode );
		
		$this->load->view('index', $data);
			
		if( $param1 == 'delete')
		{
			if( !is_numeric( $param2 ) )
			{
				redirect(base_url().'login/four_zero_four', 'refresh');
			}
			$query 	= 	$this->db->query("SELECT * FROM bills WHERE id = $param2 AND appcode = '$appcode'");
			if( !$query -> num_rows() )
			{
				redirect(base_url().'login/four_zero_four', 'refresh');
			}
			if(!$this->registrationStatus)
			{
				redirect(base_url().'expenses/bills', 'refresh');
			}
			if( $param2 !== '' )
			{
				$billData['status']    =  'Deleted';

				$this->db->where( array( 'id' => $param2, 'appcode' => $appcode ) );
   				$result 	= 	$this->db->update( 'bills', $billData );

				$this->session->set_flashdata('notification', '<div class="alert alert-success">
						<button class="close" data-dismiss="alert">×</button>Expense Bill Deleted Successfully!</div>');
				redirect(base_url() . 'expenses/bills', 'refresh');
			}
			else
			{
				redirect(base_url() , 'refresh');
			}
		}
	}

	
	public function editbill( $billId = "" )
	{
		if( !is_numeric( $billId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$id 		= 	$this->session->userdata['id'];
		$appcode	=	$this->session->userdata('appcode');
		$query 		= 	$this->db->query("SELECT * FROM bills WHERE id = $billId AND appcode = '$appcode'");
		if( !$query -> num_rows() )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		if(!$this->registrationStatus)
		{
			redirect(base_url().'user/expenses', 'refresh');
		}
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Expense Bill";
		$data['fileName'] 	= 	"editExpense.php";
		$data['getbilldata']=	$this->expenses_model->GetBillById( $billId );
		$this->load->view('index', $data);
	}


	function do_upload()
	{
		if( $_POST )
		{
			$user_id				=	$this->session->userdata('id');
			$appCode              	=   $this->session->userdata('appcode');
			$email              	=   $this->session->userdata('email');
			$bill_type              =   mysql_real_escape_string( $_POST['bill_type'] );
			$bill_number            =   mysql_real_escape_string( $_POST['bill_number'] );
			$attachments    		=   $_FILES['file_scan'];
			$receipts    			=   $_FILES['file_scan_receipts'];
			$totalamount            =   mysql_real_escape_string( $_POST['total_amount'] );
			$amountpaid             =   mysql_real_escape_string( $_POST['amount_paid'] );
			$paid_to		        =   mysql_real_escape_string( $_POST['paid_to'] );
			$payment_date_received  =  	$_POST['payment_date'];
			$paymentmode		    =   mysql_real_escape_string( $_POST['payment_mode'] );
			$reference_no		    =  	mysql_real_escape_string( $_POST['reference_no'] );
			$payment_date_object 	= 	new DateTime($payment_date_received);
			$payment_date			= 	$payment_date_object->format('Y-m-d');
				
			$billdata['user_id']   		 =   $user_id;
			$billdata['appcode']   		 =   $appCode;
			$billdata['bill_type']   	 =   $bill_type;
			$billdata['bill_number']  	 =   $bill_number;
			$billdata['total_amount']    =   $totalamount;
			$billdata['amount_paid']     =   $amountpaid;
			$billdata['paid_to']   	  	 =   $paid_to;
			$billdata['payment_date'] 	 =   $payment_date;
			$billdata['payment_mode']	 =   $paymentmode;
			$billdata['reference_no']	 =   $reference_no;
						
			$query 	= 	$this->db->query("SELECT * FROM bills WHERE appcode = '$appcode' AND bill_number =  '$bill_number'");
			if( $query -> num_rows() > 0 )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Bill Number Already Exists!</div>'));
				redirect(base_url() . 'expenses/addBill', 'refresh');
			}
			$result  	=   $this->db->insert('bills', $billdata);
			$insertId 	= 	$this->db->insert_id();

			if(!empty($_FILES['file_scan']['name']))
			{
				$config['upload_path'] = './uploads/bills';
				$config['allowed_types'] = 'gif|jpg|png|pdf';
				// $config['max_size']	= '4048';
				// $config['max_width']  = '1024';
				// $config['max_height']  = '768';
				$ext = end(explode(".", $_FILES['file_scan']['name']));
				$config['file_name'] = 'Bill_'.$insertId.'.'.$ext;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				if ( ! $this->upload->do_upload('file_scan'))
				{
					//print_r(array('error' => $this->upload->display_errors()));
					$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Error in Uploading!</div>'));
					redirect(base_url() . 'expenses/addBill', 'refresh');
				}
				else
				{
					$billdata['attachments'] 	 =   $config['file_name'];
					$this->db->where('id', $insertId);
					$this->db->update('bills', $billdata);
				}
			}
			
			//upload receipts
			if(!empty($_FILES['file_scan_receipts']['name']))
			{
				$config['upload_path'] = './uploads/receipts';
				$config['allowed_types'] = 'gif|jpg|png|pdf';
				// $config['max_size']	= '4048';
				// $config['max_width']  = '1024';
				// $config['max_height']  = '768';
				$ext = end(explode(".", $_FILES['file_scan_receipts']['name']));
				$config['file_name'] = 'Receipt_'.$insertId.'.'.$ext;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
			
				if ( ! $this->upload->do_upload('file_scan_receipts'))
				{
					//print_r(array('error' => $this->upload->display_errors()));
					$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Error in Uploading!</div>'));
					redirect(base_url() . 'user/addBill', 'refresh');
				}
				else
				{
					$billdata['receipts'] 	 =   $config['file_name'];
					$this->db->where('id', $insertId);
					$this->db->update('bills', $billdata);
				}
			}
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Expense Bill Added Successfully!</div>'));
			redirect(base_url() . 'expenses/bills', 'refresh');
		}
		else
		{
         	redirect(base_url().'login/four_zero_four', 'refresh');
       	}
	}


	
	function edit_bill()
	{
		if( $_POST )
		{
			$user_id				=	$this->session->userdata('id');
			$appCode              	=   $this->session->userdata('appcode');
			$billId					=	mysql_real_escape_string( $_POST['bill_id'] );
			$bill_type              =   mysql_real_escape_string( $_POST['bill_type'] );
			$bill_number            =   mysql_real_escape_string( $_POST['bill_number'] );
			$attachments    		=   $_FILES['file_scan'];
			$receipts    			=   $_FILES['file_scan_receipts'];
			$totalamount            =   mysql_real_escape_string( $_POST['total_amount'] );
			$amountpaid             =   mysql_real_escape_string( $_POST['amount_paid'] );
			$paid_to		        =   mysql_real_escape_string( $_POST['paid_to'] );
			$payment_date_received  =   $_POST['payment_date'];
			$paymentmode		    =   mysql_real_escape_string( $_POST['payment_mode'] );
			$reference_no		    =   mysql_real_escape_string( $_POST['reference_no'] );
			$payment_date_object 	= 	new DateTime($payment_date_received);
			$payment_date			= 	$payment_date_object->format('Y-m-d');
				
			$billdata['user_id']   		 =   $user_id;
			$billdata['bill_type']   	 =   $bill_type;
			$billdata['bill_number']  	 =   $bill_number;
			$billdata['total_amount']    =   $totalamount;
			$billdata['amount_paid']     =   $amountpaid;
			$billdata['paid_to']   	  	 =   $paid_to;
			$billdata['payment_date'] 	 =   $payment_date;
			$billdata['payment_mode']	 =   $paymentmode;
			$billdata['reference_no']	 =   $reference_no;
			$query 	= 	$this->db->query("SELECT * FROM bills WHERE appcode = '$appcode' AND bill_number =  '$bill_number' AND NOT id = $billId");
			if( $query -> num_rows() > 0 )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Bill Number Already Exists!</div>'));
				redirect(base_url() . 'expenses/editbill/'.$billId, 'refresh');
			}
			if(!empty($_FILES['file_scan']['name']))
			{
				$ext = end(explode(".", $_FILES['file_scan']['name']));
				$path = explode('/', base_url());
				if(isset($_POST['fileName']))
					unlink(APPPATH.'../uploads/bills/'. $_POST['fileName']);
				move_uploaded_file($_FILES['file_scan']['tmp_name'], 'uploads/bills/' . "Bill_".$billId.'.'.$ext);
				$billdata['attachments'] 	 =   'Bill_'.$billId.'.'.$ext;
			}
			//code for receipts
			if(!empty($_FILES['file_scan_receipts']['name']))
			{
				$ext = end(explode(".", $_FILES['file_scan_receipts']['name']));
				$path = explode('/', base_url());
				if(isset($_POST['fileName']))
					unlink(APPPATH.'../uploads/receipts/'. $_POST['fileName']);
				move_uploaded_file($_FILES['file_scan_receipts']['tmp_name'], 'uploads/receipts/' . "Receipts_".$billId.'.'.$ext);
				$billdata['receipts'] 	 =   'Receipts_'.$billId.'.'.$ext;
			}
			$this->db->where('id', $billId);
			$this->db->update('bills', $billdata);
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Expense Bill Updated Successfully!</div>'));
			redirect(base_url() . 'expenses/bills', 'refresh');		
		}
		else
		{
         	redirect(base_url().'login/four_zero_four', 'refresh');
       	}
	}

}

/* End of file expenses.php */
/* Location: ./application/controllers/expenses.php */
