<?php echo $this->session->flashdata('notification');?>

<div id="page">
       <div class="row-fluid">
          <div class="span12">
             <!-- BEGIN SAMPLE FORM PORTLET-->	
             <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i><?php if( isset( $service['id'] ) ) echo 'Edit'; else echo 'Add'?> Service</h4>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                   <form action="<?php if( isset( $service['id'] ) ) echo base_url().'services/editService/update'; else echo base_url().'services/addService/add'?>" method="POST" class="form-horizontal">
                      <?php if( isset( $service['id'] ) ): ?>
                            <input type="hidden" name="id" value="<?php if( isset( $service['id'] ) ) echo $service['id']; ?>" />
                        <?php endif; ?>
                      <div class="control-group">
                         <label class="control-label" for="input1">Service Name</label>
                         <div class="controls">
                            <input type="text" required placeholder="Enter Service Name" class="span6" id="input" name="serviceName" value="<?php if( isset( $service['id'] ) ) echo $service['serviceName']; ?>" />
                         </div>
                      </div>
                      <div class="control-group">
                         <label class="control-label" for="input1">Service Cost</label>
                         <div class="controls">
                            <input type="number" required placeholder="Enter Service Cost" class="span6" id="input" name="serviceCost" value="<?php if( isset( $service['id'] ) ) echo $service['cost']; ?>" />
                         </div>
                      </div>
                      <div class="control-group">
                         <label class="control-label" for="input1">Service description</label>
                         <div class="controls">
                            <textarea name="description" rows="5" placeholder="Enter Service Description(optional)" class="span6"><?php if( isset( $service['id'] ) ) echo $service['description']; ?></textarea>
                         </div>
                      </div>
                     <div class="control-group">
                      <div class="form-actions">
                         <button type="submit" class="btn btn-primary">Save Service</button>
                      </div>
                      </div>
                   </form>
                             <!-- END FORM-->			
                </div>
             </div>
             <!-- END SAMPLE FORM PORTLET-->
          </div>
       </div>
      
      
    </div>
