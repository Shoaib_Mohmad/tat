<?php echo $this->session->flashdata('notification');?>
<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Services</h4>
						<span class="tools">
							<div class="btn-group">
								<a href="<?php echo base_url().'services/addservice';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Service</button></a>
							</div>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:8px">#</th>
									<th>Service Name</th>
									<th class="hidden-phone">Description</th>	
									<th>Service Cost</th>								
									<th class="hidden-phone">Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($serviceList as $row) { ?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><?php echo $row['serviceName']; ?></td>
								<td><?php echo $row['description']; ?></td>
								<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['cost']; ?></td>
								<td class="hidden-phone">
									<a href="<?php echo base_url().'services/editService/'.$row['id']; ?>" title="Edit Service"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
									 <a href="<?php echo base_url()?>services/viewservice/delete/<?php echo $row['id']; ?>" title="Delete Service" onclick="return confirm('Are you sure you want to delete the Service?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
								</td>
							</tr>
							 <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
