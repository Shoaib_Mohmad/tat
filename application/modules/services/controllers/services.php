<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting(0);

class Services extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'services';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('services');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('services/services_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	
	public function deleteService()
	{
		if($_POST)
		{
			$id 		= 	$_POST['id'];
			$appcode 	= 	$this->session->userdata['appcode'];
			$service 	= 	$this->db->query("SELECT invoice_id FROM services WHERE id = $id AND appcode = '$appcode'") -> row_array();
			$query 		= 	$this->db->query("DELETE FROM services WHERE id = $id AND user_id = $userId");
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Service Removed Successfully!</div>');

			echo base_url()."services/editInvoice/".$service['invoice_id'];
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}
		
	
	public function addService( $param1 = '' )
	{
		$appcode 	= 	$this->session->userdata['appcode'];

		if( $param1 == 'add')
		{	
			if( !isset( $_POST['serviceName'] ) )
			{
				redirect(base_url().'services/viewService', 'refresh');	
			}

			$temp = trim( $_POST['serviceName'] );
			
			$query = $this->db->get_where('servicetype', array(
            	'serviceName' 	=> $temp,
            	'appcode'  		=> $appcode         
        	));

	        if ($query->num_rows() == 1 ) 
	        {
	           $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Service name already exists!</div>');
				redirect(base_url().'services/viewService', 'refresh');        
	        } 
	        
			$data['serviceName']  	= 	trim( $_POST['serviceName'] );
			$data['cost']  			= 	trim( $_POST['serviceCost'] );	
			$data['description']  	= 	trim( $_POST['description'] );
			$data['appcode'] 		= 	$appcode;

			$result = $this->db->insert('servicetype', $data); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Service Saved Successfully!</div>');
				redirect(base_url().'index.php/services/viewservice', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				redirect(base_url().'services/addservice', 'refresh');
			}

		}

		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add Service";
		$data['fileName'] 		= 	"addService.php";
		$this->load->view('index', $data);
	}


	public function editService( $param1 = '' )
	{
		$appcode	= 	$this->session->userdata['appcode'];
		if( $param1 == 'update' )
		{	
			if( !isset( $_POST['id'] ) )
			{
				redirect(base_url().'services/viewservice', 'refresh');	
			}
			$id 		=	$_POST['id'];
			$temp 		= 	trim( $_POST['serviceName'] );
			$appcode  	= 	$this->session->userdata['appcode'];
			$query 		= 	$this->db->query("SELECT id FROM servicetype WHERE id != $id AND serviceName = '$temp' AND appcode = '$appcode'");

	        if ($query->num_rows() == 1 ) 
	        {
	           $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Service name already exists!</div>');
				redirect(base_url().'services/editService/'.$_POST['id'], 'refresh');        
	        } 

			$data['serviceName']  	= 	trim( $_POST['serviceName'] );
			$data['cost']  			= 	trim( $_POST['serviceCost'] );	
			$data['description']  	= 	trim( $_POST['description'] );
			

			$creteria 	=	array( 'id' => $id, 'appcode' => $appcode );
			$this->db->where( $creteria );
   			$result 	= 	$this->db->update( 'servicetype', $data );
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Service Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'index.php/services/viewService', 'refresh');
		}

		$data['service'] 	= 	$this->services_model->getServiceDetails( $param1, $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Service";
		$data['fileName'] 	= 	"addService.php";
		$this->load->view( 'index', $data );
	}

	
	
	public function viewService($param1 = '', $param2 = '')
	{

		if( $param1 == 'delete')
		{
			
			if( $param2 == '')
			{
				edirect(base_url().'services/viewservice', 'refresh');
			}

			$this->db->where('id', $param2);
	        $result = $this->db->delete('servicetype');    

	        if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Service Deleted Successfully!
					</div>');
				redirect(base_url().'services/viewservice', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Error in Deleting.</div>');
				redirect(base_url().'services/viewservice', 'refresh');
			}
		}

		$appcode 				=	$this->session->userdata['appcode'];
		$data['serviceList']	=	$this->services_model->getServiceTypes( $appcode );
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Services";
		$data['fileName'] 		= 	"services.php";
		$this->load->view('index', $data);		

	}
	
	
	public function insertServiceAjax()
	{
		$data['serviceName']  	= 	trim( $_POST['serviceName'] );
		$data['cost']  			= 	trim( $_POST['serviceCost'] );
		$data['description']  	= 	trim( $_POST['description'] );
		$data['appcode'] 		= 	$this->session->userdata['appcode'];
		$result = $this->db->insert('servicetype', $data);
		if($result)
			return true;
		else
			return false; 
	}

	
	public function getServiceAjax()
	{
		$appcode	=	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT serviceName, cost FROM servicetype WHERE appcode = '$appcode'")->result_array();
		echo json_encode($query);
	}


}

/* End of file services.php */
/* Location: ./application/controllers/services.php */
