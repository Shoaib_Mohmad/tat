<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function GetServicesById( $id )
    {
        $sql    = $this->db->get_where('services', array('invoice_id' => $id));
    	$result = $sql->result_array();
    	return $result ;
    }

    
    function getServiceTypes( $appcode )
    {
        $serviceTypes = $this->db->query("SELECT * FROM servicetype WHERE appcode = '$appcode' ORDER BY serviceName ASC") -> result_array();
        return $serviceTypes;
    }


    function getServiceDetails( $id, $appcode )
    {
        $serviceDetails   =   $this->db->query("SELECT * FROM servicetype WHERE id = $id AND appcode = '$appcode'");
        if( !$serviceDetails->num_rows() )
        {
            redirect(base_url().'services/editServices/'.$id, 'refresh');
        }
        else
        {    
            return $serviceDetails ->row_array();
        }
    }

    function checkEmailExists( $email )
    {
        $users   =   $this->db->query("SELECT * FROM users WHERE email= '$email'")->num_rows();
        return $users;
    }

}