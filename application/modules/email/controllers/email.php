<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller 
{

	function __construct()
	 {
		parent::__construct();
        $this->load->library('email');
		$this->load->library('parser');

		$this->load->model('sales/sales_model');
		$this->load->model('customers/customers_model');
		$this->load->model('hotels/hotels_model');
		$this->load->model('transporters/transporters_model');
		$this->load->model('tours/tours_model');
		$this->load->model('services/services_model');
          
	   	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	   	$this->output->set_header('Pragma: no-cache');
	 }


	public function sendEmail( $invoiceId = "" )
	{
		if( $_POST )
		{
			$email 		= $this->input->post('email');
			$query 		= $this->db->query("SELECT * FROM users WHERE email = '$email' AND status = 1");
			
			if($query->num_rows() == 1)
			{
				$result 	= $query -> row_array();
				$Id 		= $result['id'];
			    $customerEmail 	= $email;
			    $customerId		= $Id;
			    $password = rand(100000,999999);
			    $md5_password = md5($password);
			    $query 		= $this->db->query("UPDATE users SET password = '$md5_password' WHERE id = '$customerId'");
			    if($query)
			    {
			    
				    $message = "Hi,<br>Your new password is $password. Please change your password after login.<br>Thanks & Regards,<br>Team OCTA Travel";
			    	
				    $this->email->from('noreply@myasa.net', 'OCTA Travel - Password Reset');
				    $this->email->to( $customerEmail );
				    $this->email->subject('Password Reset');
				    $this->email->set_mailtype("html");
				    $this->email->message( $message ); 
				    
				    $this->email->send();
				    echo '<div class="alert alert-success" style="width: 77%; margin-left: 2em;">Password reset mail has been sent to your registered email!</div>';
				    return true;
			    }
			}
			else
			{
	         	echo '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">This Email Id is not Registered with Us!</div>';
	         	return true;
        	}
           	
		}

		elseif( is_numeric( $invoiceId ) )
		{
			$appcode 	= 	$this->session->userdata['appcode'];
			$query 		= 	$this->db->query("SELECT * FROM services_invoices WHERE id = $invoiceId AND appcode = '$appcode'");
			if( !$query -> num_rows() )
			{
				redirect(base_url().'index.php/login/four_zero_four', 'refresh');
			}
			$data['profile'] 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'") -> row_array(); 
			$data['invoiceDetails'] =	$query 	= 	$query -> row_array();

			$this->email->from('noreply@myasa.net', 'OCTA Travel');
		    $this->email->to( $query['email'] );
		   	$this->email->subject('Sales Invoice');
		   	$this->email->set_mailtype("html");
		    $email = $this->load->view('email/emailInvoice', $data, TRUE);
		    $this->email->message( $email );
		    $this->email->send();
		    //echo $this->email->print_debugger();
		    
		    $this->db->query("UPDATE services_invoices SET email_status = 1 WHERE id = $invoiceId AND appcode = '$appcode'");
		    $this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Sales Invoice has been successfully sent as an Email to the Customer!</div>'));
		    
			redirect(base_url() . 'sales/invoices', 'refresh');
		}

		else
		{
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
	}



    public function customerInvoice( $invoiceId = "",  $phone = "" )
    {
    	$invoiceId 		= 	base64_decode( $invoiceId );
		$phone 			= 	base64_decode( $phone );
    	if( !is_numeric($invoiceId) || !is_numeric($phone) )
    	{
	 		redirect(base_url().'login/four_zero_four', 'refresh');
	 	}

 		$this->load->library("Pdf");
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Octa Travel | Sales Invoice');
		$pdf->SetSubject('Invoices');
		$pdf->SetKeywords('OctaSoft, Travel, Invoices');
		
		$query 	= 	$this->db->query("SELECT * FROM services_invoices WHERE id = $invoiceId AND phone = $phone");
		$invoice_result 	= $query -> row_array();
		if( !$invoice_result ){
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$appcode 	= 	$invoice_result['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		
		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

				$pdf->AddPage();

		$query 	= 	$this->db->query("SELECT * FROM services_invoices WHERE id = $invoiceId AND appcode = '$appcode'");
		$invoice_result 	= $query -> row_array();
		if( !$invoice_result )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 15);
		$pdf->Cell(100, 5, 'SALES INVOICE', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'INVOICE #    :: '.$invoice_result['invoice_code'], 0, 0 );
		$pdf->Cell(100, 5, 'ARRIVAL        :: '.date("jS F, Y", strtotime($invoice_result['arrival'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON  :: '.date("jS F, Y", strtotime($invoice_result['invoice_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'DEPARTURE :: '.date("jS F, Y", strtotime($invoice_result['departure'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'TO', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $invoice_result['invoice_to'], 0, 1 );
		$pdf->Cell(100, 5, $invoice_result['city'].', '.$invoice_result['state'].', '.$invoice_result['country'], 0, 1 );
		$pdf->Cell(100, 5, 'Pin: '.$invoice_result['pin_code'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $invoice_result['phone'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$invoice_result['email'], 0, 1 );
		
		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);

		if( isset( $invoice_result['accomodation_total'] ) )
		{
			$pdf->Ln(5);
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'ACCOMODATION DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="3" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
							<th style="width:4%; text-align: center;"> #      		</th>
							<th style="width:18%;"> Hotel Name	</th>
							<th style="width:11%;"> Checkin	</th>
							<th style="width:11%;"> Checkout	</th>
							<th style="width:17%;"> Room Category 	</th>
							<th style="width:7%; text-align: center;"> Meal Plan  </th>
							<th style="width:9%; text-align: center;"> Rooms (#)  </th>
							<th style="width:7%; text-align: center;"> Extrabed   </th>
							<th style="width:7%; text-align: center;"> Days  (#)  </th>
							<th style="width:7%; text-align: center;"> Child WOB </th>
						</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->sales_model->getInvoiceHotelServices( $invoiceId, $appcode );

			

			if( $invoice_result['accomodation_total'] )
				$accomodation_total = $invoice_result['accomodation_total'].' INR';
			else
				$accomodation_total = $invoice_result['accomodation_total'];

			foreach ( $services as $service ) 
			{
				$hotel 	= 	$this->hotels_model->getHotelDetails( $service['hotel_id'], $appcode );
				$days 		= 	(strtotime($service['hotel_checkout']) - strtotime($service['hotel_checkin'])) / (60 * 60 * 24);
				$tbl .= '
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;">'.$sno++ .					'</td>
								<td>'.$hotel['hotel_name'].' ('.$hotel['address'].			' )</td>
								<td>'.date("jS F, Y", strtotime($service['hotel_checkin'])).'</td>
								<td>'.date("jS F, Y", strtotime($service['hotel_checkout'])).'</td>
								<td>'.$service['room_category'].' ('.$service['occupancy'].')'.'</td>
								<td style="text-align: center;">'.$service['meal_plan'].	'</td>
								<td style="text-align: center;">'.$service['no_of_rooms'].	'</td>
								<td style="text-align: center;">'.$service['extrabed'].		'</td>
								<td style="text-align: center;">'.$days.	'</td>
								<td style="text-align: center;">'.$service['child_without_bed'].	'</td>
								
							</tr>
						';
			}
			$tbl_footer .='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$accomodation_total.'</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['transportation_amount'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'TRANSPORTATION DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="4" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:20%;"> Transporter Name	</th>
						<th style="width:10%;"> Pickup	</th>
						<th style="width:18%;"> Trip 						</th>
						<th style="width:11%;"> Dated 						</th>
						<th style="width:15%;"> Vechile 					</th>
						<th style="width:10%; text-align: center;"> Days (#)</th>
						<th style="width:9%; text-align: center;"> Pax  (#)</th>
					</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->sales_model->getInvoiceTransportationServices( $invoiceId, $appcode );

			if( $invoice_result['transportation_total'] )
				$transportation_total = $invoice_result['transportation_total'].' INR';
			else
				$transportation_total = $invoice_result['transportation_total'];

			foreach ( $services as $service ) 
			{
				$transporter 	=  $this->transporters_model->getTransporterDetails( $service['transporter_id'], $appcode );
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$transporter['transporter_name'].				'</td>
							<td>'.$service['pickup'].							'</td>
							<td>'.$service['trip'].								'</td>
							<td>'.date("jS F, Y", strtotime($service['dot'])).	'</td>
							<td>'.$service['vechile'].							'</td>
							<td style="text-align: center;">'.$service['days'].	'</td>
							<td style="text-align: center;">'.$service['pax'].	'</td>
						</tr>';
			}
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$transportation_total.'</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['general_services_total'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'OTHER SERVICE DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:45%;"> Service Name</th>
						<th style="width:15%;"> Charges</th>
					</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->services_model->getInvoiceOtherServices( $invoiceId, $appcode );

			if( $invoice_result['general_services_total'] )
				$general_services_total = $invoice_result['general_services_total'].' INR';
			else
				$general_services_total = $invoice_result['general_services_total'];

			foreach ( $services as $service ) 
			{
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$service['service_type'].						'</td>
							<td>'.$service['total'].							'</td>
						</tr>';
			}
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$invoice_result['general_services_total'].'</strong></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['tour_id'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'TOUR PACKAGE', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:30%;">Tour Package Name</th>
						<th style="width:15%;">Each Paying</th>
						<th style="width:15%;">Plan</th>

					</tr>';

			$sno 	= 	1;
			
			$tour_invoice 	= 	$this->services_model->getTourInvoice( $invoiceId, $appcode );

			$paying    				 = explode('@$#', $tour_invoice['each_paying']);

			if( $invoice_result['tour_total'] )
				$tour_total = $invoice_result['tour_total'].' INR';
			else
				$tour_total = $invoice_result['tour_total'];

			
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$tour_invoice['tour_name'].						'</td>
							<td>'.$paying[0].							'</td>
							<td>'.$tour_invoice['plan'].				'</td>
						</tr>';
			
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$tour_total.'</strong></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}




		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(100, 10, 'GRAND TOTAL', 0, 1 );

		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
					<th style="width:22%; text-align: center;"> Total Amount</th>
					<th style="width:22%; text-align: center;"> Amount Paid</th>
					<th style="width:22%; text-align: center;"> Balance</th>
				</tr>';

		$services 	= 	$this->sales_model->getInvoiceOtherServices( $invoiceId, $appcode );

		$balance 	= 	$invoice_result['total_amount'] - $invoice_result['advance_payment'];
		
		if( !$balance )
			$balance 	=	'NILL';
		else
			$balance    .=	' INR';

		$tbl .= '
				<tr style="background-color:#f7f7f7; font-size:12px;">
					<td style="text-align: center;"><strong>'.$invoice_result['total_amount'].' INR</strong></td>
					<td style="text-align: center;"><strong>'.$invoice_result['advance_payment'].' INR</strong></td>
					<td style="text-align: center;"><strong>'.$balance.'</strong></td>
				</tr>';
		
		$tbl_footer ='	
					</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$pdf->AddPage();

		$pdf->Image(base_url().$logo, 145, 5, 50, 20);

		$html = '<hr />
		<h2>Terms & Conditions</h2><ul>';
		
		$tos	= 	$this->db->query("SELECT * FROM tos WHERE appcode = '$appcode' AND status = 1 ORDER BY id ASC") -> result_array();
		foreach($tos as $ts)
		{
 			$html .= '<li>'.$ts['tos_text'].'</li>';
 		}
 		$html .= '</ul>';
		
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );
		
		ob_end_clean();

		$pdf->Output('Sales-Invoice_' .$invoice_result['id']. '.pdf', 'I');
	}


	public function emailTransportVoucher( $voucherId = '', $appcode = '' )
	{
		$data['profile'] 	= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'") -> row_array(); 
		$data['voucher'] 	=	$this->services_model->getTransportVoucherDetails( $voucherId, $appcode );
		$data['transporter']= 	$this->services_model->getTransporterDetails( $data['voucher']['transporter_id'], $appcode );

		$this->email->from('noreply@myasa.net', 'OCTA Travel');
	    $this->email->to( $data['transporter']['email'] );
	   	$this->email->subject('Transport Voucher');
	   	$this->email->set_mailtype("html");
	    $email = $this->load->view('email/emailTransportVoucher', $data, TRUE);
	    $this->email->message( $email );
	    $this->email->send();
	    //echo $this->email->print_debugger();
	    $this->db->query("UPDATE transport_vouchers SET email_status = 1 WHERE id = $voucherId AND appcode = '$appcode'");
	    $this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Voucher has been successfully sent as an Email to the Transporter!</div>'));
	    
		redirect(base_url() . 'transporters/transportVouchers', 'refresh');
	}

	
	public function transportVoucher( $voucherId = '', $appcode = '', $voucherType = '' )
	{
		$voucherId 		= 	base64_decode( $voucherId );
		$appcode 		= 	base64_decode( $appcode );
		$voucherType 	= 	base64_decode( $voucherType );

		if( !is_numeric( $voucherId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
		$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Hotel Voucher');
		$pdf->SetSubject('Hotel Voucher');
		$pdf->SetKeywords('OCTA, Travel, Hotel Voucher');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate Voucher!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$voucher 		= 	$this->services_model->getTransportVoucherDetails( $voucherId, $appcode );
		if( !$voucher )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 15);
		$pdf->Cell(100, 5, 'TRANSPORT VOUCHER', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'VOUCHER # :: '.$voucher['voucher_no'], 0, 0 );
		$pdf->Cell(100, 5, 'Arrival      :: '.date("jS F, Y", strtotime($voucher['arrival'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON   :: '.date("jS F, Y", strtotime($voucher['voucher_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'Departure:: '.date("jS F, Y", strtotime($voucher['departure'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'CUSTOMER DETAILS', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $voucher['customer_name'], 0, 1 );
		$pdf->Cell(100, 5, $voucher['customer_city'].', '.$voucher['customer_state'].', '.$voucher['customer_country'], 0, 1 );

		$pdf->Cell(100, 5, 'Pin: '.$voucher['customer_pin'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $voucher['customer_mobile'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$voucher['customer_email'], 0, 1 );

		$pdf->Ln(3);
		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 8, 'Please provide these services to the above captioned customer:', 0, 1 );

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:20%;"> Date 						</th>
						<th style="width:25%;"> Trip 						</th>
						<th style="width:20%;"> Vehicle 					</th>
						<th style="width:15%; text-align: center;"> Days 	</th>
						<th style="width:15%; text-align: center;"> Pax 	</th>
					</tr>';
		
		$services_result 	= 	$this->services_model->getTransportVoucherServices( $voucher['id'], $appcode );

		$sno 	= 	1;
		$total 	=	0;

		foreach ($services_result as $row ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .				'</td>
							<td>'.date("jS F, Y", strtotime($row['dot'])).	'</td>
							<td>'.$row['trip'].	'</td>
							<td>'.$row['vechile'].	'</td>
							<td style="text-align: center;">'.$row['days'].	'</td>
							<td style="text-align: center;">'.$row['pax'].	'</td>
							<td style="text-align: center;">'.$row['rate'].	'</td>
						</tr>
					';
			$total += 	$row['rate'];	
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';


		$hotel 	= 	$this->services_model->getHotelDetails( $voucher['pickup'], $appcode );

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'PICKUP', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $hotel['hotel_name'], 0, 1 );
		$pdf->Cell(100, 5, $hotel['address'], 0, 1 );


		$pdf->SetFont('helveticab', 'B', 10);
		
		$transporter 	=	$this->services_model->getTransporterDetails( $voucher['transporter_id'], $appcode );

		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'FOR', 0, 1 );

		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 5, $transporter['transporter_name'], 0, 1 );
		$pdf->Cell(100, 5, $transporter['address'], 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );
		
		ob_end_clean();

		$pdf->Output('Transport_Voucher - ' .$voucher['id']. '.pdf', 'I');
	}


	public function emailHotelVoucher( $voucherId = '', $appcode = '' )
	{
		$data['profile'] 	= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'") -> row_array(); 
		$data['voucher'] 	=	$this->services_model->getHotelVoucherDetails( $voucherId, $appcode );
		$data['hotel']		= 	$this->services_model->getHotelDetails( $data['voucher']['hotel_id'], $appcode );

		$this->email->from('noreply@myasa.net', 'OCTA Travel');
	    $this->email->to( $data['hotel']['email'] );
	   	$this->email->subject('Hotel Voucher');
	   	$this->email->set_mailtype("html");
	    $email = $this->load->view('email/emailHotelVoucher', $data, TRUE);
	    $this->email->message( $email );
	    $this->email->send();
	    //echo $this->email->print_debugger();
	    $this->db->query("UPDATE hotel_vouchers SET email_status = 1 WHERE id = $voucherId AND appcode = '$appcode'");
	    $this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Voucher has been successfully sent as an Email to the Hotel!</div>'));
	    
		redirect(base_url() . 'hotels/hotelVouchers', 'refresh');
	}

	
	public function hotelVoucher( $voucherId = '', $appcode = '', $voucherType = '' )
	{
		$voucherId 		= 	base64_decode( $voucherId );
		$appcode 		= 	base64_decode( $appcode );
		$voucherType 	= 	base64_decode( $voucherType );

		if( !is_numeric( $voucherId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
				$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Hotel Voucher');
		$pdf->SetSubject('Hotel Voucher');
		$pdf->SetKeywords('OCTA, Travel, Hotel Voucher');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate Voucher!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$voucher 		= 	$this->services_model->getHotelVoucherDetails( $voucherId, $appcode );
		if( !$voucher )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 14);
		$pdf->Cell(100, 5, 'HOTEL VOUCHER', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'VOUCHER # :: '.$voucher['voucher_no'], 0, 0 );
		$pdf->Cell(100, 5, 'CHECKIN   :: '.date("jS F, Y", strtotime($voucher['checkin'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON   :: '.date("jS F, Y", strtotime($voucher['voucher_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'CHECKOUT:: '.date("jS F, Y", strtotime($voucher['checkout'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'CUSTOMER DETAILS', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $voucher['customer_name'], 0, 1 );
		$pdf->Cell(100, 5, $voucher['customer_city'].', '.$voucher['customer_state'].', '.$voucher['customer_country'], 0, 1 );

		$pdf->Cell(100, 5, 'Pin: '.$voucher['customer_pin'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $voucher['customer_mobile'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$voucher['customer_email'], 0, 1 );
		
		$pdf->Ln(5);

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 8, 'Please provide these services to the above captioned customer:', 0, 1 );

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #      		</th>
						<th style="width:20%;"> Room Category 	</th>
						<th style="width:15%;"> Occupancy 	</th>
						<th style="width:15%; text-align: center;"> Meal Plan 	</th>
						<th style="width:15%; text-align: center;"> Rooms (#) </th>
						<th style="width:15%; text-align: center;"> Extrabed </th>
						<th style="width:15%; text-align: center;"> Child Without Bed </th>
					</tr>';
		
		$services_result 	= 	$this->services_model->getHotelVoucherServices( $voucher['id'], $appcode );

		$sno 	= 	1;
		
		$hotel 	 = 	$this->services_model->getHotelDetails( $voucher['hotel_id'], $appcode );

		$days = (strtotime($voucher['checkout']) - strtotime($voucher['checkin'])) / (60 * 60 * 24);
		$total = $hotel['cost'] * $days;

		foreach ($services_result as $row ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .				'</td>
							<td>'.$row['room_category'].	'</td>
							<td>'.$row['occupancy'].	'</td>
							<td style="text-align: center;">'.$row['meal_plan'].	'</td>
							<td style="text-align: center;">'.$row['no_of_rooms'].	'</td>
							<td style="text-align: center;">'.$row['extrabed'].	'</td>
							<td style="text-align: center;">'.$row['child_without_bed'].'</td>
						</tr>
					';
			$total += 	$row['amount'];	
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';
		
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(100, 10, 'FOR ', 0, 1 );

		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 5, $hotel['hotel_name'], 0, 1 );
		$pdf->Cell(100, 5, $hotel['address'], 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );
		
		ob_end_clean();

		$pdf->Output('Hotel_Voucher - ' .$voucher['id']. '.pdf', 'I');
	}


	public function contactUsMail()
	{
		if($_POST)
		{
			$this->load->library('email');
			$this->load->library('parser');
			
			$name 	= $this->input->post('name');
			$email 		= $this->input->post('email');
			$message 	= $this->input->post('message');

		    $message .= "<br>Thanks & Regards,<br>Team Octa Travel";
	    	
		    $this->email->from( $email, $name );
		    $this->email->to( 'info@myasa.net' );
		    $this->email->subject('Contact Us');
		    $this->email->set_mailtype("html");
		    $this->email->message( $message ); 
		    
		    $this->email->send();
		    $this->session->set_flashdata('notification', '<h6 style="color:#E48900;">Thanks for contacting us. Your message has been sent!</h6>');
		    redirect(base_url().'home/contactUs', 'refresh');
		}
		else
		{
	        redirect(base_url().'login/four_zero_four', 'refresh');
    	}
	}


	public function emailTourPackage()
	{
		$data['appcode']	= 	$this->session->userdata['appcode'];
		$appcode			=	$this->session->userdata['appcode'];
		$data['profile'] 	= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'") -> row_array(); 
		$data['tour'] 		=	$this->tours_model->getTourDetails($_POST['tour_id'] , $appcode );
		

		$this->email->from('noreply@myasa.net', 'OCTA Travel');
	    $this->email->to( $_POST['customers'] );
	   	$this->email->subject('Tour Package');
	   	$this->email->set_mailtype("html");
	    $email = $this->load->view('email/emailTourPackage', $data, TRUE);
	    $this->email->message( $email );
	    $this->email->send();
	    //echo $this->email->print_debugger();
	   // $this->db->query("UPDATE hotel_vouchers SET email_status = 1 WHERE id = $voucherId AND appcode = '$appcode'");
	    $this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Tour Package has been successfully sent as an Email to Customers!</div>'));
	    
		redirect(base_url() . 'tours/emailTour', 'refresh');
	}

	
	public function viewTour( $tourId = '', $appcode = '' )
    {
    	$tourId 		= 	base64_decode( $tourId );
		$appcode 		= 	base64_decode( $appcode );
		

		if( !is_numeric( $tourId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

       
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Tour Details');
        $pdf->SetSubject('OCTA Travel Tour Details');
        $pdf->SetKeywords('OCTA Travel, Tour Details');

       // $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Tour Details');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

        $tour     =   $this->tours_model->getTourDetails( $tourId, $appcode );

       
        // set font
        //$pdf->setCellPaddings ('', '', '', '5px');
        // add a page
        $pdf->AddPage('P');


		$img = "uploads/tourimg/".$tourId.'.png';
		
		$pdf->Image(base_url().$img, 15, 20, 180, 70,'' ,'' ,'N' ,true ,100,'',false);
		//$pdf->SetY(120);

		$pdf->SetFont('helveticab', 'B', 15);
		$pdf->SetTextColor(34,68,255);
		$pdf->ln(3);
		$pdf->Cell(180, 0, $tour['heading'], 0, 1,'C' );
		$pdf->ln(3);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('times', '', 12);
		$pdf->SetTextColor(255,0,0);
		$pdf->Cell(30, 6, 'Validity', 0, 0 );
		$pdf->Cell(100, 6, date("jS F, Y", strtotime($tour['validity'])), 0, 1 );
		$pdf->SetTextColor(0,0,0);
		$pdf->Ln(2);
		$pdf->writeHTML($tour['details'], true, false, false, false, '');
		
		$pdf->Ln(2);

		
		
		

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 10, 'TOUR TARIFF', 0, 1 );
		
		$pdf->SetTextColor(0,0,0);
				$pdf->Ln(3);
		$tbl_header = '<table cellpadding="10" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#333; font-size:12px; font-weight: bold">
						<th> Each Paying</th>
						<th> budget 	</th>
						<th > Economic  </th>
						<th > Standard  </th>
						<th > Super Deluxe </th>
					
					</tr>';
		
		$tariffs 	= 	$this->tours_model->getTourTariffs( $tourId, $appcode );

		foreach ($tariffs as $tariff ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td>'.$tariff['each_paying'].'</td>
							<td style="text-align: center;">'.$tariff['budget'].	'</td>
							<td style="text-align: center;">'.$tariff['economic'].'</td>
							<td style="text-align: center;">'.$tariff['standard'].'</td>
							<td style="text-align: center;">'.$tariff['super_deluxe'].'</td>
							
						</tr>
					';
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');



		$pdf->SetFont('times', '', 13);
		$pdf->SetTextColor(255,0,0);
		$pdf->Cell(100, 10, 'Inclusions', 0, 1 );
		$pdf->SetTextColor(0,0,0);
		$inclusion_array = explode("\n", $tour['inclusions']);

        $inclusion_str='<ul>';
		foreach ($inclusion_array as $key ) {

			$inclusion_str= $inclusion_str.'<li>'.$key.'</li>';
		}
		$inclusion_str= $inclusion_str.'</ul>';

		$pdf->writeHTML($inclusion_str, true, false, false, false, '');
		
		$pdf->SetTextColor(0,0,0);

		

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Tour_List.pdf', 'I');

    }


}

/* End of file email.php */
/* Location: ./application/controllers/email.php */
