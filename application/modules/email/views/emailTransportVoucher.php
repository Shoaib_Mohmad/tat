<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OCTA Travel | Transport Voucher</title>
</head>

<body >
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#8d8e90">
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding-top:2em;"><img src="<?php echo base_url().'assets/img/logo.png';?>"></td>
                
                <td width="393"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="46" align="right" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="10" width="100%" style="background-color:#05bcdb";></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="left" valign="top"><font style="font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:20px"><strong><em><?php echo $transporter['transporter_name'];?>,</em></strong></font><br /><br />
                  <font style="font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px">Please click on the link below to view the Voucher.


              <br /><br />
              Thanks & Regards, <br> 
              <?php echo $profile['company_name']; ?></font></td>
                <td width="10%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right" valign="top"><table width="108" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><!-- <img src="images/PROMO-GREEN2_04_01.jpg" width="108" height="9" style="display:block" border="0" alt=""/> --></td>
                  </tr>
                 
                  <tr>
                    <td align="center" valign="middle" bgcolor="#03bcda"><font style="font-family: Georgia, 'Times New Roman', Times, serif; color:#ffffff; font-size:15px"><strong><a href=""<?php echo base_url().'email/hotelVoucher/'.base64_encode($voucher['id']).'/'.base64_encode($appcode).'/'.base64_encode('Hotel')?>" style="color:#ffffff; text-decoration:none"><em>View / Download</em></a></strong></font></td>
                  </tr>
                  <tr>
                    <td height="10" align="center" valign="middle" bgcolor="#03bcda"> </td>
                  </tr>
                </table></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="7" width="100%" style="background-color:#05bcdb";></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#231f20; font-size:8px"><strong><?php echo $profile['company_name'].', '.$profile['street'].', '.$profile['city'].', '.$profile['state'].', '.$profile['country'].', '.' Zip Code - '.$profile['pin'].'<br>Phone - '.$profile['phone'].' | Fax - '.$profile['phone'];?><a href= "http://yourlink" style="color:#010203; text-decoration:none"><?php $profile['fax'];?></a></strong></font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
