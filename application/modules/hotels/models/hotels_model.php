<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotels_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function getHotels( $appcode )
    {
        $hotelRates   =   $this->db->query("SELECT * FROM hotels WHERE appcode = '$appcode' AND status = 'Active' ORDER BY hotel_name") ->result_array();
        return $hotelRates;
    }


    function getHotelDetails( $id, $appcode )
    {
        $hotelRate   =   $this->db->query("SELECT * FROM hotels WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
        if( !$hotelRate->num_rows() )
        {
            redirect(base_url().'services/hotelRates', 'refresh');
        } 
        return $hotelRate->row_array();
    }


    function getHotelTariffs( $hotel_id, $appcode )
    {
        $hotelTariffs   =   $this->db->query("SELECT * FROM hotel_tariff WHERE hotel_id = $hotel_id AND appcode = '$appcode' AND status = 'Active' ORDER BY room_category DESC")->result_array();
        return $hotelTariffs;
    }


    function getTariffDetails( $tariff_id, $appcode )
    {
        $hotelTariff   =   $this->db->query("SELECT * FROM hotel_tariff WHERE id = $tariff_id AND appcode = '$appcode' AND status = 'Active'")->row_array();
        return $hotelTariff;
    }


    function getHotelExtrabedding( $hotel_id, $appcode )
    {
        $extrabedding   =   $this->db->query("SELECT * FROM hotel_extrabedding WHERE hotel_id = $hotel_id AND appcode = '$appcode' AND status = 'Active'")->row_array();
        return $extrabedding;
    }


    function getHotelVouchers( $appcode )
    {
        $vouchers   =   $this->db->query("SELECT * FROM hotel_vouchers WHERE appcode = '$appcode' AND status = 'Active' ORDER BY id DESC") -> result_array();
        return $vouchers;
    }


    function getHotelVoucherNo( $appcode )
    {
        $query  =   $this->db->query("SELECT voucher_no FROM hotel_vouchers WHERE appcode = '$appcode' ORDER BY voucher_no DESC LIMIT 0,1") -> row_array();
        if(isset($query['voucher_no'])) 
            $voucher_no     =   ++$query['voucher_no'];
        else
            $voucher_no     =   1;
        return $voucher_no;
    }


    function hotelVoucherNoAvailable( $voucher_no, $appcode )
    { 
        $query = $this->db->get_where('hotel_vouchers', array(
            'voucher_no'     =>     $voucher_no,
            'appcode'        =>     $appcode
        ));

        if ($query->num_rows() == 1 ) 
        {
            return false;        
        } 
        else 
        {
            return true;
        }
    }


    function getHotelVoucherDetails( $id, $appcode )
    {
        $voucher   =   $this->db->query("SELECT hotel_vouchers.*, SUM( hotel_voucher_services.amount ) as amount FROM hotel_vouchers LEFT JOIN hotel_voucher_services ON hotel_vouchers.id = hotel_voucher_services.voucher_id WHERE hotel_vouchers.id = $id AND hotel_vouchers.appcode = '$appcode' AND hotel_vouchers.status = 'Active'") -> row_array();
        return $voucher;
    }


    function getHotelVoucherServices( $voucher_id, $appcode )
    {
        $voucherServices   =   $this->db->query("SELECT * FROM hotel_voucher_services WHERE voucher_id = $voucher_id AND appcode = '$appcode'") -> result_array();
        return $voucherServices;
    }


    function getHotelVoucherServicesSum( $voucher_id, $appcode )
    {
        $voucherServices   =   $this->db->query("SELECT SUM( amount ) as servicesTotal FROM hotel_voucher_services WHERE voucher_id = $voucher_id AND appcode = '$appcode'") -> row_array();
        return $voucherServices['servicesTotal'];
    }


    function getVoucherHotelServicesTotal( $voucher_id )
    {
        $servicesVoucher   =   $this->db->query("SELECT SUM(amount) as total FROM hotel_voucher_services WHERE voucher_id = $voucher_id")->row_array();
        return $servicesVoucher['total'];
    }


 }