<?php echo $this->session->flashdata('notification');?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Hotels</h4>
					
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'hotels/hotelsPdf';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button></a>
						</div>
					</span>	
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'hotels/addHotel';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Hotel</button></a>
						</div>
					</span>								
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Hotel Name</th>
								<th>Address</th>
								<th>Phone No</th>
								<th>Fax</th>
								<th>Email</th>
								<th>Website</th>
								<th>Description</th>	
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						<?php 
      						$count = 1;
      						foreach ($hotels as $hotel) 
      							{ 
      					?>
						<tr class="odd gradeX">
							<td><?php echo $count++; ?></td>
							<td><?php echo $hotel['hotel_name']; ?></td>
							<td><?php echo $hotel['address']; ?></td>
							<td><?php echo $hotel['phone']; ?></td>
							<td><?php echo $hotel['fax']; ?></td>
							<td><a href="mailto:<?php echo $hotel['email']; ?>"><?php echo $hotel['email']; ?></a></td>
							<td><a href="<?php echo $hotel['website']; ?>" target="_Blank"><?php echo $hotel['website']; ?></a></td>
							<td><?php echo $hotel['description']; ?></td>
							<td>
								<a href="javascript:void(0);" title="View Hotel" onClick = 'window.open("<?php echo base_url().'hotels/hotel/'.$hotel['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> |
								<a href="<?php echo base_url().'hotels/addHotel/edit/'.$hotel['id']; ?>" title="Edit Hotel"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
								<a href="<?php echo base_url()?>hotels/index/delete/<?php echo $hotel['id']; ?>" title="Delete Hotel" onclick="return confirm('Are you sure you want to delete the Hotel?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
							</td>
						</tr>
						 <?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
