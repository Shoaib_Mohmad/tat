<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Hotel Vouchers</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'hotels/addHotelVoucher'; ?>" style="margin: -5px 6px 1px 0px; padding-right:30px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Voucher </a>
						</div>
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Voucher No</th>
								<th>Invoice No</th>
								<th>Voucher Date</th>
								<th>Customer Name</th>
								<th>Hotel Name</th>
								<th>Email</th>
								<th>Contact No</th>
								<th>Amount</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ( $vouchers as $voucher ) 
							{ 
								if( $voucher['status'] !== 'Deleted' )
								{
									$hotel 	= 	$this->hotels_model->getHotelDetails( $voucher['hotel_id'], $appcode );
									if( $voucher['email_status'] == 0 )
									{
										$emailIcon 	= '<i class="fa fa-envelope-o" style="font-size: 18px;"></i>';
										$title 		= 'Send Email';
									}
									else
									{
										$emailIcon 	= '<i class="fa fa-envelope" style="font-size: 18px;"></i>';
										$title 		= 'Email Sent';
									}

									$servicesTotal 	= $this->hotels_model->getHotelVoucherServicesSum( $voucher['id'], $appcode );
							?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><center><span class="label label-primary"><?php echo $voucher['voucher_no']; ?></span></center></td>
									<td><center><span class="label label-primary"><?php echo $voucher['invoice_no']; ?></span></center></td>
									<td><?php print(date("jS F, Y", strtotime($voucher['voucher_date'])));?></td>
									<td><?php echo $voucher['customer_name']; ?></td>
									<td><?php echo $hotel['hotel_name']; ?></td>
									<td><?php echo $hotel['email']; ?></td>
									<td><?php echo $hotel['phone']; ?></td>
									<td><i class="fa fa-fw fa-rupee"></i><?php echo $servicesTotal; ?></td>
									<td> <a href="javascript:void(0);" title="View Voucher" onClick = 'window.open("<?php echo base_url().'hotels/hotelVoucher/'.$voucher['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> | <a href="<?php echo base_url().'email/emailHotelVoucher/'.$voucher['id'].'/'.$appcode; ?>" title="<?php echo $title; ?>" ><?php echo $emailIcon; ?></a> | <a href="<?php echo base_url().'hotels/editHotelVoucher/'. $voucher['id']; ?>" title="Edit Voucher" ><i class="fa fa-edit" style="font-size: 18px; color: blue;"></i></a> | <a href="<?php echo base_url().'hotels/hotelVouchers/delete/'.$voucher['id']; ?>" title="Delete Voucher" onclick="return confirm('Are you sure you want to delete the Voucher?')"><i class="fa fa-trash-o" style="font-size: 18px; color: red;"></i></a> </td>
								</tr>
							<?php } }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
