<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
        var date = new Date();
        var day  = date.getDate();
        var month  = date.getMonth() + 1;
        var yy  = date.getYear();
        var year = (yy < 1000) ? yy + 1900 : yy;
        var newdate = month + "/" + day + "/" + year;
		$('#date').daterangepicker({ minDate: newdate});   //Here  Set the Min Display Date
		
    });    

    
    function checkDate()
    {
      	var checkIn = new Date($('#checkin').val());
      	var checkOut = new Date($('#checkout').val());
      	if(checkIn > checkOut){
       		alert("Please ensure that the CheckOut Date is greater than the CheckIn Date.");
        	return false;
      	}
      	return true;
  	}


	function getRoomCategories()
	{
		var hotel_id 		= 	$('#hotel_id').val();
		var serviceLength 	= 	($('.hotel-voucher-services .control-group').length).toString();
		
		var serviceLenght1 	= 	serviceLength;
		var serviceLenght2 	= 	serviceLength;

		if( hotel_id == '' )
		{
			for(i=serviceLenght1;serviceLenght1>=i;serviceLenght1--)
	        {
		        var x 	= 	document.getElementById('room_type_'+serviceLenght1);
		        x.options.length = 0;
		        
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Room Category';
		        x.add(option);

			    var x 	= 	document.getElementById('extrabed_'+serviceLenght2);
		        x.options.length = 0;
		        
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Extrabed';
		        x.add(option);

		    }
			return false;
		}
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."hotels/ajaxGetRooms";?>',
			data: { hotel_id: hotel_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);
		        
		        for(i=serviceLenght1;serviceLenght1>=i;serviceLenght1--)
		        {
			        var x 	= 	document.getElementById('room_type_'+serviceLenght1);
			        x.options.length = 0;
			        
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'';
			        option.text 	= 	'Room Category';
			        x.add(option);

				    for(i=0;i<jsonObj.length;i++)
	                {
	                    var option    =   document.createElement("option");
	                    option.value  =   jsonObj[i].id;
	                    option.text   =   jsonObj[i].room_category + ' ( '+ jsonObj[i].occupancy +' )';
	                    x.add(option);
	                }

			    }
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."hotels/ajaxGetExtrabedding";?>',
			data: { hotel_id: hotel_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        for(i=serviceLenght2;serviceLenght2>0;serviceLenght2--)
		        {
			        var x 	= 	document.getElementById('extrabed_'+serviceLenght2);
			        x.options.length = 0;
			        
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'0';
			        option.text 	= 	'Extrabed';
			        option.setAttribute("cost", "");
			        x.add(option);

				    var option 		= 	document.createElement("option");
			        option.value 	= 	'1';
			        option.text 	= 	'1';
			        option.setAttribute("cost", jsonObj.charges*1);
			        x.add(option);

			        var option 		= 	document.createElement("option");
			        option.value 	= 	'2';
			        option.text 	= 	'2';
			        option.setAttribute("cost", jsonObj.charges*2);
			        x.add(option);

			    }
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		return false;
			
	}

	function getTariffDetails( param )
	{
		var tariff_id = $('#room_type_'+ param).val();
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."hotels/ajaxGetTariffDetails";?>',
			data: { tariff_id: tariff_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        var x 	= 	document.getElementById('meal_plan_' + param);
		        x.options.length = 0;
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Meal Plan';
		        option.setAttribute("cost", "");
		        x.add(option);
			    
			    if( jsonObj.ep !== '0' )
			    {
	                var option    =   document.createElement("option");
	                option.value  =   'ep';
	                option.text   =   'EP';
	                option.setAttribute("cost", jsonObj.ep);
	                x.add(option);
            	}

                if( jsonObj.cp !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'cp';
	                option.text   =   'CP';
	                option.setAttribute("cost", jsonObj.cp);
	                x.add(option);
            	}

                if( jsonObj.map !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'map';
	                option.text   =   'MAP';
	                option.setAttribute("cost", jsonObj.map);
	                x.add(option);
	            }

                if( jsonObj.ap !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'ap';
	                option.text   =   'AP';
	                option.setAttribute("cost", jsonObj.ap);
	                x.add(option);
            	}
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});
		return false;
	}


  	
	function startHotelCalc()
	{
		var itemLength 	= 	$('.hotel-voucher-services .control-group').length; 
		var meal_plan, no_of_rooms, extrabed, days, amount = 0;
		var date 		= 	$('#date').val();
		if( date !== '' )
		{
			date 		= 	date.split("-");
			checkIn 	= 	moment(date[0]);
			checkOut 	= 	moment(date[1]);
			days 		= 	checkOut.diff(checkIn, "days")
		}
		else
		{
			days 	= 	1;
		}

		for( i=1; i<=itemLength; i++ )
		{
			meal_plan 	=	$("#meal_plan_"+i).find(':selected').attr('cost');
			extrabed 	=	$("#extrabed_"+i).find(':selected').attr('cost');
			no_of_rooms =   $("#roomNos_"+i).val();
			if( no_of_rooms == "" && extrabed == "" )
			{
				amount = amount + parseInt(meal_plan, 10);
			}
			else if( no_of_rooms !== "" && extrabed == "" )
			{
				amount = amount + parseInt(meal_plan, 10) * no_of_rooms;
			}
			else if( no_of_rooms == ""  && extrabed !== "" )
			{
				amount = amount + parseInt(meal_plan, 10) + parseInt(extrabed, 10);
			}
			else
			{
				amount = amount + (parseInt(meal_plan, 10) * no_of_rooms) + parseInt(extrabed, 10);
			}
		}
		amount = amount * days;
		
		if( amount >= 0 )
		{
			$('#total_amount').val(Math.ceil(amount));
		}
		return false;
	}

	jQuery(document).ready(function($) {
	 	$( '.daterangepicker' ).focusout(function() {
	 		startHotelCalc();
			return true;
		});
	});

</script>

<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Edit Hotel Voucher
					</h4>
					<span class="tools"> <a href="javascript:;"
						class="icon-chevron-down"></a> <a href="#widget-config"
						data-toggle="modal" class="icon-wrench"></a> <a
						href="javascript:;" class="icon-refresh"></a> <a
						href="javascript:;" class="icon-remove"></a>
					</span>
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form onsubmit="return checkDate();" action="<?php echo base_url();?>hotels/updateHotelVoucher" method="POST" class="form-horizontal">
					<input type="hidden" name="voucher_id" value="<?php echo $voucher['id']; ?>">
						<div class="control-group">
							<label class="control-label" for="input1">Voucher Date</label>
							<div class="controls">
								<input class="input-small date-picker span2" required size="16"
									type="text" placeholder="MM/DD/YYYY" name="voucher_date" id="voucher_date" value="<?php echo date('m/d/Y', strtotime($voucher['voucher_date'])); ?>" />
							</div>
						</div>
						
						<input type="hidden" readonly required class="span6" id="invoice_to" name="invoice_to" />
							
						<div class="control-group">
							<label class="control-label" for="input1">Invoice No</label>
							<div class="controls">
								<select class="chosen span6" id="invoice_no" name="invoice_no" required>
									<option value="na">Select Invoice No</option>
									<?php 
										foreach($invoiceNos as $invoiceNo)
										{
									?>
										<option value="<?php echo $invoiceNo['invoice_code'];?>" <?php if( $voucher['invoice_no'] == $invoiceNo['invoice_code'] ) echo 'Selected'; ?> ><?php echo $invoiceNo['invoice_code'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Hotel Name</label>
							<div class="controls">
								<select onchange="getRoomCategories();" class="chosen span6" id="hotel_id" name="hotel_id" required>
									<option value="">Select Existing Hotel</option>
									<?php 
										foreach($hotels as $hotel)
										{
									?>
										<option value="<?php echo $hotel['id'];?>" <?php if( $voucher['hotel_id'] == $hotel['id'] ) echo 'Selected'; ?> ><?php echo $hotel['hotel_name'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">CheckIn - CheckOut</label>
							<div class="controls">
								<div class="input-prepend">
									<span class="add-on"><i class="fa fa-calendar"></i> </span>
									<input type="text" id="date" onInput="javascript:startHotelCalc();" name="date" required placeholder="<?php echo date( 'm/d/Y' ); ?>"
										class="input-large date-range" value="<?php echo date( 'm/d/Y', strtotime( $voucher['checkin'] ) ).' - '.date( 'm/d/Y', strtotime( $voucher['checkout'] ) ); ?>"/>
								</div>
							</div>
						</div>

						<div class="hotel-voucher-services">
							
							<label class="control-label" for="inputText">Add Room</label>
								<?php 
									$sno = 1; 
									$total = 0; 
									$currentTariff = array();
									foreach( $voucherServices as $service ): 
								?>
									<div class="control-group">
										<div class="controls">
											
											<select required class="span4" onchange="getTariffDetails(<?php echo $sno; ?>);" id="room_type_<?php echo $sno; ?>" name="room_type[]">
												<option value=''>Room Category</option>
												<?php foreach( $tariffs as $tariff ): ?>
													<option value='<?php echo $tariff['id']; ?>' <?php if( $service['room_category'].' ( '.$service['occupancy'].' )' == $tariff['room_category'].' ( '.$tariff['occupancy'].' )' ) {echo 'Selected'; $currentTariff = $tariff; } ?> ><?php echo $tariff['room_category'].' ( '.$tariff['occupancy'].' )'; ?></option>
												<?php endforeach; ?>
					   						</select>
					   						
					   						<select required class="span2" onchange="javascript:startHotelCalc();" id="meal_plan_<?php echo $sno; ?>" name="meal_plan[]">
												<option value='' cost="0">Meal Plan</option>
												<option value='ep' cost="<?php echo $currentTariff['ep']; ?>" <?php if( $service['meal_plan'] == 'ep' ) echo 'Selected'; ?> >EP</option>
												<option value='cp' cost="<?php echo $currentTariff['cp']; ?>" <?php if( $service['meal_plan'] == 'cp' ) echo 'Selected'; ?> >CP</option>
												<option value='map' cost="<?php echo $currentTariff['map']; ?>" <?php if( $service['meal_plan'] == 'map' ) echo 'Selected'; ?> >MAP</option>
												<option value='ap' cost="<?php echo $currentTariff['ap']; ?>" <?php if( $service['meal_plan'] == 'ap' ) echo 'Selected'; ?> >AP</option>
					   						</select>

					   						<select required class="span2" onchange="javascript:startHotelCalc();" id="roomNos_<?php echo $sno; ?>" name="roomNos[]">
												<option value=''>No of Rooms</option>
						   						<option value="1" <?php if( $service['no_of_rooms'] == '1' ) echo 'Selected'; ?> >1</option>
						   						<option value="2" <?php if( $service['no_of_rooms'] == '2' ) echo 'Selected'; ?> >2</option>
						   						<option value="3" <?php if( $service['no_of_rooms'] == '3' ) echo 'Selected'; ?> >3</option>
					   						</select>

					   						<select class="span2" onchange="javascript:startHotelCalc();" id="extrabed_<?php echo $sno++; ?>" name="extrabed[]">
												<option value='0' cost="" <?php if( $service['extrabed'] == '0' ) echo 'Selected'; ?> >Extrabed</option>
												<option value="1" cost="<?php echo $extrabedding['charges']; ?>" <?php if( $service['extrabed'] == '1' ) echo 'Selected'; ?> >1</option>
												<option value="2" cost="<?php echo $extrabedding['charges'] * 2; ?>" <?php if( $service['extrabed'] == '2' ) echo 'Selected'; ?> >2</option>
					   						</select>

					   						<select class="span2" onchange="javascript:startHotelCalc();" id="child_without_bed<?php echo $sno; ?>" name="child_without_bed[]">
									            <option value=''>Child WOB</option>
									            <option value="1" <?php if( $service['child_without_bed'] == '1' ) echo 'Selected'; ?> >1</option>
									            <option value="2" <?php if( $service['child_without_bed'] == '2' ) echo 'Selected'; ?> >2</option>
									            <option value="3" <?php if( $service['child_without_bed'] == '3' ) echo 'Selected'; ?> >3</option>
									        </select>
												
										</div>
									</div>
								<?php 
									$total += $service['amount']; 
									endforeach; 
								?>
							
						</div>
						<br>
						<div class="control-group">
							<div class="controls">
								<input type="button" class="btn btn-success" value="Add Room" id="addRoomVoucherButton"> 
								<input type="button" class="btn btn-danger"	value="Remove Room" id="removeRoomVoucherButton">
							</div>							
						</div>
												
						<div class="control-group">
							<label class="control-label" for="input1">Total Amount (<i class="fa fa-fw fa-rupee"></i>)</label>
							<div class="controls">
								<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="total_amount" name="total_amount" value="<?php echo $total; ?>" />
							</div>

							<div style="padding-left: 24em; margin-top: -2.3em;">
								<label class="control-label" for="input1">No of Pax </label>
								<div class="controls">
									<input type="number" autocomplete="off" required pattern="\d+(\.\d{2})?" class="span3" id="total_amount" name="pax" value="<?php echo $voucher['pax']; ?>" />
								</div>
							</div>
						</div>
						
						<div class="control-group">
	                        <label class="control-label" for="input1">Remarks</label>
	                        <div class="controls">
	                            <textarea name="remarks" rows="3" placeholder="Enter Additional Information (If Any)" class="span6"><?php echo $voucher['remarks']; ?></textarea>
	                        </div>
	                    </div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>

</div>
