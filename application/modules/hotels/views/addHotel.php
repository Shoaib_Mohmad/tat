<?php echo $this->session->flashdata('notification');?>

<div id="page">
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php if( isset( $hotel['id'] ) ) echo 'Edit'; else echo 'Add'?> Hotel</h4>
            </div>
            <div class="widget-body form">
                <!-- BEGIN FORM-->
               <form action="<?php if( isset( $hotel['id'] ) ) echo base_url().'hotels/addHotel/update'; else echo base_url().'hotels/addHotel/add'?>" method="POST" class="form-horizontal">
                  <?php if( isset( $hotel['id'] ) ): ?>
                        <input type="hidden" name="id" value="<?php if( isset( $hotel['id'] ) ) echo $hotel['id']; ?>" />
                    <?php endif; ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">Hotel Name</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter Hotel Name" class="span6" id="input" name="hotel_name" value="<?php if( isset( $hotel['hotel_name'] ) ) echo $hotel['hotel_name']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Address</label>
                     <div class="controls">
                        <textarea required name="address" rows="3" placeholder="Enter Hotel Address" class="span6"><?php if( isset( $hotel['address'] ) ) echo $hotel['address']; ?></textarea>
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Phone No</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter Phone No" class="span6" id="input" name="phone" value="<?php if( isset( $hotel['phone'] ) ) echo $hotel['phone']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Fax</label>
                     <div class="controls">
                        <input type="text" placeholder="Enter Fax" class="span6" id="input" name="fax" value="<?php if( isset( $hotel['fax'] ) ) echo $hotel['fax']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Email</label>
                     <div class="controls">
                        <input type="email" required placeholder="Enter Email" class="span6" id="input" name="email" value="<?php if( isset( $hotel['email'] ) ) echo $hotel['email']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Website</label>
                     <div class="controls">
                        <input type="text" placeholder="Enter Website (Optional)" class="span6" id="input" name="website" value="<?php if( isset( $hotel['website'] ) ) echo $hotel['website']; ?>" />
                     </div>
                  </div>
                  
                  <div class="control-group">
                     <label class="control-label" for="input1">Description</label>
                     <div class="controls">
                        <textarea name="description" rows="3" placeholder="Enter Hotel Description (Optional)" class="span6"><?php if( isset( $hotel['description'] ) ) echo $hotel['description']; ?></textarea>
                     </div>
                  </div>
                 <div class="control-group">
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">Save Hotel</button>
                  </div>
                  </div>
               </form>
                         <!-- END FORM-->			
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>

<?php if( isset( $hotel['id'] ) ): ?>

   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Hotel Tariff</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body form">
                <!-- BEGIN TABLE-->
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th width="20%">Room Category</th>
                        <th width="20%">Room Occupancy</th>
                        <th width="15%">Meal Plan (EP)</th>
                        <th width="15%">Meal Plan (CP)</th>
                        <th width="15%">Meal Plan (MAP)</th>
                        <th width="15%">Meal Plan (AP)</th>
                        <th width="10%"><center>Actions</center></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php foreach( $tariffs as $tariff ): ?>
                        <tr class="odd gradeX">
                           <form action="<?php echo base_url().'hotels/addHotelRoom/update'; ?>" method="POST">
                              <input type="hidden" name="id" value="<?php echo $tariff['id']; ?>">
                              <input type="hidden" name="hotel_id" value="<?php echo $hotel['id']; ?>">
                              <td>
                                 <select name="room_category" class="span10" required>
                                    <option value=""> </option>
                                    <option <?php if( $tariff['room_category'] == 'Standard Room' ) echo 'Selected'; ?> value="Standard Room">Standard Room</option>
                                    <option <?php if( $tariff['room_category'] == 'Deluxe Room' ) echo 'Selected'; ?> value="Deluxe Room">Deluxe Room</option>
                                 </select>
                              </td>
                              <td>
                                 <select name="occupancy" class="span10" required>
                                    <option value=""></option>
                                    <option <?php if( $tariff['occupancy'] == 'Single' ) echo 'Selected'; ?> value="Single">Single</option>
                                    <option <?php if( $tariff['occupancy'] == 'Double' ) echo 'Selected'; ?> value="Double">Double</option>
                                    <option <?php if( $tariff['occupancy'] == 'Triple' ) echo 'Selected'; ?> value="Triple">Triple</option>
                                 </select>
                              </td>
                              <td><input type="number" class="span10" placeholder="EP" value="<?php echo $tariff['ep']; ?>" name="ep" required></td>
                              <td><input type="number" class="span10" placeholder="CP" value="<?php echo $tariff['cp']; ?>" name="cp"></td>
                              <td><input type="number" class="span10" placeholder="MAP" value="<?php echo $tariff['map']; ?>" name="map"></td>
                              <td><input type="number" class="span10" placeholder="AP" value="<?php echo $tariff['ap']; ?>" name="ap"></td>
                              <td>
                                 <center>
                                    <button class="btn btn-alert btn-sm" title="Update Tariff" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button>
                                    <a href="<?php echo base_url().'hotels/deleteTariff/'.$hotel['id'].'/'.$tariff['id']; ?>" title="Delete Tariff" class="btn btn-alert btn-sm"><i class="fa fa-trash-o" style="font-size: 18px; color: red;"></i></a>
                                 </center>
                              </td>
                           </form>
                        </tr>
                     <?php endforeach; ?>
                     <tr class="odd gradeX">
                        <form action="<?php echo base_url().'hotels/addHotelRoom'; ?>" method="POST">
                           <td>
                              <input type="hidden" name="hotel_id" value="<?php echo $hotel['id']; ?>">
                              <select name="room_category" class="span10" required>
                                 <option value=""></option>
                                 <option value="Standard Room">Standard Room</option>
                                 <option value="Deluxe Room">Deluxe Room</option>
                              </select>
                           </td>
                           <td>
                              <select name="occupancy" class="span10" required>
                                 <option value=""></option>
                                 <option value="Single">Single</option>
                                 <option value="Double">Double</option>
                                 <option value="Triple">Triple</option>
                              </select>
                           </td>
                           <td><input type="number" class="span10" placeholder="EP" name="ep" required></td>
                           <td><input type="number" class="span10" placeholder="CP" name="cp"></td>
                           <td><input type="number" class="span10" placeholder="MAP" name="map"></td>
                           <td><input type="number" class="span10" placeholder="AP" name="ap"></td>
                           <td><center><button class="btn btn-alert btn-sm" title="Add Tariff" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button></center></td>
                        </form>
                     </tr>
                  </tbody>
               </table>
                         <!-- END TABLE-->       
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>

   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Extra Bedding</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body form">
                <!-- BEGIN TABLE-->
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th width="20%">Room Occupancy</th>
                        <th width="15%">Charges</th>
                        <th width="10%"><center>Action</center></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr class="odd gradeX">
                        <form action="<?php if(isset($extrabedding['id'])) echo base_url().'hotels/addHotelExtrabedding/update'; else echo base_url().'hotels/addHotelExtrabedding'; ?>" method="POST">
                           <?php if( isset( $extrabedding['id'] ) ): ?>
                              <input type="hidden" name="id" value="<?php echo $extrabedding['id']; ?>">
                           <?php endif; ?>
                           
                           <input type="hidden" name="hotel_id" value="<?php echo $hotel['id']; ?>">
                           <td>
                              <select name="occupancy" class="span10">
                                 <option value="Single">Single</option>
                              </select>
                           </td>
                           <td><input type="number" class="span10" value="<?php if(isset($extrabedding['charges'])) echo $extrabedding['charges']; ?>" placeholder="Charges" name="charges" required></td>
                           <td><center><button class="btn btn-alert btn-sm" title="Add Extrabedding" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button></center></td>
                        </form>
                     </tr>
                  </tbody>
               </table>
                         <!-- END TABLE-->       
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>

<?php endif; ?>

</div>

