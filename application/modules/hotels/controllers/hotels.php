<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotels extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'hotels';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('hotels');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('sales/sales_model');
		$this->load->model('services/services_model');
		$this->load->model('hotels/hotels_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function index( $param1 = '', $param2 = '' )
	{
		$appcode 			=	$this->session->userdata['appcode'];

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$data['status']  	= 	'Deleted';

			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('hotels', $data);

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels', 'refresh'); 
		}
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Hotels";
		$data['fileName'] 	= 	"hotels.php";
		$data['hotels'] 	= 	$this->hotels_model->getHotels( $appcode );
		$this->load->view( 'index', $data );
	}


	public function addHotel( $param1 = '', $param2 = '' )
	{
		$appcode 	=	$this->session->userdata['appcode'];

		if( $param1 == 'add' && $_POST )
		{	
			$data['appcode'] 		= 	$appcode;
			$data['hotel_name']  	= 	trim( $_POST['hotel_name'] );
			$data['address']  		= 	trim( $_POST['address'] );
			$data['phone']  		= 	trim( $_POST['phone'] );	
			$data['fax']  			= 	trim( $_POST['fax'] );	
			$data['email']  		= 	trim( $_POST['email'] );
			$data['website']  		= 	trim( $_POST['website'] );
			$data['description']  	= 	trim( $_POST['description'] );

			$result 	= 	$this->db->insert('hotels', $data); 
			$hotelId 	= 	$this->db->insert_id();
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Details Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels/addHotel/edit/'.$hotelId, 'refresh');
		}

		if( $param1 == 'update' && $_POST )
		{	
			$id  					= 	trim( $_POST['id'] );
			$data['hotel_name']  	= 	trim( $_POST['hotel_name'] );
			$data['address']  		= 	trim( $_POST['address'] );
			$data['phone']  		= 	trim( $_POST['phone'] );	
			$data['fax']  			= 	trim( $_POST['fax'] );	
			$data['email']  		= 	trim( $_POST['email'] );
			$data['website']  		= 	trim( $_POST['website'] );
			$data['description']  	= 	trim( $_POST['description'] );

			$this->db->where( array('id' => $id, 'appcode' => $appcode ) );
			$result = $this->db->update('hotels', $data); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels/addHotel/edit/'.$id, 'refresh');
		}

		if( $param1 == 'edit' )
		{
			$data['hotel'] 			= 	$this->hotels_model->getHotelDetails( $param2, $appcode );
			$data['tariffs'] 		= 	$this->hotels_model->getHotelTariffs( $param2, $appcode );
			$data['extrabedding'] 	= 	$this->hotels_model->getHotelExtrabedding( $param2, $appcode );

		}
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add Hotel";
		$data['fileName'] 		= 	"addHotel.php";
		$this->load->view( 'index', $data );
	}


	public function addHotelRoom( $param1 = '', $param2 = '' )
	{
		if( $_POST )
		{
			$appcode 	=	$this->session->userdata['appcode'];

			if( $param1 == 'update' )
			{
				$data['room_category']  = 	trim( $_POST['room_category'] );
				$data['occupancy']  	= 	trim( $_POST['occupancy'] );	
				$data['ep']  			= 	trim( $_POST['ep'] );	
				$data['cp']  			= 	trim( $_POST['cp'] );
				$data['map']  			= 	trim( $_POST['map'] );
				$data['ap']  			= 	trim( $_POST['ap'] );

				$this->db->where( array('id' => $_POST['id'], 'appcode' => $appcode ) );
				$result 	= 	$this->db->update('hotel_tariff', $data); 
			}
			else
			{
				$data['appcode'] 		= 	$appcode;
				$data['hotel_id']  		= 	trim( $_POST['hotel_id'] );
				$data['room_category']  = 	trim( $_POST['room_category'] );
				$data['occupancy']  	= 	trim( $_POST['occupancy'] );	
				$data['ep']  			= 	trim( $_POST['ep'] );	
				$data['cp']  			= 	trim( $_POST['cp'] );
				$data['map']  			= 	trim( $_POST['map'] );
				$data['ap']  			= 	trim( $_POST['ap'] );

				$result 	= 	$this->db->insert('hotel_tariff', $data); 
			}

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Tariff Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels/addHotel/edit/'.$_POST['hotel_id'], 'refresh');
		}
		else
		{
			redirect(base_url().'hotels', 'refresh');
		}
	}


	public function deleteTariff( $param1 = '', $param2 = '' )
	{
		if( is_numeric( $param1 ) && is_numeric( $param2 ) )
		{
			$appcode 			=	$this->session->userdata['appcode'];
			$data['status']  	= 	'Deleted';
			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('hotel_tariff', $data); 

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Tariff Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels/addHotel/edit/'.$param1, 'refresh');
		}
		else
		{
			redirect(base_url().'hotels', 'refresh');	
		}
	}


	public function addHotelExtrabedding( $param = '' )
	{
		if( $_POST )
		{
			$appcode 	=	$this->session->userdata['appcode'];
			if( $param == 'update' )
			{
				$data['charges']  	= 	trim( $_POST['charges'] );
				
				$this->db->where( array('id' => $_POST['id'], 'appcode' => $appcode ) );
				$result 	= 	$this->db->update('hotel_extrabedding', $data); 
			}
			else
			{
				$data['appcode'] 	= 	$appcode;
				$data['hotel_id']  	= 	trim( $_POST['hotel_id'] );
				$data['charges']  	= 	trim( $_POST['charges'] );

				$result 	= 	$this->db->insert('hotel_extrabedding', $data);
			} 

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Extrabedding Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'hotels/addHotel/edit/'.$_POST['hotel_id'], 'refresh');
		}
		else
		{
			redirect(base_url().'hotels', 'refresh');
		}
	}


	public function hotel( $hotelId )
    {
    	if( !is_numeric( $hotelId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Hotel Rates');
        $pdf->SetSubject('OCTA Travel Hotel Details');
        $pdf->SetKeywords('OCTA Travel, Hotel Details');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Hotel Details');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

        $hotel     =   $this->hotels_model->getHotelDetails( $hotelId, $appcode );

        $pdf->AddPage('P');
		$pdf->SetFont('helveticab', 'B', 15);

		$pdf->Cell(100, 0, $hotel['hotel_name'], 0, 1 );
		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 0, $hotel['address'], 0, 1 );
		
		$pdf->Ln(3);

		$pdf->SetFont('times', '', 11);
		$pdf->Cell(30, 6, 'PHONE', 0, 0 );
		$pdf->Cell(100, 6, $hotel['phone'], 0, 1 );

		$pdf->Cell(30, 6, 'FAX ', 0, 0 );
		$pdf->Cell(100, 6, $hotel['fax'], 0, 1 );

		$pdf->Cell(30, 6, 'EMAIL ', 0, 0 );
		$pdf->Cell(100, 6, $hotel['email'], 0, 1 );

		$pdf->Cell(30, 6, 'WEBSITE ', 0, 0 );
		$pdf->Cell(100, 6, $hotel['website'], 0, 1 );
		$pdf->Cell(30, 6, 'DESCRIPTION ', 0, 0 );
		$pdf->Cell(100, 6, $hotel['description'], 0, 1 );

		$pdf->Ln(10);

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 10, 'ROOM TARIFF', 0, 1 );
		
		$pdf->SetTextColor(0,0,0);

		$tbl_header = '<table cellpadding="10" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#333; font-size:12px; font-weight: bold">
						<th> CATEGORY</th>
						<th> OCCUPANCY 	</th>
						<th style="text-align: center;"> EP   </th>
						<th style="text-align: center;"> CP   </th>
						<th style="text-align: center;"> MAP  </th>
						<th style="text-align: center;"> AP   </th>
					</tr>';
		
		$tariffs 	= 	$this->hotels_model->getHotelTariffs( $hotelId, $appcode );

		foreach ($tariffs as $tariff ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td>'.$tariff['room_category'].'</td>
							<td>'.$tariff['occupancy'].	'</td>
							<td style="text-align: center;">'.$tariff['ep'].'</td>
							<td style="text-align: center;">'.$tariff['cp'].'</td>
							<td style="text-align: center;">'.$tariff['map'].'</td>
							<td style="text-align: center;">'.$tariff['ap'].'</td>
						</tr>
					';
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');



		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 10, 'EXTRA BEDDING', 0, 1 );
		
		$pdf->SetTextColor(0,0,0);

		$tbl_header = '<table cellpadding="10" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#333; font-size:12px; font-weight: bold">
						<th style="width: 25%;"> OCCUPANCY 	</th>
						<th style="width: 25%; text-align: center;"> CHARGES   </th>
					</tr>';
		
		$extrabedding 	= 	$this->hotels_model->getHotelExtrabedding( $hotelId, $appcode );

		$tbl .= '		<tr style="background-color:#f7f7f7; font-size:12px;">
							<td> SINGLE</td>
							<td style="text-align: center;">'.$extrabedding['charges'].'</td>
						</tr>
					';
		
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';

        ob_end_clean();

        $pdf->Output('Hotels_List.pdf', 'I');

    }


	public function hotelVouchers( $param1 = '', $param2 = '' )
	{
		$data['appcode'] 		=	$appcode 		=	$this->session->userdata['appcode'];
		
		if( $param1 = 'delete' && is_numeric( $param2 ) )
		{
			$voucherData['status'] 	=	'Deleted';
			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result = $this->db->update('hotel_vouchers', $voucherData); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Voucher Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url() . 'hotels/hotelVouchers', 'refresh');
		}

		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Hotel Vouchers";
		$data['fileName'] 		= 	"hotelVouchers.php";
		$data['vouchers'] 		= 	$this->hotels_model->getHotelVouchers( $appcode );
		$this->load->view( 'index', $data );
	}


	public function addHotelVoucher()
	{
		$appcode 			=	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Hotel Voucher";
		$data['fileName'] 	= 	"addHotelVoucher.php";
		$data['hotels'] 	= 	$this->hotels_model->getHotels( $appcode );
		$data['invoiceNos'] = 	$this->sales_model->getInvoiceNos( $appcode );
		$data['services'] 	= 	$this->services_model->getServiceTypes( $appcode );
		$this->load->view( 'index', $data );
	}


	public function insertHotelVoucher()
	{
		$user_id					=	$this->session->userdata['id'];
		$appcode					=	$this->session->userdata['appcode'];
		$voucher_date_received  	=   trim($_POST['voucher_date']);
		$voucher_date_object 		= 	new DateTime($voucher_date_received);
		$voucher_date				= 	$voucher_date_object->format('Y-m-d');
		$invoiceData 				=	$this->sales_model->getInvoiceUserData( $appcode, $_POST['invoice_no'] );
		$date 						=   explode('-', $_POST['date']);
		$checkin_date_received  	=   $date[0];
		$checkin_date_object 		= 	new DateTime($checkin_date_received);
		$checkin					= 	$checkin_date_object->format('Y-m-d');
		$checkout_date_received 	=   $date[1];
		$checkout_date_object 		= 	new DateTime($checkout_date_received);
		$checkout					= 	$checkout_date_object->format('Y-m-d');
				
		$room_type     		=   $_POST['room_type'];
		$meal_plan    		=   $_POST['meal_plan'];
		$roomNos      		=   $_POST['roomNos'];
		$extrabed    		=   $_POST['extrabed'];
		$child_without_bed 	= 	$_POST['child_without_bed'];

		$voucherData['appcode']   		 	 =   $appcode;
		$voucherData['voucher_date']   		 =   $voucher_date;
		$voucherData['voucher_no']  	 	 =   $this->hotels_model->getHotelVoucherNo( $appcode );
		$voucherData['invoice_no']  	 	 =   $_POST['invoice_no'];
		$voucherData['hotel_id']    		 =   $_POST['hotel_id'];
		$voucherData['customer_name']     	 =   $invoiceData['invoice_to'];
		$voucherData['customer_email']     	 =   $invoiceData['email'];
		$voucherData['customer_mobile']   	 =   $invoiceData['phone'];
		$voucherData['customer_city'] 	  	 =   $invoiceData['city'];
		$voucherData['customer_state'] 	  	 =   $invoiceData['state'];
		$voucherData['customer_country'] 	 =   $invoiceData['country'];
		$voucherData['customer_pin'] 	 	 =   $invoiceData['pin_code'];
		$voucherData['checkin']       		 =   $checkin;
		$voucherData['checkout']      		 =   $checkout;
		$voucherData['pax']      		 	 =   $_POST['pax'];
		$voucherData['remarks']    	 		 =   trim($_POST['remarks']);

		if( $this->hotels_model->hotelVoucherNoAvailable( $voucherData['voucher_no'], $appcode ) )
		{ 
			$result   		=   $this->db->insert('hotel_vouchers', $voucherData);
			$lastinsert_id 	= 	$this->db->insert_id();
			$voucherServices['voucher_id']  	=  	$lastinsert_id;
			$voucherServices['appcode'] 	 	=	$appcode;

			$days 				= 	(strtotime($checkout) - strtotime($checkin)) / (60 * 60 * 24);
			$extrabedding 		= 	$this->hotels_model->getHotelExtrabedding( $_POST['hotel_id'], $appcode );
			$extrabedCharges 	= 	$extrabedding['charges'];
			
			foreach ( $room_type as $key=>$value )
			{
				$tariff		=  	$this->hotels_model->getTariffDetails( $value, $appcode );
				$voucherServices['room_category'] 		= 	$tariff['room_category'];
				$voucherServices['occupancy'] 			= 	$tariff['occupancy'];
				$voucherServices['meal_plan'] 			= 	$meal_plan[$key];
				$voucherServices['no_of_rooms']			= 	$roomNos[$key];
				$voucherServices['extrabed']			= 	$extrabed[$key];
				$voucherServices['child_without_bed']  	= 	$child_without_bed[$key];
				$voucherServices['amount']				= 	(($tariff[$meal_plan[$key]] + ( $extrabed[$key] * $extrabedCharges) ) * $voucherServices['no_of_rooms']) * $days;
				$this->db->insert('hotel_voucher_services', $voucherServices);
			}
		}
		else
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Voucher No Already Exists!</div>'));
			redirect(base_url() . 'hotels/addHotelVoucher', 'refresh');
		}

		if( $result )
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Voucher Added Successfully!</div>'));
			redirect(base_url() . 'hotels/hotelVouchers', 'refresh');
		}
		else
		{
			redirect(base_url(), 'refresh');
		}
	}


	public function editHotelVoucher( $param1 = '' )
	{
		$appcode 					=	$this->session->userdata['appcode'];
		$data['pathType'] 			= 	$this->pathType;
		$data['pageName'] 			= 	"Hotel Voucher";
		$data['fileName'] 			= 	"editHotelVoucher.php";
		$data['hotels'] 			= 	$this->hotels_model->getHotels( $appcode );
		$data['invoiceNos'] 		= 	$this->sales_model->getInvoiceNos( $appcode );
		$data['voucher']			= 	$this->hotels_model->getHotelVoucherDetails( $param1, $appcode );
		$data['voucherServices'] 	= 	$this->hotels_model->getHotelVoucherServices( $param1, $appcode );
		$data['extrabedding'] 		= 	$this->hotels_model->getHotelExtrabedding( $data['voucher']['hotel_id'], $appcode );
		$data['tariffs'] 			= 	$this->hotels_model->getHotelTariffs( $data['voucher']['hotel_id'], $appcode );
		$this->load->view( 'index', $data );
	}


	public function updateHotelVoucher()
	{
		$user_id					=	$this->session->userdata['id'];
		$appcode					=	$this->session->userdata['appcode'];
		$voucher_id					=	trim($_POST['voucher_id']);
		$voucher_date_received  	=   trim($_POST['voucher_date']);
		$voucher_date_object 		= 	new DateTime($voucher_date_received);
		$voucher_date				= 	$voucher_date_object->format('Y-m-d');
		$invoiceData 				=	$this->sales_model->getInvoiceUserData( $appcode, $_POST['invoice_no'] );
		$date 						=   explode('-', $_POST['date']);
		$checkin_date_received  	=   $date[0];
		$checkin_date_object 		= 	new DateTime($checkin_date_received);
		$checkin					= 	$checkin_date_object->format('Y-m-d');
		$checkout_date_received 	=   $date[1];
		$checkout_date_object 		= 	new DateTime($checkout_date_received);
		$checkout					= 	$checkout_date_object->format('Y-m-d');
				
		$room_type     		=   $_POST['room_type'];
		$meal_plan    		=   $_POST['meal_plan'];
		$roomNos      		=   $_POST['roomNos'];
		$extrabed    		=   $_POST['extrabed'];
		$child_without_bed 	= 	$_POST['child_without_bed'];


		$voucherData['voucher_date']   		 =   $voucher_date;
		$voucherData['voucher_no']  	 	 =   $this->hotels_model->getHotelVoucherNo( $appcode );
		$voucherData['invoice_no']  	 	 =   $_POST['invoice_no'];
		$voucherData['hotel_id']    		 =   $_POST['hotel_id'];
		$voucherData['customer_name']     	 =   $invoiceData['invoice_to'];
		$voucherData['customer_email']     	 =   $invoiceData['email'];
		$voucherData['customer_mobile']   	 =   $invoiceData['phone'];
		$voucherData['customer_city'] 	  	 =   $invoiceData['city'];
		$voucherData['customer_state'] 	  	 =   $invoiceData['state'];
		$voucherData['customer_country'] 	 =   $invoiceData['country'];
		$voucherData['customer_pin'] 	 	 =   $invoiceData['pin_code'];
		$voucherData['checkin']       		 =   $checkin;
		$voucherData['checkout']      		 =   $checkout;
		$voucherData['pax']      		 	 =   $_POST['pax'];
		$voucherData['remarks']    	 		 =   trim($_POST['remarks']);

		$this->db->where( array('id' => $voucher_id, 'appcode' => $appcode ) );
		$this->db->update('hotel_vouchers', $voucherData);

		$this->db->where( array( 'voucher_id' => $voucher_id, 'appcode' => $appcode ) );
		$this->db->delete('hotel_voucher_services');

		$voucherServices['voucher_id']  	=  	$voucher_id;
		$voucherServices['appcode'] 	 	=	$appcode;

		$days 				= 	(strtotime($checkout) - strtotime($checkin)) / (60 * 60 * 24);
		$extrabedding 		= 	$this->hotels_model->getHotelExtrabedding( $_POST['hotel_id'], $appcode );
		$extrabedCharges 	= 	$extrabedding['charges'];
		
		foreach ( $room_type as $key=>$value )
		{
			$tariff		=  	$this->hotels_model->getTariffDetails( $value, $appcode );
			$voucherServices['room_category'] 		= 	$tariff['room_category'];
			$voucherServices['occupancy'] 			= 	$tariff['occupancy'];
			$voucherServices['meal_plan'] 			= 	$meal_plan[$key];
			$voucherServices['no_of_rooms']			= 	$roomNos[$key];
			$voucherServices['extrabed']			= 	$extrabed[$key];
			$voucherServices['child_without_bed']  	= 	$child_without_bed[$key];
			$voucherServices['amount']				= 	(($tariff[$meal_plan[$key]] + ( $extrabed[$key] * $extrabedCharges) ) * $voucherServices['no_of_rooms']) * $days;
			$result 	= 	$this->db->insert('hotel_voucher_services', $voucherServices);
		}
		

		if( $result )
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Voucher Updated Successfully!</div>'));
			redirect(base_url() . 'hotels/hotelVouchers', 'refresh');
		}
		else
		{
			redirect(base_url(), 'refresh');
		}
	}


	public function hotelVoucher( $voucherId )
	{
		if( !is_numeric( $voucherId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
		$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Hotel Voucher');
		$pdf->SetSubject('Hotel Voucher');
		$pdf->SetKeywords('OCTA, Travel, Hotel Voucher');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate Voucher!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$voucher 		= 	$this->hotels_model->getHotelVoucherDetails( $voucherId, $appcode );
		if( !$voucher )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 14);
		$pdf->Cell(100, 5, 'HOTEL VOUCHER', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'VOUCHER # :: '.$voucher['voucher_no'], 0, 0 );
		$pdf->Cell(100, 5, 'CHECKIN   :: '.date("jS F, Y", strtotime($voucher['checkin'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON   :: '.date("jS F, Y", strtotime($voucher['voucher_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'CHECKOUT:: '.date("jS F, Y", strtotime($voucher['checkout'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'CUSTOMER DETAILS', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $voucher['customer_name'], 0, 1 );
		$pdf->Cell(100, 5, $voucher['customer_city'].', '.$voucher['customer_state'].', '.$voucher['customer_country'], 0, 1 );

		$pdf->Cell(100, 5, 'Pin: '.$voucher['customer_pin'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $voucher['customer_mobile'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$voucher['customer_email'], 0, 1 );
		
		$pdf->Ln(5);

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 8, 'Please provide these services to the above captioned customer & bill us for the same.', 0, 1 );

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #      		</th>
						<th style="width:20%;"> Room Category 	</th>
						<th style="width:15%;"> Occupancy 	</th>
						<th style="width:15%; text-align: center;"> Meal Plan 	</th>
						<th style="width:15%; text-align: center;"> Rooms (#) </th>
						<th style="width:15%; text-align: center;"> Extrabed </th>
						<th style="width:14%; text-align: center;"> Child WOB </th>
					</tr>';
		
		$services_result 	= 	$this->hotels_model->getHotelVoucherServices( $voucher['id'], $appcode );

		$sno 	= 	1;
		
		$hotel 	 = 	$this->hotels_model->getHotelDetails( $voucher['hotel_id'], $appcode );

		$days = (strtotime($voucher['checkout']) - strtotime($voucher['checkin'])) / (60 * 60 * 24);
		$total = $hotel['cost'] * $days;

		foreach ($services_result as $row ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .				'</td>
							<td>'.$row['room_category'].	'</td>
							<td>'.$row['occupancy'].	'</td>
							<td style="text-align: center;">'.$row['meal_plan'].	'</td>
							<td style="text-align: center;">'.$row['no_of_rooms'].	'</td>
							<td style="text-align: center;">'.$row['extrabed'].	'</td>
							<td style="text-align: center;">'.$row['child_without_bed'].'</td>
						</tr>
					';
			$total += 	$row['amount'];	
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';
		
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(100, 10, 'FOR ', 0, 1 );

		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 5, $hotel['hotel_name'], 0, 1 );
		$pdf->Cell(100, 5, $hotel['address'], 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 11);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', '', 11);
		$remarks = '<strong>Additional Information / Remarks:</strong> '.$voucher['remarks'];

		$pdf->writeHTML($remarks, true, false, false, false, '');
		
		ob_end_clean();

		$pdf->Output('Hotel_Voucher - ' .$voucher['id']. '.pdf', 'I');
	}


	function hotelsPdf()
    {

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Hotels List');
        $pdf->SetSubject('OCTA Travel Hotels List');
        $pdf->SetKeywords('OCTA Travel, Hotels List');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Hotels List');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');
        // add a page
        $pdf->AddPage('L');
        $HeaderText = 'LIST OF HOTELS';
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);
        $pdf->Ln(3);
        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold; ">
                        <th width="5%;" style="text-align: center;"> S NO </th>
                        <th> HOTEL NAME  </th>
                        <th> ADDRESS  </th>
                        <th> PHONE NO    </th>
                        <th> FAX    </th>
                        <th> EMAIL       </th>
                        <th> WEBSITE  </th>
                        <th> DESCRIPTON  </th>
                    </tr>';
        $count      =   1; 
        $hotels     =   $this->hotels_model->getHotels( $appcode );
        foreach ($hotels as $hotel ) 
        {
            $tbl .= '
                <tr style="font-size: 13px;">
                    <td  style="text-align: center;"> '.$count++ .' </td>
                    
                    <td>'.$hotel['hotel_name'].'</td>

                    <td>'.$hotel['address'].'</td>

                    <td>'.$hotel['phone'].'</td>

                    <td>'.$hotel['fax'].'</td>
                    
                    <td>'.$hotel['email'].'   </td>
                    
                    <td>'.$hotel['website'].'</td>

                    <td>'.$hotel['description'].'</td>
                    
                </tr>
            ';
            $tbl_footer = '
                            </table>
                          ';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Hotels_List.pdf', 'D');

    }


    public function ajaxGetRooms()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$rooms 		=  	$this->hotels_model->getHotelTariffs( $_POST['hotel_id'], $appcode );
			
			echo json_encode( $rooms );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}

	
	public function ajaxGetExtrabedding()
	{
		if( $_POST )
		{ 
			$appcode    	=   $this->session->userdata['appcode'];  
			$extrabedding 	=  	$this->hotels_model->getHotelExtrabedding( $_POST['hotel_id'], $appcode );
			echo json_encode( $extrabedding );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	public function ajaxGetTariffDetails()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$tariff		=  	$this->hotels_model->getTariffDetails( $_POST['tariff_id'], $appcode );
			echo json_encode( $tariff );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


}

/* End of file hotels.php */
/* Location: ./application/controllers/hotels.php */
