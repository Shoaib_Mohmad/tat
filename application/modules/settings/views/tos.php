<style type="text/css">
   .myfont{
      font-weight: bold;
      color: #016FA7;
   }
</style>
<?php echo $this->session->flashdata('notification');?>
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN SAMPLE FORM PORTLET-->   
      <div class="widget">
         <div class="widget-title">
            <h4><i class="icon-reorder"></i>Terms & Conditions</h4>
            <span class="tools">
           
            </span>                    
         </div>

         <div class="widget-body form">
            <!-- BEGIN FORM-->
            <form action="<?php echo base_url().'settings/updatetos';?>" class="form-horizontal" method="POST">
               	<?php
               		$sno = 1;
               		foreach($tos as $ts){
               	?>
		               <div class="control-group">
		                  	<div class="controls">
		                     	<textarea class="span10" rows="2" name="<?php echo $ts['id'];?>" id="inputRemarks" ><?php echo $ts['tos_text'];?></textarea>
		                     	<a onclick="return confirm('Are you sure you want to delete the Term of Service?');" href="<?php echo base_url().'index.php/user/deletetos/'.$ts['id'];?>"><img style="cursor:pointer;" src="<?php echo base_url().'assets/img/icons/delete.gif';?>" title="Delete"></a>
		                  	</div>
		               </div>
               	<?php
               			$sno++;
               		}
               	?>
               <div class="control-group">
                  	<div class="controls">
                     	<textarea class="span10" rows="2" name="newTos" id="inputRemarks" placeholder="Add T&C"></textarea>
                  	</div>
               </div>

               <div class="form-actions">
                  <button type="submit" style="cursor:pointer" class="btn btn-primary disabled">Update/Save</button>
               </div>
            </form>
            <!-- END FORM-->           
         </div>
      </div>
      <!-- END SAMPLE FORM PORTLET-->
   </div>
</div>
