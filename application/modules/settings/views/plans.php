<?php echo $this->session->flashdata('notification');?>

<div class="row-fluid">
   <div class="span6">
      <!-- BEGIN PORTLET-->	
      <div class="widget">
         <div class="widget-title">
            <h4>MONTHLY</h4>
         </div>
         <div class="widget-body form">
            <br><center>
               <button class="btn btn-large btn-primary" type="button">MONTHLY SUBSCRIPTION</button><br><br>
            <a style="text-decoration:none; font-weight:bold; font-size:14px;" href="javascript: void(0);"><i class="fa fa-fw fa-rupee"></i>1000 PER MONTH</a>
            <h5>SELECT NO OF MONTHS</h5>
            <form action="<?php echo base_url().'upgrade/upgradeUser';?>" method="post">
               <select name="numberOfmonths" class="span8">
                  <option value="1">1 Month</option>
                  <option value="3">3 Months</option>\
                  <option value="6">6 Months</option>
                  <option value="9">9 Months</option>
                  <option value="12">12 Months</option>                                    
               </select><br><br><br>
               <input type="hidden" name="plan" value="MONTHLY SUBSCRIPTION">
               <input type="hidden" name="amount" value="1000">
               <button class="btn btn-success" type="submit">BUY NOW!</button>
            </form></center><br>
         </div>
      </div>
      <!-- END PORTLET-->
   </div>

   <div class="span6">
      <!-- BEGIN PORTLET-->   
      <div class="widget">
         <div class="widget-title">
            <h4>YEARLY</h4>
         </div>
         <div class="widget-body form">
            <br><center>
               <button class="btn btn-large btn-primary" type="button">YEARLY SUBSCRIPTION</button>
            <br><br> 
            <a style="text-decoration:none; font-weight:bold; font-size:14px;" href="javascript: void(0);"><i class="fa fa-fw fa-rupee"></i>10000 PER YEAR</a>
            <h5>SELECT NO OF YEARS</h5>
            <form action="<?php echo base_url().'upgrade/upgradeUser';?>" method="post">
               <select name="numberOfYears" class="span8">
                  <option value="1">1 Year</option>
                  <option value="2">2 Years</option>
                  <option value="3">3 Years</option>
                  <option value="4">4 Years</option>
                  <option value="5">5 Years</option>        
               </select><br><br><br>
               <input type="hidden" name="plan" value="YEARLY SUBSCRIPTION">
               <input type="hidden" name="amount" value="10000">
               <button class="btn btn-success" type="submit">BUY NOW!</button>
            </form></center> <br>
         </div>
      </div>
      <!-- END PORTLET-->
   </div>
</div>

