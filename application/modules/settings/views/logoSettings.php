<?php echo $this->session->flashdata('notification');?>
<div id="page">
       <div class="row-fluid">
          <div class="span6">
             <!-- BEGIN SAMPLE FORM PORTLET-->  
             <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i>Set / Update Logo</h4>
                              
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                   <form onsubmit="return checkExtLogo(this);" action="<?php echo base_url();?>settings/logo/update" method="POST" enctype="multipart/form-data" class="form-horizontal">
                      

                        <?php $logo = $this->session->userdata("id");
                              $logo = "uploads/logo/".$logo.'.png';
                        ?>
                        <label class="control"><img src="<?php echo base_url().$logo;?>" alt="logo" height="30" width="250"/></label>
                      <div class="control-group">

                         <label class="control-label" >Select Logo</label>
                         <div class="controls">
                            <input type="file" id="file" class="input-small" name="file_scan"/> 
                         </div>
                      </div>

                      <div class="control-group">

                         <label class="tools icon-reorder" ><b>Note:</b></label>
                         <div>
                            <p>1]&nbsp;  Uploaded logo should have extension jpeg, jpg, png, gif.<br> 
                            2]&nbsp; For best view uploaded logo should be 100 × 300 (H × W).</p>
                         </div>
                      </div>
                      
                     <div class="control-group">
                      <div class="form-actions">
                         <button type="submit" class="btn btn-primary">Update Logo</button>
                      </div>
                      </div>
                   </form>
                             <!-- END FORM-->         
                </div>
             </div>
             <!-- END SAMPLE FORM PORTLET-->
          </div>
       </div>
      
      
    </div>              
     
