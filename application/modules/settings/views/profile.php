<script src="<?php echo base_url().'assets/js/countries.js';?>" type="text/javascript"></script>
<style type="text/css">
   .myfont{
      font-weight: bold;
      color: #016FA7;
   }
</style>
<?php echo $this->session->flashdata('notification');?>
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN SAMPLE FORM PORTLET-->   
      <div class="widget">
         <div class="widget-title">
            <h4><i class="icon-reorder"></i>Profile</h4>
         </div>
         <div class="widget-body form">
            <!-- BEGIN FORM-->
            <form action="<?php echo base_url().'settings/updateProfile';?>" class="form-horizontal" method="POST">
               <div id="warning"> </div>
               
               <div class="control-group">
                  <label class="control-label" for="input1">Your Name</label>
                  <div class="controls">
                     <input type="text" name="user_name" required class="span6 myfont" value="<?php if(isset($profile['user_name'])) echo $profile['user_name'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Company Name</label>
                  <div class="controls">
                     <input type="text" name="company_name" required class="span6 myfont" value="<?php if(isset($profile['company_name'])) echo $profile['company_name'];?>" />
                  </div>
               </div>
               
               <div class="control-group">
                  <label class="control-label" for="input1">Street</label>
                  <div class="controls">
                     <input type="text" name="street" required class="span6 myfont" value="<?php if(isset($profile['street'])) echo $profile['street'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">City</label>
                  <div class="controls">
                     <input type="text" name="city" required class="span6 myfont" value="<?php if(isset($profile['city'])) echo $profile['city'];?>" />
                  </div>
               </div>

                <div class="control-group">
					<label class="control-label" for="input1">Country</label>
					<div class="controls">
						<select required style="font-weight: bold;" class="span6" id="country" name="country">
									
						</select>
					</div>
				</div>
               
                <div class="control-group">
					<label class="control-label" for="input1">State</label>
					<div class="controls">
						<select required style="font-weight: bold;" class="span6" id="state" name="state">
									
						</select>
					</div>
               </div>
               
               <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>
               
               <script language="javascript">
					populateCountries("country", "state");
					populateCountries("country2");
				</script>
				
				<?php if(isset($profile['country']) && isset($profile['state'])):
				?>
					<script>
						$(document).ready(function() {
							//alert('Workinf')
							$("#country [value='<?php echo $profile['country'];?>']").attr("selected","selected");
							$("#country").change();
							$("#state [value='<?php echo $profile['state'];?>']").attr("selected","selected");
						});
						countrywe();
					</script>  
				<?php
					endif;
				?>
				
				<div class="control-group">
                  <label class="control-label" for="input1">Pin</label>
                  <div class="controls">
                     <input type="number" name="pin" required class="span6 myfont" value="<?php if(isset($profile['pin'])) echo $profile['pin'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Phone</label>
                  <div class="controls">
                     <input type="text" name="phone" required class="span6 myfont" pattern="\d{10}" maxlength="10" value="<?php if(isset($profile['phone'])) echo $profile['phone'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Fax</label>
                  <div class="controls">
                     <input type="text" name="fax" required class="span6 myfont" maxlength="12" value="<?php if(isset($profile['fax'])) echo $profile['fax'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Email</label>
                  <div class="controls">
                     <input type="email" name="company_email" required class="span6 myfont" value="<?php if(isset($profile['company_email'])) echo $profile['company_email'];?>" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Website</label>
                  <div class="controls">
                     <input type="text" name="website" required class="span6 myfont" value="<?php if(isset($profile['website'])) echo $profile['website'];?>" />
                  </div>
               </div>

               <div class="form-actions">
                  <button type="submit" style="cursor:pointer" class="btn btn-primary disabled">Update Profile</button>
               </div>
            </form>
            <!-- END FORM-->           
         </div>
      </div>
      <!-- END SAMPLE FORM PORTLET-->
   </div>
</div>
