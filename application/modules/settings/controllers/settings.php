<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting(0);

class Settings extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'settings';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('settings');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->helper(array('form', 'url'));
		$this->load->helper("file");

		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function profile()
	{
		$userId 			= 	$this->session->userdata['id'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Company Profile";
		$data['fileName'] 	= 	"profile.php";
		$data['profile'] 	= 	$this->user_model->getUserProfile( $userId );
		$this->load->view('index', $data);
	}



	public function updateProfile()
	{
		if($_POST)
		{
			$id 		= 	$this->session->userdata['id'];
			$appcode 	= 	$this->session->userdata['appcode'];
			$query 		=	$this->db->query("SELECT * FROM profile WHERE user_id = $id");

			if($query -> num_rows())
			{
				$query 	= 	$this->db->query("UPDATE profile SET user_name = '".mysql_real_escape_string( $_POST['user_name'] )."', company_name = '".mysql_real_escape_string( $_POST['company_name'] )."', street = '".mysql_real_escape_string( $_POST['street'] )."', city = '".mysql_real_escape_string( $_POST['city'] )."', state = '".mysql_real_escape_string( $_POST['state'] )."', country = '".mysql_real_escape_string( $_POST['country'] )."', pin = '".mysql_real_escape_string( $_POST['pin'] )."', phone = '".mysql_real_escape_string( $_POST['phone'] )."', fax = '".mysql_real_escape_string( $_POST['fax'] )."', company_email = '".mysql_real_escape_string( $_POST['company_email'] )."', website = '".mysql_real_escape_string( $_POST['website'] )."' WHERE user_id = '$id'");
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Profile Updated Successfully!</div>');
				redirect(base_url().'settings/profile', 'refresh');
			}
			
			else
			{
				$query 	= 	$this->db->query("INSERT INTO profile (user_id, appcode, user_name, company_name, street, city, state, country, pin, phone, fax, company_email, website) VALUES ('$id', '$appcode', '".mysql_real_escape_string( $_POST['user_name'] )."', '".mysql_real_escape_string( $_POST['company_name'] )."', '".mysql_real_escape_string( $_POST['street'] )."', '".mysql_real_escape_string( $_POST['city'] )."', '".mysql_real_escape_string( $_POST['state'] )."', '".mysql_real_escape_string( $_POST['country'] )."', '".mysql_real_escape_string( $_POST['pin'] )."', '".mysql_real_escape_string( $_POST['phone'] )."', '".mysql_real_escape_string( $_POST['fax'] )."', '".mysql_real_escape_string( $_POST['company_email'] )."', '".mysql_real_escape_string( $_POST['website'] )."')");
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Profile Updated Successfully!</div>');
				redirect(base_url().'settings/profile', 'refresh');
			}
		}

		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}
	
	
	public function tos()
	{
		$appcode			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Terms & Conditions";
		$data['fileName'] 	= 	"tos.php";
		$data['tos'] 		= 	$this->user_model->getTOS( $appcode );
		$this->load->view('index', $data);
	}


	
	public function updatetos()
	{
		if( $_POST )
		{
			$appcode 	= 	$this->session->userdata['appcode'];
			foreach($_POST as $key => $ts)
			{
				if($key == 'newTos')
				{
					break;
				}
				$query = sprintf("UPDATE tos SET tos_text = '%s' WHERE id = $key",	mysql_real_escape_string($ts));
				$this->db->query($query);
			}
			if(!empty($_POST['newTos']))
			{
				$query = sprintf("INSERT INTO tos (appcode, tos_text) VALUES ('$appcode', '%s') ", mysql_real_escape_string($_POST['newTos']));
				$this->db->query($query);
			}
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Terms & Conditions Saved Successfully!</div>');
			redirect(base_url().'settings/tos', 'refresh');	
		}
		else
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button "close" data-dismiss="alert">×</button>You Must Login First!</div>');
			redirect(base_url().'login', 'refresh');
		}
	}


	
	public function deletetos( $id = "" )
	{
		if( !is_numeric($id) ){
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("UPDATE tos SET status = 0 WHERE id = $id AND appcode = '$appcode'");
		if(!$query){
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.!</div>');
		}
		else{
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Terms & Conditions Deteled Successfully!
				</div>');
		}
		redirect(base_url().'settings/tos', 'refresh');	
	}


	
	public function plans()
	{
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"User Plans";
		$data['fileName'] 	= 	"plans.php";
		$this->load->view('index', $data);	
	}


	function logo( $param1 = '' )
	{
		
        if( $param1 == 'update')
        {
        	$UID = $this->session->userdata['id'];
        	move_uploaded_file($_FILES['file_scan']['tmp_name'],'./uploads/logo/'.$UID.'.png');	
        	$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Logo Updated Successfully!</div>'));
        	redirect(base_url().'settings/logo', 'refresh');	
        }

		$id 				= 	$this->session->userdata['id'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Logo Settings";
		$data['fileName'] 	= 	"logoSettings.php";
		$this->load->view('index', $data);
		
	}
	
}

/* End of file settings.php */
/* Location: ./application/controllers/settings.php */
