<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}


    function invoiceCodeAvailable( $invoice_code, $appcode)
    { 
        $query = $this->db->get_where('services_invoices', array(
            'invoice_code'  => $invoice_code,
            'appcode'       => $appcode           
        ));

        if ($query->num_rows() == 1 ) 
        {
            return false;        
        } 
        else 
        {
            return true;
        }
    }

    
    function getSalesInvoices( $appcode )
    {
        $sql    = $this->db->order_by('id', 'desc')->get_where('services_invoices', array('appcode' => $appcode, 'status' => 'Active'));
        $result = $sql->result_array();
        return $result ;
    }
    
    
    function GetInvoiceById( $id )
    {
        $sql    = $this->db->get_where('services_invoices', array('id' => $id));
        $result = $sql->row_array();
        return $result ;
    }
	   
    
    function getInvoiceNos( $appcode )
    {
        $invoiceNos = $this->db->query("SELECT invoice_code FROM services_invoices WHERE appcode = '$appcode' AND status = 'Active' ORDER BY invoice_code DESC") -> result_array();
        return $invoiceNos;
    }


    function getInvoiceUserData( $appcode, $invoice_no )
    {
        $user   =   $this->db->query("SELECT * FROM services_invoices WHERE appcode = '$appcode' AND invoice_code = '$invoice_no'") -> row_array();
        return $user; 
    }

    
    function getInvoiceHotelServices( $invoice_id, $appcode )
    {
        $hotelServices   =   $this->db->query("SELECT * FROM invoice_hotel_services WHERE invoice_id = $invoice_id AND appcode = '$appcode'")->result_array();
        return $hotelServices;
    }


    function getInvoiceTransportationServices( $invoice_id, $appcode )
    {
        $transportationServices   =   $this->db->query("SELECT * FROM invoice_transportation_services WHERE invoice_id = $invoice_id AND appcode = '$appcode'")->result_array();
        return $transportationServices;
    }


    function getInvoiceOtherServices( $invoice_id, $appcode )
    {
        $otherServices   =   $this->db->query("SELECT * FROM services WHERE invoice_id = $invoice_id AND appcode = '$appcode'")->result_array();
        return $otherServices;
    }


    function getTourInvoice($id, $appcode)
    {
        $tourinvoice    = $this->db->query("SELECT * FROM invoice_tours WHERE invoice_id = $id AND appcode = '$appcode'  ")->row_array();
        return $tourinvoice;
    }


}