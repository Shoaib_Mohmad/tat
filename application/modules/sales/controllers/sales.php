<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'sales';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('sales');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('sales/sales_model');
		$this->load->model('customers/customers_model');
		$this->load->model('hotels/hotels_model');
		$this->load->model('transporters/transporters_model');
		$this->load->model('tours/tours_model');
		$this->load->model('services/services_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function invoices($param1 ='', $param2 = '')
	{
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName']		=	"Sales Invoices";
		$data['fileName']		=	"invoices.php";
		$user_id				=	$this->session->userdata('id');
		$appcode				=	$this->session->userdata('appcode');
		$data['invoicelist']	=	$this->sales_model->getSalesInvoices( $appcode );
		$this->load->view('index', $data);

		if( $param1 == 'delete')
		{
			if(!$this->registrationStatus)
			{
				redirect(base_url().'sales/invoices', 'refresh');
			}
			if( $param2 != '' )
			{
				$invoiceData['status'] = 'Deleted';
				$this->db->where( array( 'id' => $param2, 'appcode' => $appcode ) );
				$result 	=   $this->db->update( 'services_invoices', $invoiceData );
				if( $result )
					$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Sales Invoice Deleted Successfully!</div>');
				else
					$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				redirect(base_url() . 'sales/invoices', 'refresh');

			}
			else
			{
				redirect(base_url(), 'refresh');
			}
		}
	}

	
	public function addInvoice()
	{
		if(!$this->registrationStatus)
		{
			redirect(base_url().'sales/invoices', 'refresh');
		}
		$userId					=	$this->session->userdata['id'];
		$appcode				=	$this->session->userdata['appcode'];
		$data 					= 	array();
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add Sales Invoice";
		$data['fileName'] 		= 	"addInvoice.php";
		$data['service_name'] 	= 	$this->services_model->getServiceTypes( $appcode );
		$data['customers'] 		= 	$this->customers_model->getCustomers( $appcode );
		$data['hotels'] 		= 	$this->hotels_model->getHotels( $appcode );
		$data['transporters'] 	= 	$this->transporters_model->getTransporters( $appcode );
		$data['tours']			=   $this->tours_model->getTours($appcode);

		$this->load->view('index', $data);
	}


	public function salesInvoice( $invoiceId )
	{
		if( !is_numeric( $invoiceId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
		$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Sales Invoice');
		$pdf->SetSubject('Sales Invoice');
		$pdf->SetKeywords('OCTA, Travel, Invoice');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate an Invoice!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$query 	= 	$this->db->query("SELECT * FROM services_invoices WHERE id = $invoiceId AND appcode = '$appcode'");
		$invoice_result 	= $query -> row_array();
		if( !$invoice_result )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 15);
		$pdf->Cell(100, 5, 'SALES INVOICE', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'INVOICE #    :: '.$invoice_result['invoice_code'], 0, 0 );
		$pdf->Cell(100, 5, 'ARRIVAL        :: '.date("jS F, Y", strtotime($invoice_result['arrival'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON  :: '.date("jS F, Y", strtotime($invoice_result['invoice_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'DEPARTURE :: '.date("jS F, Y", strtotime($invoice_result['departure'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'TO', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $invoice_result['invoice_to'], 0, 1 );
		$pdf->Cell(100, 5, $invoice_result['city'].', '.$invoice_result['state'].', '.$invoice_result['country'], 0, 1 );
		$pdf->Cell(100, 5, 'Pin: '.$invoice_result['pin_code'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $invoice_result['phone'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$invoice_result['email'], 0, 1 );
		
		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);

		if( isset( $invoice_result['accomodation_total'] ) )
		{
			$pdf->Ln(5);
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'ACCOMODATION DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="3" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
							<th style="width:4%; text-align: center;"> #      		</th>
							<th style="width:18%;"> Hotel Name	</th>
							<th style="width:11%;"> Checkin	</th>
							<th style="width:11%;"> Checkout	</th>
							<th style="width:17%;"> Room Category 	</th>
							<th style="width:7%; text-align: center;"> Meal Plan  </th>
							<th style="width:9%; text-align: center;"> Rooms (#)  </th>
							<th style="width:7%; text-align: center;"> Extrabed   </th>
							<th style="width:7%; text-align: center;"> Days  (#)  </th>
							<th style="width:7%; text-align: center;"> Child WOB </th>
						</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->sales_model->getInvoiceHotelServices( $invoiceId, $appcode );

			

			if( $invoice_result['accomodation_total'] )
				$accomodation_total = $invoice_result['accomodation_total'].' INR';
			else
				$accomodation_total = $invoice_result['accomodation_total'];

			foreach ( $services as $service ) 
			{
				$hotel 	= 	$this->hotels_model->getHotelDetails( $service['hotel_id'], $appcode );
				$days 		= 	(strtotime($service['hotel_checkout']) - strtotime($service['hotel_checkin'])) / (60 * 60 * 24);
				$tbl .= '
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;">'.$sno++ .					'</td>
								<td>'.$hotel['hotel_name'].' ('.$hotel['address'].			' )</td>
								<td>'.date("jS F, Y", strtotime($service['hotel_checkin'])).'</td>
								<td>'.date("jS F, Y", strtotime($service['hotel_checkout'])).'</td>
								<td>'.$service['room_category'].' ('.$service['occupancy'].')'.'</td>
								<td style="text-align: center;">'.$service['meal_plan'].	'</td>
								<td style="text-align: center;">'.$service['no_of_rooms'].	'</td>
								<td style="text-align: center;">'.$service['extrabed'].		'</td>
								<td style="text-align: center;">'.$days.	'</td>
								<td style="text-align: center;">'.$service['child_without_bed'].	'</td>
								
							</tr>
						';
			}
			$tbl_footer .='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$accomodation_total.'</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['transportation_amount'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'TRANSPORTATION DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="4" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:20%;"> Transporter Name	</th>
						<th style="width:10%;"> Pickup	</th>
						<th style="width:18%;"> Trip 						</th>
						<th style="width:11%;"> Dated 						</th>
						<th style="width:15%;"> Vechile 					</th>
						<th style="width:10%; text-align: center;"> Days (#)</th>
						<th style="width:9%; text-align: center;"> Pax  (#)</th>
					</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->sales_model->getInvoiceTransportationServices( $invoiceId, $appcode );

			if( $invoice_result['transportation_total'] )
				$transportation_total = $invoice_result['transportation_total'].' INR';
			else
				$transportation_total = $invoice_result['transportation_total'];

			foreach ( $services as $service ) 
			{
				$transporter 	=  $this->transporters_model->getTransporterDetails( $service['transporter_id'], $appcode );
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$transporter['transporter_name'].				'</td>
							<td>'.$service['pickup'].							'</td>
							<td>'.$service['trip'].								'</td>
							<td>'.date("jS F, Y", strtotime($service['dot'])).	'</td>
							<td>'.$service['vechile'].							'</td>
							<td style="text-align: center;">'.$service['days'].	'</td>
							<td style="text-align: center;">'.$service['pax'].	'</td>
						</tr>';
			}
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$transportation_total.'</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['general_services_total'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'OTHER SERVICE DETAILS', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:45%;"> Service Name</th>
						<th style="width:15%;"> Charges</th>
					</tr>';

			$sno 	= 	1;
			
			$services 	= 	$this->sales_model->getInvoiceOtherServices( $invoiceId, $appcode );

			if( $invoice_result['general_services_total'] )
				$general_services_total = $invoice_result['general_services_total'].' INR';
			else
				$general_services_total = $invoice_result['general_services_total'];

			foreach ( $services as $service ) 
			{
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$service['service_type'].						'</td>
							<td>'.$service['total'].							'</td>
						</tr>';
			}
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$invoice_result['general_services_total'].'</strong></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		if( isset( $invoice_result['tour_id'] ) )
		{
			$pdf->SetFont('times', 'B', 12);
			$pdf->Cell(100, 10, 'TOUR PACKAGE', 0, 1 );

			$pdf->SetFont('helvetica', '', 9);
			$pdf->SetTextColor(0,0,0);
			$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
			$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:30%;">Tour Package Name</th>
						<th style="width:15%;">Each Paying</th>
						<th style="width:15%;">Plan</th>

					</tr>';

			$sno 	= 	1;
			
			$tour_invoice 	= 	$this->sales_model->getTourInvoice( $invoiceId, $appcode );

			$paying    				 = explode('@$#', $tour_invoice['each_paying']);

			if( $invoice_result['tour_total'] )
				$tour_total = $invoice_result['tour_total'].' INR';
			else
				$tour_total = $invoice_result['tour_total'];

			
				$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .  		'</td>
							<td>'.$tour_invoice['tour_name'].						'</td>
							<td>'.$paying[0].							'</td>
							<td>'.$tour_invoice['plan'].				'</td>
						</tr>';
			
			$tbl_footer ='
							<tr style="background-color:#f7f7f7; font-size:12px;">
								<td style="text-align: center;"></td>
								<td><strong>TOTAL AMOUNT</strong></td>
								<td><strong>'.$tour_total.'</strong></td>
								<td></td>
							</tr>
						</table>';

			$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		}

		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(100, 10, 'GRAND TOTAL', 0, 1 );

		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
					<th style="width:22%; text-align: center;"> Total Amount</th>
					<th style="width:22%; text-align: center;"> Amount Paid</th>
					<th style="width:22%; text-align: center;"> Balance</th>
				</tr>';

		$services 	= 	$this->sales_model->getInvoiceOtherServices( $invoiceId, $appcode );

		$balance 	= 	$invoice_result['total_amount'] - $invoice_result['advance_payment'];
		
		if( !$balance )
			$balance 	=	'NILL';
		else
			$balance    .=	' INR';

		$tbl .= '
				<tr style="background-color:#f7f7f7; font-size:12px;">
					<td style="text-align: center;"><strong>'.$invoice_result['total_amount'].' INR</strong></td>
					<td style="text-align: center;"><strong>'.$invoice_result['advance_payment'].' INR</strong></td>
					<td style="text-align: center;"><strong>'.$balance.'</strong></td>
				</tr>';
		
		$tbl_footer ='	
					</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$pdf->AddPage();

		$pdf->Image(base_url().$logo, 145, 5, 50, 20);

		$html = '<hr />
		<h2>Terms & Conditions</h2><ul>';
		
		$tos	= 	$this->db->query("SELECT * FROM tos WHERE appcode = '$appcode' AND status = 1 ORDER BY id ASC") -> result_array();
		foreach($tos as $ts)
		{
 			$html .= '<li>'.$ts['tos_text'].'</li>';
 		}
 		$html .= '</ul>';
		
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );
		
		ob_end_clean();

		$pdf->Output('Services Invoice - ' .$invoice_result['id']. '.pdf', 'I');
	}


	
	public function insertinvoice()
	{

		if( !$_POST )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$user_id					=	$this->session->userdata['id'];
		$appcode					=	$this->session->userdata['appcode'];
		$invoice_date_received  	=   trim($_POST['invoice_date']);
		$invoice_date_object 		= 	new DateTime($invoice_date_received);
		$invoice_date				= 	$invoice_date_object->format('Y-m-d');
		
		$query 						= 	$this->db->query("SELECT invoice_code FROM services_invoices WHERE appcode = '$appcode' ORDER BY id DESC LIMIT 0,1") -> row_array();
		if(isset($query['invoice_code'])) 
			$invoice_code           =   ++$query['invoice_code'];
		else
			$invoice_code           =   1;
			
		$date 						=   explode('-', $_POST['date']);
		$checkin_date_received  	=   $date[0];
		$checkin_date_object 		= 	new DateTime($checkin_date_received);
		$arrival					= 	$checkin_date_object->format('Y-m-d');
		$checkout_date_received 	=   $date[1];
		$checkout_date_object 		= 	new DateTime($checkout_date_received);
		$departure					= 	$checkout_date_object->format('Y-m-d');
				
		$invoicedata['user_id']   		=   $user_id;
		$invoicedata['appcode']   		=   $appcode;
		$invoicedata['invoice_date']   	=   $invoice_date;
		$invoicedata['invoice_code']  	=   $invoice_code;
		$invoicedata['invoice_to']    	=   trim($_POST['invoice_to']);
		$invoicedata['email']     	  	=   trim($_POST['email']);
		$invoicedata['phone']   	  	=   trim($_POST['phone']);
		$invoicedata['country'] 	  	=   trim($_POST['country']);
		$invoicedata['city'] 	  		=   trim($_POST['city']);
		$invoicedata['state'] 	  		=   trim($_POST['state']);
		$invoicedata['pin_code'] 	  	=   trim($_POST['pin_code']);
		$invoicedata['no_of_pax']       =   trim($_POST['no_of_pax']);
		$invoicedata['arrival']       	=   $arrival;
		$invoicedata['departure']      	=   $departure;
		$invoicedata['advance_payment']	=   trim($_POST['advance_payment']);
		$invoicedata['total_amount']	=   trim($_POST['grand_total']);

		if( !$this->sales_model->invoiceCodeAvailable( $invoice_code, $appcode ) )
		{ 
			$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><strong>Error!</strong> Invoice Already Exists</div>'));
			redirect(base_url() . 'sales/addinvoice', 'refresh');
		}

		$result   		=   $this->db->insert('services_invoices', $invoicedata);
		$lastinsert_id 	= 	$this->db->insert_id();

		if( isset( $_POST['accomodation_services'] ) )	
		{
			$hotelData['accomodation_amount']	=	$_POST['accomodation_amount'];	
			$hotelData['accomodation_charges']	=	$_POST['accomodation_charges'];		
			$hotelData['accomodation_total']	=	$_POST['accomodation_total'];	

			$this->db->where( array( 'id' => $lastinsert_id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'services_invoices', $hotelData );

			$hotelid 			=	$_POST['hotel_id'];
			$accomodationdate 	=	$_POST['accomodation_date'];
			$room_type     		=   $_POST['room_type'];
			$meal_plan    		=   $_POST['meal_plan'];
			$roomNos      		=   $_POST['roomNos'];
			$extrabed    		=   $_POST['extrabed'];
			$child_without_bed 	= 	$_POST['child_without_bed'];

			$invoice_hotel_services['invoice_id'] 	= 	$lastinsert_id;
			$invoice_hotel_services['appcode'] 		= 	$appcode;

			foreach ( $room_type as $key=>$value )
			{
				$extrabedding 				= 	$this->hotels_model->getHotelExtrabedding( $hotelid[$key], $appcode );
				$extrabedCharges 			= 	$extrabedding['charges'];
				$tariff						=  	$this->hotels_model->getTariffDetails( $value, $appcode );
				$date 						=   explode('-', $accomodationdate[$key]);
				$checkin_date_received  	=   $date[0];
				$checkin_date_object 		= 	new DateTime($checkin_date_received);
				$checkin					= 	$checkin_date_object->format('Y-m-d');
				$checkout_date_received 	=   $date[1];
				$checkout_date_object 		= 	new DateTime($checkout_date_received);
				$checkout					= 	$checkout_date_object->format('Y-m-d');
				$days 						= 	(strtotime($checkout) - strtotime($checkin)) / (60 * 60 * 24);

				$invoice_hotel_services['hotel_id']			=	$hotelid[$key];
				$invoice_hotel_services['hotel_checkin']	=	$checkin;
				$invoice_hotel_services['hotel_checkout']	=	$checkout;
				$invoice_hotel_services['room_category'] 	= 	$tariff['room_category'];
				$invoice_hotel_services['occupancy'] 		= 	$tariff['occupancy'];
				$invoice_hotel_services['meal_plan'] 		= 	$meal_plan[$key];
				$invoice_hotel_services['no_of_rooms']		= 	$roomNos[$key];
				$invoice_hotel_services['extrabed']			= 	$extrabed[$key];
				$invoice_hotel_services['child_without_bed']= 	$child_without_bed[$key];
				$invoice_hotel_services['amount']			= 	(($tariff[$meal_plan[$key]] + ( $extrabed[$key] * $extrabedCharges) ) * $invoice_hotel_services['no_of_rooms']) * $days;
				$this->db->insert( 'invoice_hotel_services', $invoice_hotel_services );
			}
		}	

		if( isset( $_POST['transportation_services'] ) )	
		{
			$transportData['transportation_amount']		=	$_POST['transportation_amount'];	
			$transportData['transportation_charges']	=	$_POST['transportation_charges'];		
			$transportData['transportation_total']		=	$_POST['transportation_total'];	

			$this->db->where( array( 'id' => $lastinsert_id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'services_invoices', $transportData );

			$tid 		=	$_POST['transporter_id'];
			$pickup		=	$_POST['pickup'];
			$trip     	=   $_POST['trip'];
			$vechile    =   $_POST['vechile'];
			$capacity   =   $_POST['capacity'];
			$days      	=   $_POST['days'];
			$rate    	=   $_POST['rate'];
			$pax      	=   $_POST['pax'];
			$dot      	=   $_POST['dot'];

			
			$invoice_transport_services['appcode'] 		= 	$appcode;
			$invoice_transport_services['invoice_id'] 	= 	$lastinsert_id;

			foreach ( $trip as $key=>$value )
			{
				$invoice_transport_services['transporter_id']	=	$tid[$key];
				$invoice_transport_services['pickup'] 			= 	$pickup[$key];
				$invoice_transport_services['trip'] 			= 	$value;
				$invoice_transport_services['vechile'] 			= 	$vechile[$key];
				$invoice_transport_services['capacity'] 		= 	$capacity[$key];
				$invoice_transport_services['days'] 			= 	$days[$key];
				$invoice_transport_services['pax'] 				= 	$pax[$key];
				$travel_date									= 	new DateTime($dot[$key]);
				$invoice_transport_services['dot']				= 	$travel_date->format('Y-m-d');
				$invoice_transport_services['rate']				= 	$rate[$key];
				$this->db->insert( 'invoice_transportation_services', $invoice_transport_services );
			}
		}
		if( isset( $_POST['tour_services'] ) )	
		{
			$tourData['tour_id']			=	$_POST['tour_id'];
			$tourData['tour_amount']		=	$_POST['tour_amount'];	
			$tourData['tour_charges']		=	$_POST['tour_charges'];		
			$tourData['tour_total']			=	$_POST['tour_total'];	

			$this->db->where( array( 'id' => $lastinsert_id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'services_invoices', $tourData );


			$invoice_tour['invoice_id'] 	= 	$lastinsert_id;
			$invoice_tour['appcode'] 		= 	$appcode;

				$tour_id					=	$_POST['tour_id'];
				$tour_tariff_id 			=	$_POST['tour_tarriff_id'];
				$tourname 					= 	$this->db->query("SELECT heading FROM tours WHERE appcode = '$appcode' AND id= '$tour_id'") -> row_array();
				$tourTariff 				=	$this->db->query("SELECT * FROM tour_tariff WHERE appcode = '$appcode' AND id= '$tour_tariff_id'") -> row_array();
			    
				$invoice_tour['tour_name'] 		= 	$tourname['heading'];
				$invoice_tour['each_paying'] 	= 	$tourTariff['each_paying'].'@$#'.$tour_tariff_id;
				$invoice_tour['plan'] 			= 	$_POST['plan'];
				$invoice_tour['amount'] 		= 	$_POST['tour_amount'];
				
				$this->db->insert( 'invoice_tours', $invoice_tour );
			
		}


		if( isset( $_POST['general_services'] ) )
		{
			$gsData['general_services_total'] 		= 	$_POST['services_total'];	

			$this->db->where( array( 'id' => $lastinsert_id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'services_invoices', $gsData );

			$servicesdata['invoice_id'] 	= 	$lastinsert_id;
			$servicesdata['appcode'] 		= 	$appcode;

			$service_type     	=   $_POST['service_type'];
			$actual_cost    	=   $_POST['actual_cost'];
			$charges   			=   $_POST['charges'];
			$cost      			=   $_POST['cost'];

			foreach ($service_type as $key=>$value)
			{
				$servicesdata['service_type'] 	= 	$value;
				$servicesdata['cost']			= 	$actual_cost[$key];
				$servicesdata['charges'] 		= 	$charges[$key];
				$servicesdata['total'] 			= 	$cost[$key];
				$result             			=   $this->db->insert( 'services', $servicesdata );
			}
		}

		if($result)
		{
			$noOfInvoices		=	$this->db->query("SELECT COUNT(id) FROM services_invoices WHERE user_id = $user_id")->row_array();	
			$paymentStatus		=	$this->db->query("SELECT payment_status FROM user_registration WHERE user_id = $user_id")->row_array();	
			if($noOfInvoices ['COUNT(id)'] == 5 && $paymentStatus['payment_status'] == 0){
				$this->load->library('email');
				$this->load->library('parser');
				$query 		= 	$this->db->query("SELECT email FROM users WHERE id = $user_id")->row_array();
				$to 		=	$query['email'];
				$message 	= 	"Hi,<br>Your Trail Version of Octa Travel has been expired. Please upgrade to continue services.<br>Thanks & Regards,<br>Team Octa Billing"; 
			    $this->email->from('noreply@myasa.net', 'OCTA Travel');
			    $this->email->to( $to );
			    $this->email->subject('Expiry of Trail Version');
			    $this->email->set_mailtype("html");
			    $this->email->message( $message ); 
			    $this->email->send();
			}
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Sales Invoice Generated Successfully!</div>'));
			redirect(base_url() . 'sales/invoices', 'refresh');
		}
		else
		{
			redirect(base_url(), 'refresh');
		}


	}



	public function editInvoice( $id = '', $param2 = '' )
	{
		if( !is_numeric( $id ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		} 
		$user_id 	= 	$this->session->userdata['id'];
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM services_invoices WHERE id = $id AND appcode = '$appcode'");
		if( !$query -> num_rows() )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		if( !$this->registrationStatus )
		{
			redirect(base_url().'services/invoices', 'refresh');
		}
		$data['pathType']			 = 		$this->pathType;
		$data['getinvoicedata']      =		$this->sales_model->GetInvoiceById( $id );
		$data['getservicesdata']     =		$this->services_model->GetServicesById( $id );
		$data['pageName']			 =		"Edit Sales Invoice";
		$data['fileName']			 =		"editInvoice.php";
		$data['service_name'] 		 = 		$this->services_model->getServiceTypes( $appcode );
		$data['customers'] 			 = 		$this->customers_model->getCustomers( $appcode );
		$data['hotels'] 			 = 		$this->hotels_model->getHotels( $appcode );
		$data['hotelServices'] 	 	 = 		$this->sales_model->getInvoiceHotelServices( $id, $appcode );
		$data['tours']				 =   	$this->tours_model->getTours($appcode);
		$hServices 					 =		$data['hotelServices'];
	
		if( $data['getinvoicedata']['accomodation_amount'] )
		{
			foreach ($hServices as $k => $value)
			 {
			 
			$hoteltariff[$k]       	 = 		$this->hotels_model->getHotelTariffs($hServices[$k]['hotel_id'], $appcode );
			$extrabedding[$k]		 =		$this->hotels_model->getHotelExtrabedding($hServices[$k]['hotel_id'], $appcode );
			}
			$data['hTariffs'] 		 = 		$hoteltariff;
			$data['extrabedding']	 =		$extrabedding;
			
		}
		$data['transporters'] 		 = 		$this->transporters_model->getTransporters( $appcode );

		$data['otherServices'] 		 = 		$this->sales_model->getInvoiceOtherServices( $id, $appcode );
		$data['transportServices'] 	 = 		$this->sales_model->getInvoiceTransportationServices( $id, $appcode );
		$tservices 					 = 		$data['transportServices'];
		if( $data['getinvoicedata']['transportation_amount'] )
			foreach ($tservices as $key => $value) 
			{
				$tTarifs[$key] 		 = 		$this->transporters_model->getTransporterTariffs( $tservices[$key]['transporter_id'], $appcode );
			}
			$data['tTariffs'] 		 = 		$tTarifs;
			//die(print_r($tTarifs[0][0]));
			if($data['getinvoicedata']['tour_id'])
			{
				$data['tour_tariff']	 = 		$this->tours_model->getTourTariffs($data['getinvoicedata']['tour_id'],$appcode);

				$data['tour_invoice']	 =		$this->sales_model->getTourInvoice($id,$appcode);
				//die(print_r($data['tour_invoice']));
				if(isset($data['tour_invoice']['each_paying']))
				{
				$paying    				 = explode('@$#', $data['tour_invoice']['each_paying']);
				$data['each_paying'] 	 =		$paying[0];
				$data['tour_tariff_id']	 =		$paying[1];
				
				$data['currentTourTariff'] =    $this->tours_model->getTourPlan($paying[1],$appcode);
				}
			}

			$this->load->view('index', $data);
		
			if( $id == '' )
			{
				redirect(base_url() . 'services/invoices', 'refresh');
			}
		
			if( $param2 == 'update')
			{
				$invoice_date_received  	=   trim($_POST['invoice_date']);
				$invoice_date_object 		= 	new DateTime($invoice_date_received);
				$invoice_date				= 	$invoice_date_object->format('Y-m-d');
				
				$date 						=   explode('-', $_POST['date']);
				$checkin_date_received  	=   $date[0];
				$checkin_date_object 		= 	new DateTime($checkin_date_received);
				$arrival					= 	$checkin_date_object->format('Y-m-d');
				$checkout_date_received 	=   $date[1];
				$checkout_date_object 		= 	new DateTime($checkout_date_received);
				$departure					= 	$checkout_date_object->format('Y-m-d');
						
				$invoicedata['user_id']   		=   $user_id;
				$invoicedata['appcode']   		=   $appcode;
				$invoicedata['invoice_date']   	=   $invoice_date;
				$invoicedata['no_of_pax']       =   trim($_POST['no_of_pax']);
				$invoicedata['arrival']       	=   $arrival;
				$invoicedata['departure']      	=   $departure;
				$invoicedata['advance_payment']	=   trim($_POST['advance_payment']);
				$invoicedata['total_amount']	=   trim($_POST['grand_total']);

				
				$invoicedata['accomodation_amount']		=	null;	
				$invoicedata['accomodation_charges']	=	null;		
				$invoicedata['accomodation_total']		=	null;

				
				$invoicedata['transportation_amount']	=	null;	
				$invoicedata['transportation_charges']	=	null;		
				$invoicedata['transportation_total']	=	null;	

				$invoicedata['general_services_total'] 	= 	null;

				$invoicedata['tour_id']					= 	null;
				$invoicedata['tour_amount']				=	null;
				$invoicedata['tour_charges']			=	null;
				$invoicedata['tour_total']				=	null;

				$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
				$result 	=   $this->db->update('services_invoices', $invoicedata);

				$this->db->where( array( 'invoice_id' => $id, 'appcode' => $appcode ) );
				$this->db->delete('invoice_hotel_services');

				$this->db->where( array( 'invoice_id' => $id, 'appcode' => $appcode ) );
				$this->db->delete('invoice_transportation_services');
				
				$this->db->where( array( 'invoice_id' => $id, 'appcode' => $appcode ) );
				$this->db->delete('services');
				$this->db->where( array( 'invoice_id' => $id, 'appcode' => $appcode ) );
				$this->db->delete('invoice_tours');

			if( isset( $_POST['accomodation_services'] ) )	
			{
				
				$hotelData['accomodation_amount']	=	$_POST['accomodation_amount'];	
				$hotelData['accomodation_charges']	=	$_POST['accomodation_charges'];		
				$hotelData['accomodation_total']	=	$_POST['accomodation_total'];	

				$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
				$result 	=   $this->db->update( 'services_invoices', $hotelData );

				$hotelid 			=	$_POST['hotel_id'];
				$accomodationdate 	=	$_POST['accomodation_date'];
				$room_type     		=   $_POST['room_type'];
				$meal_plan    		=   $_POST['meal_plan'];
				$roomNos      		=   $_POST['roomNos'];
				$extrabed    		=   $_POST['extrabed'];
				$child_without_bed 	= 	$_POST['child_without_bed'];

				$invoice_hotel_services['invoice_id'] 	= 	$id;
				$invoice_hotel_services['appcode'] 		= 	$appcode;

				foreach ( $room_type as $key=>$value )
				{
					$extrabedding 				= 	$this->services_model->getHotelExtrabedding( $hotelid[$key], $appcode );
					$extrabedCharges 			= 	$extrabedding['charges'];
					$tariff						=  	$this->services_model->getTariffDetails( $value, $appcode );
					$date 						=   explode('-', $accomodationdate[$key]);
					$checkin_date_received  	=   $date[0];
					$checkin_date_object 		= 	new DateTime($checkin_date_received);
					$checkin					= 	$checkin_date_object->format('Y-m-d');
					$checkout_date_received 	=   $date[1];
					$checkout_date_object 		= 	new DateTime($checkout_date_received);
					$checkout					= 	$checkout_date_object->format('Y-m-d');
					$days 						= 	(strtotime($checkout) - strtotime($checkin)) / (60 * 60 * 24);

					$invoice_hotel_services['hotel_id']			=	$hotelid[$key];
					$invoice_hotel_services['hotel_checkin']	=	$checkin;
					$invoice_hotel_services['hotel_checkout']	=	$checkout;
					$invoice_hotel_services['room_category'] 	= 	$tariff['room_category'];
					$invoice_hotel_services['occupancy'] 		= 	$tariff['occupancy'];
					$invoice_hotel_services['meal_plan'] 		= 	$meal_plan[$key];
					$invoice_hotel_services['no_of_rooms']		= 	$roomNos[$key];
					$invoice_hotel_services['extrabed']			= 	$extrabed[$key];
					$invoice_hotel_services['child_without_bed']= 	$child_without_bed[$key];
					$invoice_hotel_services['amount']			= 	(($tariff[$meal_plan[$key]] + ( $extrabed[$key] * $extrabedCharges) ) * $invoice_hotel_services['no_of_rooms']) * $days;
					$this->db->insert( 'invoice_hotel_services', $invoice_hotel_services );
				}
			}	
			
			if( isset( $_POST['transportation_services'] ) )	
			{
				$transportData['transportation_amount']		=	$_POST['transportation_amount'];	
				$transportData['transportation_charges']	=	$_POST['transportation_charges'];		
				$transportData['transportation_total']		=	$_POST['transportation_total'];	

				$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
				$result 	=   $this->db->update( 'services_invoices', $transportData );

				$tid 		=	$_POST['transporter_id'];
				$pickup		=	$_POST['pickup'];
				$trip     	=   $_POST['trip'];
				$vechile    =   $_POST['vechile'];
				$capacity   =   $_POST['capacity'];
				$days      	=   $_POST['days'];
				$rate    	=   $_POST['rate'];
				$pax      	=   $_POST['pax'];
				$dot      	=   $_POST['dot'];

				
				$invoice_transport_services['appcode'] 		= 	$appcode;
				$invoice_transport_services['invoice_id'] 	= 	$id;

				foreach ( $trip as $key=>$value )
				{
					$invoice_transport_services['transporter_id']	=	$tid[$key];
					$invoice_transport_services['pickup'] 			= 	$pickup[$key];
					$invoice_transport_services['trip'] 			= 	$value;
					$invoice_transport_services['vechile'] 			= 	$vechile[$key];
					$invoice_transport_services['capacity'] 		= 	$capacity[$key];
					$invoice_transport_services['days'] 			= 	$days[$key];
					$invoice_transport_services['pax'] 				= 	$pax[$key];
					$travel_date									= 	new DateTime($dot[$key]);
					$invoice_transport_services['dot']				= 	$travel_date->format('Y-m-d');
					$invoice_transport_services['rate']				= 	$rate[$key];
					$this->db->insert( 'invoice_transportation_services', $invoice_transport_services );
				}
			}
			
			if( isset( $_POST['tour_services'] ) )	
			{
				$tourData['tour_id']			=	$_POST['tour_id'];
				$tourData['tour_amount']		=	$_POST['tour_amount'];	
				$tourData['tour_charges']		=	$_POST['tour_charges'];		
				$tourData['tour_total']			=	$_POST['tour_total'];	

				$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
				$result 	=   $this->db->update( 'services_invoices', $tourData );


				$invoice_tour['invoice_id'] 	= 	$id;
				$invoice_tour['appcode'] 		= 	$appcode;

				$tour_id					=	$_POST['tour_id'];
				$tour_tariff_id 			=	$_POST['tour_tarriff_id'];
				$tourname 					= 	$this->db->query("SELECT heading FROM tours WHERE appcode = '$appcode' AND id= '$tour_id'") -> row_array();
				$tourTariff 				=	$this->db->query("SELECT * FROM tour_tariff WHERE appcode = '$appcode' AND id= '$tour_tariff_id'") -> row_array();
			    
				$invoice_tour['tour_name'] 					= 	$tourname['heading'];
				$invoice_tour['each_paying'] 				= 	$tourTariff['each_paying'].'@$#'.$tour_tariff_id;
				$invoice_tour['plan'] 						= 	$_POST['plan'];
				$invoice_tour['amount'] 					= 	$_POST['tour_amount'];
				
				$this->db->insert( 'invoice_tours', $invoice_tour );
				
			}

			if( isset( $_POST['general_services'] ) )
			{
				$gsData['general_services_total'] 		= 	$_POST['services_total'];	

				$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
				$result 	=   $this->db->update( 'services_invoices', $gsData );

				$servicesdata['invoice_id'] 	= 	$id;
				$servicesdata['appcode'] 		= 	$appcode;

				$service_type     	=   $_POST['service_type'];
				$actual_cost    	=   $_POST['actual_cost'];
				$charges   			=   $_POST['charges'];
				$cost      			=   $_POST['cost'];

				foreach ($service_type as $key=>$value)
				{
					$servicesdata['service_type'] 	= 	$value;
					$servicesdata['cost']			= 	$actual_cost[$key];
					$servicesdata['charges'] 		= 	$charges[$key];
					$servicesdata['total'] 			= 	$cost[$key];
					$result             			=   $this->db->insert( 'services', $servicesdata );
				}
			}

			if( $result )
			{
				$noOfInvoices		=	$this->db->query("SELECT COUNT(id) FROM services_invoices WHERE user_id = $user_id")->row_array();	
				$paymentStatus		=	$this->db->query("SELECT payment_status FROM user_registration WHERE user_id = $user_id")->row_array();	
				if($noOfInvoices ['COUNT(id)'] == 5 && $paymentStatus['payment_status'] == 0){
					$this->load->library('email');
					$this->load->library('parser');
					$query 		= 	$this->db->query("SELECT email FROM users WHERE id = $user_id")->row_array();
					$to 		=	$query['email'];
					$message 	= 	"Hi,<br>Your Trail Version of Octa Travel has been expired. Please upgrade to continue services.<br>Thanks & Regards,<br>Team Octa Billing"; 
				    $this->email->from('noreply@myasa.net', 'OCTA Billing');
				    $this->email->to( $to );
				    $this->email->subject('Expiry of Trail Version');
				    $this->email->set_mailtype("html");
				    $this->email->message( $message ); 
				    $this->email->send();
				}
				$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Sales Invoice Updated Successfully!</div>'));
				redirect(base_url() . 'sales/invoices', 'refresh');
				//die;
			}
			else
			{
				redirect(base_url(), 'refresh');
			}

		}
	}


	public function insertCustomerDetails()
	{
		if( $_POST )
		{
			$userId 	= 	$this->session->userdata['id'];
			$appcode	= 	$this->session->userdata['appcode'];
			$query		= 	$this->db->query("INSERT INTO customers( appcode, name, email, phone, landline, city, state, country, pin ) VALUES( '$appcode', '".$_POST['name']."', '".$_POST['email']."', '".$_POST['phone']."', '".$_POST['landline']."', '".$_POST['city']."', '".$_POST['state']."', '".$_POST['country']."', '".$_POST['pin']."' )");
			echo $this->db->insert_id(); 
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}	
	

	public function ajaxGetRooms()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$rooms 		=  	$this->hotels_model->getHotelTariffs( $_POST['hotel_id'], $appcode );
			
			echo json_encode( $rooms );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}

	
	public function ajaxGetExtrabedding()
	{
		if( $_POST )
		{ 
			$appcode    	=   $this->session->userdata['appcode'];  
			$extrabedding 	=  	$this->hotels_model->getHotelExtrabedding( $_POST['hotel_id'], $appcode );
			echo json_encode( $extrabedding );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	public function ajaxGetTariffDetails()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$tariff		=  	$this->hotels_model->getTariffDetails( $_POST['tariff_id'], $appcode );
			echo json_encode( $tariff );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	public function ajaxGetTransporterTrips()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$trips 		=  	$this->transporters_model->getTransporterTariffs( $_POST['transporter_id'], $appcode );
			
			echo json_encode( $trips );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	public function ajaxGetTourPaying()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$result		=  	$this->tours_model->getTourTariffs( $_POST['tour_id'], $appcode );
			echo json_encode( $result );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}
	 

	public function ajaxGetTourPlan()
	{
	 	if($_POST)
	 	{
	 		$appcode    =   $this->session->userdata['appcode'];
	 		$result 	= 	$this->tours_model->getTourPlan( $_POST['id'], $appcode );
	 		echo json_encode( $result );
			return true;
	 	}
	 	else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

	}


	public function getCustomerDetails()
	{
		$id 		= 	$_POST['id'];
		$appcode	=	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM customers WHERE id = $id AND appcode = '$appcode' AND status = 1") ->row_array();
		echo json_encode($query);

	}


	public function insertServiceAjax()
	{
		$data['serviceName']  	= 	trim( $_POST['serviceName'] );
		$data['cost']  			= 	trim( $_POST['serviceCost'] );
		$data['description']  	= 	trim( $_POST['description'] );
		$data['appcode'] 		= 	$this->session->userdata['appcode'];
		$result = $this->db->insert('servicetype', $data);
		if($result)
			return true;
		else
			return false; 
	}

	
	public function getServiceAjax()
	{
		$appcode	=	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT serviceName, cost FROM servicetype WHERE appcode = '$appcode'")->result_array();
		echo json_encode($query);
	}


}

/* End of file sales.php */
/* Location: ./application/modules/sales/controllers/sales.php */
