<script src="<?php echo base_url().'assets/js/countries.js';?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
        var date = new Date();
        var day  = date.getDate();
        var month  = date.getMonth() + 1;
        var yy  = date.getYear();
        var year = (yy < 1000) ? yy + 1900 : yy;
        var newdate = month + "/" + day + "/" + year;
		$('#date').daterangepicker({ minDate: newdate});   //Here  Set the Min Display Date
		
    });    

    
    function eraseModalMsg()
	{
		$('#warningMessage').html('');
		return true;
	}


	function saveService()
	{
		var serviceName = $('#serviceName').val();
		var serviceCost = $('#serviceCost').val();
		var description = $('#description').val();
		if( serviceName == "" || serviceCost == "" )
		{
			$('#warningMessage').html('<div  style="width: 85%; margin-left: 16px; margin-top: 1em; margin-bottom: 0em;" class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please Enter Service Name.</div>');
			return false;
		}
		else{
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()."sales/insertServiceAjax";?>',
				data: { serviceName: serviceName, serviceCost: serviceCost, description: description },
				
				success:function(){
				  	$('#serviceName').val('');
				  	$('#serviceCost').val('');
					$('#description').val('');
					$('#warningMessage').html('<div  style="width: 85%; margin-left: 16px; margin-top: 1em; margin-bottom: 0em;" class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Service Added Successfully.</div>');
					 
			        var serviceLength = ($('.form-services .control-group').length).toString();

			        for(i=serviceLength;serviceLength>0;serviceLength--){
	                    var x 			= 	document.getElementById(serviceLength);
						var option 		= 	document.createElement("option");
				        option.value 	= 	serviceName;
				        option.text 	= 	serviceName;
				        option.setAttribute("actualCost", serviceCost);
				        x.add(option);
	                }
	                $('#myModal1').modal('hide');
					return false;
				}
			});
			
		}
	}

	
	function displayCustomerDetails( paramID )
	{
		if(paramID == null)
			var id = document.getElementById('customer').value;
		else
			var id = paramID;
		if(id == 'na')
		{
			document.getElementById('invoice_to').value = null;
		    document.getElementById('txtEmail').value 	= null;
		    document.getElementById('phone').value 		= null;
		    document.getElementById('landline').value 	= null;
		    document.getElementById('country').value 	= null;
		    document.getElementById('state').value 		= null;
		    document.getElementById('city').value 		= null;
		    document.getElementById('pin_code').value 	= null;
		    $('#country').val(null);
		    return false;
		}
		$.ajax({
		  type: 'POST',
		  url: '<?php echo base_url()."sales/getCustomerDetails";?>',
		  data: { id: id },
		  success:function(data){
		  	var jsonObj = JSON.parse(data);
		    document.getElementById('invoice_to').value = jsonObj.name;
		    document.getElementById('txtEmail').value 	= jsonObj.email;
		    document.getElementById('phone').value 		= jsonObj.phone;
		    document.getElementById('landline').value 	= jsonObj.landline;
		    document.getElementById('country').value 	= jsonObj.country;
		    document.getElementById('state').value 		= jsonObj.state;
		    document.getElementById('city').value 		= jsonObj.city;
		    document.getElementById('pin_code').value 	= jsonObj.pin;
		  },
			error:function(){
			  	$('#addCustomerMessage').html('<div  style="width: 85%; margin-left: 16px; margin-bottom: 0em;" class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
			}
		});
		return false;
	}


	function insertCustomerDetails()
	{
		var name 		= 	document.getElementById('custname').value;
	   	var email 		= 	document.getElementById('custemail').value;
	   	var phone 		= 	document.getElementById('custphone').value;
	   	var landline	= 	document.getElementById('custlandline').value;
	   	var country 	= 	document.getElementById('custcountry').value;
	   	var state 		= 	document.getElementById('custstate').value;
	   	var city 		= 	document.getElementById('custcity').value;
	   	var pin 		= 	document.getElementById('custpin').value;
		$.ajax({
		  type: 'POST',
		  url: '<?php echo base_url()."sales/insertCustomerDetails";?>',
		  data: { name: name, email: email, phone: phone, landline: landline, country: country, state: state, city: city, pin: pin },
		  success:function( id )
		  {
	        $('#myModal2').modal('hide');
	        var x    	  =   document.getElementById('customer');
	        var option    =   document.createElement("option");
            option.value  =   id;
            option.text   =   name;
            x.add(option);
	        $('select').trigger('liszt:updated'); 
	        $("select").val(id);
			$("select").trigger("liszt:updated");
		  	displayCustomerDetails( id );
		  },
			error:function(){
			  	$('#addCustomerMessage').html('<div  style="width: 85%; margin-left: 16px; margin-bottom: 0em;" class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
			}
		});
		return false;
	}
	
	
	function checkAdvanceAmount( payAmount )
    {
        var amountPaid 		= 	payAmount.value;
        var total_amount 	= 	document.getElementById('grand_total').value;
        if( amountPaid > balance )
        {
            alert('Entered Amount is greater than Total Amount!')
            document.getElementById('advance_payment').value = "";
        }
        return false;
    }


    function displayHA()
    {
     	$('#accomodation').toggle();
     	if($('#accomodation_services').prop('checked') != true)
   	{
    	$('#accomodation :input').removeAttr('required');
    	$('#accomodation :select').removeAttr('required');
    }
   	else
   	{
     	$('#accomodation .req' ).attr('required','');
    }
     	calcGrandTotal();
     	return true;
    }

    
    function displayTP()
    {
     	$('#transportation').toggle();
     	if($('#transportation_services').prop('checked') != true)
   		{
		    $('#transportation :input').removeAttr('required');
		    $('#transportation :select').removeAttr('required');
   		}
   		else
   		{
     		$('#transportation .req').attr('required','');
     	}
     		calcGrandTotal();
     		return true;
    }


    function displayGS()
    {
     	$('#generalServices').toggle();
     	if($('#general_services').prop('checked') != true)
   		{
    		$('#generalServices :input').removeAttr('required');
   			$('#generalServices :select').removeAttr('required');
   		}
   		else
     		$('#generalServices .req').attr('required','');
     	calcGrandTotal();
     	return true;
    }


    function displayTourP()
    {
     	$('#tours').toggle();
      	if($('#tour_services').prop('checked') != true)
   		{
    		$('#tours :input').removeAttr('required');
   			$('#tours :select').removeAttr('required');
   		}
   		else
     		$('#tours .req').attr('required','');
     	calcGrandTotal();
     	return true;
    }

</script>

<style type="text/css">
	.general-control-group, .transport-control-group, .hotel-control-group{ margin-top: 1em; }
	#accomodation, #transportation, #generalServices{ margin-bottom: 3em; }
</style>

<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Add Sales Invoice (Services)
					</h4>
					<span class="tools"> <a href="javascript:;"
						class="icon-chevron-down"></a> <a href="#widget-config"
						data-toggle="modal" class="icon-wrench"></a> <a
						href="javascript:;" class="icon-refresh"></a> <a
						href="javascript:;" class="icon-remove"></a>
					</span>
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form onsubmit="return checkDate();" action="<?php echo base_url();?>sales/insertinvoice" method="POST" class="form-horizontal">
						<div class="control-group">
							<label class="control-label" for="input1">Invoice Date</label>
							<div class="controls">
								<input class="input-small date-picker span2" required size="16"
									type="text" placeholder="MM/DD/YY" value="<?php echo date('m/d/Y');?>" name="invoice_date" id="invoice_date"/>
							</div>
						</div>
						
						<input type="hidden" readonly required class="span6" id="invoice_to" name="invoice_to" />
							
						<div class="control-group">
							<label class="control-label" for="input1">Customer Name</label>
							<div class="controls">
								<select class="chosen span6" onchange="displayCustomerDetails();" id="customer" name="customers" required>
									<option value="na">Select Existing Customer</option>
									<?php 
										foreach($customers as $customer)
										{
									?>
										<option value="<?php echo $customer['id'];?>"><?php echo $customer['name'];?></option>
									<?php
										}
									?>
								</select>
								<input type="button" style="margin-left: 1%; margin-top:-2.5%;" class="btn btn-inverse" id="demo" value="OR">
								<a href="#myModal2" style="margin-left: 1%; margin-top:-2.5%;" role="button" class="btn btn-inverse" data-toggle="modal">  +  </a>
								
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">Email Address</label>
							<div class="controls">
								<input type="email" placeholder="Enter Email address" class="span6" id='txtEmail' name="email" />
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">Mobile No 
							</label>
							<div class="controls">
								<input type="text" placeholder="Enter Mobile Number"
									pattern="\d{10}" title="(Should be numeric 10 digits)"
									maxlength="10" class="span6" id="phone" name="phone" />
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">Landline No</label>
							<div class="controls">
								<input type="text" placeholder="Enter Landline Number" class="span6" id="landline" name="landline" value="" />
							</div>
						</div>
						
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">Country</label>
							<div class="controls">
								<input type="text" placeholder="Enter State"
									class="span6" id="country" name="country" />
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">State</label>
							<div class="controls">
								<input type="text" placeholder="Enter State"
									class="span6" id="state" name="state" />
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">City</label>
							<div class="controls">
								<input type="text" placeholder="Enter City"
									class="span6" id="city" name="city" />
								
							</div>
						</div>
						<div style="display: none;" class="control-group">
							<label class="control-label" for="input1">Pin Code</label>
							<div class="controls">
								<input type="text" placeholder="Enter Pin Code"
									class="span6" id="pin_code"
									name="pin_code" />
							</div>
						</div>
						<!-- Address-City-State-Pincode ends-->
						<div class="control-group">
							<label class="control-label">Arrival - Departure</label>
							<div class="controls">
								<div class="input-prepend">
									<span class="add-on"><i class="fa fa-calendar"></i> </span><input
										type="text" id="date" name="date" required placeholder="<?php echo date( 'm/d/Y' ); ?>"	class="input-large date-range" />
								</div>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">No of Pax</label>
							<div class="controls">
								<input type="text" required maxlength="2" pattern="\d+(\.\d{2})?" placeholder="Enter No of Pax"	class="span4" id="no_of_pax" name="no_of_pax" />
							</div>
						</div>

						<div class="control-group">
							<!-- <label class="control-label">Accomodation</label> -->
							<div class="controls">
								<div>
									<input onclick="displayHA();" type="checkbox" id="accomodation_services" name="accomodation_services" value="1" />
									<span>Add Accomodation </span>&nbsp;&nbsp;&nbsp;
									<input onclick="displayTP();" type="checkbox" id="transportation_services" name="transportation_services" value="1"/>
									<span>Add Transportation </span>&nbsp;&nbsp;&nbsp;
									<input onclick="displayGS();" type="checkbox" id="general_services" name="general_services" value="1" />
									<span>Add Other Services </span>&nbsp;&nbsp;&nbsp;
									<input onclick="displayTourP();" type="checkbox" id="tour_services" name="tour_services" value="1"/>
									<span>Add Tour Package </span>
								</div>
							</div>
						</div>

	<!------------------------------ Accomodation Starts -------------------------------->

						<div id="accomodation">
						<div class="hotel-voucher-services1">
							<div class="control-group cg">
								<label class="control-label" for="input1">HOTEL NAME</label>
								<div class="controls">
									<select onchange="getRoomCategories(1);" class="chosen req" id="hotel_id_1" name="hotel_id[]"  >
										<option value="">Select Existing Hotel</option>
										<?php 
											foreach($hotels as $hotel)
											{
										?>
											<option value="<?php echo $hotel['id'];?>" ><?php echo $hotel['hotel_name'];?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">CheckIn - CheckOut</label>
								<div class="controls">
									<div class="input-prepend">
										<span class="add-on"><i class="fa fa-calendar"></i> </span>
										<input type="text"  id="accomodation_date_1" onchange="javascript:startHotelCalc();" name="accomodation_date[]"  placeholder="<?php echo date( 'm/d/Y' ); ?>"
											class="input-large date-range req" value=""/>
									</div>
								</div>
							</div>

							
							<div class="hotel-voucher-services">
								<div class="control-group">
									<label class="control-label" for="inputText">Add Room</label>

									<div class="controls">
										
										<select class="span4 req" onchange="getTariffDetails(1);" id="room_type_1"  name="room_type[]" >
											<option value=''>Room Category</option>
				   						</select>
				   						
				   						<select class="span2 req" onchange="javascript:startHotelCalc();" id="meal_plan_1"  name="meal_plan[]" >
											<option value='' cost="">Meal Plan</option>
				   						</select>

				   						<select  class="span2 req" onchange="javascript:startHotelCalc();" id="roomNos_1"  name="roomNos[]" >
											<option value=''>No of Rooms</option>
					   						<option value="1">1</option>
					   						<option value="2">2</option>
					   						<option value="3">3</option>
				   						</select>

				   						<select class="span2" onchange="javascript:startHotelCalc();" id="extrabed_1" name="extrabed[]">
											<option value='' cost="">Extrabed</option>
				   						</select>
				   						<select  class="span2"  id="child_without_bed_1" name="child_without_bed[]">
										<option value='0'>Child WOB</option>
				   						<option value="1">1</option>
				   						<option value="2">2</option>
				   						<option value="3">3</option>
			   						</select>
											
									</div>
								</div>
							</div>
						</div>

							<div class="hotel-control-group">
								<div class="controls">
									<input type="button" class="btn btn-success" value="Add Room" id="addAccomodationButton"> 
									<input type="button" class="btn btn-danger"	value="Remove Room" id="removeAccomodationButton"><br><br>
								</div>							
							</div>

							<div class="control-group">
								<!-- <label class="control-label" for="input1">Total Amount (<i class="fa fa-fw fa-rupee"></i>)</label> -->
								<div class="controls">
									<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="accomodation_amount" name="accomodation_amount" placeholder="Amount"/>
								</div>

								<div style="padding-left: 12em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="text" autocomplete="off" maxlength="2" pattern="\d+(\.\d{2})?" onInput="startHotelCalc();" class="span3" id="accomodation_charges" name="accomodation_charges" placeholder="Charges(%)"/>
									</div>
								</div>

								<div style="padding-left: 28em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="number" autocomplete="off" max="99" class="span4" id="accomodation_total" readonly name="accomodation_total" placeholder="Total Amount"/>
									</div>
								</div>

							</div>
						</div>

	<!------------------------------ Accomodation Ends -------------------------------->

	<!------------------------------ Transportation Starts -------------------------------->

						<div id="transportation">
						  <div class="transport-voucher-services1">	
							<div class="control-group cg">
								<label class="control-label" for="input1">TRANSPORTER NAME</label>
								<div class="controls">
									<select onchange="getTransporterTrips(1);" class="chosen req" id="transporter_id_1" name="transporter_id[]"  >
										<option value="">Select Existing Transporter</option>
										<?php 
											foreach($transporters as $transporter)
											{
										?>
											<option value="<?php echo $transporter['id'];?>" ><?php echo $transporter['transporter_name'];?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>

							<div id="pickups_1" class="control-group">
								<label class="control-label" for="input1">Pickup</label>
								<div class="controls">
								<input type="text"  placeholder="Pickup From" class="span6" id="pickup_1" name="pickup[]" />
									
								</div>
							</div>

							<div class="transport-voucher-services">
								<div class="control-group">
									<label class="control-label" for="inputText">Add Trip</label>

									<div class="controls">
										
										<select class="span4 req" onchange="displayTripDetails(this);" id="trips_1" name="trip[]">
											<option value=''></option>
				   						</select>
				   						
				   						<input type="text" autocomplete="off" readonly placeholder="Vechile" class="span2" id="vechile_1" name="vechile[]" />

				   						<input type="number" autocomplete="off" readonly placeholder="Capacity" class="span2" id="capacity_1" name="capacity[]" />

				   						<input type="number" autocomplete="off" readonly placeholder="Days" class="span2" id="days_1" name="days[]" />

				   						<input type="number" autocomplete="off" readonly placeholder="Rate" class="span2" id="rate_1" name="rate[]" />

				   						<input type="number" autocomplete="off" onInput="checkPax(this);"  placeholder="No of Pax" class="span2" id="pax_1" name="pax[]" />

				   						<input class="input-small date-picker span2"  type="text" placeholder="Date of Travel" name="dot[]" id="dot_1" />
											
									</div>
								</div>
							</div>
						  </div>

							<div class="transport-control-group">
								<div class="controls">
									<input type="button" class="btn btn-success" value="Add Trip" id="addTransportButton"> 
									<input type="button" class="btn btn-danger"	value="Remove Trip" id="removeTransportButton"><br><br>
								</div>							
							</div>

							<div class="control-group">
								
								<div class="controls">
									<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="transportation_amount" name="transportation_amount" placeholder="Amount"/>
								</div>

								<div style="padding-left: 12em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="text" autocomplete="off" maxlength="2" pattern="\d+(\.\d{2})?" class="span3" id="transportation_charges" name="transportation_charges" onInput="startTransportCalc();" placeholder="Charges(%)"/>
									</div>
								</div>

								<div style="padding-left: 28em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="number" autocomplete="off"  max="99" class="span4" id="transportation_total" readonly name="transportation_total" placeholder="Total Amount"/>
									</div>
								</div>

							</div>

						</div>

	<!------------------------------ Transportation Ends -------------------------------->

	<!------------------------------ General Services Start -------------------------------->

						<div id="generalServices">
							<div class="form-services">
								<div class="control-group">
									<label class="control-label" for="inputText">OTHER SERVICES</label>

									<div class="controls">
									<select onchange="javascript:startCalc();" id="1" name="service_type[]" class="req" >
										<option value=''>Select Service</option>
				   						<?php foreach ( $service_name as $row ) {?>
				   							<option value="<?php echo $row['serviceName'];?>" actualCost="<?php echo $row['cost'];?>"><?php echo $row['serviceName'];?></option>
				   						<?php }?>
			   						</select>
			   							<input type="number" id="actual_cost_1" onInput="javascript:startCalc();" readonly class="input-small" placeholder="Actual Cost" name="actual_cost[]" /> 
										<input type="text" id="charges_1" onInput="javascript:startCalc();" autocomplete="off" class="input-small" maxlength="2" placeholder="Charges(%)" name="charges[]" pattern="\d+(\.\d{2})?"/>
										<input type="number" id="cost_1" onInput="javascript:startCalc();" autocomplete="off" class="input-small"  placeholder="Amount" name="cost[]" /> 
											
									</div>
								</div>
							</div>
							
							<div class="general-control-group">
								<div class="controls">
									<input type="button" class="btn btn-success" value="Add Service" id="addButton"> 
									<input type="button" class="btn btn-danger"	value="Remove Service" id="removeButton">
									<a href="#myModal1" onclick="return eraseModalMsg();" role="button" class="btn btn-inverse" data-toggle="modal">Add New Service</a>
								</div>							
							</div><br>

							<div class="general-control-group">
								
								<div class="controls">
									<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="services_total" name="services_total" placeholder="Total"/>
								</div>

							</div>

						</div>

	<!------------------------------ General Services End -------------------------------->

	<!------------------------------ Tours Starts -------------------------------->

						<div id="tours">	
							<div class="control-group">
								<label class="control-label" for="input1">Tour Package Name</label>
								<div class="controls">
									<select onchange="javascript:getTourPaying();" class="chosen req" id="tour_id" name="tour_id" >
										<option value="">Select Tour Package</option>
										<?php 
											foreach($tours as $tour)
											{
										?>
											<option value="<?php echo $tour['id'];?>" ><?php echo $tour['heading'];?></option>
										<?php
											}
										?>
									</select>
								</div>
							</div>

						

							<div class="Tour-voucher-services">
								<div class="control-group">
									<label class="control-label" for="inputText">Each Paying</label>

									<div class="controls">
										
										<select class="span4 req" onchange="GetTourPlan();" id="tour_tarriff_id" name="tour_tarriff_id">
											<option value=''></option>
				   						</select>
				  
											
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="inputText">Select Plan</label>

									<div class="controls">
										
										<select class="span4 req" onchange="startTourCalc(this);" id="plan" name="plan">
											<option value=''></option>
				   						</select>
				  
											
									</div>
								</div>
							</div>
							

						

							<div class="control-group">

								<div style="margin-top: 10px;">
								<div class="controls">
									<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="tour_amount" name="tour_amount" placeholder="Amount"/>
								</div>
								</div>
								<div style="padding-left: 12em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="text" autocomplete="off" maxlength="2" pattern="\d+(\.\d{2})?" class="span3" id="tour_charges" name="tour_charges" onInput="startTourCalc();" placeholder="Charges(%)"/>
									</div>
								</div>

								<div style="padding-left: 28em; margin-top: -2.3em;">
									<!-- <label class="control-label" for="input1">Charges(%) </label> -->
									<div class="controls">
										<input type="number" autocomplete="off"  max="99" class="span4" id="tour_total" readonly name="tour_total" placeholder="Total Amount"/>
									</div>
								</div>

							</div>

						</div>

	<!------------------------------ Tours Ends -------------------------------->


						<div class="control-group">
							<br> <label class="control-label" for="input1">Advance Payment (<i class="fa fa-fw fa-rupee"></i>)</label>
							<div class="controls">
								<input type="text" required onBlur="checkAdvanceAmount( this );"  pattern="\d+(\.\d{2})?" autocomplete="off" class="span2" id="advance_payment" name="advance_payment" />
							</div>
						</div>
						
						<div style="padding-left: 24.7em; margin-top: -3.8em;">
							<label class="control-label" for="input1">Total Amount (<i class="fa fa-fw fa-rupee"></i>)</label>
							<div class="controls">
								<input type="text" autocomplete="off" required readonly pattern="\d+(\.\d{2})?" class="span3" id="grand_total" name="grand_total" /><br><br>
							</div>
						</div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn">Cancel</button>
						</div>

					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>


	<!-- Modal 1 Starts Here -->
	<div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel1">Add New Service</h3>
		</div>
		<div id="warningMessage">
		
		</div>
		<div class="modal-body">
			<input type="text" placeholder="Enter Service Name" class="span12" id="serviceName" name="serviceName" /><br><br>
			<input type="number" placeholder="Enter Service Cost" class="span12" id="serviceCost" name="serviceName" /><br><br>
			<textarea id="description" name="description" rows="5" placeholder="Enter Service Description(Optional)" class="span12"></textarea>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button onclick="saveService();" class="btn btn-primary">Save</button>
		</div>
	</div>
	<!-- Modal 1 Ends Here -->


	<!-- Modal 2 Starts Here -->
	<div style="margin-top: -2em;" id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel1">Add New Customer</h3>
		</div>
		<div id="addCustomerMessage">
			
		</div>
		<form onSubmit = "return insertCustomerDetails();" style="padding-top:1em;"  class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="input1">Customer Name</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Customer Name" class="span6" id="custname" name="custname" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Email Address</label>
					<div class="controls">
						<!-- Email validation : onblur= "return checkEmail();" -->
						<input type="email" required placeholder="Enter Email address" class="span6" id="custemail" name="custemail" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Mobile No 
					</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Mobile Number" pattern="\d{10}" title="(Should be numeric 10 digits)" maxlength="10" class="span6" id="custphone" name="custphone" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Landline No</label>
					<div class="controls">
						<input type="text" placeholder="Enter Landline Number" class="span6" id="custlandline" name="custlandline" value="" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input1">Country</label>
					<div class="controls">
						<select class="span6" id="custcountry" name="custcountry" required>
							
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input1">State</label>
					<div class="controls">
						<select class="span6" id="custstate" name="custstate" required>
									
						</select>
					</div>
				</div>
				
				<script language="javascript">
					populateCountries("custcountry", "custstate");
					populateCountries("country2");
				</script>   
				
				<div class="control-group">
					<label class="control-label" for="input1">City</label>
					<div class="controls">
						<input type="text" required placeholder="Enter City"
						class="span6" id="custcity" name="custcity" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Pin Code</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Pin Code"
						pattern="\d+(\.\d{2})?" class="span6" id="custpin"
						name="custpin" />
					</div>
				</div>

				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" data-dismiss="modal" class="btn">Cancel</button>
				</div>
	</div>
	<!-- Modal 2 Ends Here -->


</div>

<script type="text/javascript">

	$("#accomodation").hide();
	$("#transportation").hide();
	$("#generalServices").hide();
	$("#tours").hide();

</script>

<script type="text/javascript">
	function getRoomCategories( param)
	{
		
		var serviceLength 	= 	($('.hotel-voucher-services .control-group').length).toString();
		var serviceLenght1 	= 	serviceLength;
		var serviceLenght2 	= 	serviceLength;
		var serviceLenght3 	= 	serviceLength;
		var hotel_id 		= 	$('#hotel_id_'+param).val();
		
		if( hotel_id == '' )
		{
			for(i=serviceLenght1;serviceLenght1>=i;serviceLenght1--)
	        {
		        var x 	= 	document.getElementById('room_type_'+param);
		        x.options.length = 0;
		        
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Room Category';
		        x.add(option);

			    var x 	= 	document.getElementById('extrabed_'+param);
		        x.options.length = 0;
		        
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Extrabed';
		        x.add(option);

		    }
			return false;
		}
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetRooms";?>',
			data: { hotel_id: hotel_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);
		        
		         for(i=serviceLenght1;serviceLenght1>=i;serviceLenght1--)
		        {
			        var x 	= 	document.getElementById('room_type_'+param);
			        x.options.length = 0;

			       
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'';
			        option.text 	= 	'Room Category';
			        x.add(option);

				    for(i=0;i<jsonObj.length;i++)
	                {
	                    var option    =   document.createElement("option");
	                    option.value  =   jsonObj[i].id;
	                    option.text   =   jsonObj[i].room_category + ' ( '+ jsonObj[i].occupancy +' )';
	                    x.add(option);
	                }

			    }
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetExtrabedding";?>',
			data: { hotel_id: hotel_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        for(i=serviceLenght2;serviceLenght2>=i;serviceLenght2--)
		        {
			        var x 	= 	document.getElementById('extrabed_'+param);
			        x.options.length = 0;
			        
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'0';
			        option.text 	= 	'Extrabed';
			        option.setAttribute("cost", "");
			        x.add(option);

				    var option 		= 	document.createElement("option");
			        option.value 	= 	'1';
			        option.text 	= 	'1';
			        option.setAttribute("cost", jsonObj.charges*1);
			        x.add(option);

			        var option 		= 	document.createElement("option");
			        option.value 	= 	'2';
			        option.text 	= 	'2';
			        option.setAttribute("cost", jsonObj.charges*2);
			        x.add(option);

			    }
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		return false;
			
	}

	function getTariffDetails( param )
	{
		var tariff_id = $('#room_type_'+ param).val();
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetTariffDetails";?>',
			data: { tariff_id: tariff_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        var x 	= 	document.getElementById('meal_plan_' + param);
		        x.options.length = 0;
		        var option 		= 	document.createElement("option");
		        option.value 	= 	'';
		        option.text 	= 	'Meal Plan';
		        option.setAttribute("cost", "");
		        x.add(option);
			    
			    if( jsonObj.ep !== '0' )
			    {
	                var option    =   document.createElement("option");
	                option.value  =   'ep';
	                option.text   =   'EP';
	                option.setAttribute("cost", jsonObj.ep);
	                x.add(option);
            	}

                if( jsonObj.cp !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'cp';
	                option.text   =   'CP';
	                option.setAttribute("cost", jsonObj.cp);
	                x.add(option);
            	}

                if( jsonObj.map !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'map';
	                option.text   =   'MAP';
	                option.setAttribute("cost", jsonObj.map);
	                x.add(option);
	            }

                if( jsonObj.ap !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'ap';
	                option.text   =   'AP';
	                option.setAttribute("cost", jsonObj.ap);
	                x.add(option);
            	}
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});
		return false;
	}


  	
	function startHotelCalc()
	{
		var itemLength 	= 	$('.hotel-voucher-services .control-group').length; 

		var meal_plan, no_of_rooms, extrabed, days, tamount = 0, total_amount;
		

		for( i=1; i<=itemLength; i++ )
		{
			var amount = 0;
			var date 		= 	$('#accomodation_date_'+i).val();
			if( date !== '' )
			{
				date 		= 	date.split("-");
				checkIn 	= 	moment(date[0]);
				checkOut 	= 	moment(date[1]);
				days 		= 	checkOut.diff(checkIn, "days")
			}
			else
			{
				days 	= 	1;
			}

			meal_plan 	=	$("#meal_plan_"+i).find(':selected').attr('cost');
			extrabed 	=	$("#extrabed_"+i).find(':selected').attr('cost');
			no_of_rooms =   $("#roomNos_"+i).val();
			if( no_of_rooms == "" && extrabed == "" )
			{
				amount = amount + (parseInt(meal_plan, 10)* parseInt(no_of_rooms, 10));
			}
			else if( no_of_rooms !== "" && extrabed == "" )
			{
				amount = amount + parseInt(meal_plan, 10) * parseInt(no_of_rooms, 10);
			}
			else if( no_of_rooms == ""  && extrabed !== "" )
			{
				amount = amount + (parseInt(meal_plan, 10)* parseInt(no_of_rooms, 10)) + parseInt(extrabed, 10);
			}
			else
			{
				amount = amount + (parseInt(meal_plan, 10) * parseInt(no_of_rooms, 10)) + parseInt(extrabed, 10);
			}
			tamount = tamount + (amount * days);
		}
		
		
		if( tamount >= 0 )
		{
			charges 	= 	$('#accomodation_charges').val();
			if( charges == '' )
				charges = 0;
			else
				charges = parseInt(charges, 10);

			totalAmount = tamount + ( tamount * charges )/100;
			
			$('#accomodation_amount').val(Math.ceil(tamount));
			$('#accomodation_total').val(Math.ceil(totalAmount));

		}
		calcGrandTotal();
		return false;
	}


	function getTransporterTrips( param )
	{
		var transporter_id 		= 	$('#transporter_id_'+param).val();
		var serviceLength 		= 	($('.transport-voucher-services .control-group').length).toString();
		var i 					=	serviceLength;
		
		if( transporter_id == '' )
		{
			
                var x 	= 	document.getElementById('trips_' + param);
		        x.options.length = 0;

		        $('#vechile_' + param).val('');
				$('#capacity_' + param).val('');
				$('#days_' + param).val('');
				$('#rate_' + param).val('');
            
			return false;
		}

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetTransporterTrips";?>',
			data: { transporter_id: transporter_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        
			        var x 	= 	document.getElementById('trips_'+param);
			        x.options.length = 0;
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'';
			        option.text 	= 	'Select Trip';
			        x.add(option);

				    for( k=0; k<jsonObj.length; k++ )
	                {
	                    var option    =   document.createElement("option");
	                    option.value  =   jsonObj[k].trip;
	                    option.text   =   jsonObj[k].trip;
	                    option.setAttribute("vechile_name", jsonObj[k].vechile_name);
	                    option.setAttribute("capacity", jsonObj[k].capacity);
	                    option.setAttribute("days", jsonObj[k].days);
	                    option.setAttribute("rate", jsonObj[k].rate);
	                    x.add(option);
	                }
	            

			},
			error:function()
            {
                alert('Please check your Internet connection.');
            }	
		});

		return false;
			
	}


	function displayTripDetails( trip )
	{
		var id 	= 	(trip.id).split( "_" );

		var transporter_id 	= 	$('#transporter_id').val();
		var vechile_name 	=	$(('#')+trip.id).find(':selected').attr('vechile_name');
		var capacity 		=	$(('#')+trip.id).find(':selected').attr('capacity');
		var days 			=	$(('#')+trip.id).find(':selected').attr('days');
		var rate 			=	$(('#')+trip.id).find(':selected').attr('rate');

		$('#vechile_' + id[1]).val(vechile_name);
		$('#capacity_' + id[1]).val(capacity);
		$('#days_' + id[1]).val(days);
		$('#rate_' + id[1]).val(rate);
		startTransportCalc();
		return false;
	}


	function startTransportCalc()
	{
		var itemLength 	= 	$('.transport-voucher-services .control-group').length; 
		var trip_cost, amount = 0;

		for( i=1; i<=itemLength; i++ )
		{
			trip_cost 	=	$("#rate_"+i).val();
			if( trip_cost == "" )
			{
				continue;
			}
			else
			{
				amount = amount + parseInt(trip_cost, 10);
			}
		}
		
		if( amount >= 0 )
		{
			charges 	= 	$('#transportation_charges').val();
			if( charges == '' )
				charges = 0;
			else
				charges = parseInt(charges, 10);

			totalAmount = amount + ( amount * charges )/100;
			$('#transportation_amount').val(Math.ceil(amount));
			$('#transportation_total').val(Math.ceil(totalAmount));
		}
		calcGrandTotal();
		return false;
	}


	function startCalc()
	{
		var itemLength = $('.form-services .control-group').length; 
		var amount, cost, charges;
		amount 	= 	0;
		for( i=1; i<=itemLength; i++ )
		{
			cost 		=	$("#"+i).find(':selected').attr('actualCost');
			charges		=	$('#charges_'+i).val();
			if( cost == "" )
			{
				continue;
			}
			else
			{
				$('#actual_cost_'+i).val(cost);
				if( charges == "" )
				{
					serviceAmount = parseInt(cost, 10);

					if( isNaN(serviceAmount) )
						continue;
					$('#cost_'+i).val(serviceAmount);
					amount 	= 	amount + parseInt(cost, 10);
				}
				else
				{
					serviceAmount = parseInt(cost, 10) + (parseInt(cost, 10) * parseInt(charges, 10))/100;
					if( isNaN(serviceAmount) )
						continue;
					$('#cost_'+i).val(serviceAmount);
					amount 	= 	amount + serviceAmount;
				}
			}
		}
		if( amount >= 0 )
			$('#services_total').val(Math.ceil(amount));
		calcGrandTotal();
		return false;
	}


	function checkPax(pax)
	{
		var id 			= 	(pax.id).split( "_" );
		var pax 		= 	parseInt($('#pax_'+id[1]).val(), 10);
		var capacity 	= 	parseInt($('#capacity_'+id[1]).val(), 10);
		if( pax > capacity )
		{
			alert('Number of Pax are greater than the seat capacity of Vechile!');
			$('#pax_'+id[1]).val('');
		}
		return false;
	}


	function calcGrandTotal()
	{ 
		var accomodation_services 		=	document.getElementById('accomodation_services').checked;
		var transportation_services 	=	document.getElementById('transportation_services').checked;
		var other_services 				=	document.getElementById('general_services').checked;
		var tour_services				= 	document.getElementById('tour_services').checked;
		
		//alert(accomodation_services+','+transportation_services+','+other_services)

		var accomodation_total		= 	$('#accomodation_total').val();
		var transportation_total	= 	$('#transportation_total').val();
		var services_total			= 	$('#services_total').val();
		var tour_total				=	$('#tour_total').val();

		var grand_total = 0;

		if( accomodation_services )
		{
			if( accomodation_total == '' )
				accomodation_total = 0;
			else
				accomodation_total = parseInt( accomodation_total, 10 );
			grand_total = grand_total + accomodation_total;
		}

		if( transportation_services )
		{
			if( transportation_total == '' )
				transportation_total = 0;
			else
				transportation_total = parseInt( transportation_total, 10 );
			grand_total = grand_total + transportation_total;
		}

		if( other_services )
		{
			if( services_total == '' )
				services_total = 0;
			else
				services_total = parseInt( services_total, 10 );
			grand_total = grand_total + services_total;
		}
		if( tour_services )
		{
			if( tour_total == '' )
				tour_total = 0;
			else
				tour_total = parseInt( tour_total, 10 );
			grand_total = grand_total + tour_total;
		}	

		$('#grand_total').val( grand_total );

	}
//Tour functions

	function getTourPaying()
	{
		var tour_id 		= 	$('#tour_id').val();
		
		

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetTourPaying";?>',
			data: { tour_id: tour_id },
			
				success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        var x 	= 	document.getElementById('tour_tarriff_id');
		        x.options.length = 0;

		         var option 		= 	document.createElement("option");
		        option.value 	= 	'0';
		        option.text 	= 	'Each Paying';
		       // option.setAttribute("cost", "");
		        x.add(option);
                for(i=0;i<jsonObj.length;i++)
                {


		        var option 		= 	document.createElement("option");
		        option.value 	= 	jsonObj[i].id;
		        option.text 	= 	jsonObj[i].each_paying;
		       // option.setAttribute("cost", "");
		        x.add(option);
			    }
			   
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		return false;
			
	}

	function GetTourPlan()
	{
		var tour_tarriff_id 		= 	$('#tour_tarriff_id').val();
		
		

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."sales/ajaxGetTourPlan";?>',
			data: { id: tour_tarriff_id },
			
				success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        var x 	= 	document.getElementById('plan');
		        x.options.length = 0;

		         var option 		= 	document.createElement("option");
		        option.value 	= 	'0';
		        option.text 	= 	'Select Plan';
		        option.setAttribute("cost", "");
		        x.add(option);
                


		       
			     if( jsonObj.budget !== '0' )
			    {
	                var option    =   document.createElement("option");
	                option.value  =   'budget';
	                option.text   =   'Budget';
	                option.setAttribute("cost", jsonObj.budget);
	                x.add(option);
            	}

                if( jsonObj.economic !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'economic';
	                option.text   =   'Economic';
	                option.setAttribute("cost", jsonObj.economic);
	                x.add(option);
            	}

                if( jsonObj.standard !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'standard';
	                option.text   =   'Standard';
	                option.setAttribute("cost", jsonObj.standard);
	                x.add(option);
	            }

                if( jsonObj.super_deluxe !== '0' )
                {
	                var option    =   document.createElement("option");
	                option.value  =   'super_deluxe';
	                option.text   =   'Super Deluxe';
	                option.setAttribute("cost", jsonObj.super_deluxe);
	                x.add(option);
           		}
				
			},
			error:function()
            {
                alert('Please check your Internet connection.');
                
            }	
		});

		return false;
		

	}

	function startTourCalc()
	{
		
		var amount, cost, charges;
		amount 	= 	0;

		
			cost 		=	$("#plan").find(':selected').attr('cost');
			charges		=	$('#tour_charges').val();

			if( cost == "" )
			{
				//continue;
			}
			else
			{
				$('#tour_amount').val(cost);

				if( charges == "" )
				{
					
					amount 	= 	amount + parseInt(cost, 10);
				}
				else
				{
					amount = parseInt(cost, 10) + (parseInt(cost, 10) * parseInt(charges, 10))/100;
					 if( !isNaN(amount) )
					// 	continue;
					$('#tour_total').val(amount);
					
				}
			}
		
		if( amount >= 0 )
			$('#tour_total').val(Math.ceil(amount));
		calcGrandTotal();
		return false;
	}
 jQuery(document).on('click','.daterangepicker',function($) {
 startHotelCalc();
return true;

});

</script>