<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Sales Invoices</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'sales/addInvoice'; ?>" style="margin: -5px 6px 1px 0px; padding-right:30px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Sales Invoice </a>
						</div>
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Invoice No</th>
								<th>Invoice Date</th>
								<th>Customer Name</th>
								<th>Email</th>
								<th>Mobile No</th>
								<th>Amount</th>
								<th>Paid</th>
								<th>Balance</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ($invoicelist as $row) 
							{ 
								$balance 	= 	$row['total_amount'] - $row['advance_payment'];
								if($balance == 0){
									$balance = '<span class="label label-success label-mini">Paid</span>';
								}
								else{
									$balance = '<i class="fa fa-fw fa-rupee"></i>'.$balance;
								}
								if($row['email_status'] == 0){
									$emailIcon 	= '<i class="fa fa-envelope-o" style="font-size: 18px;"></i>';
									$title 		= 'Send Email';
								}
								else{
									$emailIcon 	= '<i class="fa fa-envelope" style="font-size: 18px;"></i>';
									$title 		= 'Email Sent';
								}
								
						?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><center><span class="label label-primary"><?php echo $row['invoice_code']; ?></span></center></td>
								<td><?php print(date("jS F, Y", strtotime($row['invoice_date'])));?></td>
								<td><?php echo $row['invoice_to']; ?></td>
								<td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
								<td><?php echo $row['phone']; ?></td>
								<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['total_amount']; ?></td>
								<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['advance_payment']; ?></td>
								<td><?php echo $balance; ?></td>
								<td> <a href="javascript:void(0);" title="View Invoice" onClick = 'window.open("<?php echo base_url().'sales/salesInvoice/'.$row['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> | <a href="<?php echo base_url().'email/sendEmail/'.$row['id']?>" title="<?php echo $title; ?>" ><?php echo $emailIcon; ?></a> | <a href="<?php echo base_url().'sales/editInvoice/'. $row['id']; ?>" title="Edit Invoice" ><i class="fa fa-edit" style="font-size: 18px; color: blue;"></i></a> | <a href="<?php echo base_url().'sales/invoices/delete/'.$row['id']; ?>" title="Delete Invoice" onclick="return confirm('Are you sure you want to delete the Invoice?')"><i class="fa fa-trash-o" style="font-size: 18px; color: red;"></i></a> </td>
							</tr>
						<?php } ?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
