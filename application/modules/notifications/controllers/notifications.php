<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller 
{
	private $pathType = 'notifications';

	function __construct()
	{
		parent::__construct();


		if( !isset( $this->session->userdata['id'] ) )
        {
            $this->session->set_flashdata('nnotification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! You Must login First</div>');
            redirect(base_url().'login', 'refresh');
        }

        $this->load->model('notifications/notifications_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	public function addNotification($id = '')
	{

		$appcode 			= 	$this->session->userdata['appcode'];
		if(is_numeric( $id ))
		{
			$data['notification'] 		= 	$this->notifications_model->getNote($id, $appcode );
		}
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Notifications";
		$data['fileName'] 	= 	"addNotification.php";
		$this->load->view('index', $data);
	}


	public function insertNotification( $param1 = '', $Did = '' )
	{

		$appcode	= 	$this->session->userdata['appcode'];
		$userid     =   $this->session->userdata['id'];

		if( $param1 == 'add' && $_POST )
		{
			$note['user_id'] 		= 	$userid;
			$note['appcode'] 		= 	$appcode;
			$note['note'] 			= 	$_POST['note'];
			$note_date_received 	=   trim($_POST['notify_date']);
			$note_date_object 		= 	new DateTime($note_date_received);
			$note_date				= 	$note_date_object->format('Y-m-d');
			$note['notify_date']	= 	$note_date;
			$note['notify_status']  =   $_POST['notify_status'];
		
			
			$result   =   $this->db->insert( 'my_notifications', $note );

			if( $result )
				$this->session->set_flashdata('nnotification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Note Added Successfully!</div>');
			else
				$this->session->set_flashdata('nnotification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			redirect(base_url().'notifications/notes', 'refresh');
			
		}

		if( $param1=='update' && $_POST )
		{
			$id 		 			=   $_POST['id'];
			$note['appcode'] 		= 	$appcode;
			$note['note'] 			= 	$_POST['note'];
			$note_date_received 	=   trim($_POST['notify_date']);
			$note_date_object 		= 	new DateTime($note_date_received);
			$note_date				= 	$note_date_object->format('Y-m-d');
			$note['notify_date']	= 	$note_date;
			$note['read_status'] 	= 	'Unread';
			$note['notify_status']  =   $_POST['notify_status'];

			$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'my_notifications', $note );
			
			if( $result )
				$this->session->set_flashdata('nnotification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Note Update Successfully!</div>');
			else
				$this->session->set_flashdata('nnotification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				
			redirect(base_url().'notifications/notes', 'refresh');
		}
		
		if($param1 == 'delete')
		{
			
			$noteData['status'] = 'Deleted';
			$this->db->where( array( 'id' => $Did, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'my_notifications', $noteData );
			
			if( $result )
				$this->session->set_flashdata('nnotification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Note Deleted Successfully!</div>');
			else
				$this->session->set_flashdata('nnotification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));

			redirect(base_url().'notifications/notes', 'refresh');
		}

			
	}


	public function notes()
	{
		$appcode			= 	$this->session->userdata['appcode'];
		$userid    		    =   $this->session->userdata['id'];
		$data['notifications'] 		= 	$this->notifications_model->getNotifications( $appcode, $userid );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Notifications";
		$data['fileName'] 	= 	"notes.php";
		$this->load->view( 'index', $data );
	}


	public function viewNote($id = '')
	{
		$appcode    			 	= 	$this->session->userdata['appcode'];
		$userid    		   			=   $this->session->userdata['id'];

		if($id == 'All')
		{
			$data['notification']	=   $this->notifications_model->getNotifications( $appcode,$userid );
		}
		else
		$data['notification']		=   $this->notifications_model->getNotification( $id, $appcode );

		$data['pathType'] 			= 	$this->pathType;
		$data['pageName'] 			= 	"Notifications";
		$data['fileName'] 			= 	"viewNote.php";
		$this->load->view( 'index', $data );

	}


	public function markNote($param1='', $id='')
	{
		$appcode    	= 	$this->session->userdata['appcode'];
		if( $param1 == 'mark' )
		{
			$noteData['read_status'] 	= 	'Read';
			$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'my_notifications', $noteData );
			if( $result )
				$this->session->set_flashdata('nnotification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Note Marked Successfully!</div>');
			else
				$this->session->set_flashdata('nnotification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				
		}
		else
		{
			$noteData['status'] 	= 	'Deleted';
			$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
			$result 	=   $this->db->update( 'my_notifications', $noteData );
			
			if( $result )
				$this->session->set_flashdata('nnotification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Note Deleted Successfully!</div>');
			else
				$this->session->set_flashdata('nnotification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
		}
		
		redirect(base_url() . 'notifications/viewNote/All', 'refresh');
	}
}