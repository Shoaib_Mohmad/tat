<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function getNotifications($appcode, $userid)
    {
        $result     = $this->db->query("SELECT * FROM my_notifications WHERE appcode = '$appcode' AND user_id = $userid AND status = 'Active'")->result_array();
        return $result;
    }


    function getNotification($id, $appcode)
    {
        $result     = $this->db->query("SELECT * FROM my_notifications WHERE id= $id AND appcode = '$appcode' AND status = 'Active'")->result_array();
        return $result;
    }


    function getNote($id, $appcode)
    {
        $result     = $this->db->query("SELECT * FROM my_notifications WHERE id= $id AND appcode = '$appcode' AND status = 'Active'")->row_array();
        return $result;

    }


    function getHeaderNotifications()
    {           
        $appcode    =   $this->session->userdata['appcode'];
        $userid     =   $this->session->userdata['id'];
        $today      =   date('Y-m-d');
        $toDate     =   date('Y-m-d', strtotime("+ 2 days"));
        $notes      =   $this->db->query("SELECT * FROM my_notifications WHERE appcode = '$appcode' AND (notify_date BETWEEN '$today' AND '$toDate') AND (user_id = $userid OR notify_status = 'Broadcast') AND read_status='Unread'  AND status = 'Active'")->result_array();
        return $notes;
    }


}