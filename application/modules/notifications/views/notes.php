<?php echo $this->session->flashdata('nnotification');?>

<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Notifications</h4>
						<span class="tools">
							<div class="btn-group">
								<a href="<?php echo base_url().'notifications/addNotification';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Note</button></a>
							</div>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:5%">#</th>
									<th style="width:45%">Note</th>	
									<th>Notify Date</th>
									<th>Notify Status</th>										
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($notifications as $note) { ?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $note['note']; ?></td>
									<td><?php echo date("jS F, Y", strtotime($note['notify_date'])); ?></td>
									<td><?php echo "<span class='label label-success'>".$note['notify_status']."</span>" ?>
									
									<td>
										<a href="<?php echo base_url().'notifications/addNotification/'. $note['id']?>" title="Edit Note"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> | 
										<a href="<?php echo base_url()?>notifications/insertNotification/delete/<?php echo $note['id']; ?>" title="Delete note" onclick="return confirm('Are you sure you want to delete the note?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
