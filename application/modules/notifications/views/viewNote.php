<?php echo $this->session->flashdata('notification');?>

<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Notifications</h4>
						<span class="tools">
							
						</span>								
					</div>
					<div class="widget-body ">
					<?php foreach ($notification as $data ) {
					?>
		
					
					<div style="width:80%; margin-bottom:10px">
					 <div class="alert alert-block <?php if($data['read_status'] == 'Unread') echo 'alert-info'; else echo 'alert-success'; ?>">
							<p class=""><?php echo $data['note']; ?><br><span class='time' style='float:right;position:relative; font-size: 90%; font-weight: bold;'><i><?php echo date("jS F, Y", strtotime($data['notify_date'])); ?></i></span></p><br>
						</div>
						<?php if($data['read_status'] == 'Unread' && $data['notify_status'] == 'Personal'): ?>
						<a href="<?php echo base_url().'notifications/markNote/mark/'. $data['id']?>" class="btn btn-info">Mark As Read</a>
					<?php endif ?>
					<?php if($data['notify_status'] == 'Personal'): ?>
						<a href="<?php echo base_url().'notifications/markNote/delete/'. $data['id']?>" class="btn btn-danger">Delete</a>
					<?php endif ?>
					
					</div>
						<?php } ?>
					
					</div>

				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
