<?php echo $this->session->flashdata('notification');?>

<div id="page">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN SAMPLE FORM PORTLET-->	
            <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i>Add Notification</h4>
                   <span class="tools">
                 
                   </span>							
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    <form action="<?php if( isset( $notification['id'] ) ) echo base_url().'notifications/insertNotification/update'; else echo base_url().'notifications/insertNotification/add'?>" method="POST" class="form-horizontal">
                     <?php if( isset( $notification['id'] ) ): ?>
                        <input type="hidden" name="id" value="<?php if( isset( $notification['id'] ) ) echo $notification['id']; ?>" />
                    <?php endif; ?>
                       
                        
                        <div class="control-group">
                            <label class="control-label" for="input1">Note/Memo</label>
                            <div class="controls">
                               
                                 <textarea name="note" rows="5" placeholder="Enter Detail" class="span7"><?php if( isset( $notification['id'] ) ) echo $notification['note']; ?></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="input3">Notify Date</label>
                            <div class="controls">
                               <input class="input-small date-picker span2" required size="16"
                                    type="text" placeholder="MM/DD/YY"  value="<?php if( isset( $notification['id'] ) ) echo $notification['notify_date']; else echo date('m/d/Y'); ?>" name="notify_date" id="notify_date" required/>
                            
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input3">Notify Status</label>
                            <div class="controls">
                              <select class="span4"  id="notify_status" name="notify_status" >
                        <option value="Broadcast" <?php if( isset( $notification['notify_status'] ) )
                        { if( $notification['notify_status'] == 'Broadcast') echo 'selected'; } ?> >Broadcast</option>
                         <option value="personal"   
                         <?php 
                         if( isset( $notification['notify_status'] ) )
                         {
                          if( $notification['notify_status'] == 'Personal')
                           echo 'selected';
                            } ?>
                             >Personal</option> 
                              </select>                          
                            </div>
                            
                        </div>
                       
                        <div class="control-group">
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Note</button>
                            </div>
                        </div>
                    </form>
                             <!-- END FORM-->			
                </div>
            </div>
             <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
