<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Passes</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'tours/addPass' ?>" style="margin: -5px 6px 1px 0px; padding-right:30px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Pass </a>
						</div>
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Client Name</th>
								<th>Arrival Date</th>
								<th>Flight No</th>
								<th>Driver Name</th>
								<th>Vehicle No</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ( $passes as $pass ) 
							{ 
								
							?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $pass['client_name']; ?></td>
									<td><?php echo date("jS F, Y", strtotime($pass['arrival_date'])); ?></td>
									<td><?php echo $pass['flight_no']; ?></td>
									<td><?php echo $pass['driver_name']; ?></td>
									<td><?php echo $pass['vehicle_no']; ?></td>
									
									
									<td> 
										<a href="javascript:void(0);" title="View pass" onClick = 'window.open("<?php echo base_url().'passes/viewPass/'.$pass['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> |
										<a href="<?php echo base_url().'passes/editPass/'.$pass['id']; ?>" title="Edit pass"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
										<a href="<?php echo base_url()?>passes/deletePass/delete/<?php echo $pass['id']; ?>" title="Delete Pass" onclick="return confirm('Are you sure you want to delete the pass?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</td>
								</tr>
							<?php  }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
