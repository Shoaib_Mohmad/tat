

<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Edit Pass
					</h4>
					
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form  action="<?php echo base_url();?>passes/updatePass" method="POST" class="form-horizontal">

						<div class="control-group">
							<label class="control-label" for="input1">Arrival Date</label>
							<div class="controls">
								<input class="input-small date-picker span2" required size="16"
									type="text" placeholder="MM/DD/YYYY" name="arrival_date" id="arrival_date" value="<?php echo $pass['arrival_date']; ?>" />
							</div>
						</div>
						
							<input type="hidden" value="<?php echo $pass['id']; ?>" name="id"/>
						<div class="control-group">
							<label class="control-label" for="input1">Client Name</label>
							<div class="controls">
								<input type="text" required placeholder="Enter Client Name" class="span6" id="cname" name="client_name" value="<?php echo $pass['client_name']; ?>" />
                  
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Flight Number</label>
							<div class="controls">
								<input type="text" placeholder="Flight No." class="span3" id="flight_no" name="flight_no" value="<?php echo $pass['flight_no']; ?>" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Driver Name</label>
							<div class="controls">
								<input type="text" placeholder="Driver Name" class="span6" id="cname" name="driver_name" value="<?php echo $pass['driver_name']; ?>" />
								</div>
							</div>
						

						<div class="control-group">
							<label class="control-label">Vehicle Number</label>
							<div class="controls">
								<input type="text" placeholder="Vehicle No." class="span3" id="vehicle" name="vehicle_no" value="<?php echo $pass['vehicle_no']; ?>" />
								</div>
							</div>
						

						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>

</div>
