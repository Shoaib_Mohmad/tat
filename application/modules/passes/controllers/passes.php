<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Passes extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'passes';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('passes');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('passes/passes_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	
	public function index()
	{
    	$appcode			= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Passes";
		$data['fileName'] 	= 	"passes.php";
		$data['passes'] 	= 	$this->passes_model->getpasses( $appcode );
		
		$this->load->view('index', $data);

    }


    public function addPass()
    {
    	$appcode 			=	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Pass";
		$data['fileName'] 	= 	"addPass.php";
		$this->load->view( 'index', $data );
    }


    public function insertPass()
    {

		$appcode					=	$this->session->userdata['appcode'];
		$arrival_date_received  	=   trim($_POST['arrival_date']);
		$arrival_date_object 		= 	new DateTime($arrival_date_received);   
		$arrival_date				= 	$arrival_date_object->format('Y-m-d');		
		$passData['appcode']		=   $appcode;
		$passData['client_name']    =   $_POST['client_name'];
		$passData['arrival_date']   =   $arrival_date;
		$passData['flight_no']      =   $_POST['flight_no'];
		$passData['driver_name']	=   $_POST['driver_name'];
		$passData['vehicle_no'] 	=   $_POST['vehicle_no'];

		$result 	= 	$this->db->insert('passes', $passData); 
		$passId 	= 	$this->db->insert_id();
		
		if( $result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Pass Details Saved Successfully!</div>');
		}
		else
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
		}
		redirect(base_url().'passes', 'refresh');
    }



    public function deletePass( $param1 = '', $param2 = '' )
	{
		$appcode 			=	$this->session->userdata['appcode'];

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$data['status']  	= 	'Deleted';

			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('passes', $data);

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Pass Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'tours/viewPasses', 'refresh'); 
		}
		
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"View Passes";
		$data['fileName'] 	= 	"viewPasses.php";
		$data['passes'] 		= 	$this->passes_model->getpasses( $appcode );
	
		$this->load->view('index', $data);
	}


	public function editPass($param1='')
	{
		$appcode 			=	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Pass";
		$data['fileName'] 	= 	"editPass.php";
		$data['pass'] 	= 	$this->passes_model->getPassDetails( $param1, $appcode );
		
		$this->load->view( 'index', $data );
	}


	public function updatePass()
	{
		$appcode					=	$this->session->userdata['appcode'];
		$arrival_date_received  	=   trim($_POST['arrival_date']);
		$arrival_date_object 		= 	new DateTime($arrival_date_received);   
		$arrival_date				= 	$arrival_date_object->format('Y-m-d');
		$id 						= 	$_POST['id'];		
		$passData['appcode']		=   $appcode;
		$passData['client_name']    =   $_POST['client_name'];
		$passData['arrival_date']   =   $arrival_date;
		$passData['flight_no']      =   $_POST['flight_no'];
		$passData['driver_name']	=   $_POST['driver_name'];
		$passData['vehicle_no'] 	=   $_POST['vehicle_no'];

		$this->db->where( array('id' => $id, 'appcode' => $appcode ) );
		$result 	= 	$this->db->update('passes', $passData);

		if( $result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Pass Update Successfully!</div>');
		}
		else
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
		}

		redirect(base_url().'passes', 'refresh');

	}

	
	public function viewPass( $passId )
    {
        if( !is_numeric( $passId ) )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        
        $this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Sales Invoice');
		$pdf->SetSubject('Sales Invoice');
		$pdf->SetKeywords('OCTA, Travel, Invoice');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate an Invoice!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', 
			'ViewArea' => 'CropBox',
			'ViewClip' => 'CropBox', 
			'PrintArea' => 'CropBox', 
			'PrintClip' => 'CropBox',
			'PrintScaling' => 'AppDefault',
			'Duplex' => 'DuplexFlipLongEdge',
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);
		$pdf->AddPage();

        $pass     =   $this->passes_model->getPassDetails( $passId, $appcode );

       	if( !$pass )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);

		$pdf->SetY(35);
		$pdf->SetFont('helveticab', 'bi', 10);
        
        $pdf->Cell(0, 0, 'Dated :: '.date("jS F, Y", strtotime($pass['arrival_date'])), 0, 0,'R' );
        $pdf->Ln(5);
        
        $pdf->SetFont('times', '', 12);
        $headstr1= "Director of Tourism,";
        $headstr2= "J&K Government ";
        $headstr3= "Srinagar.";
        $pdf->Cell(0, 0, $headstr1, 0, 2 );
        $pdf->Cell(0, 0, $headstr2, 0, 2 );
        $pdf->Cell(0, 0, $headstr3, 0, 2 );
        $pdf->Ln(8);
		$pdf->Cell(0, 0, 'Re.:'.$pass['client_name'], 0, 2 );
	
		$pdf->Cell(150, 20, 'Dear Sir,', 0, 2 );

		$bodystr="                Kindly issue airport permit in our favour to enable us to receive the above named  client/s arriving Srinagar Airport on ".date("jS F, Y", strtotime($pass['arrival_date'])).", by Flight No ".$pass['flight_no'].".";
		$pdf->setCellMargins(0,0,0,0);	

		$pdf->MultiCell(180, 20, $bodystr, 0, 2 );
		//$pdf->Ln(2);
		$pdf->setCellMargins(0,0,20,0);
		$pdf->Cell(100, 6, "Driver Name : ".$pass['driver_name'], 0, 2 );
		$pdf->Cell(100, 6, "Vehicle No    : ".$pass['vehicle_no'], 0, 2 );
				
		$pdf->Ln(5);
		$pdf->Cell(100, 6, "Thankyou", 0, 2 );

		$pdf->Ln(10);
		$pdf->cell(0,5,"Yours Faithfully",0,2,'R');
		$pdf->cell(0,5,$result['company_name'],0,2,'R');
		$pdf->Ln(2);
		$pdf->cell(0,5,"Manager",0,2,'R');
		
		$pdf->AddPage();
		
		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->Ln(40);
		$pdf->setCellMargins(0,30,5,0);
		$pdf->SetFont('times', '', 80);
		$pdf->MultiCell(0, 0,$pass['client_name'].' & Party', 0, 2 );
		
        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Pass_Detail.pdf', 'I');
    }


}

/* End of file passes.php */
/* Location: ./application/controllers/passes.php */
