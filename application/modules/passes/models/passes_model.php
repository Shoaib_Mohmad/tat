<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Passes_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   

    function getPasses($appcode)
    {
        $passes = $this->db->query("SELECT * FROM passes WHERE appcode = '$appcode' AND status = 'Active' ORDER BY id DESC")-> result_array();
        return $passes;
    }
    

    function getPassDetails( $id, $appcode )
    {
        $pass   =   $this->db->query("SELECT * FROM passes WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
        
        return $pass->row_array();
    }


    function getPass( $id, $appcode )
    {
        $pass   =   $this->db->query("SELECT * FROM passes WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
        
        return $pass->row_array();
    }


}