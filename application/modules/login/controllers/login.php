<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 

class Login extends CI_Controller {
	

    function __construct()
    {
        parent::__construct();
        
        //$this->registration_model->checkMacAddress();

        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }



	public function index()
	{
        if( isset($this->session->userdata['level'] ) )
        {
            if( $this->session->userdata['level'] == '7' )
            {
                redirect(base_url().'admin/dashboard', 'refresh');
            }
            else
            {
                redirect(base_url().'user/dashboard', 'refresh');
            }
        }

		$this->load->view('login');
	}

	
	public function checkLogin()
	{
		if($_POST)
		{
			$email 		=   $this->input->post('email');
			$password 	=   md5($this->input->post('password'));
			$query 		=   sprintf("SELECT * FROM users WHERE email = '%s' AND password = '%s' AND status = 1",	mysql_real_escape_string($email), mysql_real_escape_string($password));
			$query 		=   $this->db->query( $query );
			
            if($query->num_rows() == 1)
            {
				$userData 	=    $query -> row_array();
				$this->session->set_userdata('id', $userData['id']);
				$this->session->set_userdata('email', $userData['email']);
				$this->session->set_userdata('level', $userData['level']);
                $this->session->set_userdata('appcode', $userData['appcode']);

                if($userData['level'] == 7)
                {
                    $this->session->set_userdata('name', 'Super Admin');
                    redirect(base_url().'admin/dashboard', 'refresh');
                }

				$id     =   $userData['id'];
				$query 	=   $this->db->query("SELECT user_name FROM profile WHERE user_id = $id") -> row_array();
								
                if( $this->session->userdata['level'] == '1' )
                {
                    if(!empty($query['user_name']))
                    {
                        $this->session->set_userdata('name', $query['user_name']);
                    }
                    else
                    {
                        $this->session->set_userdata('name', $this->session->userdata['email']);    
                    }

                    $this->session->set_userdata('sales', '1');
                    $this->session->set_userdata('expenses', '1');
                    $this->session->set_userdata('reporting', '1');
                    $this->session->set_userdata('hotels', '1');
                    $this->session->set_userdata('transporters', '1');
                    $this->session->set_userdata('inventory', '1');
                    $this->session->set_userdata('customers', '1');
                    $this->session->set_userdata('services', '1');
                    $this->session->set_userdata('passes', '1');
                    $this->session->set_userdata('settings', '1');
                    $this->session->set_userdata('accounts', '1');
                    $this->session->set_userdata('tours', '1');

                    $this->registration_model->adminRegistrationStatus();
                    redirect(base_url().'user/dashboard', 'refresh');
                }

                elseif( ( $this->session->userdata['level'] == '2' ) )
                {
                    $roles  =   $this->accounts_model->getUserRoles( $this->session->userdata['id'], $this->session->userdata['appcode'] );
                    $this->session->set_userdata('name', $roles['name']);

                    $this->session->set_userdata('sales', $roles['sales']);
                    $this->session->set_userdata('expenses', $roles['expenses']);
                    $this->session->set_userdata('reporting', $roles['reporting']);
                    $this->session->set_userdata('hotels', $roles['hotels']);
                    $this->session->set_userdata('transporters', $roles['transporters']);
                    $this->session->set_userdata('inventory', $roles['inventory']);
                    $this->session->set_userdata('customers', $roles['customers']);
                    $this->session->set_userdata('services', $roles['services']);
                    $this->session->set_userdata('passes', $roles['passes']);
                    $this->session->set_userdata('tours', $roles['tours']);
                    
                    $this->registration_model->userRegistrationStatus();
                    redirect(base_url().'user/dashboard', 'refresh');
                }

                else
                {
                    $this->session->set_flashdata('notification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! Invalid Login Credentials</div>');
                    redirect(base_url().'login', 'refresh');
                }
			}
            
			else
            {
                $this->session->set_flashdata('notification', '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Error! Invalid Login Credentials</div>');
                redirect(base_url().'login', 'refresh');
           	}
		}
        
		else
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
	}



	function logout()
    {
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        $this->session->sess_create();
        $this->session->set_flashdata('notification', '<div class="alert alert-success" style="width: 77%; margin-left: 2em;">Bye! Logout Successfully</div>');
        redirect(base_url().'login', 'refresh');
    }
	
    
    function four_zero_four()
    {
		$this->output->set_status_header('404'); 
     	$this->load->view('four_zero_four');
    }

    
    function registerForm()
    {
    	$this->load->view('registerForm');
    }
    
    
    function passwordForm( $id = '' )
    {
    	$userId			= 	$id;
    	$data['id']		=	base64_decode( $id );
    	$this->load->view('registerFormSelectPassword', $data);
    }

    
    function registerationMailCheck()
    {
    	if($_POST)
        {
    		$email			=	 $_POST['email'];
    		$query 		    =    $this->db->query("SELECT * FROM users WHERE email = '$email'");
			if( $query->num_rows() )
            {
                echo '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">This Email ID is already registered with us!</div>';
                return true;
			}
			
            $userdata['email']	=   $email;
			
			//** REGISTER NEW USER IN DATABASE.**//
			$query     =   $this->registration_model->registerNewUser( $userdata );
    		if($query)
            {
    			$this->load->library('email');
    			$this->load->library('parser');
    			
    			$userId = mysql_insert_id();
    			$id		= base64_encode($userId);
    			
    			$this->email->from( 'noreply@myasa.net' );
    			$this->email->to( $email );
    			$this->email->subject('Registration Link');
    			$this->email->set_mailtype("html");
    			$this->email->message( "Thanks for Registration!<br> PLEASE CLICK YOUR ACTIVATION LINK TO GET STARTED... <br><br> <a href='".base_url()."login/passwordForm/$id'> ".base_url()."login/passwordForm/$id</a> <br><br>(If this fails, copy the link and paste in the address bar of your web browser) <br> Kind Regards,<br>OCTA Travel Service Team" );
    			
    			$this->email->send();
    			
                echo '<div class="alert alert-success" style="width: 77%; margin-left: 2em;">A Link for Login has been send to your provided Email Id. Please click on the link to complete the registration process.</div>';
                return true;
    		}
    		else
            {
    			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later!</div>');
         		redirect(base_url().'login/registerForm', 'refresh');
    		}
    	}
    	else
        {
    		redirect(base_url().'login/four_zero_four', 'refresh');
    	}
    }
    

    function registerationResult( $param = '' )
    {
    	$userId		=	$param;
    	
    	if( !is_numeric($userId) )
        {
    		redirect(base_url().'login/four_zero_four', 'refresh');
    	}
        else
        {
    		$query 		= 	$this->db->query("SELECT * FROM users WHERE id = '$userId'")->row_array();
			if( (empty($query) || isset($query['password']) ) )
            {
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>This Link has been Expired!</center></div>');
         		redirect(base_url().'login/registerForm', 'refresh');
			}
            else
            {
        		$query 	= $this->db->query(" UPDATE users SET password = '".md5($_POST['password1'])."', status = '1' WHERE id = '$userId' ");
        		if($query)
                {
                    $dor    = date('Y-m-d');
        			$query 	= $this->db->query("INSERT INTO user_registration (user_id, date_of_registration ) VALUES ( $userId, '".$dor."' )");
        			$query 	= $this->db->query("SELECT * FROM users WHERE id = $userId")->row_array();
        			$this->session->set_userdata('id', $query['id']);
        			$this->session->set_userdata('email', $query['email']);
        			$this->session->set_userdata('level', $query['level']);
                    $this->session->set_userdata('appcode', $query['appcode']);
        			$this->session->set_userdata('name', $this->session->userdata['email']);

        			$this->session->set_userdata('sales', '1');
                    $this->session->set_userdata('expenses', '1');
                    $this->session->set_userdata('reporting', '1');
                    $this->session->set_userdata('hotels', '1');
                    $this->session->set_userdata('transporters', '1');
                    $this->session->set_userdata('inventory', '1');
                    $this->session->set_userdata('customers', '1');
                    $this->session->set_userdata('services', '1');
                    $this->session->set_userdata('settings', '1');
                    $this->session->set_userdata('accounts', '1');
                    $this->session->set_userdata('tours', '1');
                    $this->session->set_userdata('passes', '1');

        			$this->session->set_flashdata('registeration_notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button><center>Hi '.$query['email'].'! Welcome to OCTA Travel<center></div>');
                    redirect(base_url().'user/dashboard', 'refresh');
        		}
        		else
                {
        			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later!</div>');
        			redirect(base_url().'login/registerForm', 'refresh');
        		}
			}
    		redirect(base_url().'login/four_zero_four', 'refresh');
    	}
    }



}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
