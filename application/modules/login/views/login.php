
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="<?php echo base_url();?>css/style.css" rel='stylesheet' type='text/css' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='<?php echo base_url();?>assets/css/google-fonts.css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text.css'/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css" />
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/style_default.css" rel="stylesheet" id="style_color" />
    <script src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>
    <style type="text/css">
        .align-right{
            margin-left: 2em!important;
        }

        .grey-bg{
            background-color: #F3F3F3; 
            border-radius: 5px;
        }
    </style>
    <script type="text/javascript">
    function manageLogin( param )
    {
        $('#login').hide();
        $('#forgotPassword').hide();
        $('#createAccount').hide();
        $( '#'+param ).show();
    }


    function resetPassword()
    {
        var email   =   $('#forgotPasswordEmail').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url()."index.php/email/sendEmail";?>',
            data: { email: email },
            success:function( data )
            {
                document.getElementById('forgotNotification').innerHTML = data;
                $('#forgotPasswordEmail').val('');
            },
            error:function()
            {
                alert('Something went wrong. Please try again after sometime.');
                
            }
        });
        return false; 
    }

    function registerAccount()
    {
        var email   =   $('#registerationEmail').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url()."index.php/login/registerationMailCheck";?>',
            data: { email: email },
            success:function( data )
            {
                document.getElementById('registerNotification').innerHTML = data;
                $('#registerationEmail').val('');
            },
            error:function()
            {
                alert('Something went wrong. Please try again after sometime.');
                
            }
        });
        return false; 
    }
    </script>
</head>
<body>
    <div class="main">
        <div class="header" >
            <h1>OCTA Travel | User Login | Create a Free Account!</h1>
            <p>We encourage BUSINESS, Innovation & Flexibility, Quick & High Results - Oriented WORK. </p>
        </div>
        
      
        <ul class="left-form">
            <h2>Features</h2>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Secure Invoicing</h5>
                <span style="padding-left:2.5em;">Create transactions at any stage of the sales cycle.</span>
            </li>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Quick Reporting</h5>
                <span style="padding-left:2.5em;">Reporting on every aspect of your business.</span>
            </li>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Manage Inventory</h5>
                <span style="padding-left:2.5em;">Manage Rooms, Bookings, Ticketing and many more.</span>
            </li>
            <input onclick="manageLogin('createAccount');" type="submit" value="Create Account">
            <div class="clear"> </div>
        </ul>

       
        <ul class="right-form grey-bg" id="login">
            <form id="loginform" class="form-vertical no-padding no-margin" action="<?php echo base_url().'login/checklogin';?>" method="POST">
                <h3 class="align-right">Login</h3>
                <br>
                <?php echo $this->session->flashdata('notification');?>
                <div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="email" required placeholder="Enter Email" class="span5" name="email" />
                    </div>
                </div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="password" required placeholder="Enter Password" class="span5" name="password" />
                    </div>
                </div>
                <h4 class="align-right"><a onclick="manageLogin('forgotPassword');" href="javascript: void(0);"> I forgot my Password!</a></h4><br>
                <div class="control-group pull-right">
                    <div class="controls">
                        <input type="submit" value="Login" >
                    </div>
                </div>
            </div>
        </form>
        <div class="clear"> </div>
    </ul>

    <ul class="right-form grey-bg" id="forgotPassword" style="display:none;">
        <form onSubmit="return resetPassword();" id="loginform" class="form-vertical no-padding no-margin" action="javascript: void(0);" method="POST">
            <br>
            <h5 class="align-right">Enter your E-Mail address below to reset your password</h5>
            <div class="clear"> </div>
            <br>
            <div id="forgotNotification"></div>
            <div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="email" required placeholder="Enter Your Registered Email Address" class="span5" name="email" id="forgotPasswordEmail" />
                    </div>
                </div>
                
                <h4 class="align-right"><a onclick="manageLogin('login');" href="javascript: void(0);"> LogIn!</a></h4><br>
                <div class="control-group pull-right">
                    <div class="controls">
                        <input type="submit" value="Reset" >
                    </div>
                </div>
            </div>
        </form>
        <div class="clear"> </div>
    </ul>


    <ul class="right-form grey-bg" id="createAccount" style="display:none;">
        <form onSubmit="return registerAccount();" id="loginform" class="form-vertical no-padding no-margin" action="javascript: void(0);" method="POST">
            <br>
            <h5 class="align-right">Enter your E-Mail address to Register with Us</h5>
            <div class="clear"> </div>
            <br>
            <div id="registerNotification"></div>
            <div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="email" required placeholder="Enter Your Valid Email Address" class="span5" name="email" id="registerationEmail" />
                    </div>
                </div>
                
                <h4 class="align-right"><a onclick="manageLogin('login');" href="javascript: void(0);"> I've already an account!</a></h4><br>
                <div class="control-group pull-right">
                    <div class="controls">
                        <input type="submit" value="Register Me" >
                    </div>
                </div>
            </div>
        </form>
        <div class="clear"> </div>
    </ul>

    <div class="clear"> </div>
    
</div>

<div class="copy-right">
    <p>OCTA Travel &copy; 2015 | <a href="http://www.octasoft.biz">OCTASOFT Solutions Pvt Ltd</a></p> 
</div>

</body>
</html>