<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Select Date Range</h4>
            </div>
            <div class="widget-body form">
            <form action="<?php echo base_url().'reports/generateHotelVouchersReport';?>" method="POST" class="form-horizontal">
               <!-- BEGIN FORM-->
              <div class="form-horizontal">
                  <div class="control-group">
                     <label class="control-label">Date</label>
                     <div class="controls">
                        <div class="input-prepend">
                           <span class="add-on"><i class="fa fa-calendar"></i></span><input type="text" name="date" required placeholder="<?php echo date( 'm/d/Y' ); ?>" class="input-medium date-range" />
                        </div>
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label">Select hotel</label>
                     <div class="controls">
                        <div class="input-prepend">
                           <select name="hotel_id">
                              <option value="all">All Hotels</option>
                              <?php foreach ($hotels as $hotel) : ?>
                                 <option value="<?php echo $hotel['id']; ?>"><?php echo $hotel['hotel_name']; ?></option>
                              <?php endforeach; ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Generate Report</button>
                  </div>
               </div>
               </form>
               <!-- END FORM-->	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>
<?php
   if($reportStatus):
      if( !isset($totalamount) )
      {
         $balance = 0;
         $totalamount = 0;
         $billsamount = 0;
      }
?>                    
      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
            </div><br><br>
            <div class="widget-body form">
               <p style="margin-left:2em; margin-top:-5px; font-weight:bold;">
                  <th  class="hidden-phone sorting">Total Amount</th>
                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $total_amount;?></span>
                  <span class="actions">
                 
                  </span>  
               </p>
               <br><br><br>
               <form action="<?php echo base_url().'reports/pdfHotelVouchersTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="margin-top:0em;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
               </form>

               <form action="<?php echo base_url().'reports/excelHotelVouchersTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="float:right;margin-top:-2em;" class="btn btn-primary"><i class="fa fa-download"></i> Excel</button>
               </form>
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>
              
             
<?php
   endif;
   if($reportStatus):
?>              
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
               <div class="widget-title">
                  <h4><i class="icon-reorder"></i>Hotel Vouchers</h4>
                  <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                  <a href="javascript:;" class="icon-refresh"></a>      
                  <a href="javascript:;" class="icon-remove"></a>
                  </span>                    
               </div>
               <div class="widget-body">
                  <table class="table table-striped table-bordered" id="sample_1">
                     <thead>
                        <tr>
                           <th style="width:8px">#</th>
                           <th>Voucher No</th>
                           <th>Voucher Date</th>
                           <th>Hotel Name</th>
                           <th>Customer Name</th>
                           <th>Customer Email</th>
                           <th>Customer Mobile</th>
                           <th>Total Amount</th>
                        </tr>
                     </thead>
                     <tbody>
                     
                     <?php 
                           $count = 1;
                           foreach ($hotelVouchers as $voucher) 
                           { 
                              $total   =  $this->services_model->getVoucherHotelServicesTotal( $voucher['id'] );
                           ?>
                        <tr class="odd gradeX">
                           <td><?php echo $count++; ?></td>
                           <td><center><span class="label label-primary"><?php echo $voucher['voucher_no']; ?></span></center></td>
                           <td><?php print(date("jS F, Y", strtotime($voucher['voucher_date'])));?></td>
                           <td><?php echo $voucher['hotel_name']; ?></td>
                           <td><?php echo $voucher['customer_name']; ?></td>
                           <td><a href="mailto:<?php echo $voucher['customer_email']; ?>"><?php echo $voucher['customer_email']; ?></a></td>
                           <td><?php echo $voucher['customer_mobile']; ?></td>
                           <td><i class="fa fa-fw fa-rupee"></i><?php echo $total; ?></td>
                        </tr>
                     <?php } ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
         </div>
      </div>		
<?php
   endif;
?>         
     
