<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Sales Report</h4>
            </div>
            <div style="padding-top:3em; padding-bottom:3em;" class="widget-body form">
               <center><a href="<?php echo base_url().'reports/salesReport'; ?>">
                  <button class="btn btn-large btn-primary" type="button">Generate Sales Report</button>
               </a></center>	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Expenses Report</h4>
            </div>
            <div style="padding-top:3em; padding-bottom:3em;" class="widget-body form">
               <center><a href="<?php echo base_url().'reports/expensesReport';?>">
                  <button class="btn btn-large btn-primary" type="button">Generate Expenses Report</button>
               </a></center>  
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>

   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Hotel Vouchers Report</h4>
            </div>
            <div style="padding-top:3em; padding-bottom:3em;" class="widget-body form">
               <center><a href="<?php echo base_url().'reports/hotelVouchersReport'; ?>">
                  <button class="btn btn-large btn-primary" type="button">Hotel Vouchers Report</button>
               </a></center>  
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Transport Vouchers Report</h4>
            </div>
            <div style="padding-top:3em; padding-bottom:3em;" class="widget-body form">
               <center><a href="<?php echo base_url().'reports/transportVouchersReport';?>">
                  <button class="btn btn-large btn-primary" type="button">Transport Vouchers Report</button>
               </a></center>  
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>
              
     
