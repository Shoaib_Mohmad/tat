<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Select Date Range</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>		
               <a href="javascript:;" class="icon-remove"></a>
               </span>							
            </div>
            <div class="widget-body form">
            <form action="<?php echo base_url().'reports/generateSalesReport';?>" method="POST" class="form-horizontal">
               <!-- BEGIN FORM-->
              <div class="form-horizontal">
                  <div class="control-group"  style="min-height: 55px;">
                     <label class="control-label">Date</label>
                     <div class="controls">
                        <div class="input-prepend">
                           <span class="add-on"><i class="fa fa-calendar"></i></span><input type="text" name="date" required placeholder="<?php echo date( 'm/d/Y' ); ?>" class="input-medium date-range" />
                        </div>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Generate Report</button>
                  </div>
               </div>
               </form>
               <!-- END FORM-->	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>
<?php
   if($reportStatus):
      if( !isset($totalamount) )
      {
         $balance = 0;
         $totalamount = 0;
         $billsamount = 0;
      }
?>                    
      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body form">
               <p style="margin-left:2em; margin-top:-5px; font-weight:bold;">
                  <th  class="hidden-phone sorting">Total Amount</th>
                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $total_amount;?></span>
                  <span class="actions">
                 
                  </span>  
               </p>
               <p style="margin-left:2em; margin-top:-5px; font-weight:bold;">
                  <th  class="hidden-phone sorting">Amount Paid</th>
                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $amount_paid;?></span>
                  <span class="actions">
                 
                  </span>  
               </p>
               <p style="margin-left:2em; margin-top:-5px; font-weight:bold;">
                  <th  class="hidden-phone sorting">Balance</th>
                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php $balance = $total_amount - $amount_paid; if( $balance ) echo $balance; else echo "Nill";?></span>
                  <span class="actions">
                 
                  </span>  
               </p>
               <p style="margin-left:2em; margin-top:-5px; font-weight:bold;">
                  <th  class="hidden-phone sorting">Profit</th>
                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $total_profit;?></span>
                  <span class="actions">
                 
                  </span>  
               </p>
               <form action="<?php echo base_url().'reports/pdfServicesInvoiceTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="margin-top:0em;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
               </form>

               <form action="<?php echo base_url().'reports/excelServicesInvoiceTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="float:right;margin-top:-2em;" class="btn btn-primary"><i class="fa fa-download"></i> Excel</button>
               </form>
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>
              
             
<?php
   endif;
   if($reportStatus):
?>              
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
               <div class="widget-title">
                  <h4><i class="icon-reorder"></i>Sales Invoices</h4>
                  <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                  <a href="javascript:;" class="icon-refresh"></a>      
                  <a href="javascript:;" class="icon-remove"></a>
                  </span>                    
               </div>
               <div class="widget-body">
                  <table class="table table-striped table-bordered" id="sample_1">
                     <thead>
                        <tr>
                           <th style="width:8px">#</th>
                           <th class="hidden-phone">Invoice Code</th>
                           <th class="hidden-phone">Invoice Date</th>
                           <th>Customer Name</th>
                           <th class="hidden-phone">Email</th>
                           <th class="hidden-phone">Phone No</th>
                           <th class="hidden-phone">Total Amount</th>
                           <th class="hidden-phone">Amount Paid</th>
                           <th class="hidden-phone">Balance</th>
                           <th class="hidden-phone">Profit</th>
                        </tr>
                     </thead>
                     <tbody>
                     
                     <?php 
                           $count = 1;
                           foreach ($invoicelist as $row) 
                           { 
                              $invoiceId  =  $row['id'];
                              $sum        =  $this->db->query("SELECT SUM(total) FROM services WHERE invoice_id = $invoiceId")-> row_array();
                              $total      =  $row['total_amount'];
                              $balance    =  $total - $row['advance_payment'];

                              $sum        =  $this->db->query("SELECT SUM(cost) as actualCost FROM services WHERE invoice_id = $invoiceId")-> row_array();
                              $profit     =  $total - ( $row['accomodation_amount'] + $row['transportation_amount'] + $sum['actualCost']);
                              if($balance == 0)
                              {
                                 $balance = 'Nill';
                              }
                              else
                              {
                                 $balance = '<i class="fa fa-fw fa-rupee"></i>'.$balance;
                              }
                              if($row['email_status'] == 0)
                              {
                                 $emailIcon  = 'email_open.png';
                                 $title      = 'Send Email';
                              }
                              else
                              {
                                 $emailIcon = 'email_accept.png';
                                 $title      = 'Email Sent';
                              }
                              if($row['advance_payment'] == 0)
                              {
                                 $advance_payment = 'Nill';
                              }
                              else
                              {
                                 $advance_payment = '<i class="fa fa-fw fa-rupee"></i>'.$row['advance_payment'];
                              }
                           ?>
                        <tr class="odd gradeX">
                           <td><?php echo $count++; ?></td>
                           <td><center><span class="label label-primary"><?php echo $row['invoice_code']; ?></span></center></td>
                           <td><?php print(date("jS F, Y", strtotime($row['invoice_date'])));?></td>
                           <td><?php echo $row['invoice_to']; ?></td>
                           <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
                           <td><?php echo $row['phone']; ?></td>
                           <td><i class="fa fa-fw fa-rupee"></i><?php echo $total; ?></td>
                           <td><?php echo $advance_payment;?></td>
                           <td><?php echo $balance; ?></td>
                           <td><?php echo $profit; ?></td>
                        </tr>
                     <?php } ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
         </div>
      </div>		
<?php
   endif;
?>         
     
