<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<!-- Date Range Pickers -->
	<div class="row-fluid">
		<div class="span6">
			<!-- BEGIN SEARCH BY DATE RANGE-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Select Date Range
					</h4>
					<span class="tools"> <a href="javascript:;"
						class="icon-chevron-down"></a> <a href="#widget-config"
						data-toggle="modal" class="icon-wrench"></a> <a
						href="javascript:;" class="icon-refresh"></a> <a
						href="javascript:;" class="icon-remove"></a>
					</span>
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form
						action="<?php echo base_url()?>reports/expensesReport/search"
						method="POST">
						<div class="form-horizontal">
							<div class="control-group">
								<label class="control-label">Date</label>
								<div class="controls">
									<div class="input-prepend">
										<span class="add-on"><i class="fa fa-calendar"></i> </span><input
											type="text" name="findDate" required placeholder="<?php echo date( 'm/d/Y' ); ?>"
											class="input-large date-range" />
									</div>
								</div>
							</div>

							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Generate Report</button>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SEARCH BY DATE RANGE-->
		</div>
<?php
	if($search):
?>
	  <?php 
	  		if( !isset($totalamount) ){
	  			$balance = 0;
	  			$totalamount = 0;
	  			$billsamount = 0;
	  		}
			if($balance == 0){
				$balance = '<strong>Nill</strong>';
			}
			else{
				$balance = '<i class="fa fa-fw fa-rupee"></i> '.$balance;
			}
		?>
		<div class="span6">
			<!-- BEGIN GENERATE AS PDF-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i><?php echo $heading;?>
					</h4>
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<div class="form-horizontal">
						<div class="control-group">
							 	<p style="margin-left:2em; font-weight:bold;">
				                  <th  class="hidden-phone sorting">Total Amount </th>
				                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $totalamount;?></span>
				                   
				               	</p>
				               	<p style="margin-left:2em; font-weight:bold;">
				                  <th  class="hidden-phone sorting">Total Bill Amount Paid</th>
				                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><i class="fa fa-fw fa-rupee"></i><?php echo $billsamount;?></span>
				                    
				               	</p>
				               	<!--  balance -->
				               	<p style="margin-left:2em; font-weight:bold;">
				                  <th  class="hidden-phone sorting">Balance</th>
				                  <span style="float:right;" class="label label-reverse"><i class="icon-bell"></i><?php echo $balance;?></span>
				                    
				               	</p>
							<form action="<?php echo base_url().'reports/pdfBillTable';?>" method="POST">
								<input type="hidden" name="query" value="<?php echo $query;?>">
								<input type="hidden" name="heading" value="<?php echo $heading;?>">
								<button type="submit" style="margin-top:0em;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
							</form>

							<form action="<?php echo base_url().'reports/excelBillTable';?>" method="POST">
								<input type="hidden" name="query" value="<?php echo $query;?>">
								<input type="hidden" name="heading" value="<?php echo $heading;?>">
								<button type="submit" style="float:right;margin-top:-2em;" class="btn btn-primary"><i class="fa fa-download"></i> Excel</button>
							</form>
						</div>

					</div>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END GENERATE AS PDF-->
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="widget">
			<div class="widget-title">
				<h4>
					<i class="icon-reorder"></i>Expenses 
				</h4>
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th style="width: 8px">#</th>
							<th>Bill Name</th>
							<th>Bill No</th>
							<th>Paid To</th>
							<th>Payment Date</th>
							<th>Payment Mode</th>
							<th>Ref. No.</th>
							<th>Total Amount</th>
							<th>Amount Paid</th>
							<th>Balance</th>

						</tr>
					</thead>
					<tbody>
						<?php 
						$count = 1;//
						foreach ($billslist as $row) { //invoicelist comes from controller,
						$balance = $row['total_amount'] - $row['amount_paid'];
						if($balance == 0){
							$balance = '<strong>Nill</strong>';
						}
						else{
							$balance = '<i class="fa fa-fw fa-rupee"></i> '.$balance;
						}
						?>
						<tr class="odd gradeX">
							<td><?php echo $count++; ?></td>
							<td><?php echo $row['bill_type']; ?></td>
							<td><?php echo $row['bill_number']; ?></td>
							<td><?php echo $row['paid_to']; ?></td>
							<td><?php print(date("jS F, Y", strtotime($row['payment_date'])));?></td>
							<td><?php echo $row['payment_mode']; ?></td>
							<td><?php echo $row['reference_no']; ?></td>
							<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['total_amount']; ?></td>
							<td><i class="fa fa-fw fa-rupee"></i><?php echo $row['amount_paid']; ?></td>
							<td><?php echo $balance; ?></td>
						</tr>
						<?php } ?>
					</tbody>

				</table>
				
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
<?php
	endif;
?>
</div>
<!-- END PAGE CONTAINER-->



