<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Select Date Range</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>		
               <a href="javascript:;" class="icon-remove"></a>
               </span>							
            </div>
            <div class="widget-body form">
            <form action="<?php echo base_url().'reports/generateBookingsReport';?>" method="POST" class="form-horizontal">
               <!-- BEGIN FORM-->
              <div class="form-horizontal">
                  <div class="control-group">
                     <label class="control-label">Date</label>
                     <div class="controls">
                        <div class="input-prepend">
                           <span class="add-on"><i class="fa fa-calendar"></i></span><input type="text" name="date" required placeholder="<?php echo date( 'm/d/Y' ); ?>" class="input-medium date-range" />
                        </div>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Generate Report</button>
                  </div>
               </div>
               </form>
               <!-- END FORM-->	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>
<?php
   if($reportStatus):
      
?>                    
      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body form">
               <br><br>
               <form action="<?php echo base_url().'reports/pdfBookingsInvoiceTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="margin-top:0em;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
               </form>

               <form action="<?php echo base_url().'reports/excelBookingsInvoiceTable';?>" method="POST">
                  <input type="hidden" name="query" value="<?php echo $query;?>">
                  <input type="hidden" name="heading" value="<?php echo $heading;?>">
                  <button type="submit" style="float:right;margin-top:-2em;" class="btn btn-primary"><i class="fa fa-download"></i> Excel</button>
               </form>
               <br><br>
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>
              
             
<?php
   endif;
   if($reportStatus):
?>              
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
               <div class="widget-title">
                  <h4><i class="icon-reorder"></i>Bookings Report</h4>
                  <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                  <a href="javascript:;" class="icon-refresh"></a>      
                  <a href="javascript:;" class="icon-remove"></a>
                  </span>                    
               </div>
               <div class="widget-body">
                  <table class="table table-striped table-bordered" id="sample_1">
                     <thead>
                        <tr>
                           <th style="width:8px">#</th>
                           <th>Room Category</th>
                           <th>Rooms Booked</th>
                           <th>Booked By</th>
                           <th>Email</th>
                           <th>Mobile No</th>
                           <th>CheckIn</th>
                           <th>CheckOut</th>
                           <th>Booking Date</th>
                        </tr>
                     </thead>
                     <tbody>
                     
                     <?php 
                           $count = 1;
                           foreach ($bookings as $booking) 
                           { 
                              $customer = $this->inventory_model->getCustomerData( $booking['customer_id'], $booking['appcode'] );
                           ?>
                        <tr class="odd gradeX">
                           <td><?php echo $count++; ?></td>
                           <td><?php echo $booking['category_name']; ?></td>
                           <td style="text-align: center;"><?php echo $booking['no_of_rooms']; ?></td>
                           <td><?php echo $customer['name']; ?></td>
                           <td><a href="mailto:<?php echo $customer['email']; ?>"><?php echo $customer['email']; ?></a></td>
                           <td><?php echo $customer['phone']; ?></td>
                           <td><?php print(date("jS F, Y", strtotime($booking['checkInDate'])));?></td>
                           <td><?php print(date("jS F, Y", strtotime($booking['checkOutDate'])));?></td>
                           <td><?php print(date("jS F, Y", strtotime($booking['createdAt'])));?></td>
                        </tr>
                     <?php } ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
         </div>
      </div>		
<?php
   endif;
?>         
     
