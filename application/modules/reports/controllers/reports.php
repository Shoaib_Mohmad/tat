<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

    private $registrationStatus;
    private $pathType = 'reports';
    
    function __construct()
	{
	    parent::__construct();

        $this->user_model->checkUserCanAccess('reporting');
        
        if( $this->session->userdata['level'] == 1 )
        {
            $this->registrationStatus = $this->registration_model->adminRegistrationStatus();

            if( !$this->user_model->isProfileUpdated() )
            {
                redirect(base_url().'settings/profile', 'refresh');
            }
        }
        else
        {
            $this->registrationStatus = $this->registration_model->userRegistrationStatus();
        } 

        $this->load->model('sales/sales_model'); 
        $this->load->model('expenses/expenses_model');
        $this->load->model('hotels/hotels_model');
        $this->load->model('transporters/transporters_model'); 
         $this->load->model('inventory/inventory_model'); 
      
	    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	    $this->output->set_header('Pragma: no-cache');
	}


	function index()
    {
		if( isset($this->session->userdata['email']) )
        {
			$this->session->unset_userdata('invoiceQuery');
			$this->session->unset_userdata('billQuery');
			$data = array();
			$id 				= 	$this->session->userdata['id'];
			$data['pathType'] 	= 	$this->pathType;
			$data['pageName'] 	= 	"Reports";
			$data['fileName'] 	= 	"generateReport.php";
			$this->load->view('index', $data);
		}
		else
        {
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>You Must Login First!</div>');
         	redirect(base_url().'login', 'refresh');
        }
	}


	function salesReport()
    { 		
		if( isset($this->session->userdata['email']) )
        {
            if( !$this->registrationStatus )
            {
                redirect(base_url().'report/', 'refresh');
            }
			$data = array();
			$data['pathType']    =   $this->pathType;
			$data['pageName']    =   "Sales Report";
			$data['fileName']    =   "salesReport.php";
			$data['reportStatus'] = 0;
			$this->load->view('index', $data);
		}
		else
        {
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>You Must Login First!</div>');
			redirect(base_url().'login', 'refresh');
		}
		
	}

	
    function generateSalesReport()
    {
		if( ! isset($_POST['date']) )
        {
			redirect(base_url().'report/salesReport', 'refresh');
		}
		if( $_POST )
        {
			$data = array();
			$appcode	=	$this->session->userdata('appcode');
			$date 		= 	explode('-', $_POST['date']);
			$dateFrom 	= 	new DateTime($date[0]);
			$dateFrom	= 	$dateFrom->format('Y-m-d');
			$dateTo 	= 	new DateTime($date[1]);
			$dateTo		= 	$dateTo->format('Y-m-d');

            $data['invoicelist']    =   $this->db->query("SELECT * FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND status = 'Active' ORDER BY id ASC")-> result_array();

            $data['total_amount']   =   $this->db->query("SELECT SUM(total_amount) as totalAmount FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND status = 'Active'")-> row_array();
            $data['total_amount']   =   $data['total_amount'] ['totalAmount'];

            $temp1   =   $this->db->query("SELECT SUM(accomodation_amount) as accomodation_amount FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND status = 'Active'")-> row_array();

            $temp2   =   $this->db->query("SELECT SUM(transportation_amount) as transportation_amount FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND status = 'Active'")-> row_array();

            $temp3   =   $temp1['accomodation_amount'] + $temp2['transportation_amount'];
            $temp4   =   $this->db->query("SELECT SUM( services.cost ) as servicesSum FROM services INNER JOIN services_invoices ON services_invoices.id = services.invoice_id WHERE services_invoices.appcode = '$appcode' AND DATE( services_invoices.invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND services_invoices.status = 'Active'")-> row_array();

            $data['total_profit']   =   $data['total_amount'] - ($temp3 + $temp4['servicesSum']);
			
			$data['amount_paid'] 	=  	$this->db->query("SELECT SUM(advance_payment) FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' AND status = 'Active'")-> row_array();
			$data['amount_paid'] 	=  	$data['amount_paid']['SUM(advance_payment)'];
			
			
            
            $data['pathType']       =   $this->pathType;
			$data['pageName']       =   "Sales Report";
			$data['fileName']       =   "salesReport.php";
			$data['reportStatus']   =   1;
			$data['query'] = "SELECT * FROM services_invoices WHERE appcode = '$appcode' AND DATE( invoice_date ) BETWEEN '$dateFrom' AND '$dateTo' ORDER BY id ASC";
            $data['heading'] = "Sales Report From $dateFrom To $dateTo";

			$this->load->view('index', $data);
		}
		else
        {
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	function pdfServicesInvoiceTable()
    {

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Sales Report');
        $pdf->SetSubject('OCTA Travel Sales Report');
        $pdf->SetKeywords('OCTA Travel, Sales Report');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Sales Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('L');
        $HeaderText = $_POST['heading'];
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);

        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl 	= ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
	                    <th  style="text-align: center;"> S NO              </th>
	                    <th style="text-align: center;"> INVOICE CODE   </th>
	                    <th> INVOICE DATE   </th>
	                    <th> CUSTOMER NAME  </th>
	                    <th> EMAIL  		</th>
	                    <th style="text-align: center;"> CONTACT NO       </th>
	                    <th style="text-align: center;"> TOTAL AMOUNT   </th>
	                    <th style="text-align: center;"> AMOUNT PAID    </th>
	                    <th style="text-align: center;"> BALANCE       	</th>
                        <th style="text-align: center;"> PROFIT         </th>
            		</tr>';
        $query 		= 	$_POST['query'];
        $result 	=  	$this->db->query( $query )-> result_array();

        $count = 1;   
        $total_amount       = "";
        $totalamount_paid   = "";
        $totalProfit        = "";

        foreach ($result as $row ) 
        {
              $total      =  $row['total_amount'];
              $balance    =  $total - $row['advance_payment'];

              $sum        =  $this->db->query("SELECT SUM(cost) as actualCost FROM services WHERE invoice_id = ".$row['id']."")-> row_array();
              $profit     =  $total - ( $row['accomodation_amount'] + $row['transportation_amount'] + $sum['actualCost']);
              if($balance == 0)
              {
                 $balance = 'Nill';
              }
              else
              {
                 $balance = $balance.' INR';
              }
             
             if($row['advance_payment'] == 0)
             {
                $advance_payment = 'Nill';
             }
             else
             {
                $advance_payment = $row['advance_payment'].' INR';
             } 
             
            $total_amount       =  $total_amount + $total;
            $totalamount_paid   =  $totalamount_paid + $row['advance_payment'];
            $totalProfit        =  $totalProfit + $profit;
            $tbl .= '
                <tr style="font-size: 12px;">
                    <td style="text-align: center;">  '.$count++ .' </td>
                    
                    <td style="text-align: center;">'.$row['invoice_code'].'</td>
                    
                    <td> '.date("jS F, Y", strtotime($row['invoice_date'])).'</td>

                    <td> '.$row['invoice_to'].'   </td>
                    
                    <td>'.$row['email'].'</td>

                    <td style="text-align: center;">'.$row['phone'].'</td>

                    <td  style="text-align: center;">'.$total.' INR</td>
                    
                    <td  style="text-align: center;">'.$advance_payment.'</td>
                    
                    <td  style="text-align: center;">'.$balance.'</td>

                    <td  style="text-align: center;">'.$profit.' INR</td>
                </tr>
            ';
            $total_balance += $balance;
        }    
            
        if( !$total_balance )
            $total_balance = 'Nill';
        else
            $total_balance .= ' INR';
        $tbl_footer = '
            <tr style="font-weight: bold;">
                <td>   </td>
                
                <td></td>
                
                <td> </td>

                <td> </td>
                
                <td></td>

                <td></td>

                <td  style="text-align: center;">'.$total_amount.' INR</td>
                
                <td  style="text-align: center;">'.$totalamount_paid.' INR</td>
                
                <td  style="text-align: center;">'.$total_balance.'</td>

                <td  style="text-align: center;">'.$totalProfit.' INR</td>
            </tr>
        </table>
        ';
      

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Sales_Report.pdf', 'D');

    }


    function excelServicesInvoiceTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        //load our new PHPExcel library
        $this->load->library('Excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Invoice Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $_POST['heading']);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:J1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'S.No'); 
        $this->excel->getActiveSheet()->setCellValue('B2', 'Invoice Code'); 
        $this->excel->getActiveSheet()->setCellValue('C2', 'Invoice Date'); 
        $this->excel->getActiveSheet()->setCellValue('D2', 'Customer Name'); 
        $this->excel->getActiveSheet()->setCellValue('E2', 'Email'); 
        $this->excel->getActiveSheet()->setCellValue('F2', 'Phone No'); 
        $this->excel->getActiveSheet()->setCellValue('G2', 'Total Amount'); 
        $this->excel->getActiveSheet()->setCellValue('H2', 'Amount Paid'); 
        $this->excel->getActiveSheet()->setCellValue('I2', 'Balance'); 
        $this->excel->getActiveSheet()->setCellValue('J2', 'Profit'); 
        
        $this->excel->getActiveSheet()->getStyle('A2:J2')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A2:J2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $query  =   $_POST['query'];
        $result =   $this->db->query( $query )-> result_array();
        $count  = 1;
        $cell   = 3;  
        //print_r($result); die;
       
        $total_amount = "";
        $amount_paid  = "";
        $totalProfit  = "";
        foreach ($result as $row ) 
        {
            $total      =  $row['total_amount'];
              $balance    =  $total - $row['advance_payment'];

              $sum        =  $this->db->query("SELECT SUM(cost) as actualCost FROM services WHERE invoice_id = ".$row['id']."")-> row_array();
              $profit     =  $total - ( $row['accomodation_amount'] + $row['transportation_amount'] + $sum['actualCost']);
              if($balance == 0)
              {
                 $balance = 'Nill';
              }
              else
              {
                 $balance = $balance.' INR';
              }
             
             if($row['advance_payment'] == 0)
             {
                $advance_payment = 'Nill';
             }
             else
             {
                $advance_payment = $row['advance_payment'].' INR';
             } 
             
            $total_amount       =  $total_amount + $total;
            $amount_paid        =  $amount_paid + $row['advance_payment'];
            $totalProfit        =  $totalProfit + $profit;
        
            $this->excel->getActiveSheet()->setCellValue('A'.$cell, $count); 
            $this->excel->getActiveSheet()->setCellValue('B'.$cell, 'OSB-'.$row['invoice_code']); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell, date("jS F, Y", strtotime($row['invoice_date'])));  
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, $row['invoice_to']); 
            $this->excel->getActiveSheet()->setCellValue('E'.$cell, $row['email']); 
            $this->excel->getActiveSheet()->setCellValue('F'.$cell, $row['phone']); 
            $this->excel->getActiveSheet()->setCellValue('G'.$cell, $total); 
            $this->excel->getActiveSheet()->setCellValue('H'.$cell, $advance_payment); 
            $this->excel->getActiveSheet()->setCellValue('I'.$cell, $balance);
            $this->excel->getActiveSheet()->setCellValue('J'.$cell, $profit);
            $this->excel->getActiveSheet()->getStyle('A'.$cell.':J'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $cell++;
            $count++;
        }  

        $total_balance = $total_amount - $amount_paid;
        if( !$total_balance )
            $total_balance = 'Nill';

        $this->excel->getActiveSheet()->setCellValue('G'.$cell, $total_amount); 
        $this->excel->getActiveSheet()->setCellValue('H'.$cell, $amount_paid); 
        $this->excel->getActiveSheet()->setCellValue('I'.$cell, $total_balance );
        $this->excel->getActiveSheet()->setCellValue('J'.$cell, $totalProfit );
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':J'.$cell)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':J'.$cell)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':J'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $sheet = $this->excel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) 
        {
            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }
        $filename='Sales_Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
       
       
    }



    function bookingsReport()
    {       
        if( isset($this->session->userdata['email']) )
        {
            if( !$this->registrationStatus )
            {
                redirect(base_url().'report/', 'refresh');
            }
            $data = array();
            $data['pathType']    =   $this->pathType;
            $data['pageName']    =   "Bookings Report";
            $data['fileName']    =   "bookingsReport.php";
            $data['reportStatus'] = 0;
            $this->load->view('index', $data);
        }
        else
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>You Must Login First!</div>');
            redirect(base_url().'login', 'refresh');
        }
        
    }

    
    function generateBookingsReport()
    {
        if( ! isset($_POST['date']) )
        {
            redirect(base_url().'report/salesReport', 'refresh');
        }
        if( $_POST )
        {
            $data = array();
            $appcode    =   $this->session->userdata('appcode');
            $date       =   explode('-', $_POST['date']);
            $dateFrom   =   new DateTime($date[0]);
            $dateFrom   =   $dateFrom->format('Y-m-d');
            $dateTo     =   new DateTime($date[1]);
            $dateTo     =   $dateTo->format('Y-m-d');
            $data['bookings']    =   $this->db->query("SELECT bookings.*, room_categories.category_name FROM bookings INNER JOIN room_categories ON bookings.category_id = room_categories.id WHERE ( bookings.checkInDate BETWEEN '$dateFrom' AND '$dateTo' OR bookings.checkOutDate BETWEEN '$dateFrom' AND  '$dateTo' ) AND bookings.appcode = '$appcode' ORDER BY bookings.id ASC")-> result_array();
            
            $data['pathType']       =   $this->pathType;
            $data['pageName']       =   "Bookings Report";
            $data['fileName']       =   "bookingsReport.php";
            $data['reportStatus']   =   1;
            $data['query'] = "SELECT bookings.*, room_categories.category_name FROM bookings INNER JOIN room_categories ON bookings.category_id = room_categories.id WHERE ( bookings.checkInDate BETWEEN '$dateFrom' AND '$dateTo' OR bookings.checkOutDate BETWEEN '$dateFrom' AND  '$dateTo' ) AND bookings.appcode = '$appcode' ORDER BY bookings.id ASC";
            $data['heading'] = "Bookings Report From $dateFrom To $dateTo";

            $this->load->view('index', $data);
        }
        else
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
    }


    function pdfBookingsInvoiceTable()
    {

        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Sales Report');
        $pdf->SetSubject('OCTA Travel Bookings Report');
        $pdf->SetKeywords('OCTA Travel, Bookings Report');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Bookings Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('L');
        $HeaderText = $_POST['heading'];
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);

        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
                        <th style="text-align: center;"> S NO</th>
                        <th>ROOM CATEGORY</th>
                        <th>ROOMS BOOKED</th>
                        <th>BOOKED BY</th>
                        <th>EMAIL</th>
                        <th>MOBILE NO</th>
                        <th>CHECKIN</th>
                        <th>CHECKOUT</th>
                        <th>BOOKING DATE</th>
                    </tr>';
        $query      =   $_POST['query'];
        $bookings   =   $this->db->query( $query )-> result_array();

        $count = 1;   
        foreach ($bookings as $booking) 
       { 
          $customer = $this->services_model->getCustomerData( $booking['customer_id'], $booking['appcode'] );
        
            $tbl .= '
                <tr>
                    <td style="text-align: center;">  '.$count++ .' </td>
                    
                    <td> '.$booking['category_name'].'</td>

                    <td> '.$booking['no_of_rooms'].'</td>
                    
                    <td> '.$customer['name'].'</td>

                    <td> '.$customer['email'].'   </td>
                    
                    <td>'.$customer['phone'].'</td>

                    <td>'.date("jS F, Y", strtotime($booking['checkInDate'])).'</td>

                    <td>'.date("jS F, Y", strtotime($booking['checkOutDate'])).'</td>
                    
                    <td>'.date("jS F, Y", strtotime($booking['createdAt'])).'</td>
                    
                </tr>
            ';
            $total_balance = $total_amount - $totalamount_paid;
            $tbl_footer = '
                
            </table>
            ';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Bookings_Report.pdf', 'D');

    }


    function excelBookingsInvoiceTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        //load our new PHPExcel library
        $this->load->library('Excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Invoice Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $_POST['heading']);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:J1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'S.No'); 
        $this->excel->getActiveSheet()->setCellValue('B2', 'Room Category'); 
        $this->excel->getActiveSheet()->setCellValue('C2', 'Rooms Booked'); 
        $this->excel->getActiveSheet()->setCellValue('D2', 'Customer Name'); 
        $this->excel->getActiveSheet()->setCellValue('E2', 'Email'); 
        $this->excel->getActiveSheet()->setCellValue('F2', 'Mobile No'); 
        $this->excel->getActiveSheet()->setCellValue('G2', 'CheckIn'); 
        $this->excel->getActiveSheet()->setCellValue('H2', 'CheckOut'); 
        $this->excel->getActiveSheet()->setCellValue('I2', 'Booking Date'); 
        
        $this->excel->getActiveSheet()->getStyle('A2:I2')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A2:I2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $query  =   $_POST['query'];
        $bookings =   $this->db->query( $query )-> result_array();
        $count  = 1;
        $cell   = 3;  
        //print_r($result); die;
       
        foreach ($bookings as $booking ) 
        {
            $customer = $this->services_model->getCustomerData( $booking['customer_id'], $booking['appcode'] );
        
            $this->excel->getActiveSheet()->setCellValue('A'.$cell, $count); 
            $this->excel->getActiveSheet()->setCellValue('B'.$cell, $booking['category_name']); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell, $booking['no_of_rooms']); 
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, $customer['name']);  
            $this->excel->getActiveSheet()->setCellValue('E'.$cell, $customer['email']); 
            $this->excel->getActiveSheet()->setCellValue('F'.$cell, $customer['phone']); 
            $this->excel->getActiveSheet()->setCellValue('G'.$cell, date("jS F, Y", strtotime($booking['checkInDate']))); 
            $this->excel->getActiveSheet()->setCellValue('H'.$cell, date("jS F, Y", strtotime($booking['checkOutDate']))); 
            $this->excel->getActiveSheet()->setCellValue('I'.$cell, date("jS F, Y", strtotime($booking['createdAt']))); 
            $this->excel->getActiveSheet()->getStyle('A'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $this->excel->getActiveSheet()->getStyle('C'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $cell++;
            $count++;
        }  

        $sheet = $this->excel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) 
        {
            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }
        $filename='Bookings_Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
       
       
    }



    function expensesReport($param1 = '')
	{
		if( isset($this->session->userdata['email']) )
        {
            if(!$this->registrationStatus)
            {
                redirect(base_url().'report', 'refresh');
            }
			$data = array();
			$data['pathType']   =   $this->pathType;
			$data['pageName'] 	= 	"Expenses Report";
			$data['fileName'] 	= 	"expensesReport.php";
			$user_id			=	$this->session->userdata('id');
			$data['search']     =   false;
			
			if( $param1 == 'search' )
            {
				if( ! isset($_POST['findDate']) )
                {
					redirect(base_url().'reports/expensesReport', 'refresh');
				}
			
				$date       =   explode('-', $_POST['findDate']);
                $fromDate   =   new DateTime($date[0]);
                $fromDate   =   $fromDate->format('Y-m-d');
                $toDate     =   new DateTime($date[1]);
                $toDate     =   $toDate->format('Y-m-d');
			
				$data['FromDate']     	=     $fromDate;
				$data['ToDate']    		=     $toDate;
				
				$data['billslist']      =   $this->db->query("SELECT * FROM bills WHERE user_id = $user_id AND DATE( payment_date ) BETWEEN '$fromDate' AND '$toDate' ORDER BY id ASC")-> result_array();
				$data['query'] = "SELECT * FROM bills WHERE user_id = $user_id AND DATE( payment_date ) BETWEEN '$fromDate' AND '$toDate' ORDER BY id ASC";
                $data['heading'] = "Expenses Report From $fromDate To $toDate";

				$total_sum = 0;
				$total_amount = 0;
				foreach ($data['billslist'] as $row)
                {
					$total_sum += $row['amount_paid'];
					$total_amount += $row['total_amount'];
					$data['billsamount'] = $total_sum;
					$data['totalamount'] = $total_amount;
					$data['balance'] = $total_amount - $total_sum;
				}
				$data['search']     	=   true;
			}
			$this->load->view('index', $data);
		}
		else
        {
			$this->session->set_flashdata('notification', '<div class="alert alert-error">
					<button class="close" data-dismiss="alert">×</button>
					You Must Login First!</div>');
			redirect(base_url().'login', 'refresh');
		}
	}


	function pdfBillTable()
    {
        if( !$_POST )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Report');
        $pdf->SetSubject('OCTA Travel Report');
        $pdf->SetKeywords('OCTA Travel, Bill Report');
        
        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Expenses Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
        {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('L');

        $HeaderText = $_POST['heading'];        
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);

        $pdf->SetFont('times', '', 9    );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl 	= ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
	                    <th style="text-align: center; width: 5%;"> S NO </th>
	                    <th> BILL NAME   </th>
	                    <th> BILL NO   	 </th>
	                    <th> PAID TO  	 </th>
	                    <th> PAYMENT DATE</th>
        				<th> PAYMENT MODE</th>
        				<th> REFF NO	 </th>
        				<th style="text-align: center;"> TOTAL AMOUNT</th>
	                    <th style="text-align: center;"> AMOUNT PAID </th>
        				<th style="text-align: center;"> BALANCE	 </th>
            		</tr>';

        $query 		= 	$_POST['query'];
        $result 	=  	$this->db->query( $query )-> result_array();

        $count = 1;   
        // foreach item in your array...
        foreach ($result as $row ) 
        {
        	$balance = $row['total_amount'] - $row['amount_paid'];
        	if($balance == 0)
            {
        		$balance = '<strong>Nill</strong>';
        	}
        	else
            {
        		$balance = $balance.' INR';
        	}
        	$totalamount_paid += $row['amount_paid'];
        	$total_amount += $row['total_amount'];
        	$total_balance = $total_amount - $totalamount_paid;
        	if($total_balance == 0){
        		$balance = '<strong>Nill</strong>';
        	}
        	else{
        		$total_balance = $total_balance.' INR';
        	}
            
            $tbl .= '
                <tr style="font-size: 12px;">
                    <td style="text-align: center;">  '.$count++ .' </td>
            
                    <td> '.$row['bill_type'].'   </td>
                    
                    <td>'.$row['bill_number'].'</td>

                    <td>'.$row['paid_to'].'</td>

                    <td> '.date("jS F, Y", strtotime($row['payment_date'])).'</td>
                    
                   	<td>'.$row['payment_mode'].'</td>
                   	
                   	<td>'.$row['reference_no'].'</td>
                   			
                   	<td style="text-align: center;">'.$row['total_amount'].' INR</td>
                   			
                    <td style="text-align: center;">'.$row['amount_paid'].' INR</td>
                    		
                    <td style="text-align: center;">'.$balance.'</td>
                    
                </tr>
            ';
            $tbl_footer = '
            	<tr style="font-weight: bold;">
            		<td></td>
            		<td></td>
            		<td></td>
            		<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            		<td style="text-align: center;">'.$total_amount.' INR</td>
            		<td style="text-align: center;">'.$totalamount_paid.' INR</td>
            		<td style="text-align: center;">'.$total_balance.'</td>
        		</tr>
            </table>';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Expenses_Report.pdf', 'D');

    }



    function excelBillTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        //load our new PHPExcel library
        $this->load->library('Excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Bill Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $_POST['heading']);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:J1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'S.No'); 
        $this->excel->getActiveSheet()->setCellValue('B2', 'Bill Name'); 
        $this->excel->getActiveSheet()->setCellValue('C2', 'Bill No'); 
        $this->excel->getActiveSheet()->setCellValue('D2', 'Paid To'); 
        $this->excel->getActiveSheet()->setCellValue('E2', 'Payment Date'); 
        $this->excel->getActiveSheet()->setCellValue('F2', 'Payment Mode'); 
        $this->excel->getActiveSheet()->setCellValue('G2', 'Reff No'); 
        $this->excel->getActiveSheet()->setCellValue('H2', 'Total Amount'); 
        $this->excel->getActiveSheet()->setCellValue('I2', 'Amount Paid'); 
        $this->excel->getActiveSheet()->setCellValue('J2', 'Balance'); 
        
        $this->excel->getActiveSheet()->getStyle('A2:J2')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A2:J2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('H2:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
        $query  =   $_POST['query'];
        $result =   $this->db->query( $query )-> result_array();
        $count  = 1;
        $cell   = 3;  
        //print_r($result); die;
        $total_amount       = ""; 
        $totalamount_paid   = ""; 
        $total_balance      = ""; 
        foreach ($result as $row ) 
        {
            $balance = $row['total_amount'] - $row['amount_paid'];
            if($balance == 0)
            {
                $balance = 'Nill';
            }
            else
            {
                $balance;
            }
            $totalamount_paid += $row['amount_paid'];
            $total_amount += $row['total_amount'];
            $total_balance = $total_amount - $totalamount_paid;
            if($total_balance == 0)
            {
                $balance = 'Nill';
            }
            else
            {
                $total_balance = $total_balance;
            }
        
            $this->excel->getActiveSheet()->setCellValue('A'.$cell, $count); 
            $this->excel->getActiveSheet()->setCellValue('B'.$cell, $row['bill_type']); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell, $row['bill_number']); 
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, $row['paid_to']); 
            $this->excel->getActiveSheet()->setCellValue('E'.$cell, date("jS F, Y", strtotime($row['payment_date']))); 
            $this->excel->getActiveSheet()->setCellValue('F'.$cell, $row['payment_mode']); 
            $this->excel->getActiveSheet()->setCellValue('G'.$cell, $row['reference_no']); 
            $this->excel->getActiveSheet()->setCellValue('H'.$cell, $row['total_amount']); 
            $this->excel->getActiveSheet()->setCellValue('I'.$cell, $row['amount_paid']); 
            $this->excel->getActiveSheet()->setCellValue('J'.$cell, $balance);
            $this->excel->getActiveSheet()->getStyle('A'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $this->excel->getActiveSheet()->getStyle('H'.$cell.':J'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $cell++;
            $count++;
        }  


        $this->excel->getActiveSheet()->setCellValue('H'.$cell, $total_amount); 
        $this->excel->getActiveSheet()->setCellValue('I'.$cell, $totalamount_paid); 
        $this->excel->getActiveSheet()->setCellValue('J'.$cell, $total_amount - $totalamount_paid);
        $this->excel->getActiveSheet()->getStyle('A'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $this->excel->getActiveSheet()->getStyle('H'.$cell.':J'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
        $this->excel->getActiveSheet()->getStyle('H'.$cell.':J'.$cell)->getFont()->setBold(true);

        $sheet = $this->excel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) 
        {
            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }
        $filename='Expenses_Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }



    function hotelVouchersReport()
    {       
        if( isset($this->session->userdata['email']) )
        {
            if( !$this->registrationStatus )
            {
                redirect(base_url().'report/', 'refresh');
            }
            $appcode              =   $this->session->userdata('appcode');
            $data['pathType']     =   $this->pathType;
            $data['pageName']     =   "Hotel Vouchers Report";
            $data['fileName']     =   "hotelVouchersReport.php";
            $data['hotels']       =   $this->hotels_model->getHotels( $appcode );
            $data['reportStatus'] = 0;
            $this->load->view('index', $data);
        }
        else
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>You Must Login First!</div>');
            redirect(base_url().'login', 'refresh');
        }
        
    }

    
    function generateHotelVouchersReport()
    {
        if( ! isset($_POST['date']) )
        {
            redirect(base_url().'report/salesReport', 'refresh');
        }
        if( $_POST )
        {
            $appcode    =   $this->session->userdata('appcode');
            $date       =   explode('-', $_POST['date']);
            $dateFrom   =   new DateTime($date[0]);
            $dateFrom   =   $dateFrom->format('Y-m-d');
            $dateTo     =   new DateTime($date[1]);
            $dateTo     =   $dateTo->format('Y-m-d');

            if( $_POST['hotel_id'] == 'all' )
            {
                $hotelsStatus = '';
            }
            else
            {
                $hotelsStatus = 'AND hotel_vouchers.hotel_id = '.$_POST['hotel_id'];
            }

            $data['hotelVouchers']    =   $this->db->query("SELECT hotel_vouchers.*, hotels.hotel_name FROM hotel_vouchers INNER JOIN hotels ON hotel_vouchers.hotel_id = hotels.id WHERE hotel_vouchers.appcode = '$appcode' ".$hotelsStatus." AND DATE( hotel_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND hotel_vouchers.status = 'Active' ORDER BY id ASC")-> result_array();
            //print_r($data['hotelVouchers']); die;

            $data['total_amount']   =   $this->db->query("SELECT SUM(hotel_voucher_services.amount) as totalAmount FROM hotel_voucher_services LEFT JOIN hotel_vouchers ON hotel_voucher_services.voucher_id = hotel_vouchers.id WHERE hotel_vouchers.appcode = '$appcode' ".$hotelsStatus." AND DATE( hotel_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND hotel_vouchers.status = 'Active'")-> row_array();
            $data['total_amount']   =   $data['total_amount'] ['totalAmount'];

            $data['pathType']       =   $this->pathType;
            $data['pageName']       =   "Hotel Vouchers Report";
            $data['fileName']       =   "hotelVouchersReport.php";
            $data['reportStatus']   =   1;
            $data['hotels']         =   $this->hotels_model->getHotels( $appcode );
            $data['query']          =   "SELECT hotel_vouchers.*, hotels.hotel_name FROM hotel_vouchers INNER JOIN hotels ON hotel_vouchers.hotel_id = hotels.id WHERE hotel_vouchers.appcode = '$appcode' ".$hotelsStatus." AND DATE( hotel_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND hotel_vouchers.status = 'Active' ORDER BY id ASC";
            $data['heading'] = "Hotel Vouchers Report From $dateFrom To $dateTo";

            $this->load->view('index', $data);
        }
        else
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
    }


    function pdfHotelVouchersTable()
    {

        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Sales Report');
        $pdf->SetSubject('OCTA Travel Hotel Vouchers Report');
        $pdf->SetKeywords('OCTA Travel, Hotel Vouchers Report');

        // set default header data
        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Hotel Vouchers Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('L');
        $HeaderText = $_POST['heading'];
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);

        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
                        <th  style="text-align: center;"> S NO          </th>
                        <th style="text-align: center;"> VOUCHER NO     </th>
                        <th> VOUCHER DATE                               </th>
                        <th> HOTEL NAME                                 </th>
                        <th> CUSTOMER NAME                              </th>
                        <th> CUSTOMER EMAIL                             </th>
                        <th> CUSTOMER MOBILE                            </th>
                        <th style="text-align: center;"> TOTAL AMOUNT   </th>
                    </tr>';
        
        $query          =   $_POST['query'];
        $vouchers       =   $this->db->query( $query )-> result_array();

        $count = 1;   
        $total_amount       = "";

        foreach ($vouchers as $voucher ) 
        {
              
            $total          =  $this->services_model->getVoucherHotelServicesTotal( $voucher['id'] );
            $total_amount   =  $total_amount + $total;
            
            $tbl .= '
                <tr style="font-size: 12px;">
                    <td style="text-align: center;">  '.$count++ .' </td>
                    
                    <td style="text-align: center;">'.$voucher['voucher_no'].'</td>
                    
                    <td>'.date("jS F, Y", strtotime($voucher['voucher_date'])).'</td>

                    <td>'.$voucher['hotel_name'].'   </td>

                    <td>'.$voucher['customer_name'].'</td>
                    
                    <td>'.$voucher['customer_email'].'</td>

                    <td style="text-align: center;">'.$voucher['customer_mobile'].'</td>

                    <td  style="text-align: center;">'.$total.' INR</td>
                    
                </tr>
            ';
            
            $tbl_footer = '
                <tr style="font-weight: bold;">
                    <td></td>
                    
                    <td></td>
                    
                    <td></td>

                    <td></td>
                    
                    <td></td>

                    <td></td>

                    <td></td>

                    <td  style="text-align: center;">'.$total_amount.' INR</td>
                </tr>
            </table>
            ';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Hotel_Voucher_Report.pdf', 'D');

    }


    function excelHotelVouchersTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        //load our new PHPExcel library
        $this->load->library('Excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Invoice Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $_POST['heading']);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:H1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'S NO'); 
        $this->excel->getActiveSheet()->setCellValue('B2', 'VOUCHER NO'); 
        $this->excel->getActiveSheet()->setCellValue('C2', 'VOUCHER DATE'); 
        $this->excel->getActiveSheet()->setCellValue('D2', 'HOTEL NAME');
        $this->excel->getActiveSheet()->setCellValue('E2', 'CUSTOMER NAME'); 
        $this->excel->getActiveSheet()->setCellValue('F2', 'CUSTOMER EMAIL'); 
        $this->excel->getActiveSheet()->setCellValue('G2', 'CUSTOMER MOBILE'); 
        $this->excel->getActiveSheet()->setCellValue('H2', 'TOTAL AMOUNT'); 
        
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $this->excel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $count  = 1;
        $cell   = 3;  
       
        $query          =   $_POST['query'];
        $vouchers       =   $this->db->query( $query )-> result_array();

        $count = 1;   
        $total_amount       = "";

        foreach ($vouchers as $voucher ) 
        {
            $total          =  $this->services_model->getVoucherHotelServicesTotal( $voucher['id'] );
            $total_amount   =  $total_amount + $total;
        
            $this->excel->getActiveSheet()->setCellValue('A'.$cell, $count); 
            $this->excel->getActiveSheet()->setCellValue('B'.$cell, $voucher['voucher_no']); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell, date("jS F, Y", strtotime($voucher['voucher_date'])));  
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, $voucher['hotel_name']); 
            $this->excel->getActiveSheet()->setCellValue('E'.$cell, $voucher['customer_name']); 
            $this->excel->getActiveSheet()->setCellValue('F'.$cell, $voucher['customer_email']); 
            $this->excel->getActiveSheet()->setCellValue('G'.$cell, $voucher['customer_mobile']); 
            $this->excel->getActiveSheet()->setCellValue('H'.$cell, $total); 
            $this->excel->getActiveSheet()->getStyle('A'.$cell.':C'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $this->excel->getActiveSheet()->getStyle('G'.$cell.':H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $cell++;
            $count++;
        }  


        $this->excel->getActiveSheet()->setCellValue('H'.$cell, $total_amount); 
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':H'.$cell)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':H'.$cell)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $sheet = $this->excel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) 
        {
            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }
        $filename='Hotel_Vouchers_Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
      
    }



    function transportVouchersReport()
    {       
        if( isset($this->session->userdata['email']) )
        {
            if( !$this->registrationStatus )
            {
                redirect(base_url().'report/', 'refresh');
            }
            $appcode                =   $this->session->userdata('appcode');
            $data['transporters']   =   $this->transporters_model->getTransporters( $appcode );
            $data['pathType']       =   $this->pathType;
            $data['pageName']       =   "Transport Vouchers Report";
            $data['fileName']       =   "transportVouchersReport.php";
            $data['reportStatus']   =   0;
            $this->load->view('index', $data);
        }
        else
        {
            $this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>You Must Login First!</div>');
            redirect(base_url().'login', 'refresh');
        }
        
    }

    
    function generateTransportVouchersReport()
    {
        if( ! isset($_POST['date']) )
        {
            redirect(base_url().'report/salesReport', 'refresh');
        }
        if( $_POST )
        {
            $appcode    =   $this->session->userdata('appcode');
            $date       =   explode('-', $_POST['date']);
            $dateFrom   =   new DateTime($date[0]);
            $dateFrom   =   $dateFrom->format('Y-m-d');
            $dateTo     =   new DateTime($date[1]);
            $dateTo     =   $dateTo->format('Y-m-d');
            $data['transporters']   =   $this->transporters_model->getTransporters( $appcode );

            if( $_POST['transporter_id'] == 'all' )
            {
                $transportersStatus = '';
            }
            else
            {
                $transportersStatus = 'AND transport_vouchers.transporter_id = '.$_POST['transporter_id'];
            }

            $data['hotelVouchers']    =   $this->db->query("SELECT transport_vouchers.*, transporters.transporter_name FROM transport_vouchers INNER JOIN transporters ON transport_vouchers.transporter_id = transporters.id WHERE transport_vouchers.appcode = '$appcode' ".$transportersStatus." AND DATE( transport_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND transport_vouchers.status = 'Active' ORDER BY id ASC")-> result_array();
            //print_r($data['hotelVouchers']); die;

            $data['total_amount']   =   $this->db->query("SELECT SUM(transport_voucher_services.rate) as totalAmount FROM transport_voucher_services LEFT JOIN transport_vouchers ON transport_voucher_services.voucher_id = transport_vouchers.id WHERE transport_vouchers.appcode = '$appcode' ".$transportersStatus." AND DATE( transport_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND transport_vouchers.status = 'Active'")-> row_array();
            $data['total_amount']   =   $data['total_amount'] ['totalAmount'];

            $data['pathType']       =   $this->pathType;
            $data['pageName']       =   "Transport Vouchers Report";
            $data['fileName']       =   "transportVouchersReport.php";
            $data['reportStatus']   =   1;
            $data['query']          =   "SELECT transport_vouchers.*, transporters.transporter_name FROM transport_vouchers INNER JOIN transporters ON transport_vouchers.transporter_id = transporters.id WHERE transport_vouchers.appcode = '$appcode' ".$transportersStatus." AND DATE( transport_vouchers.voucher_date ) BETWEEN '$dateFrom' AND '$dateTo' AND transport_vouchers.status = 'Active' ORDER BY id ASC";
            $data['heading'] = "Hotel Vouchers Report From $dateFrom To $dateTo";

            $this->load->view('index', $data);
        }
        else
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
    }


    function pdfTransportVouchersTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Sales Report');
        $pdf->SetSubject('OCTA Travel Transport Vouchers Report');
        $pdf->SetKeywords('OCTA Travel, Transport Vouchers Report');

        // set default header data
        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Hotel Vouchers Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('L');
        $HeaderText = $_POST['heading'];
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);

        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
                        <th  style="text-align: center;"> S NO          </th>
                        <th style="text-align: center;"> VOUCHER NO     </th>
                        <th> VOUCHER DATE                               </th>
                        <th> TRANSPORTER NAME                           </th>
                        <th> CUSTOMER NAME                              </th>
                        <th> CUSTOMER EMAIL                             </th>
                        <th> CUSTOMER MOBILE                            </th>
                        <th style="text-align: center;"> TOTAL AMOUNT   </th>
                    </tr>';
        
        $query          =   $_POST['query'];
        $vouchers       =   $this->db->query( $query )-> result_array();

        $count = 1;   
        $total_amount       = "";

        foreach ($vouchers as $voucher ) 
        {
              
            $total          =  $this->services_model->getVoucherTransportServicesTotal( $voucher['id'] );
            $total_amount   =  $total_amount + $total;
            
            $tbl .= '
                <tr style="font-size: 12px;">
                    <td style="text-align: center;">  '.$count++ .' </td>
                    
                    <td style="text-align: center;">'.$voucher['voucher_no'].'</td>
                    
                    <td>'.date("jS F, Y", strtotime($voucher['voucher_date'])).'</td>

                    <td>'.$voucher['transporter_name'].'   </td>

                    <td>'.$voucher['customer_name'].'</td>
                    
                    <td>'.$voucher['customer_email'].'</td>

                    <td style="text-align: center;">'.$voucher['customer_mobile'].'</td>

                    <td  style="text-align: center;">'.$total.' INR</td>
                    
                </tr>
            ';
            
            $tbl_footer = '
                <tr style="font-weight: bold;">
                    <td></td>
                    
                    <td></td>
                    
                    <td></td>

                    <td></td>
                    
                    <td></td>

                    <td></td>

                    <td></td>

                    <td  style="text-align: center;">'.$total_amount.' INR</td>
                </tr>
            </table>
            ';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Transport_Voucher_Report.pdf', 'D');

    }


    function excelTransportVouchersTable()
    {
        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        //load our new PHPExcel library
        $this->load->library('Excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Invoice Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $_POST['heading']);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:H1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'S NO'); 
        $this->excel->getActiveSheet()->setCellValue('B2', 'VOUCHER NO'); 
        $this->excel->getActiveSheet()->setCellValue('C2', 'VOUCHER DATE'); 
        $this->excel->getActiveSheet()->setCellValue('D2', 'TRANSPORTER NAME');
        $this->excel->getActiveSheet()->setCellValue('E2', 'CUSTOMER NAME'); 
        $this->excel->getActiveSheet()->setCellValue('F2', 'CUSTOMER EMAIL'); 
        $this->excel->getActiveSheet()->setCellValue('G2', 'CUSTOMER MOBILE'); 
        $this->excel->getActiveSheet()->setCellValue('H2', 'TOTAL AMOUNT'); 
        
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        $this->excel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $count  = 1;
        $cell   = 3;  
       
        $query          =   $_POST['query'];
        $vouchers       =   $this->db->query( $query )-> result_array();

        $count = 1;   
        $total_amount       = "";

        foreach ($vouchers as $voucher ) 
        {
            $total          =  $this->services_model->getVoucherTransportServicesTotal( $voucher['id'] );
            $total_amount   =  $total_amount + $total;
        
            $this->excel->getActiveSheet()->setCellValue('A'.$cell, $count); 
            $this->excel->getActiveSheet()->setCellValue('B'.$cell, $voucher['voucher_no']); 
            $this->excel->getActiveSheet()->setCellValue('C'.$cell, date("jS F, Y", strtotime($voucher['voucher_date'])));  
            $this->excel->getActiveSheet()->setCellValue('D'.$cell, $voucher['transporter_name']); 
            $this->excel->getActiveSheet()->setCellValue('E'.$cell, $voucher['customer_name']); 
            $this->excel->getActiveSheet()->setCellValue('F'.$cell, $voucher['customer_email']); 
            $this->excel->getActiveSheet()->setCellValue('G'.$cell, $voucher['customer_mobile']); 
            $this->excel->getActiveSheet()->setCellValue('H'.$cell, $total); 
            $this->excel->getActiveSheet()->getStyle('A'.$cell.':C'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $this->excel->getActiveSheet()->getStyle('G'.$cell.':H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
            $cell++;
            $count++;
        }  


        $this->excel->getActiveSheet()->setCellValue('H'.$cell, $total_amount); 
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':H'.$cell)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A'.$cell.':H'.$cell)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $sheet = $this->excel->getActiveSheet();
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells( true );
        /** @var PHPExcel_Cell $cell */
        foreach( $cellIterator as $cell ) 
        {
            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
        }
        $filename='Transport_Vouchers_Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
       
       
    }


}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */