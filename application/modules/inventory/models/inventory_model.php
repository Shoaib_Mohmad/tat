<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   

    function getRooms( $appcode )
    {
        $rooms  =   $this->db->query("SELECT rooms.*, room_categories.category_name FROM rooms INNER JOIN room_categories on rooms.category_id = room_categories.id WHERE rooms.appcode = '$appcode' AND rooms.status = 'Active' ORDER BY room_categories.category_name ASC") -> result_array();
        return $rooms;
    }


    function getRoomCategories( $appcode )
    {
        $roomCats  =   $this->db->query("SELECT * FROM room_categories WHERE appcode = '$appcode' AND status = 'Active' ORDER BY category_name ASC") -> result_array();
        return $roomCats;
    }


    function getRoomDetails( $id, $appcode )
    {
        $room  =   $this->db->query("SELECT * FROM rooms WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'");
        if( !$room -> num_rows() )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        return $room -> row_array();
    }


    function getCategoryDetails( $id, $appcode )
    {
        $room  =   $this->db->query("SELECT * FROM room_categories WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'");
        if( !$room -> num_rows() )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        return $room -> row_array();
    }


    function checkRoomExists( $id, $appcode )
    {
        $roomNoExists  =   $this->db->query("SELECT id FROM rooms WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'") -> num_rows();
        if( $roomNoExists )
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }


    function catHasRooms( $id )
    {
        $roomsExists  =   $this->db->query("SELECT id FROM rooms WHERE category_id = '$id' AND status = 'Active'") -> num_rows();
        if( $roomsExists )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


    function checkRoomCategoryExists( $id, $appcode )
    {
        $catExists  =   $this->db->query("SELECT id FROM room_categories WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'") -> num_rows();
        if( $catExists )
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }


    function checkRoomCatExists( $appcode, $category_name )
    {
        $catExists  =   $this->db->query("SELECT * FROM room_categories WHERE appcode = '$appcode' AND category_name = '$category_name' AND status = 'Active'") -> num_rows();
        if( $catExists )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


    function checkRoomNoExists( $id, $appcode, $room_no )
    {
        $roomExists  =   $this->db->query("SELECT * FROM rooms WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'");
        if( !$roomExists -> num_rows() )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }

        $roomNoExists  =   $this->db->query("SELECT * FROM rooms WHERE id != '$id' AND appcode = '$appcode' AND room_no = '$room_no' AND status = 'Active'") -> num_rows();
        if( $roomNoExists )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


    function checkCatExists( $id, $appcode, $category_name )
    {
        $roomExists  =   $this->db->query("SELECT * FROM room_categories WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'");
        if( !$roomExists -> num_rows() )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }

        $roomNoExists  =   $this->db->query("SELECT * FROM room_categories WHERE id != '$id' AND appcode = '$appcode' AND category_name = '$category_name' AND status = 'Active'") -> num_rows();
        if( $roomNoExists )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


    function getAvaliableRooms( $appcode, $checkInDate, $checkOutDate )
    {
        $checkInDate    =   $checkInDate.' 12:00:00';
        $checkOutDate   =   $checkOutDate.' 11:59:59';
        $categories     =   $this -> getRoomCategories( $appcode );
        $i = 0;
        foreach( $categories as $category )
        {
            $totalRooms         =   $this->db->query("SELECT rooms_available FROM rooms WHERE category_id = ".$category['id']." AND appcode = '$appcode' AND status = 'Active'") -> row_array();
            
            if( !isset( $totalRooms['rooms_available'] ) )
                continue;
            $totalRooms         =   $totalRooms['rooms_available']; 
            $availableRooms     =   $this->db->query("SELECT SUM( no_of_rooms ) as rooms_booked FROM bookings WHERE category_id = ".$category['id']." AND ( checkInDate BETWEEN '$checkInDate' AND '$checkOutDate' OR checkOutDate BETWEEN '$checkInDate' AND  '$checkOutDate' ) AND appcode = '$appcode' AND status = 'Active'") -> row_array();
            $availableRooms     =   $availableRooms['rooms_booked'];
            $bookings[$i]['category_id']              =   $category['id'];
            $bookings[$i]['category_name']            =   $category['category_name'];
            $bookings[$i]['category_description']     =   $category['description'];
            $bookings[$i++]['totat_rooms_available']  =   $totalRooms - $availableRooms;
        }
        //$availableRooms =   $this->db->query("SELECT * FROM rooms WHERE appcode = '$appcode' AND status = 'Active' AND id NOT IN (SELECT room_id FROM bookings WHERE ( checkInDate BETWEEN '$checkInDate' AND '$checkOutDate' OR checkOutDate BETWEEN '$checkInDate' AND  '$checkOutDate' ) AND appcode = '$appcode' AND status = 'Active')") -> result_array();
        //print_r($bookings); die;
        return $bookings;
    }


    function getBookings( $appcode )
    {
        $bookings   =   $this->db->query("SELECT bookings.*, room_categories.category_name FROM bookings INNER JOIN room_categories on bookings.category_id = room_categories.id WHERE bookings.appcode = '$appcode' ORDER BY bookings.createdAt DESC") -> result_array();
        return $bookings;
    }


    function getCustomerData( $id, $appcode )
    {
        $customerDetails   =   $this->db->query("SELECT * FROM customers WHERE id = $id AND appcode = '$appcode' AND status = 1") ->row_array();
        return $customerDetails;
    }


    function checkBookingExists( $id, $appcode )
    {
        $booking  =   $this->db->query("SELECT id FROM bookings WHERE id = '$id' AND appcode = '$appcode' AND status = 'Active'") -> num_rows();
        if( $booking )
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }


}