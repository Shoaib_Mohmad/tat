<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'inventory';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('inventory');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('inventory/inventory_model');
		$this->load->model('customers/customers_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function roomCategories( $param1 = '', $param2 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$exists 	=	$this->inventory_model->checkRoomCategoryExists( $appcode, $param2 );
			if( !$exists )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Room Category Doesn\'t Exist!</div>'));
			}
			else
			{
				$room['status'] =	'Deleted';
				$this->db->where( 'id', $param2 );
				$result 		=   $this->db->update('room_categories', $room);
				
				if( $result )
		        {
					$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Category Deleted Successfully!
						</div>');
				}
				else
				{
					$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				}
				
			}
			redirect(base_url().'inventory/roomCategories', 'refresh');
		}

		$data['categories']	= 	$this->inventory_model->getRoomCategories( $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Rooms Categories";
		$data['fileName'] 	= 	"roomCategories.php";
		$this->load->view( 'index', $data );
	}


	public function addCategory( $param1 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'add' && $_POST )
		{
			$exists 	=	$this->inventory_model->checkRoomCatExists( $appcode, $_POST['category_name'] );
			if( $exists )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Cattegory Name Already Exists</div>'));
				redirect(base_url().'inventory/addCategory', 'refresh');
			}
			$category['appcode'] 		= 	$appcode;
			$category['category_name'] 	= 	$_POST['category_name'];
			$category['description'] 	= 	$_POST['description'];
			$result   =   $this->db->insert( 'room_categories', $category );

			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Category Added Successfully!
					</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'inventory/roomCategories', 'refresh');
		}

		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Room Category";
		$data['fileName'] 	= 	"addCategory.php";
		$this->load->view( 'index', $data );
	}


	public function editCategory( $param1 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'doEdit' )
		{
			$exists 	=	$this->inventory_model->checkCatExists( $_POST['id'], $appcode, $_POST['category_name'] );
			if( $exists )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Category Name Already Exists</div>'));
				redirect(base_url().'inventory/editCategory/'.$_POST['id'], 'refresh');
			}
			$category['category_name'] 	= 	$_POST['category_name'];
			$category['description'] 	= 	$_POST['description'];
			
			$this->db->where( 'id', $_POST['id'] );
			$result 	=   $this->db->update('room_categories', $category);
			
			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Category Updated Successfully!
					</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'inventory/roomCategories', 'refresh');
		}

		$data['category']	= 	$this->inventory_model->getCategoryDetails( $param1, $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Room Category";
		$data['fileName'] 	= 	"addCategory.php";
		$this->load->view( 'index', $data );
	}


	public function rooms( $param1 = '', $param2 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$exists 	=	$this->inventory_model->checkRoomExists( $appcode, $param2 );
			if( !$exists )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Room Doesn\'t Exist!</div>'));
			}
			else
			{
				$room['status'] =	'Deleted';
				$this->db->where( 'id', $param2 );
				$result 		=   $this->db->update('rooms', $room);
				
				if( $result )
		        {
					$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Deleted Successfully!
						</div>');
				}
				else
				{
					$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				}
				
			}
			redirect(base_url().'inventory/rooms', 'refresh');
		}

		$data['rooms'] 		= 	$this->inventory_model->getRooms( $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Rooms Available";
		$data['fileName'] 	= 	"rooms.php";
		$this->load->view( 'index', $data );
	}


	public function addRoom( $param1 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'add' && $_POST )
		{
			$room['appcode'] 			= 	$appcode;
			$room['category_id'] 		= 	$_POST['category_id'];
			$room['rooms_available']	= 	$_POST['rooms_available'];
			$room['description'] 		= 	$_POST['description'];
			$result   =   $this->db->insert( 'rooms', $room );

			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Added Successfully!
					</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'inventory/rooms', 'refresh');
		}

		$data['categories']	= 	$this->inventory_model->getRoomCategories( $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Add Rooms";
		$data['fileName'] 	= 	"addRoom.php";
		$this->load->view( 'index', $data );
	}


	public function editRoom( $param1 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'doEdit' )
		{
			$room['category_id'] 		= 	$_POST['category_id'];
			$room['rooms_available']	= 	$_POST['rooms_available'];
			$room['description'] 		= 	$_POST['description'];
			
			$this->db->where( array( 'id' => $_POST['id'], 'appcode' => $appcode ) );
			$result 	=   $this->db->update('rooms', $room);
			
			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Room Updated Successfully!
					</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'inventory/rooms', 'refresh');
		}

		$data['room'] 		= 	$this->inventory_model->getRoomDetails( $param1, $appcode );
		$data['categories']	= 	$this->inventory_model->getRoomCategories( $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Rooms";
		$data['fileName'] 	= 	"addRoom.php";
		$this->load->view( 'index', $data );
	}


	public function makeBooking( $param1 = '', $param2 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;

		if( $param1 == 'search' && $_POST )
		{
			$dateChunk       =   explode( "-", $_POST['date'] );
            $checkInDate     =   date( "Y-m-d", strtotime( $dateChunk[0] ) );
            $checkOutDate    =   date( "Y-m-d", strtotime( $dateChunk[1] ) );

            $this->session->set_userdata( 'checkInDate', $checkInDate );   
            $this->session->set_userdata( 'checkOutDate', $checkOutDate );

            $data['search']    =   true;
            $data['rooms']     =   $this->inventory_model->getAvaliableRooms( $appcode, $checkInDate, $checkOutDate );
            //print_r($data['rooms']); die;
            $data['pageName'] 	= 	"Book Room";
			$data['fileName'] 	= 	"makeBooking.php";
			$this->load->view( 'index', $data );
		}
		elseif( $param1 == 'addGuest' && $_POST )
		{
			$data['pathType'] 	= 	$this->pathType;
			$data['pageName'] 	= 	"Add Guest";
			$data['fileName'] 	= 	"addGuest.php";
			$data['customers'] 	= 	$this->customers_model->getCustomers( $appcode );
			$this->session->set_userdata( 'category_id', $_POST['category_id'] );
			$this->session->set_userdata( 'no_of_rooms', $_POST['no_of_rooms'] );
			$this->session->set_userdata( 'extra_bed', $_POST['extra_bed'] );
			$this->session->set_userdata( 'meal_plan', $_POST['meal_plan'] );
			$this->session->set_userdata( 'amount', $_POST['amount'] );
			$this->load->view( 'index', $data );
		}
		elseif( $param1 == 'bookRoom' && $_POST && isset( $this->session->userdata['category_id'] ) )
		{
			$booking['appcode'] 		= 	$appcode;
			$booking['category_id'] 	= 	$this->session->userdata['category_id'];
			$booking['no_of_rooms'] 	= 	$this->session->userdata['no_of_rooms'];
			$booking['extra_bed'] 		= 	$this->session->userdata['extra_bed'];
			$booking['meal_plan'] 		= 	$this->session->userdata['meal_plan'];
			$booking['amount'] 			= 	$this->session->userdata['amount'];
			$booking['customer_id'] 	= 	$_POST['customer_id'];
			$booking['checkInDate'] 	= 	$this->session->userdata['checkInDate'].' 12:00:00';
			$booking['checkOutDate'] 	= 	$this->session->userdata['checkOutDate'].' 11:59:59';
			$result   =   $this->db->insert( 'bookings', $booking );

			$this->session->unset_userdata('category_id');
			$this->session->unset_userdata('no_of_rooms');
			$this->session->unset_userdata('extra_bed');
			$this->session->unset_userdata('meal_plan');
			$this->session->unset_userdata('amount');

			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Booking Made Successfully!	</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'inventory/bookings', 'refresh');
		}
		else
		{
			$data['pageName'] 	= 	"Book Room";
			$data['fileName'] 	= 	"makeBooking.php";
			$this->load->view( 'index', $data );
		}
	}


	public function bookings( $param1 = '', $param2 = '' )
	{
		$userId 	= 	$this->session->userdata['id'];
		$appcode	= 	$this->session->userdata['appcode'];

		if( $param1 == 'cancel' && is_numeric( $param2 ) )
		{
			$exists 	=	$this->inventory_model->checkBookingExists( $param2, $appcode );
			if( !$exists )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Booking Doesn\'t Exists!</div>'));
			}
			else
			{
				$booking['status'] = 'Cancelled';
				$this->db->where( 'id', $param2 );
				$result 	=   $this->db->update('bookings', $booking);
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Booking Cancelled Successfully!
					</div>');
			}
			redirect(base_url().'inventory/bookings', 'refresh');
		}

		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Bookings";
		$data['fileName'] 	= 	"bookings.php";
		$data['bookings'] 	= 	$this->inventory_model->getBookings( $appcode );
		$this->load->view( 'index', $data );
	}



	public function bookingInvoice( $id )
	{
		if( !is_numeric($id) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
		$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Booking Invoice');
		$pdf->SetSubject('Booking Invoice');
		$pdf->SetKeywords('OCTA, Travel, Invoice');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate an Invoice!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		"\n". 'Pin: '.$result['pin'].
		"\n".'Phone: '. $result['phone']. ', Email: ' . $result['company_email'];
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$query 	= 	$this->db->query("SELECT * FROM bookings WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
		$invoice_result 	= $query -> row_array();
		if( !$invoice_result )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 15);
		$pdf->Cell(100, 5, 'SERVICES INVOICE', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 7, 'INVOICE # : OSB-'.$invoice_result['invoice_code'], 0, 0 );
		$pdf->Cell(100, 7, 'ARRIVAL DATE      : '.date("jS F, Y", strtotime($invoice_result['checkin'])), 0, 1 );
		$pdf->Cell(113, 7, 'ISSUED ON : '.date("jS F, Y", strtotime($invoice_result['invoice_date'])), 0, 0 );
		$pdf->Cell(100, 7, 'DEPARTURE DATE: '.date("jS F, Y", strtotime($invoice_result['checkout'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'CUSTOMER DETAILS:', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $invoice_result['invoice_to'], 0, 1 );
		$pdf->Cell(100, 5, $invoice_result['city'].',', 0, 1 );
		$pdf->Cell(100, 5, $invoice_result['state'].',', 0, 1 );
		$pdf->Cell(100, 5, $invoice_result['country'], 0, 1 );
		$pdf->Cell(100, 5, 'Pin: '.$invoice_result['pin_code'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $invoice_result['phone'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$invoice_result['email'], 0, 1 );
		
		$pdf->Ln(5);
		$pdf->SetFont('times', 'B', 12);
		$pdf->Cell(100, 10, 'SERVICES DETAILS:', 0, 1 );

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->SetFont('helvetica', '', 9    );
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="2" cellspacing="2" nobr="true" style="text-align: center; border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th> #      	</th>
						<th> Service   	</th>
						<th> Cost  		</th>
					</tr>';
		
		$query 	= 	$this->db->query("SELECT * FROM services WHERE invoice_id = $invoiceId AND appcode = '$appcode'");
		$services_result 	= $query -> result_array();
		if( !$services_result )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}


		$sno = 1;
		
		foreach ($services_result as $row ) 
		{
			$sum =  $this->db->query("SELECT SUM(total) FROM services WHERE invoice_id = $invoiceId")-> row_array();
			$total   = $sum['SUM(total)'].' INR';
			$balance = $sum['SUM(total)'] - $invoice_result['advance_payment'];
			if($balance == 0)
			{
				$balance = 'Nill';
			}
			else
			{
				$balance = $balance.' INR';
			}
			
			if($invoice_result['advance_payment'] == 0){
				$advance_payment = 'Nill';
			}
			else
			{
				$advance_payment = $invoice_result['advance_payment'].' INR';
			}

			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td>'.$sno++ .				'</td>
							<td>'.$row['service_type'].	'</td>
							<td>'.$row['total'].		'</td>
						</tr>
					';
							
		}
		$tbl_footer .='</table>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr style="font-size:13px; font-family:helvetica;">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$total.'</td>	
					</tr>
					<tr style="font-size:13px; font-family:helvetica;"> 
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Advance&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$advance_payment.'</td> 
					</tr>
					<tr style="font-size:13px;"> 
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>--------------------------------</td> 
					</tr>
					<tr style="font-size:13px; font-family:helvetica;"> 
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$balance.'</td> 
					</tr>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />
		<h2>Terms & Conditions</h2><ul>';
		
		$tos	= 	$this->db->query("SELECT * FROM tos WHERE appcode = '$appcode' AND status = 1 ORDER BY id ASC") -> result_array();
		foreach($tos as $ts){
 			$html .= '<li>'.$ts['tos_text'].'</li>';
 		}
 		$html .= '</ul>';
		
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 10);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );
		
		ob_end_clean();

		$pdf->Output('Booking_Invoice - ' .$invoice_result['id']. '.pdf', 'I');
	}


	public function monthlyBookings()
    {
        $userId     =   $this->session->userdata['id'];
        $appcode    =   $this->session->userdata['appcode'];
        $data['pathType']   =   $this->pathType;
        $data['pageName']   =   "Monthly Bookings";
        $data['fileName']   =   "monthlyBookings.php";
        if( $_POST )
        {
            $max        =   cal_days_in_month(CAL_GREGORIAN, $_POST['month'], $_POST['year']); 
            for( $i=1, $j=0; $i<=$max; $i++, $j++ )
            {
                $date           =   $i.'-'.$_POST['month'].'-'.$_POST['year'];
                $checkInDate    =   date('Y-m-d', strtotime($date));
                $checkOutDate   =   date('Y-m-d',strtotime("+1 day", strtotime($date)));
                $bookings[$j]['date']       =   $checkInDate;
                $bookings[$j]['booking']    =   $this->inventory_model->getAvaliableRooms( $appcode, $checkInDate, $checkOutDate );
            }
            $data['reportStatus']   =   1;
            $data['heading']        =   'Rooms Available for the Month of '.date('F, Y', strtotime('01-'.$_POST['month'].'-'.$_POST['year'].''));
            $data['bookings']       =   $bookings;
            $data['month']  =   $_POST['month'];
            $data['year']   =   $_POST['year'];
        }
        
        $data['categories']     =   $this->inventory_model->getRoomCategories( $appcode );
        $this->load->view( 'index', $data );
    }


    public function pdfMonthlyBookings()
    {

        if( !$_POST )
        {
             redirect(base_url().'login/four_zero_four', 'refresh');
        }
        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Monthly Rooms Booking Status');
        $pdf->SetSubject('OCTA Travel Monthly Rooms Booking Status');
        $pdf->SetKeywords('OCTA Travel, Monthly Rooms Booking Status');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Rooms Bookings Report');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');

        // add a page
        $pdf->AddPage('P');

        $appcode    =   $this->session->userdata['appcode'];
        $HeaderText =   $_POST['heading'];
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);
        $pdf->Ln(3);
        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true" style="text-align: center;">';
        $tbl_footer = '</table>';

        $max        =   cal_days_in_month(CAL_GREGORIAN, $_POST['month'], $_POST['year']); 
        $categories =   $this->inventory_model->getRoomCategories( $appcode );
        for( $i=1, $j=0; $i<=$max; $i++, $j++ )
        {
            $date           =   $i.'-'.$_POST['month'].'-'.$_POST['year'];
            $checkInDate    =   date('Y-m-d', strtotime($date));
            $checkOutDate   =   date('Y-m-d',strtotime("+1 day", strtotime($date)));
            $bookings[$j]['date']       =   $checkInDate;
            $bookings[$j]['booking']    =   $this->inventory_model->getAvaliableRooms( $appcode, $checkInDate, $checkOutDate );
        }

        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold">
                        <th>DATE</th>';
        foreach( $categories as $category )
        {
            if( $this->inventory_model->catHasRooms( $category['id'] ) )
                $tbl    .=   '<th>ROOM CATEGORY ('.$category['category_name'].')</th>'; 
        }
        $tbl    .=   '</tr>';

        foreach ($bookings as $booking) 
        { 
            $tbl .= '
                <tr style="font-size: 12px;">
                    <td>'.date("jS F, Y", strtotime($booking['date'])).'</td>';
            foreach( $booking['booking'] as $roomCats )
                $tbl .= '<td>'.$roomCats['totat_rooms_available'].'</td>';
            $tbl .= '</tr>';
            $tbl_footer = '</table>';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Monthly-Rooms-Available.pdf', 'D');

    }
    

}

/* End of file inventory.php */
/* Location: ./application/controllers/inventory.php */
