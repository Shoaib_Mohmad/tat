<?php echo $this->session->flashdata('notification');?>

<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Room Categories</h4>
						<span class="tools">
							<a href="<?php echo base_url().'inventory/addCategory';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Category</button></a>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:5%">#</th>
									<th>Category Name</th>	
									<th>Description</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($categories as $category) { ?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $category['category_name']; ?></td>
									<td><?php echo $category['description']; ?></td>
									<td>
										<a href="<?php echo base_url().'inventory/editCategory/'. $category['id']?>" title="Edit Category"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> | 
										<a href="<?php echo base_url()?>inventory/roomCategories/delete/<?php echo $category['id']; ?>" title="Delete Category" onclick="return confirm('Are you sure you want to delete the Room Category?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
