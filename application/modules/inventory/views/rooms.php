<?php echo $this->session->flashdata('notification');?>

<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Rooms Available</h4>
						<span class="tools">
							<div class="btn-group">
								<a href="<?php echo base_url().'inventory/addRoom';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Room</button></a>
							</div>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:5%">#</th>
									<th>Room Category</th>	
									<th>Description</th>
									<th>Rooms Available</th>										
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($rooms as $room) { ?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $room['category_name']; ?></td>
									<td><?php echo $room['description']; ?></td>
									<td><?php echo $room['rooms_available']; ?></td>
									<td>
										<a href="<?php echo base_url().'inventory/editRoom/'. $room['id']?>" title="Edit Room"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> | 
										<a href="<?php echo base_url()?>inventory/rooms/delete/<?php echo $room['id']; ?>" title="Delete Rooms" onclick="return confirm('Are you sure you want to delete the Room?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
