<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Select Date Range</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>		
               <a href="javascript:;" class="icon-remove"></a>
               </span>							
            </div>
            <div class="widget-body form">
            <form action="<?php echo base_url().'inventory/makeBooking/search';?>" method="POST" class="form-horizontal">
               <!-- BEGIN FORM-->
              <div class="form-horizontal">
                  <div class="control-group">
                     <label class="control-label">Date</label>
                     <div class="controls">
                        <div class="input-prepend">
                           <span class="add-on"><i class="fa fa-calendar"></i></span><input type="text" name="date" required placeholder="<?php echo date('m/d/Y'); ?>" class="input-medium date-range" />
                        </div>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Show Available Rooms </button>
                  </div>
               </div>
               </form>
               <!-- END FORM-->	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>
   </div>

<?php if( isset( $search ) ): ?>            
              
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE PORTLET-->
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Rooms Available</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body">
               <table class="table table-striped table-bordered" id="sample_1">
                  <thead>
                     <tr>
                        <th style="width: 5%">#</th>
                        <th style="width: 13.5%;">Room Category</th>
                        <th style="width: 13.5%;">Rooms Available</th>
                        <th style="width: 10%;">Book Rooms</th>
                        <th style="width: 10%;">Extra Bed</th>
                        <th style="width: 13.5%;">Meal Plan</th>
                        <th style="width: 15%;">Amount (<i class="fa fa-fw fa-rupee">)</th>
                        <th style="width: 13.5%;">Action</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  <?php 
                     $sno = 1; 
                     $max = sizeof($rooms);
                     for( $i = 0; $i< $max; $i++ ): 
                  ?>
                     <tr class="odd gradeX">
                        <form action="<?php echo base_url().'inventory/makeBooking/addGuest'; ?>" method="POST">
                           <input type="hidden" name="category_id" value="<?php echo $rooms[$i]['category_id']; ?>">
                           <td><?php echo $sno++; ?></td>
                           <td><?php echo $rooms[$i]['category_name']; ?></td>
                           <td><?php echo $rooms[$i]['totat_rooms_available']; ?></td>
                           <td>
                              <select name="no_of_rooms" class="span12" required>
                              <?php for ($j=1; $j<=$rooms[$i]['totat_rooms_available'] ; $j++) { ?>
                                 <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                              <?php } ?>
                              </select>
                           </td>
                           <td>
                              <select name="extra_bed" class="span12">
                                 <option value="">Select</option>
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                              </select>
                           </td>
                           <td>
                              <select name="meal_plan" class="span12" required>
                                 <option value="">Select</option>
                                 <option value="EP">EP</option>
                                 <option value="CP">CP</option>
                                 <option value="MAP">MAP</option>
                                 <option value="AP">AP</option>
                              </select>
                           </td>
                           
                           <td>
                              <input type="number" name="amount" placeholder="Enter Amount" class="span10" required>
                           </td>
                           <td><center><button class="btn btn-warning btn-sm"> Book Now!</button></center></td>
                        </form>
                     </tr>
                  
                     <?php endfor;?>
                     
                  </tbody>
               </table>
            </div>
         </div>
         <!-- END EXAMPLE TABLE PORTLET-->
      </div>
   </div>		
       
<?php endif; ?>     
