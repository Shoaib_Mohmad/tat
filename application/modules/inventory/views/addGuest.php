<?php echo $this->session->flashdata('notification');?>
<script src="<?php echo base_url().'assets/js/countries.js';?>" type="text/javascript"></script>
<script type="text/javascript">
	function displayCustomerDetails( paramID )
	{
		if(paramID == null)
			var id = document.getElementById('customer').value;
		else
			var id = paramID;
		if(id == 'na'){
			document.getElementById('invoice_to').value = null;
		    document.getElementById('txtEmail').value 	= null;
		    document.getElementById('phone').value 		= null;
		    document.getElementById('landline').value 	= null;
		    document.getElementById('country').value 	= null;
		    document.getElementById('state').value 		= null;
		    document.getElementById('city').value 		= null;
		    document.getElementById('pin_code').value 	= null;
		    $('#country').val(null);
		    return false;
		}
		$.ajax({
		  type: 'POST',
		  url: '<?php echo base_url()."index.php/services/getCustomerDetails";?>',
		  data: { id: id },
		  success:function(data){
		  	var jsonObj = JSON.parse(data);
		    document.getElementById('invoice_to').value = jsonObj.name;
		    document.getElementById('txtEmail').value 	= jsonObj.email;
		    document.getElementById('phone').value 		= jsonObj.phone;
		    document.getElementById('landline').value 	= jsonObj.landline;
		    document.getElementById('country').value 	= jsonObj.country;
		    document.getElementById('state').value 		= jsonObj.state;
		    document.getElementById('city').value 		= jsonObj.city;
		    document.getElementById('pin_code').value 	= jsonObj.pin;
		  }
		});
	}


	function insertCustomerDetails()
	{
		var name 		= 	document.getElementById('custname').value;
	   	var email 		= 	document.getElementById('custemail').value;
	   	var phone 		= 	document.getElementById('custphone').value;
	   	var landline	= 	document.getElementById('custlandline').value;
	   	var country 	= 	document.getElementById('custcountry').value;
	   	var state 		= 	document.getElementById('custstate').value;
	   	var city 		= 	document.getElementById('custcity').value;
	   	var pin 		= 	document.getElementById('custpin').value;
		$.ajax({
		  type: 'POST',
		  url: '<?php echo base_url()."index.php/services/insertCustomerDetails";?>',
		  data: { name: name, email: email, phone: phone, landline: landline, country: country, state: state, city: city, pin: pin },
		  success:function( id ){
	        $('#myModal2').modal('hide');
	        var x    	  =   document.getElementById('customer');
	        var option    =   document.createElement("option");
            option.value  =   id;
            option.text   =   name;
            x.add(option);
	        $('select').trigger('liszt:updated'); 
	        $("select").val(id);
			$("select").trigger("liszt:updated");
		  	displayCustomerDetails( id );
		  },
			error:function(){
			  	$('#addCustomerMessage').html('<div  style="width: 85%; margin-left: 16px; margin-bottom: 0em;" class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
			}
		});
		return false;
	}
</script>

<div class="row-fluid">
	<div class="span12">
	<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="widget">
		<div class="widget-title">
			<h4>
			<i class="icon-reorder"></i>Add Guest</h4>
			
		</div>
		<div class="widget-body form">
		<!-- BEGIN FORM-->
		<form action="<?php echo base_url().'inventory/makeBooking/bookRoom';?>" method="POST" class="form-horizontal">
			
			<div class="control-group">
				<label class="control-label" for="input1">Customer Name</label>
				<div class="controls">
					<select class="chosen span6" onchange="displayCustomerDetails();" id="customer" name="customer_id" required>
						<option value="na">Select Existing Customer</option>
						<?php 
							foreach($customers as $customer){
						?>
							<option value="<?php echo $customer['id'];?>"><?php echo $customer['name'];?></option>
						<?php
							}
						?>
					</select>
					<input type="button" style="margin-left: 1%; margin-top:-2.5%;" class="btn btn-inverse" id="demo" value="OR">
					<a href="#myModal2" style="margin-left: 1%; margin-top:-2.5%;" role="button" class="btn btn-inverse" data-toggle="modal">  +  </a>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary">Submit</button>
				<button type="button" class="btn">Cancel</button>
			</div>
		</form>
		<!-- END FORM-->
		</div>
		</div>
	<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>


<!-- Modal 2 Starts Here -->
	<div style="margin-top: -2em;" id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel1">Add New Customer</h3>
		</div>
		<div id="addCustomerMessage">
			
		</div>
		<form onSubmit = "return insertCustomerDetails();" style="padding-top:1em;"  class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="input1">Customer Name</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Customer Name" class="span6" id="custname" name="custname" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Email Address</label>
					<div class="controls">
						<!-- Email validation : onblur= "return checkEmail();" -->
						<input type="email" required placeholder="Enter Email address" class="span6" id="custemail" name="custemail" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Mobile No 
					</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Mobile Number" pattern="\d{10}" title="(Should be numeric 10 digits)" maxlength="10" class="span6" id="custphone" name="custphone" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Landline No</label>
					<div class="controls">
						<input type="text" placeholder="Enter Landline Number" class="span6" id="custlandline" name="custlandline" value="" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input1">Country</label>
					<div class="controls">
						<select class="span6" id="custcountry" name="custcountry" required>
							
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="input1">State</label>
					<div class="controls">
						<select class="span6" id="custstate" name="custstate" required>
									
						</select>
					</div>
				</div>
				
				<script language="javascript">
					populateCountries("custcountry", "custstate");
					populateCountries("country2");
				</script>   
				
				<div class="control-group">
					<label class="control-label" for="input1">City</label>
					<div class="controls">
						<input type="text" required placeholder="Enter City"
						class="span6" id="custcity" name="custcity" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input1">Pin Code</label>
					<div class="controls">
						<input type="text" required placeholder="Enter Pin Code"
						pattern="\d+(\.\d{2})?" class="span6" id="custpin"
						name="custpin" />
					</div>
				</div>

				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" data-dismiss="modal" class="btn">Cancel</button>
				</div>
	</div>
	<!-- Modal 2 Ends Here -->


