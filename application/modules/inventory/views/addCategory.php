<?php echo $this->session->flashdata('notification');?>

<div id="page">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN SAMPLE FORM PORTLET-->	
            <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i><?php if( isset( $category['id'] ) ) echo 'Edit'; else echo 'Add'; ?> Category</h4>
                   <span class="tools">
                   <a href="javascript:;" class="icon-chevron-down"></a>
                   <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                   <a href="javascript:;" class="icon-refresh"></a>		
                   <a href="javascript:;" class="icon-remove"></a>
                   </span>							
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    <form action="<?php if( isset( $category['id'] ) ) echo base_url().'inventory/editCategory/doEdit'; else echo base_url().'inventory/addCategory/add';?>" method="POST" class="form-horizontal">
                        <?php if( isset( $category['id'] ) ): ?>
                            <input type="hidden" name="id" value="<?php if( isset( $category['id'] ) ) echo $category['id']; ?>" />
                        <?php endif; ?>
                        
                        <div class="control-group">
                            <label class="control-label" for="input2">Category Name</label>
                            <div class="controls">
                                <input type="text" required placeholder="Enter Category Name" class="span6" id="input" name="category_name"value="<?php if( isset( $category['category_name'] ) ) echo $category['category_name']; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input4">Cayegory Description</label>
                            <div class="controls">
                                <textarea name="description" rows="5" placeholder="Enter Category Description(Optional)" class="span6"><?php if( isset( $category['description'] ) ) echo $category['description']; ?></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Category</button>
                            </div>
                        </div>
                    </form>
                             <!-- END FORM-->			
                </div>
            </div>
             <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
