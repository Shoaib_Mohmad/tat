<?php echo $this->session->flashdata('notification');?>

<div id="page">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN SAMPLE FORM PORTLET-->	
            <div class="widget">
                <div class="widget-title">
                   <h4><i class="icon-reorder"></i><?php if( isset( $room['id'] ) ) echo 'Edit'; else echo 'Add'; ?> Room</h4>
                   <span class="tools">
                   <a href="javascript:;" class="icon-chevron-down"></a>
                   <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                   <a href="javascript:;" class="icon-refresh"></a>		
                   <a href="javascript:;" class="icon-remove"></a>
                   </span>							
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    <form action="<?php if( isset( $room['id'] ) ) echo base_url().'inventory/editRoom/doEdit'; else echo base_url().'inventory/addRoom/add';?>" method="POST" class="form-horizontal">
                        <?php if( isset( $room['id'] ) ): ?>
                            <input type="hidden" name="id" value="<?php if( isset( $room['id'] ) ) echo $room['id']; ?>" />
                        <?php endif; ?>
                        
                        <div class="control-group">
                            <label class="control-label" for="input1">Room Category</label>
                            <div class="controls">
                                <select required class="span6" name="category_id" required>
                                    <option value="">Select Category</option>
                                    <?php 
                                        foreach($categories as $category){
                                            if( isset( $room['id'] ) )
                                                if( $room['category_id'] !== $category['id'] )
                                                    continue;
                                    ?>
                                        <option value="<?php echo $category['id'];?>" <?php if( isset( $room['id'] ) ) if( $room['category_id'] == $category['id'] ) echo "Selected"; ?>><?php echo $category['category_name'];?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="input3">No of Rooms</label>
                            <div class="controls">
                                <input type="text" required placeholder="Enter No of Rooms" class="span6" id="input" name="rooms_available" value="<?php if( isset( $room['rooms_available'] ) ) echo $room['rooms_available']; ?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="input4">Room Description</label>
                            <div class="controls">
                                <textarea name="description" rows="5" placeholder="Enter Room Description (Optional)" class="span6"><?php if( isset( $room['description'] ) ) echo $room['description']; ?></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Room</button>
                            </div>
                        </div>
                    </form>
                             <!-- END FORM-->			
                </div>
            </div>
             <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
