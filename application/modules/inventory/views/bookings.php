<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Bookings</h4>
					<span class="tools">
						<div class="btn-group">
							<a href="<?php echo base_url().'inventory/makeBooking'; ?>" style="margin: -5px 6px 1px 0px; padding-right:30px;" class="btn btn-primary"><i class="fa fa-plus"></i> Make Booking </a>
						</div>
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>Room Category</th>
								<th>Rooms Booked</th>
								<th>Booked By</th>
								<th>Email</th>
								<th>Mobile No</th>
								<th>CheckIn</th>
								<th>CheckOut</th>
								<th>Booking Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ($bookings as $booking) 
							{ 
								$customer = $this->inventory_model->getCustomerData( $booking['customer_id'], $booking['appcode'] );
						?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><?php echo $booking['category_name']; ?></td>
								<td><center><?php echo $booking['no_of_rooms']; ?></center></td>
								<td><?php echo $customer['name']; ?></td>
								<td><a href="mailto:<?php echo $customer['email']; ?>"><?php echo $customer['email']; ?></a></td>
								<td><?php echo $customer['phone']; ?></td>
								<td><?php print(date("jS F, Y", strtotime($booking['checkInDate'])));?></td>
								<td><?php print(date("jS F, Y", strtotime($booking['checkOutDate'])));?></td>
								<td><?php print(date("jS F, Y", strtotime($booking['createdAt'])));?></td>
								<td>
									<?php if( $booking['status'] == 'Active' ): ?>
										<center>
											<!-- <a href="javascript:void(0);" title="View Booking Invoice" onClick = 'window.open("<?php echo base_url().'inventory/bookingInvoice/'.$booking['id']; ?>", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> |  -->
											<a href="<?php echo base_url().'inventory/bookings/cancel/'.$booking['id']; ?>" title="Cancel Booking" onclick="return confirm('Are you sure you want to Cancel Booking?')"><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
										</center>
									<?php else: ?>
										<center><span class="label label-warning">Cancelled</span></center>
									<?php endif; ?>
								</td>
							</tr>
						<?php } ?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
