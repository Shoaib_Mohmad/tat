<!-- Date Range Pickers -->
   <div class="row-fluid">
      <div class="span6">
         <!-- BEGIN PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Select Date Range</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>		
               <a href="javascript:;" class="icon-remove"></a>
               </span>							
            </div>
            <div class="widget-body form">
            <form action="<?php echo base_url().'inventory/monthlyBookings';?>" method="POST" class="form-horizontal">
               <!-- BEGIN FORM-->
              <div class="form-horizontal">
                  <div class="control-group">
                     <div class="controls"  style="margin-left: 0%;">
                        <select name="month" class="span6" required>
                           <option value="">Select Month</option>
                           <option value="01">January</option>
                           <option value="02">Febuary</option>
                           <option value="03">March</option>
                           <option value="04">April</option>
                           <option value="05">May</option>
                           <option value="06">June</option>
                           <option value="07">July</option>
                           <option value="08">August</option>
                           <option value="09">September</option>
                           <option value="10">October</option>
                           <option value="11">November</option>
                           <option value="12">December</option>
                        </select>
                     
                        <select name="year" required class="span6">
                           <option value="">Select Year</option>
                           <?php 
                              $initialYear = '2015';
                              $currentYear = date('Y');
                              while( $initialYear <= ($currentYear + 1) )
                                 echo '<option value="'.$initialYear.'">'.$initialYear++.'</option>';
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Show Rooms Available!</button>
                  </div>
               </div>
               </form>
               <!-- END FORM-->	
            </div>
         </div>
         <!-- END PORTLET-->
      </div>

<?php if(isset($reportStatus)): ?>

      <div class="span6">
         <!-- BEGIN PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>      
               <a href="javascript:;" class="icon-remove"></a>
               </span>                    
            </div>
            <div class="widget-body form">
               <br><br>
               <form action="<?php echo base_url().'inventory/pdfMonthlyBookings';?>" method="POST">
                  <input type="hidden" name="month" value="<?php echo $month; ?>">
                  <input type="hidden" name="year" value="<?php echo $year; ?>">
                  <input type="hidden" name="heading" value="<?php echo $heading; ?>">
                  <center><button type="submit" style="margin-top:0em;" class="btn btn-primary"><i class="fa fa-download"></i> PDF</button></center>
               </form>
               <br><br>
               </div>
         </div>
         <!-- END PORTLET-->
      </div>

   </div>
              
             
<?php
   endif;
   if(isset($reportStatus)):
?>              
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
               <div class="widget-title">
                  <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
                  <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
                  <a href="javascript:;" class="icon-refresh"></a>      
                  <a href="javascript:;" class="icon-remove"></a>
                  </span>                    
               </div>
               <div class="widget-body">
                  <table class="table table-striped table-bordered" id="monthly_room_bookings">
                     <thead>
                        <tr>
                           <th>Date</th>
                           <?php 
                              foreach( $categories as $category )
                              {
                                 if( $this->inventory_model->catHasRooms( $category['id'] ) )
                                    echo '<th>Room Category ('.$category['category_name'].')</th>'; 
                              }
                           ?>
                        </tr>
                     </thead>
                     <tbody>
                     
                     <?php 
                           foreach ($bookings as $booking) 
                           { 
                           ?>
                        <tr class="odd gradeX">
                           <td><?php print(date("jS F, Y", strtotime($booking['date'])));?></td>
                        
                           <?php 
                              foreach( $booking['booking'] as $roomCats )
                                 echo '<td><center>'.$roomCats['totat_rooms_available'].'</center></td>';
                           ?>
                           </tr>
                     <?php } ?>
                        
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
         </div>
      </div>		
<?php
   endif;
?> 

      
     
