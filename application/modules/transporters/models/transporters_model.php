<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transporters_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function getTransporters( $appcode )
    {
        $transporters   =   $this->db->query("SELECT * FROM transporters WHERE appcode = '$appcode' AND status = 'Active' ORDER BY transporter_name") ->result_array();
        return $transporters;
    }


    function getTransporterTariffs( $transporter_id, $appcode )
    {
        $transporterTariffs   =   $this->db->query("SELECT transporter_tariff.*, vechiles.* FROM transporter_tariff INNER JOIN vechiles ON transporter_tariff.vechile_id = vechiles.id WHERE transporter_tariff.transporter_id = $transporter_id AND transporter_tariff.appcode = '$appcode' AND transporter_tariff.status = 'Active' ORDER BY transporter_tariff.vechile_id")->result_array();
        return $transporterTariffs;
    }


    function getTransporterDetails( $id, $appcode )
    {
        $transporters   =   $this->db->query("SELECT * FROM transporters WHERE id = $id AND appcode = '$appcode' AND status = 'Active'");
        if( !$transporters->num_rows() )
        {
            redirect(base_url().'services/transporters', 'refresh');
        } 
        return $transporters->row_array();
    }


    function getTransportVoucherNo( $appcode )
    {
        $query  =   $this->db->query("SELECT voucher_no FROM transport_vouchers WHERE appcode = '$appcode' ORDER BY voucher_no DESC LIMIT 0,1") -> row_array();
        if(isset($query['voucher_no'])) 
            $voucher_no     =   ++$query['voucher_no'];
        else
            $voucher_no     =   1;
        return $voucher_no;
    }


    function transportVoucherNoAvailable( $voucher_no, $appcode )
    { 
        $query = $this->db->get_where('transport_vouchers', array(
            'voucher_no'     =>     $voucher_no,
            'appcode'        =>     $appcode
        ));

        if ($query->num_rows() == 1 ) 
        {
            return false;        
        } 
        else 
        {
            return true;
        }
    }
    

    function getTransportVouchers( $appcode )
    {
        $vouchers   =   $this->db->query("SELECT * FROM transport_vouchers WHERE appcode = '$appcode' AND status = 'Active' ORDER BY id DESC") -> result_array();
        return $vouchers;
    }


    function getTransportVoucherDetails( $voucher_id, $appcode )
    {
        $vouchers   =   $this->db->query("SELECT * FROM transport_vouchers WHERE id = $voucher_id AND appcode = '$appcode' ORDER BY id DESC") -> row_array();
        return $vouchers;
    }

    

    function getTransportVoucherServices( $voucher_id, $appcode )
    {
        $voucherServices   =   $this->db->query("SELECT * FROM transport_voucher_services WHERE voucher_id = $voucher_id AND appcode = '$appcode'") -> result_array();
        return $voucherServices;
    }

  

    function getTransportVoucherServicesSum( $voucher_id, $appcode )
    {
        $voucherServices   =   $this->db->query("SELECT SUM( rate ) as servicesTotal FROM transport_voucher_services WHERE voucher_id = $voucher_id AND appcode = '$appcode'") -> row_array();
        return $voucherServices['servicesTotal'];
    }


    function getVoucherTransportServicesTotal( $voucher_id )
    {
        $servicesVoucher   =   $this->db->query("SELECT SUM(rate) as total FROM transport_voucher_services WHERE voucher_id = $voucher_id")->row_array();
        return $servicesVoucher['total'];
    }


    function getVechiles( $appcode )
    {
        $vechiles   =   $this->db->query("SELECT * FROM vechiles WHERE appcode = '$appcode' AND status = 'Active'") -> result_array();
        return $vechiles;
    }


    function getVechileDetails( $id, $appcode )
    {
        $vechile   =   $this->db->query("SELECT * FROM vechiles WHERE id = $id AND appcode = '$appcode'") -> row_array();
         return $vechile;
    }


 }