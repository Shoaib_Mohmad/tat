<?php echo $this->session->flashdata('notification');?>
<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>Services</h4>
						<span class="tools">
							<a href="<?php echo base_url().'transporters/addVechile';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Vehicle</button></a>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:8px">#</th>
									<th>Vechile Name</th>
									<th><center>Seats Capacity</center></th>
									<th>Description</th>	
									<th><center>Actions</center></th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($vechiles as $vechile) { ?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><?php echo $vechile['vechile_name']; ?></td>
								<td><center><?php echo $vechile['capacity']; ?></center></td>
								<td><?php echo $vechile['description']; ?></td>
								<td class="hidden-phone">
									<center>
										<a href="<?php echo base_url().'transporters/editVechile/'.$vechile['id']; ?>" title="Edit Vehicle"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
									 	<a href="<?php echo base_url()?>transporters/vechiles/delete/<?php echo $vechile['id']; ?>" title="Delete Vehicle" onclick="return confirm('Are you sure you want to delete the Vehicle?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</center>
								</td>
							</tr>
							 <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
