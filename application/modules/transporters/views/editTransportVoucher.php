<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
        var date = new Date();
        var day  = date.getDate();
        var month  = date.getMonth() + 1;
        var yy  = date.getYear();
        var year = (yy < 1000) ? yy + 1900 : yy;
        var newdate = month + "/" + day + "/" + year;
		$('#date').daterangepicker({ minDate: newdate});   //Here  Set the Min Display Date
		
    });    

    
    function checkDate()
    {
      	var checkIn = new Date($('#checkin').val());
      	var checkOut = new Date($('#checkout').val());
      	if(checkIn > checkOut){
       		alert("Please ensure that the CheckOut Date is greater than the CheckIn Date.");
        	return false;
      	}
      	return true;
  	}


	function getTransporterTrips()
	{
		var transporter_id 		= 	$('#transporter_id').val();
		var serviceLength 		= 	($('.transport-voucher-services .control-group').length).toString();
		var i 					=	serviceLength;
		if( transporter_id == '' )
		{
			for( j=1; j<=i; j++ )
            {
                var x 	= 	document.getElementById('trips_' + j);
		        x.options.length = 0;

		        $('#vechile_' + j).val('');
				$('#capacity_' + j).val('');
				$('#days_' + j).val('');
				$('#rate_' + j).val('');
            }
			return false;
		}

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()."transporters/ajaxGetTransporterTrips";?>',
			data: { transporter_id: transporter_id },
			
			success:function( data )
			{
				jsonObj  =   JSON.parse(data);

		        for( j=i; j>=i; j-- )
		        {
			        var x 	= 	document.getElementById('trips_'+j);
			        x.options.length = 0;
			        var option 		= 	document.createElement("option");
			        option.value 	= 	'';
			        option.text 	= 	'Select Trip';
			        x.add(option);

				    for( k=0; k<jsonObj.length; k++ )
	                {
	                    var option    =   document.createElement("option");
	                    option.value  =   jsonObj[k].trip;
	                    option.text   =   jsonObj[k].trip;
	                    option.setAttribute("vechile_name", jsonObj[k].vechile_name);
	                    option.setAttribute("capacity", jsonObj[k].capacity);
	                    option.setAttribute("days", jsonObj[k].days);
	                    option.setAttribute("rate", jsonObj[k].rate);
	                    x.add(option);
	                }
	            }

			},
			error:function()
            {
                alert('Please check your Internet connection.');
            }	
		});

		return false;
			
	}


	function displayTripDetails( trip )
	{
		var id 	= 	(trip.id).split( "_" );

		var transporter_id 	= 	$('#transporter_id').val();
		var vechile_name 	=	$(('#')+trip.id).find(':selected').attr('vechile_name');
		var capacity 		=	$(('#')+trip.id).find(':selected').attr('capacity');
		var days 			=	$(('#')+trip.id).find(':selected').attr('days');
		var rate 			=	$(('#')+trip.id).find(':selected').attr('rate');

		$('#vechile_' + id[1]).val(vechile_name);
		$('#capacity_' + id[1]).val(capacity);
		$('#days_' + id[1]).val(days);
		$('#rate_' + id[1]).val(rate);
		startTransportCalc();
		return false;
	}


	function startTransportCalc()
	{
		var itemLength 	= 	$('.transport-voucher-services .control-group').length; 
		var trip_cost, amount = 0;

		for( i=1; i<=itemLength; i++ )
		{
			trip_cost 	=	$("#rate_"+i).val();
			if( trip_cost == "" )
			{
				continue;
			}
			else
			{
				amount = amount + parseInt(trip_cost, 10);
			}
		}
		
		if( amount >= 0 )
		{
			$('#total_amount').val(Math.ceil(amount));
		}
		return false;
	}


	function checkPax(pax)
	{
		var id 			= 	(pax.id).split( "_" );
		var pax 		= 	parseInt($('#pax_'+id[1]).val(), 10);
		var capacity 	= 	parseInt($('#capacity_'+id[1]).val(), 10);
		if( pax > capacity )
		{
			alert('Number of Pax are greater than the seat capacity of Vechile!');
			$('#pax_'+id[1]).val('');
		}
		return false;
	}


</script>

<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Edit Transport Voucher
					</h4>
					<span class="tools"> <a href="javascript:;"
						class="icon-chevron-down"></a> <a href="#widget-config"
						data-toggle="modal" class="icon-wrench"></a> <a
						href="javascript:;" class="icon-refresh"></a> <a
						href="javascript:;" class="icon-remove"></a>
					</span>
				</div>

				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form onsubmit="return checkDate();" action="<?php echo base_url();?>transporters/insertTransportVoucher" method="POST" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $voucher['id']; ?>">
						<div class="control-group">
							<label class="control-label" for="input1">Voucher Date</label>
							<div class="controls">
								<input class="input-small date-picker span2" required size="16"
									type="text" placeholder="MM/DD/YYYY" name="voucher_date" id="voucher_date" value="<?php echo date('m/d/Y', strtotime($voucher['voucher_date'])); ?>" />
							</div>
						</div>
						
						<input type="hidden" readonly required class="span6" id="invoice_to" name="invoice_to" />
							
						<div class="control-group">
							<label class="control-label" for="input1">Invoice No</label>
							<div class="controls">
								<select class="chosen span6" id="invoice_no" name="invoice_no" required>
									<option value="na">Select Invoice No</option>
									<?php 
										foreach($invoiceNos as $invoiceNo)
										{
									?>
										<option value="<?php echo $invoiceNo['invoice_code'];?>" <?php if( $voucher['invoice_no'] == $invoiceNo['invoice_code'] ) echo 'Selected'; ?> ><?php echo $invoiceNo['invoice_code'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Transporter Name</label>
							<div class="controls">
								<select onchange="getTransporterTrips();" class="chosen span6" id="transporter_id" name="transporter_id" required>
									<option value="">Select Existing Transporter</option>
									<?php 
										foreach($transporters as $transporter)
										{
									?>
										<option value="<?php echo $transporter['id'];?>" <?php if( $voucher['transporter_id'] == $transporter['id'] ) echo 'Selected'; ?> ><?php echo $transporter['transporter_name'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Pickup</label>
							<div class="controls">
								<select class="chosen span6" id="hotel_id" name="hotel_id" required>
									<option value="">Select Location</option>
									<?php 
										foreach($hotels as $hotel)
										{
									?>
										<option value="<?php echo $hotel['id'];?>" <?php if( $voucher['pickup'] == $hotel['id'] ) echo 'Selected'; ?> ><?php echo $hotel['hotel_name'];?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>

						<div class="transport-voucher-services">
							<label class="control-label" for="inputText">Add Room</label>
							<?php 
								$sno = 1; 
								$total = 0; 
								$currentTariff = array();
								foreach( $voucherServices as $service ): 
							?>
								<div class="control-group">

									<div class="controls">
										
										<select class="span4" onchange="displayTripDetails(this);" id="trips_<?php echo $sno; ?>" name="trip[]">
											<?php foreach( $tariffs as $tariff ): ?>
												<option value='<?php echo $tariff['trip']; ?>' vechile_name="<?php echo $tariff['vechile_name']; ?>" capacity="<?php echo $tariff['capacity']; ?>" days="<?php echo $tariff['days']; ?>" rate="<?php echo $tariff['rate']; ?>" <?php if( $tariff['trip'] == $service['trip'] ) echo 'Selected'; ?> ><?php echo $tariff['trip']; ?></option>
											<?php endforeach; ?>
				   						</select>
				   						
				   						<input type="text" autocomplete="off" readonly placeholder="Vechile" class="span2" id="vechile_<?php echo $sno; ?>" name="vechile[]"value="<?php echo $service['vechile']; ?>" />

				   						<input type="number" autocomplete="off" readonly placeholder="Capacity" class="span2" id="capacity_<?php echo $sno; ?>" name="capacity[]" value="<?php echo $service['capacity']; ?>"/>

				   						<input type="number" autocomplete="off" readonly placeholder="Days" class="span2" id="days_<?php echo $sno; ?>" name="days[]" value="<?php echo $service['days']; ?>"/>

				   						<input type="number" autocomplete="off" readonly placeholder="Rate" class="span2" id="rate_<?php echo $sno; ?>" name="rate[]" value="<?php echo $service['rate']; ?>"/>

				   						<input type="number" autocomplete="off" onInput="checkPax(this);"  placeholder="No of Pax" class="span2" id="pax_<?php echo $sno; ?>" name="pax[]" value="<?php echo $service['pax']; ?>" />

				   						<input class="input-small date-picker span2" required type="text" placeholder="Date of Travel" name="dot[]" id="dot_<?php echo $sno++; ?>" value="<?php echo date( 'm/d/Y', strtotime( $service['dot'] ) ); ?>" />
											
									</div>
								</div>
							<?php 
								$total += $service['rate']; 
								endforeach; 
							?>
						</div>
						<br>
						<div class="control-group">
							<div class="controls">
								<input type="button" class="btn btn-success" value="Add Trip" id="addTransportVoucherButton"> 
								<input type="button" class="btn btn-danger"	value="Remove Trip" id="removeTransportVoucherButton">
							</div>							
						</div>
												
						<div class="control-group">
							<label class="control-label" for="input1">Total Amount (<i class="fa fa-fw fa-rupee"></i>)</label>
							<div class="controls">
								<input type="text" autocomplete="off" readonly pattern="\d+(\.\d{2})?" class="span2" id="total_amount" name="total_amount" value="<?php echo $total; ?>" />
							</div>
						</div>
						
						<div class="control-group">
	                        <label class="control-label" for="input1">Remarks</label>
	                        <div class="controls">
	                            <textarea name="remarks" rows="3" placeholder="Enter Additional Information (If Any)" class="span6"><?php echo $voucher['remarks']; ?></textarea>
	                        </div>
	                     </div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Update</button>
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>

</div>
