<?php echo $this->session->flashdata('notification');?>

<div id="page">
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php if( isset( $vechile['id'] ) ) echo 'Edit'; else echo 'Add'?> Vechile</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a>
               <a href="javascript:;" class="icon-refresh"></a>		
               <a href="javascript:;" class="icon-remove"></a>
               </span>							
            </div>
            <div class="widget-body form">
                <!-- BEGIN FORM-->
               <form action="<?php if( isset( $vechile['id'] ) ) echo base_url().'transporters/editVechile/update'; else echo base_url().'transporters/addVechile'?>" method="POST" class="form-horizontal">
                  <?php if( isset( $vechile['id'] ) ): ?>
                        <input type="hidden" name="id" value="<?php if( isset( $vechile['id'] ) ) echo $vechile['id']; ?>" />
                    <?php endif; ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">Vechile Name</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter Vechile Name" class="span6" id="input" name="vechile_name" value="<?php if( isset( $vechile['id'] ) ) echo $vechile['vechile_name']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Seats Capacity</label>
                     <div class="controls">
                        <input type="number" required placeholder="Enter Seats Capacity" class="span6" id="input" name="capacity" value="<?php if( isset( $vechile['id'] ) ) echo $vechile['capacity']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Description</label>
                     <div class="controls">
                        <textarea name="description" rows="5" placeholder="Enter Vechile Description(optional)" class="span6"><?php if( isset( $vechile['id'] ) ) echo $vechile['description']; ?></textarea>
                     </div>
                  </div>
                 <div class="control-group">
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save Vechile</button>
                  </div>
                  </div>
               </form>
                         <!-- END FORM-->			
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>
  
  
</div>
