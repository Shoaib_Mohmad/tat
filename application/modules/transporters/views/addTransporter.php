<?php echo $this->session->flashdata('notification');?>

<div id="page">
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php if( isset( $transporter['id'] ) ) echo 'Edit'; else echo 'Add'?> Transporter</h4>
               					
            </div>
            <div class="widget-body form">
                <!-- BEGIN FORM-->
               <form action="<?php if( isset( $transporter['id'] ) ) echo base_url().'transporters/addTransporter/update'; else echo base_url().'transporters/addTransporter/add'?>" method="POST" class="form-horizontal">
                  <?php if( isset( $transporter['id'] ) ): ?>
                        <input type="hidden" name="id" value="<?php if( isset( $transporter['id'] ) ) echo $transporter['id']; ?>" />
                    <?php endif; ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">Transporter Name</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter Transporter Name" class="span6" id="input" name="transporter_name" value="<?php if( isset( $transporter['transporter_name'] ) ) echo $transporter['transporter_name']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Address</label>
                     <div class="controls">
                        <textarea required name="address" rows="3" placeholder="Enter Transporter Address" class="span6"><?php if( isset( $transporter['address'] ) ) echo $transporter['address']; ?></textarea>
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Phone No</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter Phone No" class="span6" id="input" name="phone" value="<?php if( isset( $transporter['phone'] ) ) echo $transporter['phone']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Fax</label>
                     <div class="controls">
                        <input type="text" placeholder="Enter Fax" class="span6" id="input" name="fax" value="<?php if( isset( $transporter['fax'] ) ) echo $transporter['fax']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Email</label>
                     <div class="controls">
                        <input type="email" required placeholder="Enter Email" class="span6" id="input" name="email" value="<?php if( isset( $transporter['email'] ) ) echo $transporter['email']; ?>" />
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="input1">Website</label>
                     <div class="controls">
                        <input type="text" placeholder="Enter Website (Optional)" class="span6" id="input" name="website" value="<?php if( isset( $transporter['website'] ) ) echo $transporter['website']; ?>" />
                     </div>
                  </div>
                  
                  <div class="control-group">
                     <label class="control-label" for="input1">Description</label>
                     <div class="controls">
                        <textarea name="description" rows="3" placeholder="Enter Transporter Description (Optional)" class="span6"><?php if( isset( $transporter['description'] ) ) echo $transporter['description']; ?></textarea>
                     </div>
                  </div>
                 <div class="control-group">
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">Save Transporter</button>
                  </div>
                  </div>
               </form>
                         <!-- END FORM-->			
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>

<?php if( isset( $transporter['id'] ) ): ?>

   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->   
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i>Transporter Tariff</h4>
                              
            </div>
            <div class="widget-body form">
                <!-- BEGIN TABLE-->
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th width="20%">Vehicle Type</th>
                        <th width="25%">Trip</th>
                        <th width="15%">No of Days</th>
                        <th width="15%">Rate (<i class="fa fa-fw fa-rupee">)</th>
                        <th width="10%"><center>Actions</center></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php foreach( $tariffs as $tariff ): ?>
                        <tr class="odd gradeX">
                           <form action="<?php echo base_url().'transporters/addTransporterTrip/update'; ?>" method="POST">
                              <input type="hidden" name="id" value="<?php echo $tariff['id']; ?>">
                              <input type="hidden" name="transporter_id" value="<?php echo $transporter['id']; ?>">
                              
                              <td>
                                 <select name="vechile_id" class="span10" required>
                                    <option value=""></option>
                                    <?php foreach( $vechiles as $vechile ): ?>
                                       <option value="<?php echo $vechile['id']; ?>" <?php if( $tariff['vechile_id'] == $vechile['id'] ) echo 'Selected'; ?> ><?php echo $vechile['vechile_name']; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </td>
                              <td><input type="text" class="span12" placeholder="TRIP" name="trip" required value="<?php echo $tariff['trip']; ?>"></td>
                              <td><input type="number" class="span8" placeholder="DAYS" name="days" required value="<?php echo $tariff['days']; ?>"></td>
                              <td><input type="number" class="span8" placeholder="RATE" name="rate" required value="<?php echo $tariff['rate']; ?>"></td>
                              
                              <td>
                                 <center>
                                    <button class="btn btn-alert btn-sm" title="Update Trip" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button>
                                    <a href="<?php echo base_url().'transporters/deleteTrip/'.$transporter['id'].'/'.$tariff['id']; ?>" title="Delete Trip" class="btn btn-alert btn-sm"><i class="fa fa-trash-o" style="font-size: 18px; color: red;"></i></a>
                                 </center>
                              </td>
                           </form>
                        </tr>
                     <?php endforeach; ?>
                     <tr class="odd gradeX">
                        <form action="<?php echo base_url().'transporters/addTransporterTrip'; ?>" method="POST">
                           <td>
                              <input type="hidden" name="transporter_id" value="<?php echo $transporter['id']; ?>">
                              <select name="vechile_id" class="span10" required>
                                 <option value=""></option>
                                 <?php foreach( $vechiles as $vechile ): ?>
                                    <option value="<?php echo $vechile['id']; ?>"><?php echo $vechile['vechile_name']; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </td>
                           
                           <td><input type="text" class="span12" placeholder="TRIP" name="trip" required></td>
                           <td><input type="number" class="span8" placeholder="DAYS" name="days" required></td>
                           <td><input type="number" class="span8" placeholder="RATE" name="rate" required></td>
                           <td><center><button class="btn btn-alert btn-sm" title="Add Tariff" type="Submit"><i class="fa fa-check" style="font-size: 18px; color: green;"></i></button></center></td>
                        </form>
                     </tr>
                  </tbody>
               </table>
                         <!-- END TABLE-->       
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>


<?php endif; ?>

</div>

