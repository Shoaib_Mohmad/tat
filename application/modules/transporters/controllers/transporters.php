<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transporters extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'transporters';
	
	function __construct()
	{
		parent::__construct();

		$this->user_model->checkUserCanAccess('transporters');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('sales/sales_model');
		$this->load->model('hotels/hotels_model');
		$this->load->model('transporters/transporters_model');
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function index( $param1 = '', $param2 = '' )
	{
		$appcode 				=	$this->session->userdata['appcode'];
		
		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$data['status']  	= 	'Deleted';

			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('transporters', $data);

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transporter Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters', 'refresh'); 
		}
		
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Transporters";
		$data['fileName'] 		= 	"transporters.php";
		$data['transporters'] 	= 	$this->transporters_model->getTransporters( $appcode );
		$this->load->view( 'index', $data );
	}


	public function addTransporter( $param1 = '', $param2 = '' )
	{
		$appcode 	=	$this->session->userdata['appcode'];

		if( $param1 == 'add' && $_POST )
		{	
			$data['appcode'] 			= 	$appcode;
			$data['transporter_name']  	= 	trim( $_POST['transporter_name'] );
			$data['address']  			= 	trim( $_POST['address'] );
			$data['phone']  			= 	trim( $_POST['phone'] );	
			$data['fax']  				= 	trim( $_POST['fax'] );	
			$data['email']  			= 	trim( $_POST['email'] );
			$data['website']  			= 	trim( $_POST['website'] );
			$data['description']  		= 	trim( $_POST['description'] );

			$result 		= 	$this->db->insert('transporters', $data); 
			$transporterId 	= 	$this->db->insert_id();
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transporter Details Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters/addTransporter/edit/'.$transporterId, 'refresh');
		}

		if( $param1 == 'update' && $_POST )
		{	
			$id  						= 	trim( $_POST['id'] );
			$data['transporter_name']  	= 	trim( $_POST['transporter_name'] );
			$data['address']  			= 	trim( $_POST['address'] );
			$data['phone']  			= 	trim( $_POST['phone'] );	
			$data['fax']  				= 	trim( $_POST['fax'] );	
			$data['email']  			= 	trim( $_POST['email'] );
			$data['website']  			= 	trim( $_POST['website'] );
			$data['description']  		= 	trim( $_POST['description'] );

			$this->db->where( array('id' => $id, 'appcode' => $appcode ) );
			$result = $this->db->update('transporters', $data); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Hotel Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters/addTransporter/edit/'.$id, 'refresh');
		}

		if( $param1 == 'edit' )
		{
			$data['transporter'] 	= 	$this->transporters_model->getTransporterDetails( $param2, $appcode );
			$data['tariffs'] 		= 	$this->transporters_model->getTransporterTariffs( $param2, $appcode );
			$data['vechiles']		=	$this->transporters_model->getVechiles( $appcode );
			$data['pageName'] 		= 	"Edit Transporter";
		}
		else
		{
			$data['pageName'] 		= 	"Add Transporter";
		}

		$data['pathType'] 		= 	$this->pathType;
		$data['fileName'] 		= 	"addTransporter.php";
		$this->load->view( 'index', $data );
	}


	public function addTransporterTrip( $param1 = '' )
	{
		if( $_POST )
		{
			$appcode 	=	$this->session->userdata['appcode'];

			if( $param1 == 'update' )
			{
				$data['vechile_id']  	= 	trim( $_POST['vechile_id'] );
				$data['trip']  			= 	trim( $_POST['trip'] );	
				$data['days']  			= 	trim( $_POST['days'] );	
				$data['rate']  			= 	trim( $_POST['rate'] );

				$this->db->where( array('id' => $_POST['id'], 'appcode' => $appcode ) );
				$result 	= 	$this->db->update('transporter_tariff', $data); 
			}
			else
			{
				$data['appcode'] 		= 	$appcode;
				$data['transporter_id'] = 	trim( $_POST['transporter_id'] );
				$data['vechile_id']  	= 	trim( $_POST['vechile_id'] );
				$data['trip']  			= 	trim( $_POST['trip'] );	
				$data['days']  			= 	trim( $_POST['days'] );	
				$data['rate']  			= 	trim( $_POST['rate'] );

				$result 	= 	$this->db->insert('transporter_tariff', $data); 
			}

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transporter Trip Saved Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters/addTransporter/edit/'.$_POST['transporter_id'], 'refresh');
		}
		else
		{
			redirect(base_url().'transporters', 'refresh');
		}
	}


	public function deleteTrip( $param1 = '', $param2 = '' )
	{
		if( is_numeric( $param1 ) && is_numeric( $param2 ) )
		{
			$appcode 			=	$this->session->userdata['appcode'];
			$data['status']  	= 	'Deleted';
			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result 	= 	$this->db->update('transporter_tariff', $data); 

			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Trip Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters/addTransporter/edit/'.$param1, 'refresh');
		}
		else
		{
			redirect(base_url().'transporters', 'refresh');	
		}
	}


	public function transporter( $transporterId )
    {
    	if( !is_numeric( $transporterId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Transporter Tariff');
        $pdf->SetSubject('OCTA Travel Transporter Tariff');
        $pdf->SetKeywords('OCTA Travel, Transporter Tariff');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Transporter Details');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

        $transporter     =   $this->transporters_model->getTransporterDetails( $transporterId, $appcode );


        // set font
        //$pdf->setCellPaddings ('', '', '', '5px');
        // add a page
        $pdf->AddPage('P');
		$pdf->SetFont('helveticab', 'B', 15);

		$pdf->Cell(100, 0, $transporter['transporter_name'], 0, 1 );
		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 0, $transporter['address'], 0, 1 );
		
		$pdf->Ln(3);

		$pdf->SetFont('times', '', 11);
		$pdf->Cell(30, 6, 'PHONE', 0, 0 );
		$pdf->Cell(100, 6, $transporter['phone'], 0, 1 );

		$pdf->Cell(30, 6, 'FAX ', 0, 0 );
		$pdf->Cell(100, 6, $transporter['fax'], 0, 1 );

		$pdf->Cell(30, 6, 'EMAIL ', 0, 0 );
		$pdf->Cell(100, 6, $transporter['email'], 0, 1 );

		$pdf->Cell(30, 6, 'WEBSITE ', 0, 0 );
		$pdf->Cell(100, 6, $transporter['website'], 0, 1 );
		$pdf->Cell(30, 6, 'DESCRIPTION ', 0, 0 );
		$pdf->Cell(100, 6, $transporter['description'], 0, 1 );

		$pdf->Ln(10);

		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 10, 'TARIFF', 0, 1 );
		
		$pdf->SetTextColor(0,0,0);

		$tbl_header = '<table cellpadding="10" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#333; font-size:12px; font-weight: bold">
						<th> TRIP   </th>
						<th> VEHICLE</th>
						<th style="text-align: center;"> CAPACITY 	</th>
						<th style="text-align: center;"> DAYS   </th>
						<th style="text-align: center;"> RATE  </th>
					</tr>';
		
		$tariffs 	= 	$this->transporters_model->getTransporterTariffs( $transporterId, $appcode );

		foreach ($tariffs as $tariff ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td>'.$tariff['trip'].'</td>
							<td>'.$tariff['vechile_name'].'</td>
							<td style="text-align: center;">'.$tariff['capacity'].	'</td>
							<td style="text-align: center;">'.$tariff['days'].'</td>
							<td style="text-align: center;">'.$tariff['rate'].' INR</td>
						</tr>
					';
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Transporter_Tariff.pdf', 'I');

    }


    public function transportVouchers( $param1 = '', $param2 = '' )
	{
		$data['appcode'] 		=	$appcode 				=	$this->session->userdata['appcode'];
		
		if( $param1 = 'delete' && is_numeric( $param2 ) )
		{
			$voucherData['status'] 	=	'Deleted';
			$this->db->where( array('id' => $param2, 'appcode' => $appcode ) );
			$result = $this->db->update('transport_vouchers', $voucherData); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Voucher Deleted Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url() . 'transporters/transportVouchers', 'refresh');
		}

		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Transport Vouchers";
		$data['fileName'] 		= 	"transportVouchers.php";
		$data['vouchers'] 		= 	$this->transporters_model->getTransportVouchers( $appcode );
		$this->load->view( 'index', $data );
	}


	public function addTransportVoucher()
	{
		$appcode 			=	$this->session->userdata['appcode'];
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Transport Voucher";
		$data['fileName'] 	= 	"addTransportVoucher.php";
		$data['transporters'] 	= 	$this->transporters_model->getTransporters( $appcode );
		$data['invoiceNos'] 	= 	$this->sales_model->getInvoiceNos( $appcode );
		$data['hotels'] 		= 	$this->hotels_model->getHotels( $appcode );
		$this->load->view( 'index', $data );
	}


	public function insertTransportVoucher()
	{
		$user_id					=	$this->session->userdata['id'];
		$appcode					=	$this->session->userdata['appcode'];
		$voucher_date_received  	=   trim($_POST['voucher_date']);
		$voucher_date_object 		= 	new DateTime($voucher_date_received);
		$voucher_date				= 	$voucher_date_object->format('Y-m-d');
		$invoiceData 				=	$this->sales_model->getInvoiceUserData( $appcode, $_POST['invoice_no'] );
				
		$trip     	=   $_POST['trip'];
		$vechile    =   $_POST['vechile'];
		$capacity   =   $_POST['capacity'];
		$days      	=   $_POST['days'];
		$rate    	=   $_POST['rate'];
		$pax      	=   $_POST['pax'];
		$dot      	=   $_POST['dot'];

		$voucherData['appcode']   		 	 =   $appcode;
		$voucherData['voucher_date']   		 =   $voucher_date;
		$voucherData['transporter_id']    	 =   $_POST['transporter_id'];
		$voucherData['voucher_no']  	 	 =   $this->transporters_model->getTransportVoucherNo( $appcode );
		$voucherData['invoice_no']  	 	 =   $_POST['invoice_no'];
		$voucherData['pickup']    		 	 =   $_POST['hotel_id'];
		$voucherData['customer_name']     	 =   $invoiceData['invoice_to'];
		$voucherData['customer_email']     	 =   $invoiceData['email'];
		$voucherData['customer_mobile']   	 =   $invoiceData['phone'];
		$voucherData['customer_city'] 	  	 =   $invoiceData['city'];
		$voucherData['customer_state'] 	  	 =   $invoiceData['state'];
		$voucherData['customer_country'] 	 =   $invoiceData['country'];
		$voucherData['customer_pin'] 	 	 =   $invoiceData['pin_code'];
		$voucherData['arrival']       		 =   $invoiceData['arrival'];
		$voucherData['departure']      		 =   $invoiceData['departure'];
		$voucherData['remarks']    	 		 =   trim($_POST['remarks']);

		if( $this->transporters_model->transportVoucherNoAvailable( $voucherData['voucher_no'], $appcode ) )
		{ 
			$result   		=   $this->db->insert('transport_vouchers', $voucherData);
			$lastinsert_id 	= 	$this->db->insert_id();
			$voucherServices['voucher_id']  	=  	$lastinsert_id;
			$voucherServices['appcode'] 	 	=	$appcode;

			foreach ( $trip as $key=>$value )
			{
				$voucherServices['trip'] 		= 	$value;
				$voucherServices['vechile'] 	= 	$vechile[$key];
				$voucherServices['capacity'] 	= 	$capacity[$key];
				$voucherServices['days'] 		= 	$days[$key];
				$voucherServices['pax'] 		= 	$pax[$key];
				$travel_date					= 	new DateTime($dot[$key]);
				$voucherServices['dot']			= 	$travel_date->format('Y-m-d');
				$voucherServices['rate']		= 	$rate[$key];
				$this->db->insert('transport_voucher_services', $voucherServices);
			}
		}
		else
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Voucher No Already Exists!</div>'));
			redirect(base_url() . 'transporters/addTransportVoucher', 'refresh');
		}

		if( $result )
		{
			$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Transport Voucher Added Successfully!</div>'));
			redirect(base_url() . 'transporters/transportVouchers', 'refresh');
		}
		else
		{
			redirect(base_url(), 'refresh');
		}
	}


	public function editTransportVoucher( $param1 = '' )
	{
		$appcode 					=	$this->session->userdata['appcode'];
		$data['pathType'] 			= 	$this->pathType;
		$data['pageName'] 			= 	"Transport Voucher";
		$data['fileName'] 			= 	"editTransportVoucher.php";
		$data['transporters'] 		= 	$this->transporters_model->getTransporters( $appcode );
		$data['invoiceNos'] 		= 	$this->sales_model->getInvoiceNos( $appcode );
		$data['hotels'] 			= 	$this->hotels_model->getHotels( $appcode );
		$data['services'] 			= 	$this->transporters_model->getServiceTypes( $appcode );
		$data['voucher']			= 	$this->transporters_model->getTransportVoucherDetails( $param1, $appcode, 'Transport' );
		$data['voucherServices'] 	= 	$this->transporters_model->getTransportVoucherServices( $param1, $appcode );
		$data['tariffs'] 			= 	$this->transporters_model->getTransporterTariffs( $data['voucher']['transporter_id'], $appcode );
		$this->load->view( 'index', $data );
	}


	public function updateTransportVoucher()
	{
		if( $_POST )
		{
			$user_id					=	$this->session->userdata['id'];
			$appcode					=	$this->session->userdata['appcode'];
			$voucher_date_received  	=   trim($_POST['voucher_date']);
			$voucher_date_object 		= 	new DateTime($voucher_date_received);
			$voucher_date				= 	$voucher_date_object->format('Y-m-d');

			$invoiceData 				=	$this->sales_model->getInvoiceUserData( $appcode, $_POST['invoice_no'] );

			$date 						=   explode('-', $_POST['date']);
			$checkin_date_received  	=   $date[0];
			$checkin_date_object 		= 	new DateTime($checkin_date_received);
			$checkin					= 	$checkin_date_object->format('Y-m-d');
			$checkout_date_received 	=   $date[1];
			$checkout_date_object 		= 	new DateTime($checkout_date_received);
			$checkout					= 	$checkout_date_object->format('Y-m-d');
					
			$trip     	=   $_POST['trip'];
			$vechile    =   $_POST['vechile'];
			$capacity   =   $_POST['capacity'];
			$days      	=   $_POST['days'];
			$rate    	=   $_POST['rate'];
			$pax      	=   $_POST['pax'];
			$dot      	=   $_POST['dot'];

			$voucherData['appcode']   	 		 =   $appcode;
			$voucherData['voucher_date'] 		 =   $voucher_date;
			$voucherData['invoice_no']   		 =   $_POST['invoice_no'];
			$voucherData['voucher_to']   		 =   $_POST['transporter_id'];
			$voucherData['customer_name']     	 =   $invoiceData['invoice_to'];
			$voucherData['customer_email']     	 =   $invoiceData['email'];
			$voucherData['customer_mobile']   	 =   $invoiceData['phone'];
			$voucherData['customer_city'] 	  	 =   $invoiceData['city'];
			$voucherData['customer_state'] 	  	 =   $invoiceData['state'];
			$voucherData['customer_country'] 	 =   $invoiceData['country'];
			$voucherData['customer_pin'] 	 	 =   $invoiceData['pin_code'];
			$voucherData['arrival']      		 =   $checkin;
			$voucherData['departure']    		 =   $checkout;
			$voucherData['pickup']		 		 =   $_POST['hotel_id'];
			$voucherData['remarks']    	 		 =   trim($_POST['remarks']);
			
			$this->db->where( array('id' => $_POST['id'], 'appcode' => $appcode ) );
			$this->db->update('transport_vouchers', $voucherData);

			$this->db->where( array('voucher_id' => $_POST['id'], 'appcode' => $appcode ) );
			$this->db->delete('transport_voucher_services');
			
			$voucherServices['voucher_id']  	=  	$_POST['id'];
			$voucherServices['appcode'] 	 	=	$appcode;
			
			foreach ( $trip as $key=>$value )
			{
				$voucherServices['trip'] 		= 	$value;
				$voucherServices['vechile'] 	= 	$vechile[$key];
				$voucherServices['capacity'] 	= 	$capacity[$key];
				$voucherServices['days'] 		= 	$days[$key];
				$voucherServices['pax'] 		= 	$pax[$key];
				$travel_date					= 	new DateTime($dot[$key]);
				$voucherServices['dot']			= 	$travel_date->format('Y-m-d');
				$voucherServices['rate']		= 	$rate[$key];
				$this->db->insert('transport_voucher_services', $voucherServices);
			}

			if( $result )
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Voucher Updated Successfully!</div>'));
				redirect(base_url() . 'transporters/transportVouchers', 'refresh');
			}
			else
			{
				redirect(base_url(), 'refresh');
			}
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}


	public function transportVoucher( $voucherId )
	{
		if( !is_numeric( $voucherId ) )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
		
		$this->load->library("Pdf");
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Shoaib Mohmad');
		$pdf->SetTitle('Hotel Voucher');
		$pdf->SetSubject('Hotel Voucher');
		$pdf->SetKeywords('OCTA, Travel, Hotel Voucher');
		$appcode 	= 	$this->session->userdata['appcode'];
		$query 		= 	$this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'");
		$result 	= 	$query -> row_array();
		if( !$result )
		{
			$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Please update your Profile first to generate Voucher!</div>');
			redirect(base_url().'user/profile', 'refresh');
		}

		$Address = $result['street'].', '.$result['city'].
		"\n". $result['state'].', '.$result['country'].
		", ".$result['pin'].
		"\n".'Phone: '. $result['phone']."\n".'Email: ' . $result['company_email'];

		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $result['company_name'], $Address);
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(4);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		$preferences = array(
			'HideToolbar' => true,
			'HideMenubar' => true,
			'HideWindowUI' => true,
			'FitWindow' => true,
			'CenterWindow' => true,
			'DisplayDocTitle' => true,
			'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
			'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
			'PrintScaling' => 'AppDefault', // None, AppDefault
			'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
			'PickTrayByPDFSize' => true
		);

		$pdf->setViewerPreferences($preferences);

		$pdf->SetFont('times', '', 14);

		$pdf->AddPage();

		$voucher 		= 	$this->transporters_model->getTransportVoucherDetails( $voucherId, $appcode );
		if( !$voucher )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		$pdf->SetY(35);
		$pdf->SetFont('times', 'B', 15);
		$pdf->Cell(100, 5, 'TRANSPORT VOUCHER', 0, 1 );
		$pdf->Ln(3);
		
		$pdf->SetFont('helveticab', '', 10);
		$pdf->Cell(113, 5, 'VOUCHER # :: '.$voucher['voucher_no'], 0, 0 );
		$pdf->Cell(100, 5, 'Arrival      :: '.date("jS F, Y", strtotime($voucher['arrival'])), 0, 1 );
		$pdf->Cell(113, 5, 'ISSUED ON   :: '.date("jS F, Y", strtotime($voucher['voucher_date'])), 0, 0 );
		$pdf->Cell(100, 5, 'Departure:: '.date("jS F, Y", strtotime($voucher['departure'])), 0, 1 );
		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'CUSTOMER DETAILS', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $voucher['customer_name'], 0, 1 );
		$pdf->Cell(100, 5, $voucher['customer_city'].', '.$voucher['customer_state'].', '.$voucher['customer_country'], 0, 1 );

		$pdf->Cell(100, 5, 'Pin: '.$voucher['customer_pin'], 0, 1 );
		$pdf->Cell(100, 5, 'Mobile: '. $voucher['customer_mobile'], 0, 1 );
		$pdf->Cell(100, 5, 'Email: '.$voucher['customer_email'], 0, 1 );

		$pdf->Ln(3);
		$pdf->SetFont('times', '', 13);
		$pdf->Cell(100, 8, 'Please provide these services to the above captioned customer & bill us for the same.', 0, 1 );

		$logo = $this->session->userdata("id");
		$logo = "uploads/logo/".$logo.'.png';
		
		$pdf->Image(base_url().$logo, 145, 5, 50, 20);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetTextColor(0,0,0);
		$tbl_header = '<table cellpadding="5" cellspacing="2" nobr="true" style="border: 1px solid #ddd;">';
		$tbl 	= ' <tr style="background-color:#ccc; color:#666; font-size:12px; font-weight: bold">
						<th style="width:5%; text-align: center;"> #    	</th>
						<th style="width:20%;"> Date 						</th>
						<th style="width:25%;"> Trip 						</th>
						<th style="width:20%;"> Vehicle 					</th>
						<th style="width:15%; text-align: center;"> Days 	</th>
						<th style="width:15%; text-align: center;"> Pax 	</th>
					</tr>';
		
		$services_result 	= 	$this->transporters_model->getTransportVoucherServices( $voucher['id'], $appcode );

		$sno 	= 	1;
		$total 	=	0;

		foreach ($services_result as $row ) 
		{
			$tbl .= '
						<tr style="background-color:#f7f7f7; font-size:12px;">
							<td style="text-align: center;">'.$sno++ .				'</td>
							<td>'.date("jS F, Y", strtotime($row['dot'])).	'</td>
							<td>'.$row['trip'].	'</td>
							<td>'.$row['vechile'].	'</td>
							<td style="text-align: center;">'.$row['days'].	'</td>
							<td style="text-align: center;">'.$row['pax'].	'</td>
						</tr>
					';
			$total += 	$row['rate'];	
		}
		$tbl_footer .='</table>';

		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

		$html = '<hr />';


		$hotel 	= 	$this->hotels_model->getHotelDetails( $voucher['pickup'], $appcode );

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'PICKUP', 0, 1 );
		$pdf->SetFont('helveticab', '', 10);

		$pdf->Cell(100, 5, $hotel['hotel_name'], 0, 1 );
		$pdf->Cell(100, 5, $hotel['address'], 0, 1 );


		$pdf->SetFont('helveticab', 'B', 10);
		
		$transporter 	=	$this->transporters_model->getTransporterDetails( $voucher['transporter_id'], $appcode );

		$pdf->Ln(5);

		$pdf->SetFont('times', 'B', 12);
		$pdf->SetTextColor(0, 0, 0 );
		$pdf->Cell(100, 5, 'FOR', 0, 1 );

		$pdf->SetFont('times', '', 12);
		$pdf->Cell(100, 5, $transporter['transporter_name'], 0, 1 );
		$pdf->Cell(100, 5, $transporter['address'], 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', 'B', 11);
		$pdf->Cell(132, 0, '', 0, 0 );
		$pdf->Cell(100, 0, 'AUTHORIZED SIGNATURE', 0, 1 );

		$pdf->Ln(20);
		$pdf->SetFont('times', '', 11);
		$remarks = '<strong>Additional Information / Remarks:</strong> '.$voucher['remarks'];
		
		$pdf->writeHTML($remarks, true, false, false, false, '');
		ob_end_clean();

		$pdf->Output('Transport_Voucher - ' .$voucher['id']. '.pdf', 'I');
	}


	
	public function addVechile( $param1 = '' )
	{
		if( $_POST )
		{	
			$data['appcode'] 		= 	$this->session->userdata['appcode'];
			$data['vechile_name']  	= 	trim( $_POST['vechile_name'] );
			$data['capacity']  		= 	trim( $_POST['capacity'] );	
			$data['description']  	= 	trim( $_POST['description'] );

			$result = $this->db->insert('vechiles', $data); 
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Vehicle Added Successfully!</div>');
				redirect(base_url().'transporters/vechiles', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
				redirect(base_url().'transporters/addVechile', 'refresh');
			}

		}

		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add Vehicle";
		$data['fileName'] 		= 	"addVechile.php";
		$this->load->view('index', $data);
	}


	public function editVechile( $param1 = '' )
	{
		$appcode	= 	$this->session->userdata['appcode'];
		if( $param1 == 'update' )
		{	
			if( !isset( $_POST['id'] ) )
			{
				redirect(base_url().'transporters/vechiles', 'refresh');	
			}
			$id 	=	$_POST['id'];

			$data['vechile_name']  	= 	trim( $_POST['vechile_name'] );
			$data['capacity']  		= 	trim( $_POST['capacity'] );	
			$data['description']  	= 	trim( $_POST['description'] );			

			$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'vechiles', $data );
			
			if( $result )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Vehicle Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', ('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>'));
			}
			redirect(base_url().'transporters/vechiles', 'refresh');
		}

		$data['vechile'] 	= 	$this->transporters_model->getVechileDetails( $param1, $appcode );
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Edit Vehicle";
		$data['fileName'] 	= 	"addVechile.php";
		$this->load->view( 'index', $data );
	}

	
	
	public function vechiles( $param1 = '', $param2 = '' )
	{
		$appcode 	=	$this->session->userdata['appcode'];

		if( $param1 == 'delete' )
		{
			
			if( $param2 == '' )
			{
				redirect(base_url().'transporters/vechiles', 'refresh');
			}

			$id 				=	$param2;
			$data['status'] 	=	'Deleted';

			$this->db->where( array( 'id' => $id, 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'vechiles', $data );

	        if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>Vehicle Deleted Successfully!
					</div>');
				redirect(base_url().'transporters/vechiles', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
				redirect(base_url().'transporters/vechiles', 'refresh');
			}
		}

		$data['vechiles']		=	$this->transporters_model->getVechiles( $appcode );
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Vehicles";
		$data['fileName'] 		= 	"vechiles.php";
		$this->load->view('index', $data);		
	}


	function transportersPdf()
    {

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Shoaib Mohmad');
        $pdf->SetTitle('Transporters List');
        $pdf->SetSubject('OCTA Travel Transporters List');
        $pdf->SetKeywords('OCTA Travel, Transporters List');

        $appcode    =   $this->session->userdata['appcode'];  
        $profile    =   $this->db->query("SELECT * FROM profile WHERE appcode = '$appcode'")->row_array();

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $profile['company_name'], 'Transporters List');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('times', 'BU', 12);
        $pdf->setCellPaddings ('', '', '', '5px');
        // add a page
        $pdf->AddPage('L');
        $HeaderText = 'LIST OF TRANSPORTERS';
        $pdf->Write(0, $HeaderText, '', 0, 'C', true, 0, false, false, 0);
        $pdf->Ln(3);
        $pdf->SetFont('times', '', 9 );

        $tbl_header = '<table border="1" cellpadding="5" nobr="true">';
        $tbl_footer = '</table>';
        $tbl    = ' <tr style="background-color:#FFFF00;color:#0000FF; font-weight: bold; ">
                        <th width="5%;" style="text-align: center;"> S NO </th>
                        <th> RANSPORTER NAME    </th>
                        <th> ADDRESS            </th>
                        <th> PHONE NO           </th>
                        <th> FAX                </th>
                        <th> EMAIL              </th>
                        <th> WEBSITE            </th>
                        <th> DESCRIPTON         </th>
                    </tr>';
        $count          =   1; 
        $transporters   =   $this->transporters_model->getTransporters( $appcode );
        foreach ($transporters as $transporter ) 
        {
            $tbl .= '
                <tr style="font-size: 13px;">
                    <td  style="text-align: center;"> '.$count++ .' </td>
                    
                    <td>'.$transporter['transporter_name'].'</td>

                    <td>'.$transporter['address'].'</td>

                    <td>'.$transporter['phone'].'</td>

                    <td>'.$transporter['fax'].'</td>
                    
                    <td>'.$transporter['email'].'   </td>
                    
                    <td>'.$transporter['website'].'</td>

                    <td>'.$transporter['description'].'</td>
                    
                </tr>
            ';
            $tbl_footer = '
                            </table>
                          ';
        }    

        $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');

        ob_end_clean();

        // close and output PDF document
        $pdf->Output('Transporters_List.pdf', 'D');

    }


    public function ajaxGetTransporterTrips()
	{
		if( $_POST )
		{ 
			$appcode    =   $this->session->userdata['appcode'];  
			$trips 		=  	$this->transporters_model->getTransporterTariffs( $_POST['transporter_id'], $appcode );
			
			echo json_encode( $trips );
			return true;
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}
	
}

/* End of file transporters.php */
/* Location: ./application/controllers/transporters.php */
