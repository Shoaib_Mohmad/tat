<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting( 0 );

class Admin extends CI_Controller 
{
	private $pathType = 'admin';
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('admin/admin_model');
		
		if( $this->session->userdata['level'] != 7 )
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}

		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	public function dashboard()
	{
		$id					= 	$this->session->userdata['id'];
		$data['pathType'] 	= 	$this->pathType;
		$data['users']		=	$this->admin_model->getUsers();
		$data['pageName'] 	= 	"Dashboard";
		$data['fileName'] 	= 	"dashboard.php";
		$this->load->view('index', $data);
	}

	public function changePassword()
	{
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Change Password";
		$data['fileName'] 	= 	"changePassword.php";
		$this->load->view('index', $data);
	}


	
	public function changePasswordResult()
	{
		if($_POST)
		{
			if( $_POST['password1'] !== $_POST['password2'] )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error">
						<button class="close" data-dismiss="alert">×</button>
						Passwords You Entered Don\'t Match.</div>');
				redirect(base_url().'admin/changePassword', 'refresh');
			}
			$password 	= md5($this->input->post('password1'));
			$id 		= $this->session->userdata['id'];
			$query 		= $this->db->query("UPDATE users SET password = '$password' WHERE id = '$id' AND status = 1");
			if( $query )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-success">
						<button class="close" data-dismiss="alert">×</button>
						Password Changed Successfully!</div>');
				redirect(base_url().'admin/changePassword', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error">
						<button class="close" data-dismiss="alert">×</button>
						Something Went Wrong. Please! Try Again After Sometime.</div>');
				redirect(base_url().'admin/changePassword', 'refresh');
			}
		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');
		}
	}

	public function manageuser( $id = '')
	{
		$data['pathType'] 	= 	$this->pathType;
		$data['pageName'] 	= 	"Manage User";
		$data['fileName'] 	= 	"manageUser.php";
		$data['user']		=	$this->admin_model->getUserDetails($id);
		$this->load->view('index', $data);

	} 

	public function updateUser()
	{
		if($_POST)
		{
			$id 		=	$_POST['user_id'];
			if($_POST  != '')	
			$query 		= 	$this->db->query("UPDATE users SET status = '".$_POST['status']."' WHERE id = $id");
			$eor 		= 	date('Y-m-d', strtotime($_POST['EOD']));
			$result 	=	$this->db->query("UPDATE user_registration SET end_of_registration ='".$eor."', payment_status = '".$_POST['type']."' WHERE user_id = $id");
			
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Detail Update Successfully!</div>');
			
			redirect(base_url().'admin/dashboard', 'refresh');


		}
		else
		{
			redirect(base_url().'login/four_zero_four', 'refresh');

		}
	}



}
