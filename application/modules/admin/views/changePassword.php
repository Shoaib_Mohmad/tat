<?php echo $this->session->flashdata('notification');?>

<script type="text/javascript">
   function checkPasswordMatches()
   {
      var pass1 = document.getElementById('password1').value;
      var pass2 = document.getElementById('password2').value;
      if(pass1 == pass2)
      {
         return true;
      }
      else
      {
         $("#pass1").addClass("form-group has-error");
         $("#pass2").addClass("form-group has-error");
         document.getElementById('warning').innerHTML='<div class="alert alert-danger"><a href="javascript:void(0)" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i></a><center>Passwords You Entered Don\'t Match.</center> </div>';       
         return false;
      }
   }
</script>
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN SAMPLE FORM PORTLET-->   
      <div class="widget">
         <div class="widget-title">
            <h4><i class="icon-reorder"></i>Change Password</h4>
         </div>
         <div class="widget-body form">
            <!-- BEGIN FORM-->
            <form onSubmit="return checkPasswordMatches();" action="<?php echo base_url().'admin/changePasswordResult';?>" class="form-horizontal" method="POST">
               <div id="warning"> </div>
               <div class="control-group">
                  <label class="control-label" for="input1">New Password</label>
                  <div class="controls">
                     <input type="password" pattern=".{6,}" title="6 characters minimum" name="password1" required id="password1" class="span6" />
                  </div>
               </div>

               <div class="control-group">
                  <label class="control-label" for="input1">Confirm Password</label>
                  <div class="controls">
                     <input type="password" pattern=".{6,}" title="6 characters minimum" name="password2" required id="password2" class="span6" />
                  </div>
               </div>

                <p style="padding-left:3.5em;">Password must be atleast 6 characters</p>
               
               <div class="form-actions">
                  <button type="submit" style="cursor:pointer" class="btn btn-primary disabled">Update Password</button>
               </div>
            </form>
            <!-- END FORM-->           
         </div>
      </div>
      <!-- END SAMPLE FORM PORTLET-->
   </div>
</div>