
<?php echo $this->session->flashdata('notification');?>
<div id="page">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4>
						<i class="icon-reorder"></i>Manage User
					</h4>
					
				</div>
				<div class="widget-body form">
					<!-- BEGIN FORM-->
					<form  action="<?php echo base_url();?>admin/updateUser" method="POST" class="form-horizontal">

					<input type="hidden" name="user_id" value="<?php echo $user['id']; ?>">
					<div class="control-group">
							<label class="control-label" for="input1">User Name</label>
							<div class="controls">
								<input type="text" autocomplete="off" readonly  class="span4" id="user_name" name="user_name" value="<?php echo $user['user_name']; ?>" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="input1">Email</label>
							<div class="controls">
								<input type="text" autocomplete="off" readonly  class="span4" id="email" name="user_name"value="<?php echo $user['email']; ?>" />
							</div>
						</div>

						<div class="control-group">
									<label class="control-label" for="inputText">Type</label>
									<div class="controls">
										
										<select required class="span4"  id="type" name="type">
											
											<option value='1' <?php if( $user['payment_status'] == '1' ) echo 'Selected'; ?> >Paid</option>
											<option value='0'  <?php if( $user['payment_status'] == '0' ) echo 'Selected'; ?> >Trail</option>
				   						</select>
									</div>
								</div>

						<div class="control-group">
							<label class="control-label" for="input1">Date Of Registration</label>
								<div class="controls">
									<input class="input-small  span2" readonly  size="16"
									type="text" placeholder="MM/DD/YYYY" name="DOR" id="DOR" value="<?php echo date('m/d/Y', strtotime($user['date_of_registration'])); ?>" />
								</div>
						</div>


						<div class="control-group">
							<label class="control-label" for="input1">End Of Registration</label>
							<div class="controls">
								<input class="input-small date-picker span2" size="16"
									type="text" placeholder="MM/DD/YYYY" name="EOD" id="EOD" value="<?php if($user['end_of_registration']) echo date('m/d/Y', strtotime($user['end_of_registration'])); ?>" />
							</div>
						</div>

								<div class="control-group">
									<label class="control-label" for="inputText">User Status</label>
									<div class="controls">
										
										<select required class="span4"  id="status" name="status">
											<option value=''>Select Status</option>
											<option value='1' <?php if( $user['status'] == '1' ) echo 'Selected'; ?> >Active</option>
											<option value='0'  <?php if( $user['status'] == '0' ) echo 'Selected'; ?> >Suspend User</option>
											
				   						</select>
									</div>
								</div>
								</br>
								</br>
								</br>
								</br>
								

						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>

</div>