<?php echo $this->session->flashdata('notification'); ?>

<div id="page" class="dashboard">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-reorder"></i>Registered Users</h4>
					<span class="tools">
						
					</span>							
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="sample_1">
						<thead>
							<tr>
								<th style="width:8px">#</th>
								<th>User Name</th>
								<th>Email</th>
								<th>Type</th>
								<th>DOR</th>
								<th>EOR</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
							$count = 1;
							foreach ( $users as $user ) 
							{ 
								
							?>
								<tr class="odd gradeX">
									<td><?php echo $count++; ?></td>
									<td><?php echo $user['user_name']  ?></td>
									<td><?php echo $user['email']  ?></td>
									<td><?php if($user['payment_status'] == 1) echo '<span class="label-success label label-default">Paid</span>'; else echo '<span class="label-warning label label-default">Trail</span>'; ?>  </td>
									<td><?php if($user['date_of_registration']) print(date("jS F, Y", strtotime($user['date_of_registration'])));?></td>
									<td><?php if($user['end_of_registration']) print(date("jS F, Y", strtotime($user['end_of_registration'])));  ?></td>
									<td><?php if($user['status'] == 1) echo '<span class="label-success label label-default">Active</span>'; elseif($user['status'] == 0) echo '<span class="label-default label label-danger">Suspended</span>'; else echo '<span class="label-warning label label-default">Pending<span>' ?> </td>
									
									<td> <!--<a href="javascript:void(0);" title="" onClick = 'window.open("", "myWindow", "width=800, height=750")';><i class="fa fa-file-text" style="font-size: 18px; color: blue;"></i></a> |-->
								<a href="<?php echo base_url().'admin/manageUser/'.$user['id']; ?>" title="Manage User"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a>
								
							</td>
								</tr>
							<?php  }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
			<!-- END BORDERED TABLE PORTLET-->
</div>
