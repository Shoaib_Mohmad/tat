<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}

	public function getUsers()
	{
		$query	 = 		$this->db->query("SELECT a.id, a.email,a.level,a.created_at, a.status, b.date_of_registration,b.end_of_registration,b.payment_status, c.user_name FROM users a INNER JOIN user_registration b on a.id = b.user_id LEFT JOIN profile c on a.id = c.user_id WHERE a.level <> 7 AND a.level = 1") -> result_array();
		return $query;
	}

	function getUserDetails($id)	
	{
		$result 	= 	$this->db->query("SELECT a.id, a.email,a.level,a.created_at, a.status, b.date_of_registration,b.end_of_registration,b.payment_status, c.user_name FROM users a INNER JOIN user_registration b on a.id = b.user_id LEFT JOIN profile c on a.id = c.user_id WHERE a.id = $id") -> row_array();

		return $result;
	}
}