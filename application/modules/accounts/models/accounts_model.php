<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	
    function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	   
    
    function getUsers( $appcode )
    {
        $users   =   $this->db->query("SELECT users.*, user_roles.name FROM users INNER JOIN user_roles ON users.id = user_roles.user_id  WHERE users.level != '1' AND users.appcode = '$appcode'")->result_array();
        return $users;
    }


    function getUserDetails( $id, $appcode )
    {
        $user   =   $this->db->query("SELECT users.*, user_roles.name FROM users INNER JOIN user_roles ON users.id = user_roles.user_id WHERE users.id = $id AND users.appcode = '$appcode'")->row_array();
        return $user;
    }


    function getUserRoles( $user_id, $appcode )
    {
        $user   =   $this->db->query("SELECT * FROM user_roles WHERE user_id = $user_id AND appcode = '$appcode'");
        if( !$user->num_rows() )
        {
            redirect(base_url().'login/four_zero_four', 'refresh');
        }
        else
        {
            return $user->row_array();
        }
    }


}