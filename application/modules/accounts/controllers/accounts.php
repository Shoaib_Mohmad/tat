<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata"); 
error_reporting(0);

class Accounts extends CI_Controller 
{

	private $registrationStatus;
	private $pathType = 'accounts';
	
	function __construct()
	{
		parent::__construct();
		
		$this->user_model->checkUserCanAccess('accounts');
		
		if( $this->session->userdata['level'] == 1 )
		{
			$this->registrationStatus = $this->registration_model->adminRegistrationStatus();

			if( !$this->user_model->isProfileUpdated() )
			{
				redirect(base_url().'settings/profile', 'refresh');
			}
		}
		else
		{
			$this->registrationStatus = $this->registration_model->userRegistrationStatus();
		}	

		$this->load->model('accounts/accounts_model');	
		
		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function users( $param1 = '', $param2 = '' )
	{
		$appcode 	=	$this->session->userdata['appcode'];

		if( $param1 == 'add' )
		{
			$email 		=	mysql_real_escape_string( $_POST['email'] );
			$exists     =	$this->services_model->checkEmailExists( $email );

			if( $exists )
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>This Email ID is already registered with Us!</div>');
				redirect(base_url().'accounts/addUser', 'refresh');
			}

			$data['appcode']	=	$appcode;
			$data['email'] 		=	$email;
			$data['password'] 	=	md5( $_POST['password1'] );
			$data['level'] 		=	2;

			$result   	=   $this->db->insert('users', $data);

			$roles['appcode']	=	$appcode;
			$roles['user_id'] 	=	$this->db->insert_id();
			$roles['name'] 		=	mysql_real_escape_string( $_POST['name'] );

			$result   	=   $this->db->insert('user_roles', $roles);

			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Added Successfully!</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
		}

		if( $param1 == 'delete' && is_numeric( $param2 ) )
		{
			$data['status'] 	=	0;
			$this->db->where( array( 'id' => $param2, 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'users', $data );

   			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Suspended Successfully!</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
		}

		if( $param1 == 'activate' && is_numeric( $param2 ) )
		{
			$data['status'] 	=	1;
			$this->db->where( array( 'id' => $param2, 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'users', $data );

   			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Activated Successfully!</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
		}

		$data['users']			=	$this->accounts_model->getUsers( $appcode );
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"User Accounts";
		$data['fileName'] 		= 	"users.php";
		$this->load->view('index', $data);
	}


	public function addUser()
	{
		$appcode 				=	$this->session->userdata['appcode'];
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add User Account";
		$data['fileName'] 		= 	"addUser.php";
		$this->load->view('index', $data);
	}


	public function editUser( $param = '' )
	{
		$appcode 				=	$this->session->userdata['appcode'];

		if( $param == 'update' )
		{
			$data['name'] 		=	mysql_real_escape_string( $_POST['name'] );
			$data['password'] 	=	md5( $_POST['password1'] );
			$this->db->where( array( 'id' => $_POST['id'], 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'users', $data );

   			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Updated Successfully!</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
				redirect(base_url().'accounts/users', 'refresh');
			}
		}

		$data['user']			=	$this->accounts_model->getUserDetails( $param, $appcode );
		$data['pathType'] 		= 	$this->pathType;
		$data['pageName'] 		= 	"Add User";
		$data['fileName'] 		= 	"addUser.php";
		$this->load->view('index', $data);
	}



	public function userRoles( $param1 = '', $param2 = '' )
	{
		$appcode 	=	$this->session->userdata['appcode'];

		if( is_numeric( $param1 ) )
		{
			$data['user']			=	$this->accounts_model->getUserRoles( $param1, $appcode );
			$data['pathType'] 		= 	$this->pathType;
			$data['pageName'] 		= 	"User Roles";
			$data['fileName'] 		= 	"userRoles.php";
			$this->load->view('index', $data);
		}

		if( $param1 == 'update' )
		{
			$data['sales'] 			= 	$_POST['sales'];
			$data['expenses'] 		= 	$_POST['expenses'];
			$data['reporting'] 		= 	$_POST['reporting'];
			$data['hotels'] 		= 	$_POST['hotels'];
			$data['transporters'] 	= 	$_POST['transporters'];
			$data['inventory'] 		= 	$_POST['inventory'];
			$data['customers'] 		= 	$_POST['customers'];
			$data['services'] 		= 	$_POST['services'];
			$data['tours'] 			= 	$_POST['tours'];
			$data['passes'] 		= 	$_POST['passes'];

			$this->db->where( array( 'user_id' => $param2, 'appcode' => $appcode ) );
   			$result 	= 	$this->db->update( 'user_roles', $data );

			if( $result )
	        {
				$this->session->set_flashdata('notification', '<div class="alert alert-success"><button class="close" data-dismiss="alert">×</button>User Roles Updated Successfully!</div>');
			}
			else
			{
				$this->session->set_flashdata('notification', '<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>Something went wrong. Please try again later.</div>');
			}
			redirect(base_url().'accounts/userRoles/'.$param2, 'refresh');
		}
	}
	
}

/* End of file accounts.php */
/* Location: ./application/controllers/accounts.php */
