<?php echo $this->session->flashdata('notification');?>

<div id="page" class="dashboard">
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="widget">
					<div class="widget-title">
						<h4><i class="icon-reorder"></i>User Accounts</h4>
						<span class="tools">
							<div class="btn-group">
								<a href="<?php echo base_url().'accounts/addUser';?>"><button type="button" style="margin: -6px 10px 0px 0px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add User</button></a>
							</div>
						</span>								
					</div>
					<div class="widget-body">
						<table class="table table-striped table-bordered" id="sample_1">
							<thead>
								<tr>
									<th style="width:8px">#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Created At</th>	
									<th>Status</th>	
									<th><center>Actions</center></th>
								</tr>
							</thead>
							<tbody>
							<?php 
      						$count = 1;
      						foreach ($users as $user) { ?>
							<tr class="odd gradeX">
								<td><?php echo $count++; ?></td>
								<td><?php echo $user['name']; ?></td>
								<td><?php echo $user['email']; ?></td>
								<td><?php echo date("jS F, Y", strtotime($user['created_at'])); ?></td>
								<?php if( $user['status'] == '1' ): ?>
									<td><center><span class="label label-success">Active</span></center></td>
									<td>
									<center>
										<a href="<?php echo base_url().'accounts/userRoles/'.$user['id']; ?>" title="Assign Roles"><i class="fa fa-user" style="font-size: 20px; color: green;"></i></a> |
										<a href="<?php echo base_url().'accounts/edituser/'.$user['id']; ?>" title="Edit User"><i class="fa fa-edit" style="font-size: 20px; color: blue;"></i></a> |
									 	<a href="<?php echo base_url()?>accounts/users/delete/<?php echo $user['id']; ?>" title="Suspend User" onclick="return confirm('Are you sure you want to suspend the User?')" ><i class="fa fa-trash-o" style="font-size: 20px; color: red;"></i></a>
									</center>
								</td>
								<?php else: ?>
									<td><center><span class="label label-warning">Suspended</span></center></td>
									<td>
									<center>
										<a href="<?php echo base_url()?>accounts/users/activate/<?php echo $user['id']; ?>" title="Activate User" onclick="return confirm('Are you sure you want to activate the User?')" ><i class="fa fa-check" style="font-size: 20px; color: green;"></i></a>
									</center>
								</td>
								<?php endif; ?>
								
							</tr>
							 <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
				<!-- END BORDERED TABLE PORTLET-->
	</div>
