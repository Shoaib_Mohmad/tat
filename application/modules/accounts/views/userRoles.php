<?php echo $this->session->flashdata('notification');?>

<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN EXAMPLE TABLE PORTLET-->
      <div class="widget">
         <div class="widget-title">
            <h4><i class="icon-reorder"></i>User Roles</h4>
         </div>
         <div class="widget-body">
            <form action="<?php echo base_url().'accounts/userRoles/update/'.$user['user_id']; ?>" method="POST">
               <table class="table table-striped table-bordered" id="monthly_room_bookings">
                  <thead>
                     <tr>
                        <th style="width:5%;">#</th>
                        <th>Role</th>
                        <th>Assign</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  <tr class="odd gradeX">
                     <td>1</td>
                     <td>Manage Sales</td>
                     <td><input type="checkbox" name="sales" <?php if( $user['sales'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>2</td>
                     <td>Manage Expenses</td>
                     <td><input type="checkbox" name="expenses" <?php if( $user['expenses'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>3</td>
                     <td>Manage Reporting</td>
                     <td><input type="checkbox" name="reporting" <?php if( $user['reporting'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>4</td>
                     <td>Manage Hotels</td>
                     <td><input type="checkbox" name="hotels" <?php if( $user['hotels'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>5</td>
                     <td>Manage Transporters</td>
                     <td><input type="checkbox" name="transporters" <?php if( $user['transporters'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>6</td>
                     <td>Manage Inventory</td>
                     <td><input type="checkbox" name="inventory" <?php if( $user['inventory'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>7</td>
                     <td>Manage Customers</td>
                     <td><input type="checkbox" name="customers" <?php if( $user['customers'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>8</td>
                     <td>Manage Services</td>
                     <td><input type="checkbox" name="services" <?php if( $user['services'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>9</td>
                     <td>Manage Tours</td>
                     <td><input type="checkbox" name="tours" <?php if( $user['tours'] ) echo 'checked'; ?> value="1"></td>
                  </tr>

                  <tr class="odd gradeX">
                     <td>10</td>
                     <td>Manage Passes</td>
                     <td><input type="checkbox" name="passes" <?php if( $user['passes'] ) echo 'checked'; ?> value="1"></td>
                  </tr>
                     
                  </tbody>
               </table>
               <br>
               <button type="submit" class="btn btn-primary pull-right">Update Roles</button>
            </form>
         </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
   </div>
</div>		