<?php echo $this->session->flashdata('notification');?>

<script type="text/javascript">
   function checkPasswordMatches()
   {
      var pass1 = document.getElementById('password1').value;
      var pass2 = document.getElementById('password2').value;
      if(pass1 == pass2)
      {
         return true;
      }
      else
      {
         document.getElementById('password1').value = '';
         document.getElementById('password2').value = '';
         $("#pass1").addClass("form-group has-error");
         $("#pass2").addClass("form-group has-error");
         document.getElementById('warning').innerHTML='<div class="alert alert-danger"><a href="javascript:void(0)" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i></a><center>Passwords You Entered Don\'t Match.</center> </div>';       
         return false;
      }
   }
</script>

<div id="page">
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN SAMPLE FORM PORTLET-->	
         <div class="widget">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i><?php if( isset( $user['id'] ) ) echo 'Edit'; else echo 'Add'?> User</h4>
            </div>
            <div class="widget-body form">
                <!-- BEGIN FORM-->
               <form onSubmit="return checkPasswordMatches();" action="<?php if( isset( $user['id'] ) ) echo base_url().'accounts/editUser/update'; else echo base_url().'accounts/users/add'?>" method="POST" class="form-horizontal">
                  <div id="warning"> </div>
                  <?php if( isset( $user['id'] ) ): ?>
                     <input type="hidden" name="id" value="<?php if( isset( $user['id'] ) ) echo $user['id']; ?>" />
                  <?php endif; ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">User Name</label>
                     <div class="controls">
                        <input type="text" required placeholder="Enter User Name" class="span6" id="input" name="name" value="<?php if( isset( $user['id'] ) ) echo $user['name']; ?>" />
                     </div>
                  </div>
                  <?php if( !isset( $user['id'] ) ): ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">User Email</label>
                     <div class="controls">
                        <input type="email" required placeholder="Enter User Email" class="span6" id="input" name="email" value="<?php if( isset( $user['id'] ) ) echo $user['email']; ?>" />
                     </div>
                  </div>
                  <?php endif; ?>
                  <div class="control-group">
                     <label class="control-label" for="input1">Enter<?php if( isset( $user['id'] ) ) echo ' New'; ?> Password</label>
                     <div class="controls">
                        <input type="password" pattern=".{6,}" placeholder="Enter Password" title="6 characters minimum" name="password1" required id="password1" class="span6" />
                     </div>
                  </div>

                  <div class="control-group">
                     <label class="control-label" for="input1">Confirm Password</label>
                     <div class="controls">
                        <input type="password" pattern=".{6,}" placeholder="Confirm Password" title="6 characters minimum" name="password2" required id="password2" class="span6" />
                     </div>
                  </div>

                   <p style="padding-left:3.5em;">Password must be atleast 6 characters</p>
                  
                 <div class="control-group">
                  <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save User</button>
                  </div>
                  </div>
               </form>
                         <!-- END FORM-->			
            </div>
         </div>
         <!-- END SAMPLE FORM PORTLET-->
      </div>
   </div>
  
  
</div>
