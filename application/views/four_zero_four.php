<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing | 404</title>
<style type="text/css">
	#heading{
		padding-top:100px;
		text-align: center;
	}

	.button_ok {
	    border: 0 none;
	    border-radius: 1.2em;
	    color: #1b486a;
	    cursor: pointer;
	    display: block;
	    font-size: 15px;

	    font-weight: bold;
	    height: 30px;
	    margin-top: 70px;
	    outline: medium none !important;
	    overflow: hidden;
	    padding: 15px 15px 5px 15px !important;
	    text-decoration: none;
	    background-color: black;
	    width:150px;
	}

	body{
		    background-color: rgba(0, 0, 0, 0.43);
	}

</style>
</head>

<body>
<div class="container">
  <div class="grid_6 prefix_5 suffix_5">
   	  <h1 id="heading">404 - Page Not Found</h1>
    	<center><a  href="<?php echo base_url();?>" class="button_ok"><span style="color: white;">Back to Home</span></a></center>
       <!--  <div id="forgot">
        <a href="#" class="forgotlink"><span>Forgot your username or password?</span></a></div> -->
  </div>

 
</div>

</body>
</html>
