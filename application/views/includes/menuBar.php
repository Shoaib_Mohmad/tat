<style type="text/css">
	.myMenu{
		font-weight: bold;
		font-size: 14px;
		margin-bottom: -5px;
	}
	.menuFont{
		color: #016FA7;
	}
</style>

	<div class="stats-overview-cont" id="menu">

		<div class="row-fluid ">

			<?php if($this->session->userdata['level'] == 7 ): ?>				
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'admin/dashboard';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/dashboard.png';?>" width="30px" height="30px">
							<span class="menuFont">Dashboard</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>

			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'admin/changePassword';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/settings.png';?>" width="30px" height="30px">
							<span class="menuFont">Settings</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php else: ?>

			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'user/dashboard';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/dashboard.png';?>" width="30px" height="30px">
							<span class="menuFont">Dashboard</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>

			<?php if( $this->session->userdata['sales'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'sales/invoices';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/payment.png';?>" width="30px" height="30px">
							<span class="menuFont">Sales Invoices</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['expenses'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'expenses/bills';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/billing.png';?>" width="30px" height="30px">
							<span class="menuFont">Expenses</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['reporting'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'reports';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/reports.png';?>" width="30px" height="30px">
							<span class="menuFont">Reporting</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['hotels'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'hotels';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/hotels.png';?>" width="30px" height="30px">
							<span class="menuFont">Hotels</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['transporters'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'transporters';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/transporters.png';?>" width="30px" height="30px">
							<span class="menuFont">Transporters</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>
		
			<?php if( $this->session->userdata['inventory'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'inventory/rooms';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/rooms.png';?>" width="30px" height="30px">
							<span class="menuFont">Rooms</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['tours'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'tours';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/tours.png';?>" width="30px" height="30px">
							<span class="menuFont">Tours</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['passes'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'passes';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/passes.png';?>" width="30px" height="30px">
							<span class="menuFont">Passes</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['level'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'accounts/users';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/users.png';?>" width="30px" height="30px">
							<span class="menuFont">Accounts</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if( $this->session->userdata['level'] == '1' ): ?>
			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'settings/profile';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/profile.png';?>" width="30px" height="30px">
							<span class="menuFont">Profile</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<div class="span2 responsive" data-tablet="span4" data-desktop="span2" style="width: 15.53%; margin-left: 1%;">
				<a href="<?php echo base_url().'user/changePassword';?>" style="text-decoration:none;" >
					<div class="stats-overview block clearfix">
						<div class="details">
							<div class="title"></div>
							<div class="myMenu">
							<img src="<?php echo base_url().'assets/img/menu/settings.png';?>" width="30px" height="30px">
							<span class="menuFont">Settings</span>
							</div>
							<br>
						</div>
					</div>
				</a>
			</div>
		<?php endif; ?>
	</div>
</div>




