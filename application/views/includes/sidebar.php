<?php $open = ""; ?>

<div id="sidebar" class="nav-collapse collapse">
	
	<!-- BEGIN SIDEBAR MENU -->
	<ul style="margin-top:0%;">

		<?php if($this->session->userdata['level'] == 7 ): ?>
		<li <?php if($fileName == 'dashboard.php') echo 'class="active"';?>>
			<a href="<?php echo base_url().'admin/dashboard'; ?>">Dashboard</a>					
		</li>
		<li class="has-sub <?php if($fileName == 'changePassword.php') {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Settings<span class="arrow <?php echo $open; $open = "";?>"></span></a>
			<ul class="sub">
				<li <?php if($fileName == 'changePassword.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'admin/changePassword';?>">Change Password</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="<?php echo base_url().'login/logout';?>">Logout</a>					
		</li>

		<?php else: ?>
			
		<li <?php if($fileName == 'dashboard.php') echo 'class="active"';?>>
			<a href="<?php echo base_url().'user/dashboard'; ?>">Dashboard</a>					
		</li>
	
		<?php if( $this->session->userdata['sales'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'addInvoice.php' || $fileName == 'invoices.php' || $fileName == 'editInvoice.php') {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Sales Invoices<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addInvoice.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'sales/addInvoice';?>">Add Sales Invoice</a>
				</li>
				<li <?php if($fileName == 'invoices.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'sales/invoices';?>">View Sales Invoices</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['expenses'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'addExpense.php' || $fileName == 'expenses.php' || $fileName == 'editExpense.php') {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Expenses<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				
				<li <?php if($fileName == 'addExpense.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'expenses/addBill';?>">Add Expense</a>
				</li>
				
				<li <?php if($fileName == 'expenses.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'expenses/bills';?>">View Expenses</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['reporting'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'salesReport.php' || $fileName == 'expensesReport.php' || $fileName == 'generateReport.php' || $fileName == 'bookingsReport.php' || $fileName == 'hotelVouchersReport.php' || $fileName == 'transportVouchersReport.php'){echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Reporting<span class="arrow <?php echo $open; $open = ""; ?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'salesReport.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'reports/salesReport';?>">Sales Report</a>
				</li>
				<li <?php if($fileName == 'expensesReport.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'reports/expensesReport';?>">Expenses Report</a>
				</li>
				<li <?php if($fileName == 'hotelVouchersReport.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'reports/hotelVouchersReport';?>">Hotel Voucher Report</a>
				</li>
				<li <?php if($fileName == 'transportVouchersReport.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'reports/transportVouchersReport';?>">Transport Voucher Report</a>
				</li>
				<li <?php if($fileName == 'bookingsReport.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'reports/bookingsReport';?>">Bookings Report</a>
				</li>
				
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['hotels'] == '1' ): ?>
		
		<li class="has-sub <?php if( $fileName == 'hotels.php' || $fileName == 'addHotel.php' || $fileName == 'addHotelVoucher.php' || $fileName == 'hotelVouchers.php' || $fileName == 'editHotelVoucher.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Hotels<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addHotel.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'hotels/addHotel';?>">Add Hotel</a>
				</li>
				<li <?php if($fileName == 'hotels.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'hotels';?>">View Hotels</a>
				</li>
				<li <?php if($fileName == 'addHotelVoucher.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'hotels/addHotelVoucher';?>">Add Voucher</a>
				</li>
				<li <?php if($fileName == 'hotelVouchers.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'hotels/hotelVouchers';?>">View Vouchers</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['transporters'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'transporters.php' || $fileName == 'addTransporter.php' || $fileName == 'addTransportVoucher.php' || $fileName == 'transportVouchers.php' || $fileName == 'editTransportVoucher.php' || $fileName == 'addVechile.php' || $fileName == 'vechiles.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Transporters<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addTransporter.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters/addTransporter';?>">Add Transporter</a>
				</li>
				<li <?php if($fileName == 'transporters.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters';?>">View Transporters</a>
				</li>
				<li <?php if($fileName == 'addTransportVoucher.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters/addTransportVoucher';?>">Add Voucher</a>
				</li>
				<li <?php if($fileName == 'transportVouchers.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters/transportVouchers';?>">View Vouchers</a>
				</li>
				<li <?php if($fileName == 'addVechile.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters/addVechile';?>">Add Vehicle</a>
				</li>
				<li <?php if($fileName == 'vechiles.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'transporters/vechiles';?>">View Vehicle</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['inventory'] == '1' ): ?>

		<li class="has-sub <?php if( $fileName == 'rooms.php' || $fileName == 'addRoom.php' || $fileName == 'makeBooking.php' || $fileName == 'bookings.php' || $fileName == 'addGuest.php' || $fileName == 'roomCategories.php' || $fileName == 'addCategory.php' || $fileName == 'monthlyBookings.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Room Inventory<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addCategory.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/addCategory';?>">Add Category</a>
				</li>
				<li <?php if($fileName == 'roomCategories.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/roomCategories';?>">View Categories</a>
				</li>
				<li <?php if($fileName == 'addRoom.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/addRoom';?>">Add Rooms</a>
				</li>
				<li <?php if($fileName == 'rooms.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/rooms';?>">View Rooms</a>
				</li>
				<li <?php if($fileName == 'makeBooking.php' || $fileName == 'addGuest.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/makeBooking';?>">Make Booking</a>
				</li>
				<li <?php if($fileName == 'bookings.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/bookings';?>">View Bookings</a>
				</li>
				<li <?php if($fileName == 'monthlyBookings.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'inventory/monthlyBookings';?>">Monthly Room Status</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['customers'] == '1' ): ?>

		<li class="has-sub <?php if( $fileName == 'addCustomer.php' || $fileName == 'editCustomer.php' || $fileName == 'customers.php') {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Customers<span class="arrow <?php echo $open; $open = ""; ?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addCustomer.php') echo 'class="active "';?>>
				<a href="<?php echo base_url().'customers/addCustomer';?>">Add Customer</a>
				</li>
				<li <?php if($fileName == 'customers.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'customers';?>">View Customers</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['services'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'services.php' || $fileName == 'addService.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Services<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addService.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'services/addService';?>">Add Service</a>
				</li>
				<li <?php if($fileName == 'services.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'services/viewService';?>">View Services</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['tours'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'viewTours.php' || $fileName == 'addTour.php' || $fileName == 'emailTour.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Tour Packages<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addTour.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'tours/addTour';?>">Add Tour</a>
				</li>
				<li <?php if($fileName == 'viewTours.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'tours';?>">View Tours</a>
				</li>
				<li <?php if($fileName == 'emailTour.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'tours/emailTour';?>">Email Tour</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['passes'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'passes.php' || $fileName == 'addPass.php' || $fileName == 'editPass.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Airport Passes<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addPass.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'passes/addPass';?>">Add Pass</a>
				</li>
				<li <?php if($fileName == 'passes.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'passes';?>">View Passes</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<?php if( $this->session->userdata['level'] == '1' ): ?>

		<li class="has-sub <?php if($fileName == 'users.php' || $fileName == 'addUser.php' || $fileName == 'userRoles.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">User Accounts<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addUser.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'accounts/addUser';?>">Add User Account</a>
				</li>
				<li <?php if($fileName == 'users.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'accounts/users';?>">View User Accounts</a>
				</li>
			</ul>
		</li>

		<?php endif; ?>

		<li class="has-sub <?php if($fileName == 'notes.php' || $fileName == 'viewNote.php' || $fileName == 'addNotification.php' ) {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Notes / Memo<span class="arrow <?php echo $open; $open = "";?>"></span>
			</a>
			<ul class="sub">
				<li <?php if($fileName == 'addNotification.php') echo 'class="active"';?>>
				<a href="<?php echo base_url().'notifications/addNotification';?>">Add Note / Memo</a>
				</li>
				<li <?php if($fileName == 'notes') echo 'class="active"';?>>
				<a href="<?php echo base_url().'notifications/notes';?>">View Note / Memo</a>
				</li>
			</ul>
		</li>

		
		<li class="has-sub <?php if($fileName == 'changePassword.php' || $fileName == 'profile.php' || $fileName == 'logosettings.php' || $fileName == 'tos.php' || $fileName == 'plans.php') {echo 'active'; $open = 'open';} ?>">
			<a href="javascript:;" class="">Settings<span class="arrow <?php echo $open; $open = "";?>"></span></a>
			<ul class="sub">

				<?php if( $this->session->userdata['level'] == '1' ): ?>
					<li <?php if($fileName == 'profile.php') echo 'class="active"';?>>
						<a href="<?php echo base_url().'settings/profile';?>">Profile Settings</a>
					</li>
					<li <?php if($fileName == 'logosettings.php') echo 'class="active"';?>>
						<a href="<?php echo base_url().'settings/logo';?>">Logo Settings</a>
					</li>
					<li <?php if($fileName == 'tos.php') echo 'class="active"';?>>
						<a href="<?php echo base_url().'settings/tos';?>">Terms & Conditions</a>
					</li>
					<li <?php if($fileName == 'plans.php') echo 'class="active"';?>>
						<a href="<?php echo base_url().'settings/plans';?>">Plans & Packages</a>
					</li>
				<?php endif; ?>
				<li <?php if($fileName == 'changePassword.php') echo 'class="active"';?>>
					<a href="<?php echo base_url().'user/changePassword';?>">Change Password</a>
				</li>
			</ul>
		</li>
		
		<li>
			<a href="<?php echo base_url().'login/logout';?>">Logout</a>					
		</li>
		<?php endif; ?>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>
