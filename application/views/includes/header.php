<div id="header" class="navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!-- BEGIN LOGO -->
			<div style="margin-left: -1%;">
				<a class="brand" href="<?php echo base_url(); ?>">
					<?php echo $this->user_model->getUserLogo(); ?>
				</a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="arrow"></span>
			</a>          
			<!-- END RESPONSIVE MENU TOGGLER -->				
			<div class="top-nav">
				
				<!-- BEGIN TOP NAVIGATION MENU -->					
				<ul class="nav pull-right" id="top_menu">
									
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Hi! <?php echo $this->session->userdata['name'];?></strong>
						<i class="icon-user"></i>
						<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<?php if( $this->session->userdata['level'] == 1 ): ?>
								<li style="padding-top:.5em;"><a href="<?php echo base_url().'settings/profile';?>"> Profile</a></li>
							<?php 
								endif; 
							?>
							<?php if( $this->session->userdata['level'] != 7 ): ?>
					        	<li><a href="<?php echo base_url().'user/changePassword';?>"> Change Password</a></li>
					        <?php else: ?>
					        	<li><a href="<?php echo base_url().'admin/changePassword';?>"> Change Password</a></li>
					        <?php endif; ?>
							<li class="divider"></li>
							<li><a href="<?php echo base_url().'login/logout';?>"> Log Out</a></li>
						</ul>
					</li>
					
					<?php $hNotifications 	= 	$this->notifications_model->getHeaderNotifications(); ?>
							<!-- notification -->
				
					<li class="dropdown" id="header_notification_bar">

					<?php 
						if( count( $hNotifications ) )
							echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">'; 
					?> 
						
					<i class="icon-bell"></i>
					
					<?php 
						if( count( $hNotifications ) )
							echo '<span class="badge badge-success">'.count( $hNotifications ).'</span>'; 
					?> 
					
					</a>
					
					<ul class="dropdown-menu extended notification">
						<?php if( count( $hNotifications ) ): ?>
						<li>
							<p>

								 You have <?php echo count( $hNotifications ); ?> new Notifications
							</p>
							<li>
							<div class="slimScrollDiv" style="position: relative; overflow: auto; width: auto; height: 200px;">
								<ul class="dropdown-menu-list scroller" style="list-style:none!important; margin:0 0 0px 0px;" >
									<?php 
										foreach ($hNotifications as $item => $value) 
										{
											echo "<li><a href= '".base_url()."notifications/viewNote/" .$value['id']."' >".substr($value['note'],0,50)."... <span class='time' style='float:right;position:relative; font-size: 90%; font-weight: bold;'><i>".date("jS F, Y", strtotime($value['notify_date']))."</i></span></a></li>";
										}

									?>
								</ul>
								<li>
									<?php if( count( $hNotifications ) > 0) echo "<a href= '".base_url()."notifications/viewNote/All' >Show all Notifications </a>" ?>

								</li>
							</div>
							</li>
						</li>
						<?php endif; ?>
					</li>
				
					<!-- notification data end  -->

				</ul>
					<!-- END USER LOGIN DROPDOWN -->
				
				</ul>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>