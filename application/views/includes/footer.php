    <div id="footer"><center>2015 &copy; OCTA Travel | User Dashboard</center></div>
    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->      
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <!--script src="<?php echo base_url();?>assets/js/jquery-1.3.2.min.js"></script-->
    <script src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>       
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.blockui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!--script type="text/javascript" src="<?php echo base_url();?>assets/uniform/jquery.uniform.min.js"></script-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/app.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/barChart/TableBarChart.js"></script> 
         
    
    <script>
        jQuery(document).ready(function($) {            
            // initiate layout and plugins
            App.init();
            $('#source').tableBarChart('#target', '', false);
            $('#source2').tableBarChart('#target2', '', false);
        });
    </script>
    
    <script type="text/javascript">
        jQuery(document).ready(function($) 
        {
            $("#addButton").click(function () 
            {
                if( ($('.form-services .control-group').length+1) > 10) 
                {
                    alert("Only 11 Services Allowed");
                    return false;
                }
                var id = ($('.form-services .control-group').length + 1).toString();
                var jsonObj = {};
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url()."services/getServiceAjax";?>',
                    success:function(data)
                    {
                        jsonObj = JSON.parse(data);
                        $('.form-services').append('<div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputText' + id + '"></label><div class="controls"> <select onchange="javascript:startCalc();" name="service_type[]" id="'+ id +'" required> <option value="">Select Service</option> </select> <input type="text" required onInput="javascript:startCalc();" autocomplete="off" class="input-small" id="actual_cost_' + id + '" name="actual_cost[]" placeholder="Actual Cost" readonly> <input type="text" onInput="javascript:startCalc();" autocomplete="off" maxlength="2" class="input-small" id="charges_' + id + '" name="charges[]" placeholder="Charges(%)" > <input type="number" id="cost_' + id + '" onInput="javascript:startCalc();" autocomplete="off" class="input-small" required placeholder="Amount" name="cost[]" /> </div></div>');
                        var x    =   document.getElementById(id);
                        for(i=0;i<jsonObj.length;i++){
                            var option    =   document.createElement("option");
                            option.value  =   jsonObj[i].serviceName;
                            option.text   =   jsonObj[i].serviceName;
                            option.setAttribute("actualCost", jsonObj[i].cost);
                            x.add(option);
                        }
                    }
                });

                $('#monthly_room_bookings').dataTable( {
                    "pageLength": 31
                } );

            });
              

            $("#removeButton").click(function () 
            {
                if ($('.form-services .control-group').length == 1) 
                {
                    alert("No More Services To Remove");
                    return false;
                }
                $(".form-services .control-group:last").remove(); 
                startCalc();               
            });

           

        });


        jQuery(document).ready(function($) {

            $("#addRoomVoucherButton").click(function () 
            {

                if( ($('.hotel-voucher-services .control-group').length+1) > 10) 
                {
                    alert("Only 11 Services Allowed");
                    return false;
                }
                var id = ($('.hotel-voucher-services .control-group').length + 1).toString();
                
                $('.hotel-voucher-services').append('<div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputText' + id + '"></label><div class="controls"> <select onchange="getTariffDetails(' + id + ');" name="room_type[]" id="room_type_'+ id +'" class="span4"> <option value="">Room Category</option> </select> <select onchange="javascript:startHotelCalc();" name="meal_plan[]" id="meal_plan_'+ id +'" class="span2"> <option value="" cost="">Meal Plan</option> </select> <select onchange="javascript:startHotelCalc();" name="roomNos[]" id="roomNos_'+ id +'" class="span2"> <option value="">No of Rooms</option><option value="1">1</option> <option value="2">2</option> <option value="3">3</option> </select> <select onchange="javascript:startHotelCalc();" name="extrabed[]" id="extrabed_'+ id +'" class="span2"> <option value="0" cost="">Extrabed</option> </select> <select onchange="javascript:startHotelCalc();" name="child_without_bed[]" id="childs_without_bed_'+ id +'" class="span2"> <option value="0">Child WOB </option><option value="1">1</option> <option value="2">2</option> <option value="3">3</option> </select></div></div>');
                getRoomCategories();
            });
              

            $("#removeRoomVoucherButton").click(function () {
                if ($('.hotel-voucher-services .control-group').length == 1) {
                    alert("No More Rooms To Remove");
                    return false;
                }
                $(".hotel-voucher-services .control-group:last").remove(); 
                startHotelCalc();               
            });

        });


        jQuery(document).ready(function($) {

            $("#addTransportVoucherButton").click(function () 
            {
                if( ($('.transport-voucher-services .control-group').length+1) > 10) 
                {
                    alert("Only 11 Services Allowed");
                    return false;
                }
                var id = ($('.transport-voucher-services .control-group').length + 1).toString();
                
                $('.transport-voucher-services').append('<div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputText' + id + '"></label><div class="controls"> <select class="span4" onchange="displayTripDetails(this);" id="trips_'+ id + '" name="trip[]"><option value=""></option></select>&nbsp;<input type="text" autocomplete="off" readonly placeholder="Vechile" class="span2" id="vechile_'+ id +'" name="vechile[]" /><input type="number" autocomplete="off" readonly placeholder="Capacity" class="span2" id="capacity_'+ id +'" name="capacity[]" />&nbsp;<input type="number" autocomplete="off" readonly placeholder="Days" class="span2" id="days_'+ id +'" name="days[]" />&nbsp;<input type="number" autocomplete="off" readonly placeholder="Rate" class="span2" id="rate_'+ id +'" name="rate[]" />&nbsp;<input type="number" onInput="checkPax(this);" autocomplete="off" placeholder="No of Pax" class="span2" id="pax_'+ id +'" name="pax[]" />&nbsp;<input class="input-small date-picker span2" required type="text" placeholder="Date of Travel" name="dot[]" id="dot_'+ id +'"/> </div></div>');
                    $('#dot_'+ id).datepicker();
                getTransporterTrips();
            });
              

            $("#removeTransportVoucherButton").click(function () {
                if ($('.transport-voucher-services .control-group').length == 1) {
                    alert("No More Trips To Remove");
                    return false;
                }
                $(".transport-voucher-services .control-group:last").remove(); 
                startTransportCalc();               
            });

        });


        $(window).load(function () 
        {
            if($(window).width() < 768)
            {
                $('#menu').hide();
            }
            else
            {
                $('#menu').show();
            }
        });

        $(window).resize(function()
        {
            if($(window).width() < 768)
            {
                $('#menu').hide();
            }
            else
            {
                $('#menu').show();
            }
        });

        
        $(document).ready(function () {
                
            $('#datepickerm').datepicker({
                startView: 1
            });
        
        });


        $("#addAccomodationButton").click(function () 
        {
          

            if( ($('.hotel-voucher-services1 .cg').length+1) > 10) 
            {
                alert("Only 11 Services Allowed");
                return false;
            }
            var id = ($('.hotel-voucher-services1 .cg').length + 1).toString();
            $('.hotel-voucher-services1').append('<div class="cg"></br><div class="control-group " id="control-group' + id + '"><label class="control-label" for="input1">HOTEL NAME</label><div class="controls"><select onchange="getRoomCategories(' + id + ');" class="chosen " required id="hotel_id_' + id +'" name="hotel_id[]" ><option value="">Select Existing Hotel</option><?php foreach($hotels as $hotel){ ?><option value="<?php echo $hotel['id'];?>" ><?php echo $hotel['hotel_name'];?></option><?php } ?></select></div></div> <div class="control-group" id="control-group' + id + '"><label class="control-label">CheckIn - CheckOut</label><div class="controls"><div class="input-prepend"><span class="add-on"><i class="fa fa-calendar"></i> </span><input type="text" id="accomodation_date_'+ id +'" onchange="javascript:startHotelCalc();" name= "accomodation_date[]" required  placeholder="<?php echo date( 'm/d/Y'); ?>" class="input-large date-range" value=""/></div></div></div> <div class="hotel-voucher-services"> <div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputText' + id + '"></label><div class="controls"> <select onchange="getTariffDetails(' + id + ');" name="room_type[]" id="room_type_'+ id +'" class="span4" required> <option value="">Room Category</option> </select> <select onchange="javascript:startHotelCalc();" required name="meal_plan[]" id="meal_plan_'+ id +'" class="span2"> <option value="" cost="">Meal Plan</option> </select> <select onchange="javascript:startHotelCalc();" required name="roomNos[]" id="roomNos_'+ id +'" class="span2"> <option value="">No of Rooms</option><option value="1">1</option> <option value="2">2</option> <option value="3">3</option> </select> <select onchange="javascript:startHotelCalc();" name="extrabed[]" id="extrabed_'+ id +'" class="span2"> <option value="0" cost="">Extrabed</option> </select> <select onchange="javascript:startHotelCalc();" name="child_without_bed[]" id="childs_without_bed_'+ id +'" class="span2"> <option value="0">Child WOB </option><option value="1">1</option> <option value="2">2</option> <option value="3">3</option> </select> </div></div></div></div>');
           
               $('#accomodation_date_'+ id).daterangepicker();
               $('#hotel_id_'+ id).chosen();
               
           
        });

          
        $("#removeAccomodationButton").click(function () {
            if ($('.hotel-voucher-services1 .cg').length == 1) {
                alert("No More Rooms To Remove");
                return false;
            }
            $(".hotel-voucher-services1 .cg:last").remove(); 
            startHotelCalc();               
        });


        $("#addTransportButton").click(function () 
        {
            if( ($('.transport-voucher-services1 .cg').length+1) > 10) 
            {
                alert("Only 11 Services Allowed");
                return false;
            }
            var id = ($('.transport-voucher-services1 .cg').length + 1).toString();
        
            
            $('.transport-voucher-services1').append('<div class="cg"><br><div class="control-group "><label class="control-label" for="input1">TRANSPORTER NAME</label><div class="controls"><select onchange="getTransporterTrips('+ id +');" class="chosen" required id="transporter_id_'+ id +'" name="transporter_id[]" ><option value="">Select Existing Transporter</option><?php foreach($transporters as $transporter) { ?><option value="<?php echo $transporter['id'];?>" ><?php echo $transporter['transporter_name'];?></option> <?php } ?></select></div></div><div id="pickups_'+ id +'" class="control-group"> <label class="control-label" for="input1">Pickup</label><div class="controls"><input type="text"  placeholder="Pickup From" class="span6" id="pickup_'+ id +'" name="pickup[]" /> </div></div> <div class="transport-voucher-services"><div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputText' + id + '"></label><div class="controls"> <select class="span4" onchange="displayTripDetails(this);" id="trips_'+ id + '" required name="trip[]"><option value=""></option></select>&nbsp;<input type="text" autocomplete="off" readonly placeholder="Vechile" class="span2" id="vechile_'+ id +'" name="vechile[]" /><input type="number" autocomplete="off" readonly placeholder="Capacity" class="span2" id="capacity_'+ id +'" name="capacity[]" />&nbsp;<input type="number" autocomplete="off" readonly placeholder="Days" class="span2" id="days_'+ id +'" name="days[]" />&nbsp;<input type="number" autocomplete="off" readonly placeholder="Rate" class="span2" id="rate_'+ id +'" name="rate[]" />&nbsp;<input type="number" onInput="checkPax(this);" autocomplete="off" placeholder="No of Pax" class="span2" id="pax_'+ id +'" name="pax[]" />&nbsp;<input class="input-small date-picker span2" required type="text" placeholder="Date of Travel" name="dot[]" id="dot_'+ id +'"/> </div></div></div></div>');
                $('#dot_'+ id).datepicker();
                $('#transporter_id_'+ id).chosen();

            getTransporterTrips();
        });
          

        $("#removeTransportButton").click(function () {
            if ($('.transport-voucher-services1 .cg').length == 1) {
                alert("No More Trips To Remove");
                return false;
            }
            $(".transport-voucher-services1 .cg:last").remove(); 
            startTransportCalc();               
        });
       
    </script>  


    <script>

        $('.textarea').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": false, //Button to insert a link. Default true
            "image": false, //Button to insert an image. Default true,
            "color": false
        });

    </script>
   <!-- END JAVASCRIPTS --> 
</body>
<!-- END BODY -->
</html>
