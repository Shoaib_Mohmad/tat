<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8" />
	<title>OCTA Travel | <?php echo $pageName;?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel='shortcut icon' href='<?php echo base_url();?>assets/img/favicon.ico' type='image/x-icon' />
	<?php include_once('includes/includes.php'); ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">

	<!-- BEGIN HEADER -->
	<?php include_once('includes/header.php'); ?>
	<!-- END HEADER -->
	
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php'); ?>
		<!-- END SIDEBAR -->
		
		<!-- BEGIN PAGE -->
		<div id="body">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
					
						<!-- BEGIN MENU BAR-->
						<br>
						<?php 
							echo $this->session->flashdata('registeration_notification');
							include_once('includes/menuBar.php');
						?>
						<!-- END MENU BAR-->
						
						<!-- BEGIN BREADCRUMB-->		
						
						<ul class="breadcrumb span12" style="margin-left:1px;">
							<li>
								<img style="margin-top: -4px;" src="<?php echo base_url().'assets/img/menu/home.png';?>" height="15px" width="15px"/>&nbsp;&nbsp;
								<a href="<?php echo base_url().'user/dashboard'; ?>" style="text-decoration:none;">Home</a> <span class="divider">/</span>
							</li>
							<li><a href="javascript: void(0);" style="text-decoration:none;"><?php echo $pageName;?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					
					<!-- Page Contents Start -->
					<?php include_once( APPPATH.'modules/'.$pathType.'/views/'.$fileName ); ?>
					<!-- Page Contents End -->

				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN FOOTER -->
	<?php include_once('includes/footer.php'); ?>
	<!-- END FOOTER -->