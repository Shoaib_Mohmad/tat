
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="<?php echo base_url();?>css/style.css" rel='stylesheet' type='text/css' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='<?php echo base_url();?>assets/css/google-fonts.css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text.css'/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css" />
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/style_default.css" rel="stylesheet" id="style_color" />
    <script src="<?php echo base_url();?>assets/js/jquery-1.8.2.min.js"></script>
    <style type="text/css">
        .align-right{
            margin-left: 2em!important;
        }

        .grey-bg{
            background-color: #F3F3F3; 
            border-radius: 5px;
        }
    </style>
    <script type="text/javascript">
    function checkPassword()
    {
        var password1 = $('#password1').val();
        var password2 = $('#password2').val();
        if( password1 == password2 )
        {
            return true;
        }
        else
        {
            document.getElementById('warning').innerHTML = '<div class="alert alert-error" style="width: 77%; margin-left: 2em;">Warning! Entered Passwords Don\'t Match</div>';
            $('#password1').val('');
            $('#password2').val('');
            return false;
        }
    }
    </script>
</head>
<body>
    <div class="main">
        <div class="header" >
            <h1>OCTA Travel | User Login | Create a Free Account!</h1>
            <p>We encourage BUSINESS, Innovation & Flexibility, Quick & High Results - Oriented WORK. </p>
        </div>
        
      
        <ul class="left-form">
            <h2>Features</h2>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Secure Invoicing</h5>
                <span style="padding-left:2.5em;">Create transactions at any stage of the sales cycle.</span>
            </li>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Quick Reporting</h5>
                <span style="padding-left:2.5em;">Reporting on every aspect of your business.</span>
            </li>
            <li>
                <h5><img src="<?php echo base_url();?>assets/img/icons/tick.png"> Manage Inventory</h5>
                <span style="padding-left:2.5em;">Manage Rooms, Bookings, Ticketing and many more.</span>
            </li>
            <!-- <input onclick="manageLogin('createAccount');" type="submit" value="Create Account"> -->
            <div class="clear"> </div>
        </ul>

       
        <ul class="right-form grey-bg" id="login">
            <form onSubmit="return checkPassword();" class="form-vertical no-padding no-margin" action="<?php echo base_url().'index.php/login/registerationResult/'.$id;?>" method="POST">
                <h3 class="align-right">User Registration</h3>
                <br>
                <?php echo $this->session->flashdata('notification');?>
                <div id="warning"> </div>
                <div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="password" required pattern=".{6,}" title="6 Characters Minimum" placeholder="Choose Password" class="span5" name="password1" id="password1" />
                    </div>
                </div>
                <div class="control-group align-right">
                    <div class="controls">
                        <input type="password" required pattern=".{6,}" title="6 Characters Minimum" placeholder="Confirm Password" class="span5" name="password2" id="password2" />
                    </div>
                </div>
                <h4 class="align-right">Password must be atleast 6 characters long</h4><br>
                <div class="control-group pull-right">
                    <div class="controls">
                        <input type="submit" value="Login" >
                    </div>
                </div>
            </div>
        </form>
    </ul>

    <div class="clear"> </div>
    
</div>

<div class="copy-right">
    <p>OCTA Travel &copy; 2015 | <a href="http://www.octasoft.biz">OCTASOFT Solutions Pvt Ltd</a></p> 
</div>

</body>
</html>