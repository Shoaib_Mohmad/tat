-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2015 at 04:33 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `octa_travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE IF NOT EXISTS `bills` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `appcode` varchar(100) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `factory_id` int(11) DEFAULT NULL,
  `bill_type` varchar(100) NOT NULL,
  `bill_number` varchar(100) NOT NULL,
  `attachments` text NOT NULL,
  `receipts` text NOT NULL,
  `total_amount` int(100) NOT NULL,
  `amount_paid` int(100) NOT NULL,
  `paid_to` varchar(100) NOT NULL,
  `payment_mode` text NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `payment_date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `user_id`, `appcode`, `store_id`, `factory_id`, `bill_type`, `bill_number`, `attachments`, `receipts`, `total_amount`, `amount_paid`, `paid_to`, `payment_mode`, `reference_no`, `payment_date`, `timestamp`, `status`) VALUES
(1, 1, 'OSB-876549', NULL, NULL, 'Tours & Travels Bill', '1', 'Bill_1.jpg', 'Receipt_1.jpg', 1000, 1000, 'PWD', 'Cash', 'FG-43', '2014-12-05', '2014-12-05 09:53:21', 'Active'),
(2, 17, 'OSB-236549', 1, NULL, 'Store1 Expense', '1', '', '', 1000, 800, 'BSNL', 'Check', 'FG-43', '2014-12-11', '2014-12-06 09:43:44', 'Active'),
(3, 1, 'OSB-876549', NULL, NULL, 'Test Expense', '123', 'Bill_3.png', '', 1200, 1000, 'Mr XYZ', 'Cash', '12de', '2015-04-05', '2015-06-11 07:23:52', 'Active'),
(4, 1, 'OSB-876549', NULL, NULL, 'Last Bill', '1234', 'Bill_4.jpg', '', 1000, 990, 'PWD', 'Cash', '4321', '2015-06-11', '2015-06-11 07:07:06', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `extra_bed` int(11) NOT NULL,
  `meal_plan` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `checkInDate` datetime NOT NULL,
  `checkOutDate` datetime NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Cancelled') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `appcode`, `category_id`, `no_of_rooms`, `extra_bed`, `meal_plan`, `amount`, `customer_id`, `checkInDate`, `checkOutDate`, `createdAt`, `status`) VALUES
(10, 'OSB-876549', 2, 2, 0, 'APAI', 2000, 11, '2015-05-25 12:00:00', '2015-05-26 11:59:59', '2015-05-14 08:22:01', 'Active'),
(11, 'OSB-876549', 1, 5, 0, 'EPAI', 9000, 17, '2015-05-27 12:00:00', '2015-05-30 11:59:59', '2015-05-15 05:53:31', 'Active'),
(12, 'OSB-876549', 3, 2, 1, 'MAPAI', 3500, 17, '2015-05-26 12:00:00', '2015-05-28 11:59:59', '2015-05-18 10:01:56', 'Active'),
(13, 'OSB-876549', 1, 2, 1, 'CMAI', 1200, 1, '2015-05-22 12:00:00', '2015-05-28 11:59:59', '2015-05-19 12:13:05', 'Active'),
(14, 'OSB-876549', 1, 1, 1, 'MAPAI', 1200, 17, '2015-05-28 12:00:00', '2015-05-30 11:59:59', '2015-05-22 11:50:36', 'Active'),
(15, 'OSB-876549', 1, 2, 1, 'CP', 2000, 8, '2015-06-01 12:00:00', '2015-06-05 11:59:59', '2015-06-15 05:32:52', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `landline` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `pin` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `appcode`, `name`, `email`, `phone`, `landline`, `city`, `state`, `country`, `pin`, `status`) VALUES
(1, 'OSB-876549', 'Mohd Ibrahim', 'ibrahim@gmail.cxom', '1231231231', '1231231231', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', 1),
(2, 'OSB-876549', 'Junaid Tahir', 'shoaib.mohd@myasa.net', '1231231231', '1231231231', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', 1),
(4, 'OSB-876549', 'Sajad', 'sajadbinnazir@gmail.com', '9906829059', '34345345', 'Baramulla', 'Jammu and Kashmir', 'India', '193198', 1),
(5, 'OSB-876549', 'jutjk', 'euuu7u@gmsil.com', '9876543219', 'ru', '56u', 'Jidd Hafs', 'Bahrain', '12345', 1),
(6, 'OSB-876549', 'Muntazir', 'muntazir@gmail.com', '1231231231', '1231231231', 'Srinagar', 'Jammu and Kashmir', 'India', '123123', 1),
(7, 'OSB-876549', 'wani muntazir', 'muntazir786wani@gmail.com', '9596340641', '9596340641', 'sirnager', 'Jammu and Kashmir', 'India', '113411', 1),
(8, 'OSB-876549', 'Abdul Rahim', 'ab_rm@gmail.com', '1231321231', 'Jammu & Kashmir', 'Srinagar', 'Anguilla', 'Anguilla', '190012', 1),
(9, 'OSB-876549', 'Shoaib Mohamd', 'mohmad.shoaib@gmail.com', '1231231231', 'Jammu & Kashmir', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', 1),
(10, 'OSB-876549', 'Mohd Amir', 'amir@gmail.com', '3123123123', '1231231231', 'Srinagar', 'Eastern', 'American Samoa', '190012', 1),
(11, 'OSB-876549', 'Abdul Azim ', 'azim@gmail.com', '1231231231', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', 1),
(12, 'OSB-876549', 'Musa', 'musa@gmail.com', '1231231231', '', 'Srinagar', 'Faryab', 'Afghanistan', '190012', 1),
(13, 'OSB-876549', 'Shoaib Mohamd2', 'mohmad.shoaib@gmail.com', '1231231231', 'Jammu & Kashmir', 'Srinagar', 'Antartica', 'Antartica', '190012', 1),
(14, 'OSB-876549', 'Shoaib Mohamd3', 'mohmad.shoaib@gmail.com', '1231231231', 'Jammu & Kashmir', 'Srinagar', 'Antartica', 'Antartica', '190012', 1),
(15, 'OSB-876549', 'Shoaib Mohamd', 'mohmad.shoaib@gmail.com', '1231231231', 'Jammu & Kashmir', 'Srinagar', 'Chlef', 'Algeria', '190012', 0),
(16, 'OSB-876549', 'Amir', 'mohmad.shoaib@gmail.com', '1231231231', '1214141', 'Srinagar', 'Delhi', 'India', '190012', 1),
(17, 'OSB-876549', 'Eshan Hussain', 'eshan@gmail.com', '1231231231', '12313123', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `hotel_name` varchar(64) NOT NULL,
  `address` varchar(512) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `website` varchar(64) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `appcode`, `hotel_name`, `address`, `phone`, `fax`, `email`, `website`, `description`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 'Hotel Akeet', 'Boulevard Road, Dalgate, \nSrinagar, J&K, 190001', '9596340641', '9622724909', 'hotelakeet@gmail.com', 'www.hotelakeet.com', 'Two Star Hotel', '2015-05-23 08:20:03', 'Active'),
(2, 'OSB-876549', 'Hotel Khayam', 'Khayam Chowk,\nSrinagar, J&K, 190002', '1231231231', '1231231231', 'hotel_khayam@gmail.com', 'www.hotelkhayam.com', 'A Three Star Hotel', '2015-05-23 08:42:16', 'Active'),
(3, 'OSB-876549', 'Hotel Shalimar', 'Residency Road, Lal Chowk,\nSrinagar, J&K, 190001', '9622724000', '9622724000', 'hotel_shalimar@gmail.com', 'www.hotel_shalimar.com', 'Three Star Hotel.', '2015-05-23 13:45:50', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_extrabedding`
--

CREATE TABLE IF NOT EXISTS `hotel_extrabedding` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  `charges` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `hotel_extrabedding`
--

INSERT INTO `hotel_extrabedding` (`id`, `appcode`, `hotel_id`, `charges`, `created_at`, `status`) VALUES
(2, 'OSB-876549', 2, 500, '2015-05-23 10:12:52', 'Active'),
(5, 'OSB-876549', 1, 300, '2015-05-23 11:18:25', 'Active'),
(6, 'OSB-876549', 3, 600, '2015-05-23 13:46:24', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_tariff`
--

CREATE TABLE IF NOT EXISTS `hotel_tariff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  `room_category` varchar(32) NOT NULL,
  `occupancy` varchar(32) NOT NULL,
  `ep` int(11) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `map` int(11) DEFAULT NULL,
  `ap` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hotel_tariff`
--

INSERT INTO `hotel_tariff` (`id`, `appcode`, `hotel_id`, `room_category`, `occupancy`, `ep`, `cp`, `map`, `ap`, `created_at`, `status`) VALUES
(2, 'OSB-876549', 2, 'Standard Room', 'Single', 3000, 3400, 4000, 4500, '2015-05-23 09:15:46', 'Active'),
(3, 'OSB-876549', 2, 'Deluxe Room', 'Double', 4000, 4500, 5000, 5500, '2015-05-23 10:29:29', 'Active'),
(4, 'OSB-876549', 1, 'Standard Room', 'Double', 1200, 1300, 1400, 1600, '2015-05-23 11:18:16', 'Active'),
(5, 'OSB-876549', 3, 'Standard Room', 'Single', 1000, 2000, 3000, 4000, '2015-05-23 13:46:14', 'Active'),
(6, 'OSB-876549', 3, 'Standard Room', 'Double', 1500, 2500, 3500, 4500, '2015-05-23 13:46:57', 'Active'),
(7, 'OSB-876549', 3, 'Deluxe Room', 'Double', 2000, 3000, 4000, 5000, '2015-05-23 13:47:23', 'Active'),
(8, 'OSB-876549', 3, 'Standard Room', 'Triple', 3000, 4000, 5000, 6000, '2015-05-23 15:19:05', 'Active'),
(9, 'OSB-876549', 1, 'Standard Room', 'Single', 1000, 1200, 1300, 1400, '2015-05-23 16:01:16', 'Active'),
(10, 'OSB-876549', 1, 'Standard Room', 'Triple', 1300, 1400, 1500, 1600, '2015-05-23 16:02:12', 'Active'),
(11, 'OSB-876549', 1, 'Deluxe Room', 'Single', 1200, 1400, 1600, 1800, '2015-05-25 10:10:06', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_vouchers`
--

CREATE TABLE IF NOT EXISTS `hotel_vouchers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `voucher_no` bigint(20) NOT NULL,
  `voucher_date` date NOT NULL,
  `invoice_no` bigint(20) NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  `customer_name` varchar(64) NOT NULL,
  `customer_email` varchar(64) NOT NULL,
  `customer_mobile` varchar(64) NOT NULL,
  `customer_city` varchar(64) NOT NULL,
  `customer_state` varchar(64) NOT NULL,
  `customer_country` varchar(64) NOT NULL,
  `customer_pin` varchar(64) NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `pax` int(11) NOT NULL,
  `remarks` varchar(1024) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `hotel_vouchers`
--

INSERT INTO `hotel_vouchers` (`id`, `appcode`, `voucher_no`, `voucher_date`, `invoice_no`, `hotel_id`, `customer_name`, `customer_email`, `customer_mobile`, `customer_city`, `customer_state`, `customer_country`, `customer_pin`, `checkin`, `checkout`, `pax`, `remarks`, `timestamp`, `email_status`, `status`) VALUES
(13, 'OSB-876549', 1, '2015-06-04', 7, 1, 'Abdul Azim', 'azim@gmail.com', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', '2015-06-10', '2015-06-11', 2, 'Take extra service charges directly from the customer', '2015-06-04 16:24:24', 0, 'Active'),
(14, 'OSB-876549', 4, '2015-06-08', 7, 2, 'Abdul Azim', 'azim@gmail.com', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', '2015-06-10', '2015-06-12', 2, 'Take Extra Charges Directly', '2015-06-08 15:03:49', 0, 'Active'),
(15, 'OSB-876549', 5, '2015-06-11', 8, 3, 'Abdul Azim', 'azim@gmail.com', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', '2015-06-17', '2015-06-18', 1, '', '2015-06-11 05:30:09', 0, 'Active'),
(16, 'OSB-876549', 6, '2015-06-15', 9, 3, 'Junaid Tahir', 'junaid@yahoo.in', '1231231231', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', '2015-06-15', '2015-06-17', 8, 'Take hghg nh nb', '2015-06-15 04:45:25', 0, 'Active'),
(17, 'OSB-876549', 7, '2015-06-15', 10, 2, 'jutjk', 'euuu7u@gmsil.com', '9876543219', '56u', 'Jidd Hafs', 'Bahrain', '12345', '2015-06-15', '2015-06-16', 10, 'asdad adasd', '2015-06-15 05:11:08', 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_voucher_services`
--

CREATE TABLE IF NOT EXISTS `hotel_voucher_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucher_id` bigint(20) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `room_category` varchar(32) NOT NULL,
  `occupancy` varchar(32) NOT NULL,
  `meal_plan` varchar(32) NOT NULL,
  `no_of_rooms` int(11) DEFAULT NULL,
  `extrabed` int(11) DEFAULT NULL,
  `child_without_bed` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `hotel_voucher_services`
--

INSERT INTO `hotel_voucher_services` (`id`, `voucher_id`, `appcode`, `room_category`, `occupancy`, `meal_plan`, `no_of_rooms`, `extrabed`, `child_without_bed`, `amount`) VALUES
(70, 13, 'OSB-876549', 'Standard Room', 'Double', 'ep', 1, 1, NULL, 1500),
(72, 14, 'OSB-876549', 'Standard Room', 'Single', 'ep', 1, 0, 2, 6000),
(73, 15, 'OSB-876549', 'Deluxe Room', 'Double', 'ep', 1, 2, 2, 3200),
(74, 16, 'OSB-876549', 'Standard Room', 'Double', 'map', 1, 0, 0, 7000),
(75, 17, 'OSB-876549', 'Deluxe Room', 'Double', 'map', 1, 0, 0, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_hotel_services`
--

CREATE TABLE IF NOT EXISTS `invoice_hotel_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `hotel_checkin` date NOT NULL,
  `hotel_checkout` date NOT NULL,
  `room_category` varchar(32) NOT NULL,
  `occupancy` varchar(32) NOT NULL,
  `meal_plan` varchar(32) NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `extrabed` int(11) DEFAULT NULL,
  `child_without_bed` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `invoice_hotel_services`
--

INSERT INTO `invoice_hotel_services` (`id`, `invoice_id`, `appcode`, `hotel_id`, `hotel_checkin`, `hotel_checkout`, `room_category`, `occupancy`, `meal_plan`, `no_of_rooms`, `extrabed`, `child_without_bed`, `amount`) VALUES
(1, 2, 'OSB-876549', 1, '2015-06-02', '2015-06-03', 'Standard Room', 'Double', 'ep', 1, 1, 1, 1500),
(2, 2, 'OSB-876549', 3, '2015-06-01', '2015-06-02', 'Standard Room', 'Single', 'ep', 1, 0, 0, 1000),
(3, 3, 'OSB-876549', 1, '2015-06-03', '2015-06-04', 'Standard Room', 'Single', 'ep', 1, 0, 0, 1000),
(7, 4, 'OSB-876549', 1, '2015-06-27', '2015-06-28', 'Standard Room', 'Single', 'ep', 1, 1, 0, 1300),
(8, 4, 'OSB-876549', 2, '2015-06-28', '2015-06-29', 'Deluxe Room', 'Double', 'ep', 1, 1, 0, 4500),
(9, 5, 'OSB-876549', 1, '2015-06-27', '2015-06-30', 'Standard Room', 'Double', 'ep', 1, 1, 0, 4500),
(10, 6, 'OSB-876549', 2, '2015-07-01', '2015-07-15', 'Standard Room', 'Single', 'ep', 1, 0, 0, 42000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tours`
--

CREATE TABLE IF NOT EXISTS `invoice_tours` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `tour_name` varchar(64) NOT NULL,
  `each_paying` varchar(64) NOT NULL,
  `plan` varchar(64) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `invoice_tours`
--

INSERT INTO `invoice_tours` (`id`, `invoice_id`, `appcode`, `tour_name`, `each_paying`, `plan`, `amount`) VALUES
(1, 0, '', '', '@$#4', 'economic', 2000),
(2, 51, 'OSB-876549', '3 days/ 2 Nights - Tour Code(RTPOP)', '02 pax, per pax@$#9', 'economic', 2000),
(9, 53, 'OSB-876549', '3 days/ 2 Nights - Tour Code(RTPOP)', '02 pax, per pax@$#9', 'economic', 2000),
(11, 4, 'OSB-876549', '4 days/ 3 Nights - Tour Code(RTPOP)', '02 pax, per pax@$#11', 'economic', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_transportation_services`
--

CREATE TABLE IF NOT EXISTS `invoice_transportation_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `transporter_id` int(11) NOT NULL,
  `pickup` varchar(128) DEFAULT NULL,
  `trip` varchar(64) NOT NULL,
  `vechile` varchar(32) NOT NULL,
  `capacity` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `dot` date NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `invoice_transportation_services`
--

INSERT INTO `invoice_transportation_services` (`id`, `appcode`, `invoice_id`, `transporter_id`, `pickup`, `trip`, `vechile`, `capacity`, `days`, `pax`, `dot`, `rate`) VALUES
(1, 'OSB-876549', 2, 3, 'Airport', 'Sonamarg Trip', 'INNOVA', 8, 2, 2, '2015-06-24', 4000),
(5, 'OSB-876549', 4, 3, 'Airport', 'Sonamarg Trip', 'INNOVA', 8, 1, 5, '2015-06-28', 2300),
(6, 'OSB-876549', 4, 3, NULL, 'Sonamarg Trip', 'INNOVA', 8, 2, 3, '2015-06-27', 4000),
(7, 'OSB-876549', 6, 3, 'Airport', 'Gulmarg Trip', 'TAVERA', 7, 1, 6, '2015-07-15', 2200);

-- --------------------------------------------------------

--
-- Table structure for table `my_notifications`
--

CREATE TABLE IF NOT EXISTS `my_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `note` text NOT NULL,
  `notify_date` date NOT NULL,
  `read_status` enum('Read','Unread') NOT NULL DEFAULT 'Unread',
  `notify_status` enum('Broadcast','Personal') NOT NULL DEFAULT 'Broadcast',
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `my_notifications`
--

INSERT INTO `my_notifications` (`id`, `user_id`, `appcode`, `note`, `notify_date`, `read_status`, `notify_status`, `status`) VALUES
(4, 1, 'OSB-876549', 'Mr. Muzi is Arriving on 12/07/15 & Mr. Muzafar is Arriving on 12/07/15', '2015-07-03', 'Unread', 'Broadcast', 'Active'),
(5, 30, 'OSB-876549', 'write some thing', '2015-07-02', 'Unread', 'Personal', 'Active'),
(6, 1, 'OSB-876549', 'This is a Test Notification of the day of July ', '2015-07-02', 'Unread', 'Personal', 'Active'),
(7, 1, 'OSB-876549', 'Mr. Muzi is Arriving on 12/07/15 & Mr. Muzafar is Arriving on 12/07/15', '2015-07-02', 'Unread', 'Personal', 'Active'),
(12, 1, 'OSB-876549', 'AD', '2015-07-02', 'Unread', 'Broadcast', 'Deleted'),
(13, 1, 'OSB-876549', 'AS', '2015-07-02', 'Unread', 'Broadcast', 'Deleted'),
(14, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Deleted'),
(15, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Deleted'),
(16, 1, 'OSB-876549', 'as', '2015-07-02', 'Unread', 'Broadcast', 'Active'),
(17, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Deleted'),
(18, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Active'),
(19, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Active'),
(20, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Active'),
(21, 1, 'OSB-876549', 'asd', '2015-07-02', 'Unread', 'Broadcast', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `passes`
--

CREATE TABLE IF NOT EXISTS `passes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `arrival_date` date NOT NULL,
  `flight_no` varchar(32) DEFAULT NULL,
  `driver_name` varchar(128) DEFAULT NULL,
  `vehicle_no` varchar(32) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `passes`
--

INSERT INTO `passes` (`id`, `appcode`, `client_name`, `arrival_date`, `flight_no`, `driver_name`, `vehicle_no`, `created_on`, `status`) VALUES
(3, 'OSB-876549', 'Mr. Gupta ', '2015-06-17', 'SP-4405', 'Shakeel', 'JK01A-0007', '2015-06-15 10:34:42', 'Active'),
(4, 'OSB-876549', 'Mr Singh', '2015-06-18', 'GT-225', 'Fayaz', 'JK01A-0008', '2015-06-15 10:42:36', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `appcode` varchar(100) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `appcode`, `user_name`, `company_name`, `street`, `city`, `state`, `country`, `pin`, `phone`, `fax`, `company_email`, `website`) VALUES
(1, 1, 'OSB-876549', 'Shoaib Mohmad', 'KASHMIR TOURS & TRAVELS', 'Lal Chowk', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', '9596545092', '1231231231', 'kashmirtours&travels@gmail.com', 'www.kashmirtours&travels.biz'),
(2, 2, 'OSB-236549', 'Shoaib Mohmad', 'OCTA SOFT PVT LTD', 'Rangreth', 'Srinagar', 'Jammu and Kashmir', 'India', '190001', '9596545092', '1231231231', 'info@octasoft.biz', 'www.octasoft.biz');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(50) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `rooms_available` int(11) NOT NULL,
  `description` varchar(512) NOT NULL,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `appcode`, `category_id`, `rooms_available`, `description`, `status`) VALUES
(3, 'OSB-876549', 1, 10, 'Rooms of Delux Category.', 'Active'),
(4, 'OSB-876549', 2, 2, 'Rooms of Super Delux Category.', 'Active'),
(5, 'OSB-876549', 3, 3, 'Test', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `room_categories`
--

CREATE TABLE IF NOT EXISTS `room_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(50) NOT NULL,
  `category_name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `belongs_to` enum('Hotels','Inventory') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `room_categories`
--

INSERT INTO `room_categories` (`id`, `appcode`, `category_name`, `description`, `belongs_to`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 'Delux', 'This is Delux Category.', 'Inventory', '0000-00-00 00:00:00', 'Active'),
(2, 'OSB-876549', 'Super Delux', 'This is Super Delux Category.', 'Inventory', '0000-00-00 00:00:00', 'Active'),
(3, 'OSB-876549', 'Double Bed', 'This Room has Double Bed.', 'Inventory', '0000-00-00 00:00:00', 'Active'),
(5, 'OSB-876549', 'Simplex', 'This is a Room Category...\n', 'Inventory', '0000-00-00 00:00:00', 'Deleted'),
(6, 'OSB-876549', 'Testing', '', 'Inventory', '0000-00-00 00:00:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(50) NOT NULL,
  `invoice_id` bigint(11) NOT NULL,
  `service_type` varchar(100) NOT NULL,
  `cost` int(100) NOT NULL,
  `charges` int(11) NOT NULL,
  `total` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `appcode`, `invoice_id`, `service_type`, `cost`, `charges`, `total`) VALUES
(51, 'OSB-876549', 15, 'Transportation', 1200, 90, 2280),
(52, 'OSB-876549', 15, 'Sonmarg Trip', 1000, 10, 1100),
(57, 'OSB-876549', 20, '5 Nights 6 Days in Kashmir', 10000, 10, 11000),
(58, 'OSB-876549', 21, 'Air Ticket Srinagar-Delhi', 2000, 20, 2400),
(59, 'OSB-876549', 22, 'Room: Super Deluxe', 1500, 20, 1800),
(62, 'OSB-876549', 23, 'Hotel Room', 2000, 50, 3000),
(63, 'OSB-876549', 23, 'Transportation', 1200, 10, 1320),
(64, 'OSB-876549', 24, 'Transportation', 1200, 20, 1440),
(65, 'OSB-876549', 25, 'Hotel Surya (Double Delux Room * 5 Nights)', 5000, 25, 6250),
(66, 'OSB-876549', 25, 'Transportation', 1200, 10, 1320),
(67, 'OSB-876549', 26, 'Hotel Surya (Double Delux Room * 5 Nights)', 5000, 10, 5500),
(68, 'OSB-876549', 26, 'Air Ticket Srinagar-Delhi', 2000, 20, 2400),
(74, 'OSB-876549', 41, 'Cabdel Light', 1200, 10, 1320),
(77, 'OSB-876549', 40, 'Cabdel Light', 1200, 10, 1320),
(78, 'OSB-876549', 40, 'Shikara', 900, 20, 1080),
(85, 'OSB-876549', 42, 'Cabdel Light', 1200, 5, 1260),
(86, 'OSB-876549', 42, 'Shikara', 900, 5, 945),
(87, 'OSB-876549', 44, 'Cabdel Light', 1200, 10, 1320),
(90, 'OSB-876549', 4, 'Sonmarg Trip', 1000, 10, 1100);

-- --------------------------------------------------------

--
-- Table structure for table `services_invoices`
--

CREATE TABLE IF NOT EXISTS `services_invoices` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `appcode` varchar(20) NOT NULL,
  `invoice_to` varchar(100) NOT NULL,
  `invoice_date` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_status` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `pin_code` int(12) NOT NULL,
  `no_of_pax` int(11) NOT NULL,
  `arrival` date NOT NULL,
  `departure` date NOT NULL,
  `accomodation_amount` int(11) DEFAULT NULL,
  `accomodation_charges` int(11) DEFAULT NULL,
  `accomodation_total` int(11) DEFAULT NULL,
  `invoice_code` bigint(20) NOT NULL,
  `transportation_amount` int(11) DEFAULT NULL,
  `transportation_charges` int(11) DEFAULT NULL,
  `transportation_total` int(11) DEFAULT NULL,
  `general_services_total` int(11) DEFAULT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `tour_amount` int(11) DEFAULT NULL,
  `tour_charges` int(11) DEFAULT NULL,
  `tour_total` int(11) DEFAULT NULL,
  `advance_payment` int(100) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `services_invoices`
--

INSERT INTO `services_invoices` (`id`, `user_id`, `appcode`, `invoice_to`, `invoice_date`, `email`, `email_status`, `phone`, `city`, `state`, `country`, `pin_code`, `no_of_pax`, `arrival`, `departure`, `accomodation_amount`, `accomodation_charges`, `accomodation_total`, `invoice_code`, `transportation_amount`, `transportation_charges`, `transportation_total`, `general_services_total`, `tour_id`, `tour_amount`, `tour_charges`, `tour_total`, `advance_payment`, `total_amount`, `timestamp`, `status`) VALUES
(3, 1, 'OSB-876549', '', '2015-06-27', '', 0, '', '', '', '', 0, 12, '2015-06-29', '2015-06-30', 1000, 12, 1120, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, 1120, '2015-06-27 09:49:47', 'Deleted'),
(4, 1, 'OSB-876549', 'Abdul Azim', '2015-06-27', 'azim@gmail.com', 0, '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', 190012, 12, '2015-06-27', '2015-06-30', 5800, 20, 6960, 2, 6300, 20, 7560, 1100, 9, 2000, 3, 2000, 4000, 17620, '2015-06-27 09:56:23', 'Active'),
(5, 1, 'OSB-876549', 'Eshan Hussain', '2015-06-27', 'eshan@gmail.com', 1, '1231231231', 'Srinagar', 'Jammu and Kashmir', 'India', 190001, 10, '2015-06-27', '2015-06-30', 4500, 0, 4500, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4000, 4500, '2015-06-30 12:22:57', 'Active'),
(6, 1, 'OSB-876549', 'Amir', '2015-07-01', 'mohmad.shoaib@gmail.com', 0, '1231231231', 'Srinagar', 'Delhi', 'India', 190012, 10, '2015-07-10', '2015-07-15', 42000, 15, 48300, 4, 2200, 20, 2640, NULL, NULL, NULL, NULL, NULL, 50000, 50940, '2015-07-01 05:28:37', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `servicetype`
--

CREATE TABLE IF NOT EXISTS `servicetype` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `serviceName` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `cost` int(11) NOT NULL,
  `appcode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `servicetype`
--

INSERT INTO `servicetype` (`id`, `serviceName`, `description`, `cost`, `appcode`) VALUES
(1, 'Transportation', 'This is the Transportation Service.', 1200, 'OSB-876549'),
(2, 'Sonmarg Trip', 'This is Trip to Sonmarg.', 1000, 'OSB-876549'),
(3, 'Accomodation', 'Hotel Green Woods', 500, 'OSB-876549'),
(4, 'Food', 'Just having food...', 700, 'OSB-876549'),
(24, 'Amazying Bath', '', 500, 'OSB-876549'),
(8, '5 Nights 6 Days in Kashmir', 'Entered Description...', 10000, 'OSB-876549'),
(10, 'Test Service for Cost', 'Just Testing...', 1300, 'OSB-876549'),
(14, 'Aw', 'Grest...', 200, 'OSB-876549'),
(15, 'Room: Delux', 'Room of Hotel', 1000, 'OSB-876549'),
(17, 'Room: Super Deluxe', 'Just having gud...', 1500, 'OSB-876549'),
(19, 'Hotel Room', 'Testing..', 2000, 'OSB-876549'),
(22, 'Morning Breakfast', 'Morning Tea...', 200, 'OSB-876549'),
(23, 'Candel Light Dinner', 'Just Testing', 1200, 'OSB-876549');

-- --------------------------------------------------------

--
-- Table structure for table `tos`
--

CREATE TABLE IF NOT EXISTS `tos` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `appcode` varchar(100) NOT NULL,
  `tos_text` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tos`
--

INSERT INTO `tos` (`id`, `user_id`, `appcode`, `tos_text`, `status`) VALUES
(1, 1, 'OSB-876549', 'The meal plan for your selected booking is included.', 1),
(5, 1, 'OSB-876549', 'The primary guest checking in to the hotel must be at least 18 years of age. Children accompanying adults must be between 1-12 years.', 1),
(8, 1, 'OSB-876549', 'The inclusion of ''extra bed'' with a booking is facilitated with a folding cot or a mattress as an extra bed.', 1),
(11, 1, 'OSB-876549', 'Early check-in or late check-out is subject to availability and may be chargeable by the hotel. The standard check-in time is 12PM local time and the standard check-out time is 12PM local time. After booking you will be sent an email confirmation with hotel phone number. You can contact the hotel directly for early check-in or late check-out.', 1),
(12, 1, 'OSB-876549', 'More than 2 days before check-in date: FREE CANCELLATION; 2 days before check-in date: no refund; In case of no show: no refund; An additional TravelSoft service charge of INR 250 will apply', 1),
(13, 1, 'OSB-876549', 'You can cancel your booking by logging on the info@travelsoft section of our website. ', 0),
(17, 2, 'OSB-236549', 'All prices shall be as stated by the Company unless otherwise agreed in writing. Any discounts or''special offers'' displayed will only be applicable at time of display and for in stock items only excluding furniture. Any Quotation supplied by the Company shall be valid for a period of 30 days from date of origin.', 1),
(18, 2, 'OSB-236549', 'Full payment is required on date of purchase.', 1),
(19, 0, 'OSB-236549', 'The buyer shall upon delivery examine the Goods and shall promptly (in any event within 14 days of delivery) notify in writing the Company, where appropriate, of any apparent damage, defect or shortages.', 1),
(20, 0, 'OSB-876549', 'You can cancel your booking by logging on the info@travelsoft section of our website. ', 0),
(21, 0, 'OSB-876549', 'You can cancel your booking by logging on the info@travelsoft section of our website. ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE IF NOT EXISTS `tours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `heading` varchar(128) NOT NULL,
  `details` text NOT NULL,
  `inclusions` text,
  `validity` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `appcode`, `heading`, `details`, `inclusions`, `validity`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 'Zenith of Kashmir Package Tour', ' Day 01\r\nDay 02', ' 01 Night in HouseBoat at Srinagar. ', '2015-06-12', '2015-06-11 10:24:58', 'Deleted'),
(2, 'OSB-876549', '6days/ 7 Nights - Tour Code(RTPOP)', ' Day 01 \r\ndetail here\r\n Day 02\r\nmore details\r\nday 2 detail', ' Hotel at srinagar ', '0000-00-00', '2015-06-11 10:31:52', 'Deleted'),
(3, 'OSB-876549', '8 days/ 7 Nights - Tour Code(RTPOP)', ' Details', '  01 Night at houseboat', '2015-06-11', '2015-06-11 11:37:50', 'Active'),
(4, 'OSB-876549', '5 days/ 4 Nights - Tour Code(RTPOP)', '   Details', '    Details  ', '2015-06-11', '2015-06-11 11:46:13', 'Active'),
(5, 'OSB-876549', 'Zenith of Kashmir Package Tour', ' details', '  details', '2015-06-11', '2015-06-11 12:02:59', 'Active'),
(6, 'OSB-876549', 'Kashmir Package1 Tour', '   Day1\r\nday4', '    1 Night at houseboat   ', '2015-06-12', '2015-06-12 10:20:34', 'Deleted'),
(7, 'OSB-876549', '3 days/ 2 Nights - Tour Code(RTPOP)', '<b><u><span class="wysiwyg-color-blue">Day 1</span><br></u></b><u></u>AM: Arrive Srinagar, meeting upon arrival &amp; transfer with assistance to Dlx.&nbsp;<br><b><u>Day 2:<br></u></b><u></u>AM: Boat ride by (shikara) to Explore the Dallake.<b><u><br><br><br></u></b>', '       01 Night at HouseBoat.\r\n  Boatride By Shikara on Lake.\r\n All Applicable Hotel/Transport Taxes     ', '2015-06-20', '2015-06-13 08:55:07', 'Deleted'),
(8, 'OSB-876549', 'Kashmir Package2 Tour', '<h2>Day 1</h2>test the tour&nbsp;<br><h2>Day 2</h2>here are some more text.<br>', '1 night at houseboat\r</br>2nd night at Hotel\r</br></br>', '2015-06-13', '2015-06-13 09:18:19', 'Deleted'),
(9, 'OSB-876549', '4 days/ 3 Nights - Tour Code(RTPOP)', '<b>Day 01: Srinagar</b><br><i>AM: arrive srinagar, meeting upon arrival &amp; transfer with assistance to Dlx. Free for relax to enjoy the scene view of mountain around Dal Lake. Dinner &amp; Overnight in Dlx. Houseboat/Hotel.</i><br><br><b>Day 02: Srinagar Local Sightseeing (27Kms)</b><br><i>PM: arrive srinagar, meeting upon arrival &amp; transfer with assistance\n to Dlx. Free for relax to enjoy the scene view of mountain around Dal \nLake. Dinner &amp; Overnight in Dlx. Houseboat/Hotel.</i><br><br><b>Day 03: Srinagar-Sonmarg-Srinagar(174)</b><br><i>AM: arrive srinagar, meeting upon arrival &amp; transfer with assistance\n to Dlx. Free for relax to enjoy the scene view of mountain around Dal \nLake. Dinner &amp; Overnight in Dlx. Houseboat/Hotel.</i><br><br><b>Day 04: Srinagar-Gulmarg-Srinagar(112)</b><b><br></b><i>AM: arrive srinagar, meeting upon arrival &amp; transfer with assistance\n to Dlx. Free for relax to enjoy the scene view of mountain around Dal \nLake. Dinner &amp; Overnight in Dlx. Houseboat/Hotel.</i><br><br><b>Day 05: Srinagar-Departure</b><br><i>Leave after breakfast for tour of Srinagar City (Old/New City) &amp; proceed to Srinagar Airport to board the scheduled flight for onward destination.&nbsp;</i><i>AM: arrive srinagar, meeting upon arrival &amp; transfer with assistance to Dlx. Free for relax to enjoy the scene view of mountain around Dal Lake. Dinner &amp; Overnight in Dlx. Houseboat/Hotel.</i><br><br><br><br><br>', '01 Night at Houseboat.  \n03 Nights in Hotel at Srinagar.\nAccommodation on Twin/Double Sharing Basis.\nDaily Breakfast and Dinner. \nTo and Fro Shikara crossings while staying on House Boat.          ', '2015-06-30', '2015-06-20 06:01:10', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tour_tariff`
--

CREATE TABLE IF NOT EXISTS `tour_tariff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `each_paying` varchar(64) NOT NULL,
  `budget` int(11) NOT NULL,
  `economic` int(11) NOT NULL,
  `standard` int(11) NOT NULL,
  `super_deluxe` int(11) NOT NULL,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tour_tariff`
--

INSERT INTO `tour_tariff` (`id`, `tour_id`, `appcode`, `each_paying`, `budget`, `economic`, `standard`, `super_deluxe`, `status`) VALUES
(1, 2, 'OSB-876549', '02 pax, per pax', 1000, 2000, 3000, 4000, 'Active'),
(2, 2, 'OSB-876549', '03 pax, per pax', 1500, 2500, 3500, 4500, 'Active'),
(3, 3, 'OSB-876549', '02 pax, per pax', 2000, 3000, 3500, 4000, 'Active'),
(4, 4, 'OSB-876549', '02 pax, per pax', 1000, 2000, 4000, 4000, 'Active'),
(5, 4, 'OSB-876549', '04 pax, per pax', 2000, 4000, 3455, 6000, 'Deleted'),
(6, 1, 'OSB-876549', '04 pax, per pax', 3000, 4000, 5000, 6000, 'Active'),
(7, 6, 'OSB-876549', '02 pax, per pax', 1000, 2000, 3000, 4000, 'Active'),
(8, 6, 'OSB-876549', '03 pax, per pax', 1500, 2500, 3500, 4500, 'Active'),
(9, 7, 'OSB-876549', '02 pax, per pax', 1000, 2000, 3000, 4000, 'Active'),
(10, 8, 'OSB-876549', '02 pax, per pax', 1000, 2000, 3000, 4000, 'Active'),
(11, 9, 'OSB-876549', '02 Pax, Per Pax', 1000, 2000, 3000, 4000, 'Active'),
(12, 9, 'OSB-876549', '04 Pax, Per Pax', 8700, 10350, 12750, 21400, 'Active'),
(13, 9, 'OSB-876549', '06 Pax, Per Pax', 7800, 9450, 11850, 20500, 'Active'),
(14, 9, 'OSB-876549', 'Extra Bed', 3800, 5000, 6200, 14450, 'Active'),
(15, 9, 'OSB-876549', 'Child WOB', 2300, 3500, 4100, 9050, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transporters`
--

CREATE TABLE IF NOT EXISTS `transporters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `transporter_name` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `website` varchar(32) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `transporters`
--

INSERT INTO `transporters` (`id`, `appcode`, `transporter_name`, `address`, `phone`, `fax`, `email`, `website`, `description`, `created_at`, `status`) VALUES
(3, 'OSB-876549', 'Kashmir Transporters', 'Residency Road, Lal Chowk,\nSrinagar, J&K, 190001', '9622724000', '9622724000', 'kashmirTransporters@gmail.com', 'www.kashmirtransporters.com', 'First Transporter to be added.', '2015-05-27 07:59:38', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transporter_tariff`
--

CREATE TABLE IF NOT EXISTS `transporter_tariff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `transporter_id` bigint(20) NOT NULL,
  `vechile_id` int(11) NOT NULL,
  `trip` varchar(128) NOT NULL,
  `days` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Ddeleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `transporter_tariff`
--

INSERT INTO `transporter_tariff` (`id`, `appcode`, `transporter_id`, `vechile_id`, `trip`, `days`, `rate`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 3, 2, 'Gulmarg Trip', 1, 2200, '2015-05-27 10:48:58', 'Active'),
(2, 'OSB-876549', 3, 2, 'Pahalgam Trip', 1, 3000, '2015-05-27 11:23:00', 'Active'),
(3, 'OSB-876549', 3, 1, 'Sonamarg Trip', 2, 4000, '2015-05-30 09:55:29', 'Active'),
(4, 'OSB-876549', 3, 2, 'Sonamarg Trip', 1, 2500, '2015-05-30 09:57:58', 'Active'),
(5, 'OSB-876549', 3, 1, 'Sonamarg Trip', 1, 2300, '2015-05-30 09:58:18', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transport_vouchers`
--

CREATE TABLE IF NOT EXISTS `transport_vouchers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `voucher_no` int(11) NOT NULL,
  `voucher_date` date NOT NULL,
  `transporter_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `customer_name` varchar(32) NOT NULL,
  `customer_email` varchar(32) NOT NULL,
  `customer_mobile` varchar(32) NOT NULL,
  `customer_city` varchar(32) NOT NULL,
  `customer_state` varchar(32) NOT NULL,
  `customer_country` varchar(32) NOT NULL,
  `customer_pin` varchar(32) NOT NULL,
  `arrival` date NOT NULL,
  `departure` date NOT NULL,
  `pickup` int(11) NOT NULL,
  `remarks` varchar(1024) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `transport_vouchers`
--

INSERT INTO `transport_vouchers` (`id`, `appcode`, `voucher_no`, `voucher_date`, `transporter_id`, `invoice_no`, `customer_name`, `customer_email`, `customer_mobile`, `customer_city`, `customer_state`, `customer_country`, `customer_pin`, `arrival`, `departure`, `pickup`, `remarks`, `timestamp`, `email_status`, `status`) VALUES
(8, 'OSB-876549', 1, '2015-06-04', 3, 7, 'Abdul Azim', 'azim@gmail.com', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', '2015-06-04', '2015-06-30', 1, 'Take extra charges directly from the client.', '2015-06-04 16:27:35', 1, 'Active'),
(9, 'OSB-876549', 2, '2015-06-04', 3, 7, 'Abdul Azim', 'azim@gmail.com', '1231231231', 'Srinagar', 'Ashmore and Cartier Island', 'Ashmore and Cartier Island', '190012', '2015-06-04', '2015-06-30', 1, 'Take extra charges directly from the client.', '2015-06-11 16:46:03', 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transport_voucher_services`
--

CREATE TABLE IF NOT EXISTS `transport_voucher_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `voucher_id` bigint(20) NOT NULL,
  `trip` varchar(128) NOT NULL,
  `vechile` varchar(32) NOT NULL,
  `capacity` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `dot` date NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `transport_voucher_services`
--

INSERT INTO `transport_voucher_services` (`id`, `appcode`, `voucher_id`, `trip`, `vechile`, `capacity`, `days`, `pax`, `dot`, `rate`) VALUES
(11, 'OSB-876549', 8, 'Gulmarg Trip', 'TAVERA', 7, 1, 3, '2015-06-11', 2200),
(12, 'OSB-876549', 9, 'Gulmarg Trip', 'TAVERA', 7, 1, 3, '2015-06-11', 2200);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `appcode`, `email`, `password`, `level`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 'user1@billing.com', '202cb962ac59075b964b07152d234b70', 1, '2014-08-07 06:46:10', 1),
(27, 'OSB-888219', 'mohmad.shoaib@gmail.com', '202cb962ac59075b964b07152d234b70', 1, '2015-05-22 16:10:02', 1),
(30, 'OSB-876549', 'shoaib.mohd@myasa.net', '202cb962ac59075b964b07152d234b70', 2, '2015-06-10 07:27:40', 1),
(31, 'OSB-876549', 'test@gm.co', '4297f44b13955235245b2497399d7a93', 2, '2015-06-15 05:13:24', 1),
(32, 'OSB-876549', 'hotel@hm.co', '4297f44b13955235245b2497399d7a93', 2, '2015-06-15 05:34:31', 1),
(33, 'OSB-12345', 'su@myasa.net', '202cb962ac59075b964b07152d234b70', 7, '2015-06-24 09:14:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_registration`
--

CREATE TABLE IF NOT EXISTS `user_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_of_registration` varchar(50) DEFAULT NULL,
  `end_of_registration` varchar(50) DEFAULT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_registration`
--

INSERT INTO `user_registration` (`id`, `user_id`, `date_of_registration`, `end_of_registration`, `payment_status`, `created_at`) VALUES
(1, 1, '2015-06-23', '2016-06-23', 1, '2014-08-28 06:13:59'),
(3, 27, '2015-06-23', '', 0, '2015-05-22 16:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `appcode` varchar(32) NOT NULL,
  `sales` tinyint(1) NOT NULL DEFAULT '0',
  `expenses` tinyint(1) NOT NULL DEFAULT '0',
  `reporting` tinyint(1) NOT NULL DEFAULT '0',
  `hotels` tinyint(1) NOT NULL DEFAULT '0',
  `transporters` tinyint(1) NOT NULL DEFAULT '0',
  `inventory` tinyint(1) NOT NULL DEFAULT '0',
  `customers` tinyint(1) NOT NULL DEFAULT '0',
  `services` tinyint(1) NOT NULL DEFAULT '0',
  `passes` tinyint(4) NOT NULL DEFAULT '0',
  `tours` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `name`, `appcode`, `sales`, `expenses`, `reporting`, `hotels`, `transporters`, `inventory`, `customers`, `services`, `passes`, `tours`, `created_at`, `status`) VALUES
(1, 30, 'Shoaib Mohmad', 'OSB-876549', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2015-06-10 07:27:40', 'Active'),
(2, 31, 'Test User', 'OSB-876549', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '2015-06-15 05:13:24', 'Active'),
(3, 32, 'Hotel1', 'OSB-876549', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '2015-06-15 05:34:32', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `vechiles`
--

CREATE TABLE IF NOT EXISTS `vechiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appcode` varchar(32) NOT NULL,
  `vechile_name` varchar(64) NOT NULL,
  `capacity` int(11) NOT NULL,
  `description` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vechiles`
--

INSERT INTO `vechiles` (`id`, `appcode`, `vechile_name`, `capacity`, `description`, `created_at`, `status`) VALUES
(1, 'OSB-876549', 'INNOVA', 8, 'AC, 8 Seats Capacity.', '2015-05-27 09:15:53', 'Active'),
(2, 'OSB-876549', 'TAVERA', 7, 'AC, 7 Seats Cab', '2015-05-27 10:12:29', 'Active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
